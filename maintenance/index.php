<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <title>Manutenção Agendada - Alianças em Moedas</title>
  <meta name="description" content="">
  <meta name="author" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,600,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" media="screen" href="css/style.css?2">
  <link href="img/favicon.ico" rel="icon" />
</head>
<body>

  <!-- particles.js container -->
  <div id="particles-js"></div>

  <div id="body">
    <div class="wrap">
      <div class="content">
        <img src="img/logo_2.png" alt="Logo">
        <h1>Manutenção em Andamento!</h1>

        <p>Estamos realizando uma manutenção para melhorias em nossa Loja Virtual.</p>
        <p><b>Prazo estimado de retorno</b></p>
        <p>Das 07:00 às 10:00 horas <small> (Horário de Brasília)</small></p>

        <h2>Nosso atendimento continua através dos contatos abaixo:</h2>
        <ul>
          <li><a href="https://web.whatsapp.com/send?phone=+5541992038552" target="_Blank"><i class="fa fa-whatsapp"></i> (41) 9.9203-8552</a></li>
          <li><a href="https://instagram.com/aliancademoeda" target="_Blank"><i class="fa fa-instagram"></i> @aliancademoeda</a></li>
          <li><a href="mailto:aliancademoedab@gmail.com"><i class="fa fa-envelope"></i> aliancademoedab@gmail.com</a></li>
        </ul>
      </div>
    </div>
  </div>

  <!-- scripts -->
  <script src="js/particles.js"></script>
  <script src="js/app.js?1"></script>

</body>
</html>