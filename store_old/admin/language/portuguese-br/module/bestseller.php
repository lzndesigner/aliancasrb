<?php
// Heading
$_['heading_title']    = 'Produtos mais Vendidos';

// Text
$_['text_module']      = 'Extesnões';
$_['text_success']     = 'Extensão Produtos mais vendidos modificado com sucesso!';
$_['text_edit']        = 'Configurações do Extensão Produtos mais vendidos';

// Entry
$_['entry_name']       = 'Título';
$_['entry_limit']      = 'Exibir';
$_['entry_image']      = 'Dimensão (Largura x Altura)';
$_['entry_width']      = 'Largura';
$_['entry_height']     = 'Altura';
$_['entry_status']     = 'Situação';

// Error
$_['error_permission'] = 'Atenção: Você não tem permissão para modificar o Extensão Produtos mais vendidos!';
$_['error_name']       = 'O Título deve ter entre 3 e 64 caracteres!';
$_['error_width']      = 'A largura é obrigatória!';
$_['error_height']     = 'A altura é obrigatória!';