<?php
// Heading
$_['heading_title']    = 'Menu de filtros';

// Text
$_['text_module']      = 'Extensões';
$_['text_success']     = 'Extensão Menu de filtros modificado com sucesso!';
$_['text_edit']        = 'Configurações do Extensão Menu de filtros';

// Entry
$_['entry_status']     = 'Situação';

// Error
$_['error_permission'] = 'Atenção: Você não tem permissão para modificar o Extensão Menu de filtros!';