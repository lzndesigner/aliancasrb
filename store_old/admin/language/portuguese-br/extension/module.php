<?php
// Heading
$_['heading_title']    = 'Extensões';

// Text
$_['text_success']     = 'Extensões modificado com sucesso!';
$_['text_layout']      = 'Depois de instalar e configurar a Extensões, você deve adicioná-lo a um layout <a href="%s" class="alert-link">clicando aqui</a>, para que ele seja exibido na loja!';
$_['text_add']         = 'Adicionar Extensão';
$_['text_list']        = 'Listando Extensões';

// Column
$_['column_name']      = 'Extensão';
$_['column_action']    = 'Ação';

// Entry
$_['entry_code']       = 'Extensão';
$_['entry_name']       = 'Nome da Extensão';

// Error
$_['error_permission'] = 'Atenção: Você não tem permissão para modificar as Extensões!';
$_['error_name']       = 'O nome da extensão deve ter entre 3 e 64 caracteres!';
$_['error_code']       = 'É necessário selecionar uma extensão!';