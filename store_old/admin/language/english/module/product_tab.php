<?php
// Heading
$_['heading_title']       = 'Produtos em TABs (abas)';

// Text
$_['text_module']      = 'Extensões';
$_['text_success']     = 'Alterado!';
$_['text_edit']        = 'Editado';

// Entry
$_['entry_name']       = 'Título';
$_['entry_product']    = 'Produtos em Destaques';
$_['entry_limit']      = 'Exibir';
$_['entry_width']      = 'Largura';
$_['entry_height']     = 'Altura';
$_['entry_status']     = 'Situação';
$_['entry_features']   = 'Exibir: Destaques';
$_['entry_latest'] 	   = 'Exibir: Últimos';
$_['entry_bestseller'] = 'Exibir: Mais Vendidos';
$_['entry_special']    = 'Exibir: Promoções';

// Error
$_['error_permission'] = 'Atenção: Você não tem permissão para modificar o Extensão Produtos em TABs (abas)!';
$_['error_name']       = 'O Título deve ter entre 3 e 64 caracteres!';
$_['error_width']      = 'A largura é obrigatória!';
$_['error_height']     = 'A altura é obrigatória!';
?>