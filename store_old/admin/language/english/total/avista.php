<?php
// Heading
$_['heading_title']    = 'Cash discount';

// Text
$_['text_total']       = 'Order Total';
$_['text_success']     = 'Success: You are modified the discount percentage!';
$_['text_edit']        = 'Cash discount Settings';

// Entry
$_['entry_total']	   = 'How many percent discount?';
$_['entry_pedido_total']	   = 'The order value should reach?';
$_['entry_methods']    = 'Give discount to (ID):';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

// Help
$_['help_methods'] = '(Ex: "bank_transfer" = Bank Transfer - no spaces, no aspas!)';

// Error
$_['error_permission'] = 'Warning: You are not allowed to modify this module!';
?>