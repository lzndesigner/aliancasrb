<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-line-chart"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">

          <link rel="stylesheet" href="<?php echo $base; ?>/view/javascript/lightbox/css/lightbox.css">
          <script src="<?php echo $base; ?>/view/javascript/lightbox/js/lightbox.js"></script>
          <style>
          .row p{font-size: 14px;}
          </style>
          <div class="row">
            <div class="col-md-12">
                <p>Para você visualizar as estatísticas de acesso de seu serviço, acompanhe o tutorial a seguir em apenas 6 passos. </p>
                    <p><b>É simples e fácil.</b> Qualquer dúvida entre em contato com <a href="https://www.innsystem.com.br/contato" target="_Blank">nosso suporte</a>.</p>
                </div>
          </div>

          <hr>
          <hr class="invisible">

          <h2>Vamos lá!</h2>

        <div class="row">          
          <div class="col-md-12">
            <h3><b>1) Acesse sua conta</b></h3>
            <p>Primeiro você precisa acessar sua conta em <a href="https://www.innsystem.com.br" target="_Blank">nosso site</a>.</p>
              <a href="<?php echo $base; ?>../_galerias/layout/awstats/1.png" data-lightbox="example-1"><img src="<?php echo $base; ?>../_galerias/layout/awstats/1.png" alt="Tutorial de Visualizar as Estatísticas de Acesso" class="img-responsive img-rounded"></a>
          </div><!-- col-md-5 -->

        </div><!-- row -->

        <hr>
        <hr class="invisible">

        <div class="row">

          <div class="col-md-12">
            <h3><b>2) Selecione o Serviço</b></h3>
            <p>Encontre o serviço que deseja visualizar as estatísticas de acessos e clique sobre o mesmo.</p>
              <a href="<?php echo $base; ?>../_galerias/layout/awstats/2.png" data-lightbox="example-1"><img src="<?php echo $base; ?>../_galerias/layout/awstats/2.png" alt="Tutorial de Visualizar as Estatísticas de Acesso" class="img-responsive img-rounded"></a>
          </div><!-- col-md-7 -->
        </div><!-- row -->

        <hr>
        <hr class="invisible">

        <div class="row">
          <div class="col-md-12">
            <h3><b>3) Sessão de Atalhos Rápidos </b></h3>
            <p>Após escolher o serviço, desça a página até os Atalhos Rápidos e encontre a ferramentas <b>Awstats</b>.</p>
            <p><small><b class="text-danger">* Atenção</b>: cuidado ao utilizar as demais ferramentas.</small></p>
              <a href="<?php echo $base; ?>../_galerias/layout/awstats/3.png" data-lightbox="example-1"><img src="<?php echo $base; ?>../_galerias/layout/awstats/3.png" alt="Tutorial de Visualizar as Estatísticas de Acesso" class="img-responsive img-rounded"></a>
          </div><!-- col-md-6 -->

        </div><!-- row -->

        <hr>
        <hr class="invisible">

        <div class="row">

          <div class="col-md-12">
            <h3><b>4) Exibir as Estatísticas de Acesso</b></h3>
            <p>Caso você possuir o Certificado de Segurança Digital em seu serviço, procure a linha com <b>Seu Domínio (SSL)</b> e clique no botão para exibir.</p>
              <a href="<?php echo $base; ?>../_galerias/layout/awstats/4.png" data-lightbox="example-1"><img src="<?php echo $base; ?>../_galerias/layout/awstats/4.png" alt="Tutorial de Visualizar as Estatísticas de Acesso" class="img-responsive img-rounded"></a>
          </div><!-- col-md-6 -->

        </div><!-- row -->

        <hr>
        <hr class="invisible">

        <div class="row">
          <div class="col-md-12">
            <h3><b>5) Resumo e Histórico Mensal das Estatísticas</b></h3>
            <p>Após abrir a página, note o último horário que foi atualizado os dados das estatísticas, a quantidade de visitantes únicos e o número total de visitas do mês atual.</p>
            <p>Note também o Histórico Mensal da <b>quantidade de visitas únicas</b> e o <b>total de visitas</b> de cada mês.</p>
              <a href="<?php echo $base; ?>../_galerias/layout/awstats/5.png" data-lightbox="example-1"><img src="<?php echo $base; ?>../_galerias/layout/awstats/5.png" alt="Tutorial de Visualizar as Estatísticas de Acesso" class="img-responsive img-rounded"></a>
          </div> 
        </div><!-- row -->

        <hr>
        <hr class="invisible">

        <div class="row">
        <div class="col-md-12">
            <h3><b>6) Histórico Diário das Estatísticas</b></h3>
            <p>Note o Histórico diário da <b>quantidade de visitas únicas</b> e o <b>total de visitas</b> de cada dia do mês atual.</p>
              <a href="<?php echo $base; ?>../_galerias/layout/awstats/6.png" data-lightbox="example-1"><img src="<?php echo $base; ?>../_galerias/layout/awstats/6.png" alt="Tutorial de Visualizar as Estatísticas de Acesso" class="img-responsive img-rounded"></a>
          </div>
        </div>

        <hr>
        <hr class="invisible">

        <div class="row">
          <div class="col-md-12">
            <a href="https://www.innsystem.com.br/contato" target="_Blank" class="btn btn-lg btn-primary"><i class="fa fa-question-circle"></i> Dúvidas? Entre em Contato</a>
          </div>
        </div>

      </div><!-- panel-body -->
    </div><!-- panel -->
  </div><!-- container -->
</div><!-- content -->
<?php echo $footer; ?>