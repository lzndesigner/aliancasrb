<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_install) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_install; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

    <script type="text/javascript">
      $(document).ready(function() { 
        setTimeout("$('#FazBackup').hide('slow')", 20000);        
      });
      var tempo = new Number();
        // Tempo em segundos
        tempo = 20;

        function startCountdown(){

          // Se o tempo não for zerado
          if((tempo - 1) >= 0){

            // Pega a parte inteira dos minutos
            var min = parseInt(tempo/60);
            // Calcula os segundos restantes
            var seg = tempo%60;

            // Formata o número menor que dez, ex: 08, 07, ...
            if(min < 10){
              min = "0"+min;
              min = min.substr(0, 2);
            }
            if(seg <=9){
              seg = "0"+seg;
            }

            // Cria a variável para formatar no estilo hora/cronômetro
            horaImprimivel = min + ':' + seg + ' segundos';
            //JQuery pra setar o valor
            $(".cronometroRegressivo").html(horaImprimivel);

            // Define que a função será executada novamente em 1000ms = 1 segundo
            setTimeout('startCountdown()',1000);
            // diminui o tempo
            tempo--;

          // Quando o contador chegar a zero faz esta ação
          } 
        }

        // Chama a função ao carregar a tela
        startCountdown();
    </script>

    <div id="FazBackup" class="alert alert-info"><h4><i class="fa fa-exclamation-circle"></i> <b>Matenha seu Backup de Segurança atualizado!</b></h4> <button type="button" class="close" data-dismiss="alert">&times;</button>
          <div style="font-size:13px; line-height:23px;">Recomendamos realizar o backup da Loja pelo menos uma vez por dia. <br/>
           Para realizar o <b>backup</b> é simples, acesse a página <a href="<?php echo $backup; ?>" class="btn btn-sm btn-primary">clicando aqui</a>.<br/> <br>
           1) Após carregar a página clique sobre o botão em Azul <b>"Backup"</b>.<br/>
           2) Irá gerar um arquivo para <b><i>download</i></b> ex: <i>2016-05-17_13-08-22_backup.sql</i><br/>
           3) Altere o nome e salve-o em segurança no seu computador.</div>
           <br>
          <p>Essa mensagem irá desaparecer em: <span class="cronometroRegressivo"></span></p>
    </div><!-- #facbackup -->

    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-6"><?php echo $order; ?></div>
      <div class="col-lg-3 col-md-3 col-sm-6"><?php echo $sale; ?></div>
      <div class="col-lg-3 col-md-3 col-sm-6"><?php echo $customer; ?></div>
      <div class="col-lg-3 col-md-3 col-sm-6"><?php echo $online; ?></div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-12 col-sm-12 col-sx-12"><?php echo $activity; ?></div>
      <div class="col-lg-8 col-md-12 col-sm-12 col-sx-12"> <?php echo $recent; ?> </div>
    </div>
    <div class="row">
      <div class="col-lg-5 col-md-12 col-sx-12 col-sm-12"><?php echo $map; ?></div>
      <div class="col-lg-7 col-md-12 col-sx-12 col-sm-12"><?php echo $chart; ?></div>
    </div>    
  </div>
</div>
<?php echo $footer; ?>