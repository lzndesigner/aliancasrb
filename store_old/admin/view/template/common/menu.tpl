<ul id="menu">
  <li id="store"><a href="../index.php" target="_blank" data-toggle="tooltip" title="<?php echo $text_store; ?>"><i class="fa fa-desktop fa-fw"></i> <span><?php echo $text_store; ?></span></a></li>
  <li id="dashboard"><a href="<?php echo $home; ?>" data-toggle="tooltip" title="<?php echo $text_dashboard; ?>"><i class="fa fa-dashboard fa-fw"></i> <span><?php echo $text_dashboard; ?></span></a></li>
  <li id="catalog"><a class="parent" data-toggle="tooltip" title="<?php echo $text_catalog; ?>"><i class="fa fa-tags fa-fw"></i> <span><?php echo $text_catalog; ?></span></a>
    <ul>
      <li><a href="<?php echo $category; ?>"><?php echo $text_category; ?></a></li>
      <li><a href="<?php echo $product; ?>"><?php echo $text_product; ?></a></li>
      <li><a href="<?php echo $option; ?>"><?php echo $text_option; ?></a></li>
      <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
      <li><a href="<?php echo $review; ?>"><?php echo $text_review; ?></a></li>
      <li><a href="<?php echo $filter; ?>"><?php echo $text_filter; ?></a></li>
      <li><a class="parent"><?php echo $text_attribute; ?></a>
        <ul>
          <li><a href="<?php echo $attribute; ?>"><?php echo $text_attribute; ?></a></li>
          <li><a href="<?php echo $attribute_group; ?>"><?php echo $text_attribute_group; ?></a></li>
        </ul>
      </li>
      <li class="hide"><a href="<?php //echo $download; ?>"><?php //echo $text_download; ?></a></li>      
      <li class="hide"><a href="<?php //echo $recurring; ?>"><?php //echo $text_recurring; ?></a></li>
      <li><a href="<?php echo $information; ?>"><?php echo $text_information; ?></a></li>
    </ul>
  </li>
  <li id="sale"><a class="parent" data-toggle="tooltip" title="<?php echo $text_sale; ?>"><i class="fa fa-shopping-cart fa-fw"></i> <span><?php echo $text_sale; ?></span></a>
    <ul>
      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
      <li class="hide"><a href="<?php //echo $order_recurring; ?>"><?php //echo $text_order_recurring; ?></a></li>
      <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
      <li><a class="parent"><?php echo $text_customer; ?></a>
        <ul>
          <li><a href="<?php echo $customer; ?>"><?php echo $text_customer; ?></a></li>
          <li><a href="<?php echo $customer_group; ?>"><?php echo $text_customer_group; ?></a></li>
          <li><a href="<?php echo $custom_field; ?>"><?php echo $text_custom_field; ?></a></li>
          <li><a href="<?php echo $customer_ban_ip; ?>"><?php echo $text_customer_ban_ip; ?></a></li>
        </ul>
      </li>
    </ul>
  </li>
  <li><a class="parent" data-toggle="tooltip" title="<?php echo $text_marketing; ?>"><i class="fa fa-share-alt fa-fw"></i> <span><?php echo $text_marketing; ?></span></a>
    <ul>
      <li class="hide"><a href="<?php //echo $marketing; ?>"><?php //echo $text_marketing; ?></a></li>
      <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
      <li><a href="<?php echo $coupon; ?>"><?php echo $text_coupon; ?></a></li>
      <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
      <li><a class="parent"><?php echo $text_voucher; ?></a>
        <ul>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $voucher_theme; ?>"><?php echo $text_voucher_theme; ?></a></li>
        </ul>
      </li>
    </ul>
  </li>
  <li id="reports" data-toggle="tooltip" title="<?php echo $text_reports; ?>"><a class="parent"><i class="fa fa-bar-chart-o fa-fw"></i> <span><?php echo $text_reports; ?></span></a>
    <ul>
      <li><a class="parent"><?php echo $text_sale; ?></a>
        <ul>
          <li><a href="<?php echo $report_sale_order; ?>"><?php echo $text_report_sale_order; ?></a></li>
          <li><a href="<?php echo $report_sale_tax; ?>"><?php echo $text_report_sale_tax; ?></a></li>
          <li><a href="<?php echo $report_sale_shipping; ?>"><?php echo $text_report_sale_shipping; ?></a></li>
          <li><a href="<?php echo $report_sale_return; ?>"><?php echo $text_report_sale_return; ?></a></li>
          <li><a href="<?php echo $report_sale_coupon; ?>"><?php echo $text_report_sale_coupon; ?></a></li>
        </ul>
      </li>
      <li><a class="parent"><?php echo $text_product; ?></a>
        <ul>
          <li><a href="<?php echo $report_product_viewed; ?>"><?php echo $text_report_product_viewed; ?></a></li>
          <li><a href="<?php echo $report_product_purchased; ?>"><?php echo $text_report_product_purchased; ?></a></li>
        </ul>
      </li>
      <li><a class="parent"><?php echo $text_customer; ?></a>
        <ul>
          <li><a href="<?php echo $report_customer_online; ?>"><?php echo $text_report_customer_online; ?></a></li>
          <li><a href="<?php echo $report_customer_activity; ?>"><?php echo $text_report_customer_activity; ?></a></li>
          <li><a href="<?php echo $report_customer_order; ?>"><?php echo $text_report_customer_order; ?></a></li>
          <li><a href="<?php echo $report_customer_reward; ?>"><?php echo $text_report_customer_reward; ?></a></li>
          <li><a href="<?php echo $report_customer_credit; ?>"><?php echo $text_report_customer_credit; ?></a></li>
        </ul>
      </li>
      <li><a class="parent"><?php echo $text_marketing; ?></a>
        <ul>
          <li><a href="<?php echo $report_marketing; ?>"><?php echo $text_marketing; ?></a></li>
          <li><a href="<?php echo $report_affiliate; ?>"><?php echo $text_report_affiliate; ?></a></li>
          <li><a href="<?php echo $report_affiliate_activity; ?>"><?php echo $text_report_affiliate_activity; ?></a></li>
        </ul>
      </li>
    </ul>
  </li>
  <li id=""><a class="parent" data-toggle="tooltip" title="Atendimento/Suporte"><i class="fa fa-question-circle fa-fw"></i> <span>Link Rápidos</span> <span class="badge badge-success">NEW!</span></a>
    <ul>
      <li><a class="parent"> <span>Central de Ajuda</span></a>
        <ul>
          <li><a href="https://innsystem.com.br/dicas-fotos-profissionais" target="_blank">Fotos Profissionais</a></li>    
          <li><a href="https://innsystem.com.br/dicas-loja-virtual" target="_blank">Dicas Úteis</a></li>    
          <li><a href="https://innsystem.com.br/treinamento-loja-virtual" target="_blank">Treinamento</a></li>    
          <li><a href="https://innsystem.com.br/treinamento-divulgacao" target="_blank">Divulgação</a></li> 
          <li><a href="https://innsystem.com.br/central-ajuda" target="_Blank">Conheça mais</a></li>
        </ul>
      </li>
      <li><a href="https://www.innsystem.com.br/grupos" target="_blank"><span>Grupos do Facebook</span></a></li>
      <li><a href="<?php echo $statistics; ?>"> <span>Estatísticas de Acessos</span></a></li>
      <li><a href="http://www.innsystem.com.br/submitticket.php?step=2&deptid=1" target="_blank"><span>Ticket de Suporte</span></a></li>
    </ul>
  </li>
  <li><a href="https://market.innsystem.com.br" target="_Blank"><i class="fa fa-magic fa-fw"></i> <span>Mercado de Artes</span> <span class="badge badge-success">NEW!</span></a></li>
  <?php if($nivelpermission == "Administrador (Desenvolvedores)"){ ?>
  <li id="extension" data-toggle="tooltip" title="<?php echo $text_extension; ?>"><a class="parent"><i class="fa fa-puzzle-piece fa-fw"></i> <span><?php echo $text_extension; ?></span></a>
    <ul>
      <li><a href="<?php echo $installer; ?>"><?php echo $text_installer; ?></a></li>
      <li><a href="<?php echo $modification; ?>"><?php echo $text_modification; ?></a></li>
      <li><a href="<?php echo $feed; ?>"><?php echo $text_feed; ?></a></li>      
    </ul>
  </li>
  <?php } ?>
</ul>