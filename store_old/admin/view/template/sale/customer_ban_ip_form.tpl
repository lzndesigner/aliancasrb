<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-customer-ban-ip" class="form-horizontal">
      <div class="pull-left" style="margin:.5em .5em 1em;">
        <button type="submit" form="form-customer-ban-ip" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-sm btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a></div>
        <div class="clearfix"></div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-ip"><?php echo $entry_ip; ?></label>
            <div class="col-sm-10">
              <input type="text" name="ip" value="<?php echo $ip; ?>" id="input-ip" class="form-control" />
              <?php if ($error_ip) { ?>
              <div class="text-danger"><?php echo $error_ip; ?></div>
              <?php } ?>
            </div>
          </div>
      <div class="pull-left" style="margin:.5em .5em 1em;">
        <button type="submit" form="form-customer-ban-ip" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-sm btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a></div>
        <div class="clearfix"></div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>