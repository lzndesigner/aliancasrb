<?php

// Text

$_['text_information']  = 'Informações';

$_['text_service']      = 'Serviços ao cliente';

$_['text_extra']        = 'Outros serviços';

$_['text_contact']      = 'Entre em contato';

$_['text_return']       = 'Solicitar devolução';

$_['text_sitemap']      = 'Mapa do site';

$_['text_manufacturer'] = 'Produtos por marca';

$_['text_voucher']      = 'Comprar vale presentes';

$_['text_affiliate']    = 'Programa de afiliados';

$_['text_special']      = 'Produtos em promoção';

$_['text_account']      = 'Minha conta';

$_['text_order']        = 'Histórico de pedidos';

$_['text_wishlist']     = 'Lista de desejos';

$_['text_newsletter']   = 'Informativo';

$_['text_powered']      = '%s &copy; %s | Todos os direitos reservados';

$_['text_confirm_payment']      = 'Confirmar Pagamento';

$_['text_tecnologia']      = 'Tecnologia';
$_['title_helpdesk']      = 'Compras e Ajuda';
$_['title_payment']      = 'Formas de Pagamentos';
$_['title_shipping']      = 'Formas de Envio';