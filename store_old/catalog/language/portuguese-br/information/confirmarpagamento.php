<?php

// Heading

$_['heading_title']  = 'Confirmar Pagamento';

$_['text_name']  = 'Seu Nome';
$_['text_email']  = 'Seu E-mail';
$_['text_pedido']  = 'Pedido N° ';
$_['text_anexo']  = 'Anexo do Comprovante';
$_['text_anexo_acc']  = '* Tipos de arquivos aceitos: .JPEG .PNG .GIF .PDF';
$_['text_notas']  = 'Notas Adicionais';

