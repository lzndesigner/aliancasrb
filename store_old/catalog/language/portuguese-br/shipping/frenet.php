<?php
$_['text_title']  = 'Correios';
$_['text_sucesso']= 'Reconexão realizada com sucesso!';
$_['text_free']   = '<span style="color:green;font-weight:bold">Grátis</span>';

$_['error_conexao']  	= 'Não foi possível estabelecer conexão com os Correios. Tentando reconectar...';
$_['error_reconexao']  	= 'Falha na tentativa de reconectar com os Correios. O WebService dos Correios apresenta instabilidade ou está fora do ar.';
