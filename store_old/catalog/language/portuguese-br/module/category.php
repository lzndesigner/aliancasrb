<?php
// Heading
$_['heading_title'] = 'Departamentos';
$_['heading_information'] = 'Informações';

$_['text_account']   = 'Minha Conta';
$_['text_contact']   = 'Contato';
$_['text_confirm']   = 'Confirmar Pagamento';
$_['text_return']   = 'Solicitar Devolução';
$_['text_map']   = 'Mapa do Site';