<?php

// Text

$_['text_search']              = 'Pesquisar';

$_['text_brand']               = 'Marca';

$_['text_manufacturer']        = 'Marca:';

$_['text_model']               = 'Modelo:';

$_['text_reward']              = 'Pontos:';

$_['text_points']              = 'Pontos necessários:';

$_['text_stock']               = 'Disponibilidade:';

$_['text_instock']             = 'Em estoque';

$_['text_instock_esgotado']    = 'ESGOTADO';
$_['button_visit']        = 'Avisar-me';

$_['text_tax']                 = 'Sem impostos:';

$_['text_discount']            = ' ou mais ';

$_['text_option']              = 'Opções disponíveis';

$_['text_minimum']             = 'Quantidade mínima para compra: %s';

$_['text_reviews']             = '%s comentários';

$_['text_write']               = 'Escreva um comentário';

$_['text_login']               = 'Você deve <a href="%s">acessar</a> ou <a href="%s">cadastrar-se</a> para comentar.';

$_['text_no_reviews']          = 'Não há comentários para este produto.';

$_['text_note']                = '<span class="text-danger">Nota:</span> O HTML não é traduzido!';

$_['text_success']             = 'Obrigado por seu comentário. Ele foi enviado para aprovação.';

$_['text_related']             = 'Produtos relacionados';

$_['text_tags']                = 'Etiquetas:';

$_['text_error']               = 'Produto não encontrado!';

$_['text_payment_recurring']   = 'Assinatura';

$_['text_trial_description']   = '%s a cada %d %s(s) repetido por %d vez(es)';

$_['text_payment_description'] = '%s a cada %d %s(s) repetido por %d vez(es)';

$_['text_payment_cancel']      = '%s a cada %d %s(s) até ser cancelado';

$_['text_day']                 = 'Diária';

$_['text_week']                = 'Semanal';

$_['text_semi_month']          = 'Quinzenal';

$_['text_month']               = 'Mensal';

$_['text_year']                = 'Anual';



// Entry

$_['entry_qty']                = 'Qtd';

$_['entry_name']               = 'Seu nome';

$_['entry_review']             = 'Seu comentário';

$_['entry_rating']             = 'Avaliação';

$_['entry_good']               = 'Bom';

$_['entry_bad']                = 'Ruim';

$_['entry_captcha']            = 'Coloque o código da imagem no campo abaixo';



// Tabs

$_['tab_description']          = 'Descrição';

$_['tab_attribute']            = 'Especificação';

$_['tab_review']               = 'Comentários (%s)';



// Error

$_['error_name']               = 'Atenção: Seu nome deve ter entre 3 e 25 caracteres!';

$_['error_text']               = 'Atenção: Seu comentário deve ter entre 25 e 1000 caracteres!';

$_['error_rating']             = 'Atenção: Faça a avaliação!';

$_['error_captcha']            = 'Atenção: O código no campo não é o mesmo da imagem!';



$_['saving_text']            = 'Economize';
$_['text_whistlist']            = 'Adicionar à <b>Lista de Desejos</b>';
$_['text_product_esgotado']            = 'Produto Esgotado';

$_['text_product_esgotado_desc']            = 'O produto não está disponível para compra, você pode preencher seus dados e ser notificado quando estivermos em estoque.';
$_['text_product_esgotado_button']            = 'Me notifique';
$_['text_title_product_esgotado']            = 'Notificação de Produto';


$_['text_your_name']            = 'Seu nome';
$_['text_your_email']            = 'Seu E-mail';
$_['text_your_city']            = 'Cidade/Estado';
$_['text_your_phone']            = 'Telefone/Celular';
$_['text_your_product']            = 'Produto';
$_['text_your_message']            = 'Mensagem';
$_['text_button_notification']            = 'Cadastrar Notificação';

$_['text_sim_frete']            = 'Simulador de Frete';
$_['text_my_cep']            = 'Não sei meu CEP';
$_['text_sim_parcelas']            = 'Simulador de Parcelas';
$_['text_sim_parcelas_parcelas']            = 'Parcelas';
$_['text_sim_parcelas_valor']            = 'Valor';
$_['text_sim_parcelas_limite']            = '* Parcelas no mínimo de R$ 5,00';



$_['text_raing_star_1']            = 'Justo';
$_['text_raing_star_2']            = 'Normal';
$_['text_raing_star_3']            = 'Bom';
$_['text_raing_star_4']            = 'Muito Bom';
$_['text_raing_star_5']            = 'Perfeito';

$_['text_instock']        = 'ESGOTADO';
$_['button_details']        = 'Detalhes';
