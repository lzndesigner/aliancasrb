<?php
// Heading
$_['heading_title'] = 'Categories';
$_['heading_information'] = 'Information';

$_['text_account']   = 'My Account';
$_['text_contact']   = 'Contact';
$_['text_confirm']   = 'Confirm Payment';
$_['text_return']   = 'Request Return';
$_['text_map']   = 'Site map';