<?php
// Heading
$_['heading_title']      = 'My Account';

// Text
$_['text_account']       = 'Account';
$_['text_my_account']    = 'My Account';
$_['text_my_orders']     = 'My Orders';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Edit your account information';
$_['text_password']      = 'Change your password';
$_['text_address']       = 'Modify your address book entries';
$_['text_wishlist']      = 'Modify your wish list';
$_['text_order']         = 'View your order history';
$_['text_download']      = 'Downloads';
$_['text_reward']        = 'Your Reward Points';
$_['text_return']        = 'View your return requests';
$_['text_transaction']   = 'Your Transactions';
$_['text_newsletter']    = 'Subscribe / unsubscribe to newsletter';
$_['text_recurring']     = 'Recurring payments';
$_['text_transactions']  = 'Transactions';


$_['box_1_title']   = 'Order History';
$_['box_1_desc']    = 'See all your orders made in our store. You can print invoices and view this page.';

$_['box_2_title']   = 'Your address book';
$_['box_2_desc']    = 'Add, edit and remove shipping addresses and invoices from your address book.';

$_['box_3_title']   = 'Details of your account';
$_['box_3_desc']    = 'Update your contact information, shipping address, and mail and / or your password.';

$_['box_4_title']   = 'Products Favorites';
$_['box_4_desc']    = 'See a complete list of items that you saved as favorite recently in our store.';