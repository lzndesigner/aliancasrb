<!DOCTYPE html>

<!--[if IE]><![endif]-->

<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->

<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->

<!--[if (gt IE 9)|!(IE)]><!-->

<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">

<!--<![endif]-->

<head>

  <meta charset="UTF-8" />

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title><?php echo $title; ?></title>

  <base href="<?php echo $base; ?>" />
  <meta name="robots" content="follow" />
  <meta name="googlebot" content="index, follow, all" />
  <meta name="language" content="pt-br" />
  <meta name="revisit-after" content="3 days">
  <meta name="rating" content="general" />
  <meta property="og:locale" content="pt_BR"/>
  <meta property="og:type" content="website"/>
  <?php if($fbMetas){ ?>
  <?php foreach ($fbMetas as $fbMeta) { ?>
  <meta property="og:image:url" content="<?php echo $base; ?>image/<?php echo $fbMeta['content']; ?>" />
  <meta property="og:image:type" content="image/jpeg" />
  <?php } ?>
  <?php }else{ ?>
  <meta property="og:image:url" content="<?php echo $base; ?>_galerias/facebook.jpg" />
  <meta property="og:image:type" content="image/jpeg" />
  <?php } ?>

  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <meta property="og:description" content="<?php echo $description; ?>"/>
  <?php } ?>

  <?php if ($keywords) { ?>

  <meta name="keywords" content= "<?php echo $keywords; ?>" />

  <?php } ?>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <?php if ($icon) { ?>

  <link href="<?php echo $base; ?>image/catalog/favicon.ico?1" rel="icon" />

  <?php } ?>

  <?php foreach ($links as $link) { ?>

  <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />

  <?php } ?>



  <!-- Requerido Template Novo --> 

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,600,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/font-awesome.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/slick.css?22">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/prettyPhoto.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/animate.css">
  <link rel="stylesheet" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/main.css?1<?php echo rand(); ?>">
  <link rel="stylesheet" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/header.css?1<?php echo rand(); ?>">

  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

  <!--- jQuery -->
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery-1.11.3.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/packery.pkgd.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/masonry.pkgd.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/isotope.pkgd.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.stellar.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/slick.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.inview.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/odometer.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/tweetie.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.timeago.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.knob.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/css3-animate-it.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/imagesloaded.pkgd.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.prettyPhoto.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/main.js?1"></script>
  <!-- Requerido Template Novo -->
  <script src='catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.elevatezoom.js'></script>


  <link href="catalog/view/theme/<?php echo $usedTpl; ?>/stylesheet/stylesheet.css?22" rel="stylesheet" type="text/css" />

  <?php foreach ($styles as $style) { ?>

  <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />

  <?php } ?>

  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/common.js" type="text/javascript"></script>
  <?php foreach ($scripts as $script) { ?>

  <script src="<?php echo $script; ?>" type="text/javascript"></script>

  <?php } ?>

  <?php echo $google_analytics; ?>

  <script src="catalog/view/javascript/jquery.maskedinput.js"></script>
  <script>
  $(document).ready(function(){
    $('#input-telephone').mask('(00) 0000-00009');
    $('#input-telephone').blur(function(event) {
          if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
            $('#input-telephone').mask('(00) 0000-0000');
          } else {
            $('#input-telephone').mask('(00) 90000-0000');
          }
        });
    $('#input-fax').mask('(00) 0000-00009');
    $('#input-fax').blur(function(event) {
          if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
            $('#input-fax').mask('(00) 0000-0000');
          } else {
            $('#input-fax').mask('(00) 90000-0000');
          }
        });
    $('#input-payment-telephone').mask('(00) 0000-00009');
    $('#input-payment-telephone').blur(function(event) {
          if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
            $('#input-payment-telephone').mask('(00) 0000-0000');
          } else {
            $('#input-payment-telephone').mask('(00) 90000-0000');
          }
        });
    $('#input-payment-fax').mask('(00) 0000-00009');
    $('#input-payment-fax').blur(function(event) {
          if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
            $('#input-payment-fax').mask('(00) 0000-0000');
          } else {
            $('#input-payment-fax').mask('(00) 90000-0000');
          }
        });
    $('#telefone').mask('(00) 0000-00009');
    $('#telefone').blur(function(event) {
          if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
            $('#telefone').mask('(00) 0000-0000');
          } else {
            $('#telefone').mask('(00) 90000-0000');
          }
        });

    $("#input-tax").mask("999.999.999-99");

    $("#input-custom-field3").mask("000.000.000-00");
    $('#input-custom-field3').blur(function(event) {
      if($(this).val().length == 14){
        $('#input-custom-field3').mask('000.000.000-00');
      } else {
        $('#input-custom-field3').mask('00.000.000/0000-00');
      }
    });

    $("#input-payment-custom-field3").mask("000.000.000-00");
    $('#input-payment-custom-field3').blur(function(event) {
      if($(this).val().length == 14){
        $('#input-payment-custom-field3').mask('000.000.000-00');
      } else {
        $('#input-payment-custom-field3').mask('00.000.000/0000-00');
      }
    });
  });
</script>

</head>

<body class="<?php echo $class; ?>">
  <div class="bodyWrap">
    <header class="doc-header style-pink bgPurple">
      <!-- BEGIN SUBTOP -->
      <div class="subTop">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-md-6">
              <div class="infosAccounts pull-left hidden-xs">

                <?php if ($logged) { ?>
                <ul>
                  <li><i class="fa fa-user"></i> Bem vindo, <?php echo $name_client; ?> <i class="fa fa-angle-down"></i>
                    <ul>
                      <li><a href="<?php echo $account; ?>"><i class="fa fa-user"></i> <?php echo $text_my_account; ?></a></li>
                      <li><a href="<?php echo $order; ?>"><i class="fa fa-book"></i> <?php echo $text_order; ?></a></li>
                      <li><a href="<?php echo $transaction; ?>"><i class="fa fa-credit-card"></i> <?php echo $text_transaction; ?></a></li>
                      <li><a href="<?php echo $logout; ?>"><i class="fa fa-sign-out"></i> <?php echo $text_logout; ?></a></li>
                    </ul>
                  </li>
                </ul>
                <?php } else { ?>
                <span><i class="fa fa-user"></i> Seja bem vindo! Faça seu <a href="<?php echo $login; ?>">Login</a> ou <a href="<?php echo $register; ?>">Cadastre-se</a></span>
                <?php } ?>
              </div>
            </div><!-- col-md-6 -->
            <div class="col-xs-12 col-md-6">
              <div class="redeSocialHeader">
                <ul>
                  <?php if (isset($fbfanpage)):?><li><a href="<?php echo $fbfanpage; ?>" data-toggle="tooltip" data-placement="bottom" title="Facebook" target="_Blank"><i class="fa fa-facebook"></i></a></li><?php endif ?>
                  <?php if (isset($userInstagram)):?><li><a href="https://www.instagram.com/<?php echo $userInstagram; ?>/" data-toggle="tooltip" data-placement="bottom" title="Instagram" target="_Blank"><i class="fa fa-instagram"></i></a></li><?php endif ?>
                </ul>
              </div><!-- redeSocialHeader -->

              <div class="infosAccounts">
                <ul>                      
                  <li><a href="<?php echo $order; ?>" data-toggle="tooltip" data-placement="bottom" title="Meus Pedidos">Meus Pedidos</a></li>
                  <li><a href="<?php echo $login; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $text_my_account; ?>"><?php echo $text_my_account; ?></a></li>
                  <li><a href="index.php?route=information/contact">Fale Conosco</a></li> 
                </ul>
              </div><!-- infoAccounts -->

            </div>
          </div><!-- row -->
        </div><!-- container -->
      </div><!-- subTop -->
      <!-- END SUBTOP -->


      <!-- BEGIN TOPHEADER -->
      <div class="topHeader">
        <div class="container">  
          <div class="row">

            <div class="col-xs-12 col-md-3">
              <ul class="logo">
                <li>
                  <a href="<?php echo $home; ?>">
                    <?php if ($logo) { ?>
                    <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="logo-dark" />
                    <?php } else { ?>
                    <h1><?php echo $name; ?></h1>
                    <?php } ?>
                  </a>                
                </li>
              </ul>
            </div><!-- col-md-3 -->

            <div class="col-xs-12 col-md-3 secondMenuTop hidden-xs">
              <div class="boxWhatsApp">
                <div>
                  <h5>Atendimento pelo WhatsApp</h5>
                  <p><a href="https://api.whatsapp.com/send?phone=5541992574787" target="_Blank"><?php echo $telephone; ?></a><br><a href="https://api.whatsapp.com/send?phone=5541992574787" target="_Blank"><?php echo $celular; ?></a></p>
                </div>
                <span>
                  <i class="fa fa-whatsapp"></i>
                </span>
              </div><!-- boxWhatsApp -->
            </div><!-- col-md-3 -->

            <div class="col-xs-12 col-md-6 secondMenuTop ">
              <div class="row">

                <div class="col-xs-12 col-md-10">
                  <div id="quick-access">
                    <?php echo $search; ?>
                  </div><!-- End #quick-access --> 
                </div><!-- col-md- 10-->

                <div class="col-xs-12 col-md-2">
                  <div class="topLinks pull-right">
                    <?php echo $cart; ?>
                  </div><!-- toplinks -->
                  <ul class="topLeftLinks">
                    <li><a class="navTriger" href="#" title="Abrir Menu de Departamentos" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-bars"></i></a></li>
                    <li><a class="cartBtn" href="index.php?route=checkout/cart" title="<?php echo $text_shopping_cart; ?>" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-shopping-cart"></i></a></li>
                  </ul> 
                </div><!-- col-md-2 -->

              </div><!-- row -->              
            </div><!-- col-md-6 -->

          </div> <!-- row -->    
        </div><!-- container -->
        <!-- END TOPHEADER -->

        <div class="container">
          <nav class="navMain">
            <ul class="mainNav">
              <li><a href="index.php"><i class="fa fa-home"></i> <span class="hidden-lg hidden-sm hidden-md">Início</span></a></li>
              <?php if ($categories) { ?>

              <?php foreach ($categories as $category) { ?>

              <?php if ($category['children']) { ?>
              <li class="<?php if($category['column'] > 1){ echo "megaparent";}?>"><a href="<?php echo $category['href']; ?>" ><?php echo $category['name']; ?> <i class="fa fa-angle-down"></i></a> 
                <?php if($category['column'] > 1){ ?>
                <div class="mega-xv">
                  <div class="container">
                    <div class="row">
                      <?php }else{ ?>
                      <ul>
                        <?php } ?>
                        <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                        <?php if($category['column'] > 1){ ?>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                          <ul>
                            <?php } ?>

                            <?php foreach ($children as $child) { ?>
                            <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?>  <?php if($child['children_lv3']){?><i class="fa fa-angle-right hidden-xs" style="float: right; margin: 2px;"></i><?php } ?></a></li>
                            <?php } ?>
                            <li class="hidden-md hidden-sm hidden-lg"><a href="<?php echo $category['href']; ?>" >- <b>Todos <?php echo $category['name']; ?></b></a></li>
                            <?php if($category['column'] > 1){ ?>
                          </ul>
                        </div>
                        <?php } ?>

                        <?php } ?>    
                        <?php if($category['column'] > 1){ ?>
                      </div>
                    </div>
                  </div>
                  <?php }else{ ?>
                </ul>
                <?php } ?>


              </li>
              <?php } else { ?>
              <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
              <?php } ?>
              <?php } ?>
              <?php } ?>
              <li><a href="index.php?route=information/contact"><?php echo $text_menu_contact; ?></a></li>
            </ul>
          </nav>  
        </div><!-- container -->
      </div><!-- top header -->
    </header>
    <div class="clearfix"></div>