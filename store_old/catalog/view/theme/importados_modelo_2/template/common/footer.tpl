<div class="container">
    <div class="row">
        <div class="col-md-12">
            <header class="head familyOs">
                <h2><i class="fa fa-instagram"></i> Instagram </h2>
            </header>
            <!-- SnapWidget -->
            <script src="https://snapwidget.com/js/snapwidget.js"></script>
            <iframe src="https://snapwidget.com/embed/387514" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; height:370px"></iframe>
        </div>
    </div>


    <footer class="doc-footer">
        <div class="bgFooter bgWhite">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h6><?php echo $text_account; ?></h6>
                        <ul class="links">
                            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                            <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                            <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>

                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h6><?php echo $text_information; ?></h6>
                        <ul class="links">
                            <?php if ($informations) { ?>
                            <?php foreach ($informations as $information) { ?>
                            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                            <?php } ?>
                            <?php } ?>            


                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <h6><?php echo $title_helpdesk; ?></h6>
                        <ul class="links">
                            <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
                            <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
                            <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                            <li><a href="index.php?route=information/confirmarpagamento"><?php echo $text_confirm_payment; ?></a></li>
                            <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
                            <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h6>Facebook</h6>
                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                          var js, fjs = d.getElementsByTagName(s)[0];
                          if (d.getElementById(id)) return;
                          js = d.createElement(s); js.id = id;
                          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8&appId=1094654680570525";
                          fjs.parentNode.insertBefore(js, fjs);
                      }(document, 'script', 'facebook-jssdk'));</script>
                      <div class="fb-page" data-href="https://www.facebook.com/aliancademoedab/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/aliancademoedab/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/aliancademoedab/">Aliança de moeda sem solda</a></blockquote></div>
                  </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-sm-12 text-center">
                    <span class="copyrights">
                        <?php echo $enderecoLoja; ?>
                        <br/>
                        <?php echo $powered; ?><br/>
                    </span>
                    <hr>
                    <div class="col-sm-8">
                        <div class="boxPayments">
                            <ul>
                                <li><img src="catalog/view/images/cartoes/deposito.png" alt="Depósito Bancário"></li>
                                <li><img src="catalog/view/images/pagseguronew.png" style="width:400px;" alt="PagSeguro"></li>
                            </ul>
                        </div>    
                    </div>
                    <div class="col-sm-4">
                        <b><?php echo $text_tecnologia; ?></b><br/>
                        <a href="http://www.innsystem.com.br" title="InnSystem - Inovação em Sistemas" target="_Blank">
                            <img src="https://www.innsystem.com.br/_galerias/logo.png" alt="InnSystem - Inovação em Sistemas" style="width:110px; margin:-18px 0 0 -2.8em;" />
                        </a>
                    </div>
                </div><!-- col-sm-12 -->        
            </div>
        </div>
    </div>

</footer>


<script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/bootstrap.min.js"></script>
</div><!-- End .bodyWrap -->
</body></html>