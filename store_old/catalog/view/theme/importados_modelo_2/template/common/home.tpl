<?php echo $header; ?>

<div class="container">
    <div class="row">
 <?php echo $column_left; ?>

    <?php if ($column_left && $column_right) { ?>

    <?php $class = 'col-sm-6'; ?>

    <?php } elseif ($column_left || $column_right) { ?>

    <?php $class = 'col-sm-9'; ?>

    <?php } else { ?>

    <?php $class = 'col-sm-12'; ?>

    <?php } ?>

    <section id="content" class="products style2 style-demo1 text-center <?php echo $class; ?>">
        <?php echo $content_top; ?>
        <?php echo $content_bottom; ?>
    </section>

    <?php echo $column_right; ?>

  </div>
</div>


<?php echo $footer; ?>