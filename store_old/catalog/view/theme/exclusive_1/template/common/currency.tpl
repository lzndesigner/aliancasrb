<?php if (count($currencies) > 1) { ?>

<div class="btn-group dropdown-money">

<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="currency">

    <button type="button" class="btn btn-purple dropdown-toggle" data-toggle="dropdown">

        <?php foreach ($currencies as $currency) { ?>

        <?php if ($currency['symbol_left'] && $currency['code'] == $code) { ?>

        <span><?php echo $currency['symbol_left']; ?></span>        

        <?php } elseif ($currency['symbol_right'] && $currency['code'] == $code) { ?>

        <span><?php echo $currency['symbol_right']; ?></span>        

        <?php } ?>

        <?php } ?>        

    </button>

    <ul class="dropdown-menu pull-right" role="menu">

      <?php foreach ($currencies as $currency) { ?>

        <?php if ($currency['symbol_left']) { ?>

        <li><button class="currency-select <?php if ($currency['code'] == $code) { ?>selected<?php } ?>" type="button" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_left']; ?> <?php echo $currency['title']; ?></button></li>

        <?php } else { ?>

        <li><button class="currency-select <?php if ($currency['code'] == $code) { ?>selected<?php } ?>" type="button" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_right']; ?> <?php echo $currency['title']; ?></button></li>

        <?php } ?>

      <?php } ?>

        

    </ul>

      <input type="hidden" name="code" value="" />

  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />

</form>

</div><!-- End .btn-group --> 

<?php } ?>

