<!DOCTYPE html>

<!--[if IE]><![endif]-->

<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->

<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->

<!--[if (gt IE 9)|!(IE)]><!-->

<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">

<!--<![endif]-->

<head>

  <meta charset="UTF-8" />

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title><?php echo $title; ?></title>

  <base href="<?php echo $base; ?>" />
  <meta name="robots" content="follow" />
  <meta name="googlebot" content="index, follow, all" />
  <meta name="language" content="pt-br" />
  <meta name="revisit-after" content="3 days">
  <meta name="rating" content="general" />
  <meta property="og:locale" content="pt_BR"/>
  <meta property="og:type" content="website"/>
  <?php if($fbMetas){ ?>
  <?php foreach ($fbMetas as $fbMeta) { ?>
  <meta property="og:image:url" content="<?php echo $base; ?>image/<?php echo $fbMeta['content']; ?>" />
  <meta property="og:image:type" content="image/jpeg" />
  <?php } ?>
  <?php }else{ ?>
  <meta property="og:image:url" content="<?php echo $base; ?>facebook.jpg" />
  <meta property="og:image:type" content="image/jpeg" />
  <?php } ?>

  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <meta property="og:description" content="<?php echo $description; ?>"/>
  <?php } ?>

  <?php if ($keywords) { ?>

  <meta name="keywords" content= "<?php echo $keywords; ?>" />

  <?php } ?>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <?php if ($icon) { ?>

  <link href="<?php echo $base; ?>image/catalog/favicon.ico?1" rel="icon" />

  <?php } ?>

  <?php foreach ($links as $link) { ?>

  <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />

  <?php } ?>



  <!-- Requerido Template Novo --> 

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/custom-icons.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/odometer-theme-default.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/slick.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/prettyPhoto.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/animate.css">
  <link rel="stylesheet" href="catalog/view/theme/<?php echo $usedTpl; ?>/_template/css/main.css?7">

  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

  <!--- jQuery -->
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery-1.11.3.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/packery.pkgd.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/masonry.pkgd.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/isotope.pkgd.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.stellar.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/my-instagram-gallery.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/slick.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.inview.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/odometer.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/tweetie.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.timeago.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.knob.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/css3-animate-it.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/imagesloaded.pkgd.min.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/jquery.prettyPhoto.js"></script>
  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/main.js"></script>
  <!-- Requerido Template Novo -->


  <link href="catalog/view/theme/<?php echo $usedTpl; ?>/stylesheet/stylesheet.css?4" rel="stylesheet" type="text/css" />

  <?php foreach ($styles as $style) { ?>

  <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />

  <?php } ?>

  <script src="catalog/view/theme/<?php echo $usedTpl; ?>/common.js?1" type="text/javascript"></script>
  <?php foreach ($scripts as $script) { ?>

  <script src="<?php echo $script; ?>" type="text/javascript"></script>

  <?php } ?>

  <?php echo $google_analytics; ?>


  <script src="catalog/view/javascript/jquery.maskedinput.js"></script>
  <script>
  jQuery(function($){
    $("#input-postcode").mask("99999-999");
    $('#input-telephone').mask('(00) 0000-00009');
    $('#input-telephone').blur(function(event) {
         if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#input-telephone').mask('(00) 0000-0000');
        } else {
          $('#input-telephone').mask('(00) 90000-0000');
        }
      });
    $('#input-fax').mask('(00) 0000-00009');
    $('#input-fax').blur(function(event) {
         if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#input-fax').mask('(00) 0000-0000');
        } else {
          $('#input-fax').mask('(00) 90000-0000');
        }
      });
    $('#input-payment-telephone').mask('(00) 0000-00009');
    $('#input-payment-telephone').blur(function(event) {
         if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#input-payment-telephone').mask('(00) 0000-0000');
        } else {
          $('#input-payment-telephone').mask('(00) 90000-0000');
        }
      });
    $('#input-payment-fax').mask('(00) 0000-00009');
    $('#input-payment-fax').blur(function(event) {
         if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#input-payment-fax').mask('(00) 0000-0000');
        } else {
          $('#input-payment-fax').mask('(00) 90000-0000');
        }
      });

    $("#input-tax").mask("999.999.999-99");
  });
</script>

</head>

<body class="<?php echo $class; ?>">
  <div class="bodyWrap">

    <header class="doc-header style-pink bgPurple">
      <div class="doc-header-top">
        <div class="container">
         <div class="banner-top-header">
         <h5>parcele em até 12 vezes  <!-- |  5% de desconto no depósito  | -->  | enviamos para todo brasil
         <br><br>
         Prazo de confecção das alianças de 5 à 8 dias
       </h5>
          </div>
          <ul class="social-icons" style="top:4px;">
            <li class="social-icons-facebook"><a href="https://www.facebook.com/aliancademoedab/" target="_Blank"><i class="fa fa-facebook"></i></a></li>
            <li class="social-icons-instagram"><a href="https://www.instagram.com/aliancademoeda/" target="_Blank"><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
      </div><!-- header-top -->

      <div class="topHeader">
        <div class="container">  
          <div class="row">

            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              <ul class="logo">
                <li>
                  <a href="<?php echo $home; ?>">
                   <?php if ($logo) { ?>
                   <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="logo-dark" />
                   <?php } else { ?>
                   <h1><?php echo $name; ?></h1>
                   <?php } ?>
                 </a>                
               </li>
             </ul>
           </div><!-- cols -->
           <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <div id="quick-access">
              <?php echo $search; ?>
            </div><!-- End #quick-access -->   
          </div>
          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 pull-right">
            <div class="topRight">
              <ul class="">

                <?php if ($logged) { ?>
                <li><a href="#" data-toggle="tooltip" title="<?php echo $text_my_account; ?>"><i class="fa fa-user"></i> <?php echo $text_my_account; ?></a>
                  <ul>
                    <li><a href="<?php echo $account; ?>">
                     <?php echo $text_my_account; ?></a>
                   </li>
                   <li><a href="<?php echo $order; ?>">
                     <?php echo $text_order; ?></a>
                   </li>
                   <li><a href="<?php echo $transaction; ?>">
                     <?php echo $text_transaction; ?></a>
                   </li>
                   <li><a href="<?php echo $logout; ?>">
                    <?php echo $text_logout; ?></a>
                  </li>
                </ul>
              </li>
              <?php } else { ?>
              <li class=""><a href="<?php echo $login; ?>" data-toggle="tooltip" title="<?php echo $text_my_account; ?>"><i class="fa fa-user"></i> <?php echo $text_my_account; ?></a></li>
              <?php } ?>

              <li>
                <a href="javascript:;" data-toggle="tooltip" title="Entre em Contato"><i class="fa fa-phone-square"></i> Contato</a>
                <ul class="dropContact">
                  <li>
                    <p><b>Horário de Atendimento</b></p>
                    <p>De Segunda à Sexta das 09h às 18h</p>
                  </li>
                  <li>
                    <p><b>Atendimento, dúvidas e compras</b></p>
                    <p><b>Telefone:</b> <?php echo $telephone; ?></p>
                    <p><b>E-mail:</b> <?php echo $emailHeader; ?></p>
                  </li>
                  <li><a href="https://api.whatsapp.com/send?phone=5541992038552" title=""><img src="/_galerias/whatsapp.png" class="img-responsive" alt="Atendimento WhatsApp"></a></li>
                </ul>
              </li>

              <div class="topLinks">
                <?php echo $cart; ?>
              </div>
            </ul>  
          </div>
          <ul class="topLeftLinks">
            <li><a class="navTriger" href="#" title="Abrir Menu de Departamentos" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-bars"></i></a></li>
            <li><a class="cartBtn" href="index.php?route=checkout/cart" title="<?php echo $text_shopping_cart; ?>" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-shopping-cart"></i></a></li>
          </ul> 
          <div class="clearfix"></div>
          <ul class="lngcur">
            <?php echo $currency; ?>
            <?php echo $language; ?>
          </ul>
        </div>

        <div class="col-md-12 hide">
         <nav class="bgBlack">
          <div class="">
            <ul class="mainNav">
              <li><a href="index.php"><i class="fa fa-home"></i> <span class="hidden-lg hidden-sm hidden-md">Início</span></a></li>
              <?php if ($categories) { ?>

              <?php foreach ($categories as $category) { ?>

              <?php if ($category['children']) { ?>

              <li><a href="<?php echo $category['href']; ?>" ><?php echo $category['name']; ?> <i class="fa fa-angle-down"></i></a> 

                <ul>

                  <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                  <?php foreach ($children as $child) { ?>
                  <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?>  <?php if($child['children_lv3']){?><i class="fa fa-angle-right hidden-xs" style="float: right; margin: 2px;"></i><?php } ?></a>
                    <?php if($child['children_lv3']){?>
                    <ul>                
                      <?php foreach ($child['children_lv3'] as $child_lv3) { ?>
                      <li><a href="<?php echo $child_lv3['href']; ?>"><?php echo $child_lv3['name']; ?></a></li>
                      <?php } ?>
                    </ul>
                    <?php } ?>
                  </li>
                  <?php } ?>
                  <?php } ?>    

                </ul>                        

              </li>

              <?php } else { ?>

              <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>

              <?php } ?>

              <?php } ?>
              <?php } ?>
              <li><a href="index.php?route=information/contact"><?php echo $text_menu_contact; ?></a></li>

              <!-- <li class="promotion"><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li> -->
            </ul>
          </div>
        </nav>  
      </div>

    </div> <!-- row -->      
  </div><!-- container -->
  <div class="clearfix"></div>

</div><!-- top header -->
</header>
<div class="clearfix"></div>
<div class="container hidden-xs">
  <div class="row">
    <div class="col-md-12">
      <div class="banners-selos clearfix">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="icon-selos"><i class="fa fa-clock-o"></i></div>
          <div>
            <h2>Prazo de Confecção</h2>
            <p>Entre 5 à 8 dias úteis</p>
          </div><!-- -->
        </div><!-- col-md-3 -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="icon-selos"><i class="fa fa-truck"></i></div>
          <div>
            <h2>Frete grátis</h2>
            <p>Para todo o Brasil por PAC</p>
          </div><!-- -->
        </div><!-- col-md-3 -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="icon-selos"><i class="fa fa-credit-card"></i></div>
          <div>
            <h2>Pagamento Fácil</h2>
            <p>Suas Compras em Até 12X.</p>
          </div><!-- -->
        </div><!-- col-md-3 -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="icon-selos"><i class="fa fa-shield"></i></div>
          <div>
            <h2>Compra Segura</h2>
            <p>Ambiente seguro e certificado</p>
          </div><!-- -->
        </div><!-- col-md-3 -->
      </div><!-- banners-selos -->
    </div><!-- col-md-12 -->
  </div><!-- row -->
    </div><!-- container -->