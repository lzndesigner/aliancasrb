<div class="container hide">
    <div class="row">
        <div class="col-md-12">
            <header class="head familyOs">
                <h2><i class="fa fa-instagram"></i> Instagram </h2>
            </header>
            <!-- SnapWidget -->
            <script src="https://snapwidget.com/js/snapwidget.js"></script>
            <iframe src="https://snapwidget.com/embed/387514" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; height:370px"></iframe>
        </div>
    </div>
</div>

   <footer class="doc-footer">
    <div class="bgFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h6><?php echo $text_account; ?></h6>
                    <ul class="links">
                        <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                        <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                        <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                        <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>

                    </ul>
                </div>
                <div class="col-sm-3">
                    <h6><?php echo $text_information; ?></h6>                    
                    <?php if ($informations) { ?>
                    <ul class="links">
                        <?php foreach ($informations as $information) { ?>
                        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                        <?php } ?>
                    </ul>
                    <?php } ?>                       
                </div>
                <div class="col-sm-3">
                    <h6><?php echo $title_helpdesk; ?></h6>
                    <ul class="links">
                        <li><a href="<?php echo $marcas; ?>"><?php echo $text_manufacturer; ?></a></li>
                        <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                        <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h6><?php echo $text_contact; ?></h6>
                    <ul class="links">
                        <li><i class="fa fa-whatsapp"></i> WhatsApp: <?php echo $telephone; ?></li>
                        <?php if($fax){ ?><li><i class="fa fa-whatsapp"></i> WhatsApp: <?php echo $fax; ?></li><?php } ?>
                        <li><i class="fa fa-envelope"></i> E-mail: <?php echo $email_admin; ?></li>
                        <?php if($open){?><li><i class="fa fa-calendar"></i> Horário: <?php echo $open; ?></li><?php } ?>
                    </ul>
                </div>
            </div><!-- row -->
        </div><!-- container -->

        <div class="bgsocial-midia">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3"><h5>Encontre-nos aqui</h5></div>
                        <div class="col-md-9">
                            <ul class="social-icons text-left">
                              <li class="social-icons-facebook" data-toggle="tooltip" title="Facebook"><a href="https://www.facebook.com/aliancademoedab" target="_Blank"><i class="fa fa-facebook"></i></a></li>
                              <li class="social-icons-instagram" data-toggle="tooltip" title="Instagram"><a href="https://www.instagram.com/aliancademoeda/" target="_Blank"><i class="fa fa-instagram"></i></a></li>
                          </ul>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h6>Formas de Pagamentos</h6>
                <div class="banner_pagamentos">
                    <ul>
                        <li class="hide"><img src="catalog/view/images/exatos/banco_brasil.png" alt="Banco do Brasil"></li>
                        <li class="hide"><img src="catalog/view/images/exatos/caixa.png" alt="Caixa"></li>
                        <li class=""><img src="catalog/view/images/exatos/deposito.png" alt="Depósito Bancário"></li>
                        <li class="" style="width:90px;"><img src="catalog/view/images/exatos/caixa.png" alt="Caixa"></li>
                        <li class="" style="width:190px;"><img src="catalog/view/images/pagseguronew.png?4" alt="PagSeguro"></li>
                        <li class=""><img src="catalog/view/images/exatos/boleto.png" alt="Boleto Bancário"></li>
                    </ul>
                </div>
            </div><!-- col-md-8 -->
            <div class="col-md-4">
                <h6>Formas de Envio</h6>
                <div class="banner_pagamentos">
                    <ul>
                        <li class=""><img src="catalog/view/images/exatos/correios.png?1" alt="Correios"></li>
                        <li class=""><img src="catalog/view/images/exatos/pac.png?1" alt="PAC"></li>
                        <li class=""><img src="catalog/view/images/exatos/sedex.png?1" alt="SEDEX"></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <h6>Certificados</h6>
                <div class="banner_pagamentos">
                    <a href="https://www.gogetssl.com" rel="nofollow" title="GoGetSSL Site Seal Logo"><div id="gogetssl-animated-seal" style="width:180px; height:58px;"></div></a> <script src="https://gogetssl-cdn.s3.eu-central-1.amazonaws.com/site-seals/gogetssl-seal.js"></script>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-ms-12">
                <div class="col-sm-8 text-center">
                    <span class="copyrights">
                        <?php echo $powered; ?><br/>
                    </span>
                </div>

                <div class="col-sm-2">
                    <b><?php echo $text_tecnologia; ?></b><br/>
                    <a href="https://www.innsystem.com.br" target="_Blank">
                        <img src="https://www.innsystem.com.br/galerias/logo_preto_azul.png" alt="InnSystem - Inovação em Sistemas" style="width:110px; margin:0px 0 0 0;">
                    </a>
                </div>
            </div><!-- col-sm-12 -->        
        </div>
    </div>
</div>

</footer>

<script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/bootstrap.min.js"></script>
</div><!-- End .bodyWrap -->
</body></html>