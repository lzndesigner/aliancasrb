<?php echo $header; ?>



<div class="container">



  <div id="category-breadcrumb">



    <div class="container">



      <ul class="breadcrumb">



        <?php foreach ($breadcrumbs as $breadcrumb) { ?>



        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>



        <?php } ?>



      </ul>



    </div>



  </div><!-- category-breadcrumb -->







  <div class="row"><?php echo $column_left; ?>



    <?php if ($column_left && $column_right) { ?>



    <?php $class = 'col-sm-6'; ?>



    <?php } elseif ($column_left || $column_right) { ?>



    <?php $class = 'col-sm-9'; ?>



    <?php } else { ?>



    <?php $class = 'col-sm-12'; ?>



    <?php } ?>



    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>



      <header class="content-title">



            <h1 class="title"><?php echo $heading_title; ?></h1>  



      </header>



      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">



        <fieldset>



          <h2 class="checkout-title"><?php echo $text_your_payment; ?></h2>



          <div class="input-group">



            <span class="input-group-addon"><span class="input-text"><?php echo $entry_tax; ?></span></span>



            <input type="text" name="tax" value="<?php echo $tax; ?>" placeholder="<?php echo $entry_tax; ?>" id="input-tax" class="form-control input-lg" />



          </div><!-- End .input-group -->











          <div class="input-group">



            <span class="input-text"><?php echo $entry_payment; ?></span>



            



              <div class="radio">



                <label>



                  <?php if ($payment == 'cheque') { ?>



                  <input type="radio" name="payment" value="cheque" checked="checked" />



                  <?php } else { ?>



                  <input type="radio" name="payment" value="cheque" />



                  <?php } ?>



                  <?php echo $text_cheque; ?></label>



              </div>



              <div class="radio">



                <label>



                  <?php if ($payment == 'paypal') { ?>



                  <input type="radio" name="payment" value="paypal" checked="checked" />



                  <?php } else { ?>



                  <input type="radio" name="payment" value="paypal" />



                  <?php } ?>



                  <?php echo $text_paypal; ?></label>



              </div>



              <div class="radio">



                <label>



                  <?php if ($payment == 'bank') { ?>



                  <input type="radio" name="payment" value="bank" checked="checked" />



                  <?php } else { ?>



                  <input type="radio" name="payment" value="bank" />



                  <?php } ?>



                  <?php echo $text_bank; ?></label>



              </div>



            



          </div>







          <div class="input-group payment" id="payment-cheque">



            <span class="input-group-addon"><span class="input-text"><?php echo $entry_cheque; ?></span></span>



            <input type="text" name="cheque" value="<?php echo $cheque; ?>" placeholder="<?php echo $entry_cheque; ?>" id="input-cheque" class="form-control input-lg" />



          </div><!-- End .input-group -->











          <div class="input-group payment" id="payment-paypal">



            <span class="input-group-addon"><span class="input-text"><?php echo $entry_paypal; ?></span></span>



            <input type="text" name="paypal" value="<?php echo $paypal; ?>" placeholder="<?php echo $entry_paypal; ?>" id="input-paypal" class="form-control input-lg" />



          </div><!-- End .input-group -->















          <div class="payment" id="payment-bank">



            <div class="input-group">



              <span class="input-group-addon"><span class="input-text"><?php echo $entry_bank_name; ?></span></span>



              <input type="text" name="bank_name" value="<?php echo $bank_name; ?>" placeholder="<?php echo $entry_bank_name; ?>" id="input-bank-name" class="form-control input-lg" />



            </div><!-- End .input-group -->







            <div class="input-group">



              <span class="input-group-addon"><span class="input-text"><?php echo $entry_bank_branch_number; ?></span></span>



              <input type="text" name="bank_branch_number" value="<?php echo $bank_branch_number; ?>" placeholder="<?php echo $entry_bank_branch_number; ?>" id="input-bank-branch-number" class="form-control input-lg" />



            </div><!-- End .input-group -->







            <div class="input-group">



              <span class="input-group-addon"><span class="input-text"><?php echo $entry_bank_swift_code; ?></span></span>



              <input type="text" name="bank_swift_code" value="<?php echo $bank_swift_code; ?>" placeholder="<?php echo $entry_bank_swift_code; ?>" id="input-bank-swift-code" class="form-control input-lg" />



            </div><!-- End .input-group -->







            <div class="input-group">



              <span class="input-group-addon"><span class="input-text"><?php echo $entry_bank_swift_code; ?></span></span>



              <input type="text" name="bank_swift_code" value="<?php echo $bank_swift_code; ?>" placeholder="<?php echo $entry_bank_swift_code; ?>" id="input-bank-swift-code" class="form-control input-lg" />



            </div><!-- End .input-group -->







            <div class="input-group">



              <span class="input-group-addon"><span class="input-text"><?php echo $entry_bank_account_name; ?></span></span>



              <input type="text" name="bank_account_name" value="<?php echo $bank_account_name; ?>" placeholder="<?php echo $entry_bank_account_name; ?>" id="input-bank-account-name" class="form-control input-lg" />



            </div><!-- End .input-group -->







            <div class="input-group">



              <span class="input-group-addon"><span class="input-text"><?php echo $entry_bank_account_number; ?></span></span>



              <input type="text" name="bank_account_number" value="<?php echo $bank_account_number; ?>" placeholder="<?php echo $entry_bank_account_number; ?>" id="input-bank-account-number" class="form-control input-lg" />



            </div><!-- End .input-group -->



          </div>



        </fieldset>



        <div class="buttons clearfix">



          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>



          <div class="pull-right">



            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-purple" />



          </div>



        </div>



      </form>



      <?php echo $content_bottom; ?></div>



    <?php echo $column_right; ?></div>



</div>



<script type="text/javascript"><!--



$('input[name=\'payment\']').on('change', function() {



    $('.payment').hide();



    



    $('#payment-' + this.value).show();



});







$('input[name=\'payment\']:checked').trigger('change');



//--></script> 



<?php echo $footer; ?> 