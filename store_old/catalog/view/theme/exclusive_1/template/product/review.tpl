<?php if ($reviews) { ?>
<h6>Comentários de Clientes</h6>
<?php foreach ($reviews as $review) { ?>
<div class="review">
    <div class="info">
        <ul class="head clearfix">
            <li><span class="name"><?php echo $review['author']; ?></span></li>
            <li>
              
                <?php if ($review['rating']) { ?>
                  <div class="star-rating star-<?php echo $review['rating'];?>"></div>
                <?php } else { ?>
                  <div class="star-rating star-0"></div>
                <?php } ?>
              

            </li>
        </ul>
        <span class="date"><?php echo $review['date_added']; ?></span>
        <p><?php echo $review['text']; ?></p>
    </div>
</div><!-- review -->
<?php } ?>
<div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>