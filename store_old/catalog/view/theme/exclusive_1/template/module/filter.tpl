 <aside>

  <div class="boxLeft">
    <h2><?php echo $heading_title; ?></h2>
    <div class="sideNav style1">

      <ul class="ulRadiosCssFilter">
        <?php foreach ($filter_groups as $filter_group) { ?>

        <b class="title"><?php echo $filter_group['name']; ?></b>
        <div id="filter-group<?php echo $filter_group['filter_group_id']; ?>">
          <?php foreach ($filter_group['filter'] as $filter) { ?>
          <?php if ($filter['quantidade'] > '0') {?>     
          
          <?php if (in_array($filter['filter_id'], $filter_category)) { ?>
          <li>
            <div>
              <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" id="filter_<?php echo $filter['filter_id']; ?>" checked="checked">
              <label for="filter_<?php echo $filter['filter_id']; ?>">
                <?php echo $filter['name']; ?>                                         
              </label>
            </div>
          </li>
          <?php } else { ?>
          <li>
            <div>
              <input name="filter[]" type="checkbox" value="<?php echo $filter['filter_id']; ?>" id="filter_<?php echo $filter['filter_id']; ?>" >
              <label for="filter_<?php echo $filter['filter_id']; ?>">
                <?php echo $filter['name']; ?>                                       
              </label>
            </div>
          </li>
          <?php } ?>

          <?php }else{?>


          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
      </ul>

      <button type="button" id="button-filter" class="btn btn-purple"><?php echo $button_filter; ?></button>
    </div>

    


  </div>

</aside>
<script type="text/javascript"><!--

$('#button-filter').on('click', function() {

 filter = [];



 $('input[name^=\'filter\']:checked').each(function(element) {

  filter.push(this.value);

});



 location = '<?php echo $action; ?>&filter=' + filter.join(',');

});

//--></script> 

