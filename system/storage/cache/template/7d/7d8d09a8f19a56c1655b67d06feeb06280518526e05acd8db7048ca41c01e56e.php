<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base/template/extension/module/html.twig */
class __TwigTemplate_90566dd70fcdbc6b6965526a65ab5b482b7f56e538495dd98d6aabbc8bace2f1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div>";
        if (($context["heading_title"] ?? null)) {
            // line 2
            echo "\t<div class=\"heading-page\">
\t\t<h1 class=\"title\">";
            // line 3
            echo ($context["heading_title"] ?? null);
            echo "</h1>
\t</div>
\t";
        }
        // line 6
        echo "\t";
        echo ($context["html"] ?? null);
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "base/template/extension/module/html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  43 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base/template/extension/module/html.twig", "");
    }
}
