<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* setting/setting.twig */
class __TwigTemplate_d00abae43ddc83c7e22e5b20549ca4aff5128a0be0678599681bb0b6290c966c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" id=\"button-save\" form=\"form-setting\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" disabled=\"disabled\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\"> ";
        // line 16
        if (($context["error_warning"] ?? null)) {
            // line 17
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 21
        echo "    ";
        if (($context["success"] ?? null)) {
            // line 22
            echo "    <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 26
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 28
        echo ($context["text_edit"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 31
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-setting\" class=\"form-horizontal\">
          <ul class=\"nav nav-tabs\">
            <li class=\"active\"><a href=\"#tab-general\" data-toggle=\"tab\">";
        // line 33
        echo ($context["tab_general"] ?? null);
        echo "</a></li>
            <li><a href=\"#tab-store\" data-toggle=\"tab\">";
        // line 34
        echo ($context["tab_store"] ?? null);
        echo "</a></li>
            <li><a href=\"#tab-local\" data-toggle=\"tab\">";
        // line 35
        echo ($context["tab_local"] ?? null);
        echo "</a></li>
            <li><a href=\"#tab-image\" data-toggle=\"tab\">";
        // line 36
        echo ($context["tab_image"] ?? null);
        echo "</a></li>
            <li><a href=\"#tab-redesocial\" data-toggle=\"tab\">Rede Social</a></li>
            <li><a href=\"#tab-option\" data-toggle=\"tab\">";
        // line 38
        echo ($context["tab_option"] ?? null);
        echo "</a></li>
            <li><a href=\"#tab-mail\" data-toggle=\"tab\">";
        // line 39
        echo ($context["tab_mail"] ?? null);
        echo "</a></li>
            <li><a href=\"#tab-server\" data-toggle=\"tab\">";
        // line 40
        echo ($context["tab_server"] ?? null);
        echo "</a></li>
          </ul>
          <div class=\"tab-content\">
            <div class=\"tab-pane active\" id=\"tab-general\">
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-meta-title\">";
        // line 45
        echo ($context["entry_meta_title"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"config_meta_title\" value=\"";
        // line 47
        echo ($context["config_meta_title"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_meta_title"] ?? null);
        echo "\" id=\"input-meta-title\" class=\"form-control\" />
                  ";
        // line 48
        if (($context["error_meta_title"] ?? null)) {
            // line 49
            echo "                  <div class=\"text-danger\">";
            echo ($context["error_meta_title"] ?? null);
            echo "</div>
                  ";
        }
        // line 50
        echo " </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-meta-description\">";
        // line 53
        echo ($context["entry_meta_description"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <textarea name=\"config_meta_description\" rows=\"5\" placeholder=\"";
        // line 55
        echo ($context["entry_meta_description"] ?? null);
        echo "\" id=\"input-meta-description\" class=\"form-control\">";
        echo ($context["config_meta_description"] ?? null);
        echo "</textarea>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-meta-keyword\">";
        // line 59
        echo ($context["entry_meta_keyword"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <textarea name=\"config_meta_keyword\" rows=\"5\" placeholder=\"";
        // line 61
        echo ($context["entry_meta_keyword"] ?? null);
        echo "\" id=\"input-meta-keyword\" class=\"form-control\">";
        echo ($context["config_meta_keyword"] ?? null);
        echo "</textarea>
                </div>
              </div>
              <div class=\"form-group hide d-none\">
                <label class=\"col-sm-2 control-label\" for=\"input-theme\">";
        // line 65
        echo ($context["entry_theme"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_theme\" id=\"input-theme\" class=\"form-control\">
                    
                    ";
        // line 69
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["themes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["theme"]) {
            // line 70
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["theme"], "value", [], "any", false, false, false, 70) == ($context["config_theme"] ?? null))) {
                // line 71
                echo "                    
                    <option value=\"";
                // line 72
                echo twig_get_attribute($this->env, $this->source, $context["theme"], "value", [], "any", false, false, false, 72);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["theme"], "text", [], "any", false, false, false, 72);
                echo "</option>
                    
                    ";
            } else {
                // line 75
                echo "                    
                    <option value=\"";
                // line 76
                echo twig_get_attribute($this->env, $this->source, $context["theme"], "value", [], "any", false, false, false, 76);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["theme"], "text", [], "any", false, false, false, 76);
                echo "</option>
                    
                    ";
            }
            // line 79
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['theme'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "                  
                  </select>
                  <br />
                  <img src=\"\" alt=\"\" id=\"theme\" class=\"img-thumbnail\" /></div>
              </div>
              <div class=\"form-group hide d-none\">
                <label class=\"col-sm-2 control-label\" for=\"input-layout\">";
        // line 86
        echo ($context["entry_layout"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_layout_id\" id=\"input-layout\" class=\"form-control\">
                    
                    ";
        // line 90
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["layouts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["layout"]) {
            // line 91
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["layout"], "layout_id", [], "any", false, false, false, 91) == ($context["config_layout_id"] ?? null))) {
                // line 92
                echo "                    
                    <option value=\"";
                // line 93
                echo twig_get_attribute($this->env, $this->source, $context["layout"], "layout_id", [], "any", false, false, false, 93);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["layout"], "name", [], "any", false, false, false, 93);
                echo "</option>
                    
                    ";
            } else {
                // line 96
                echo "                    
                    <option value=\"";
                // line 97
                echo twig_get_attribute($this->env, $this->source, $context["layout"], "layout_id", [], "any", false, false, false, 97);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["layout"], "name", [], "any", false, false, false, 97);
                echo "</option>
                    
                    ";
            }
            // line 100
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['layout'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 101
        echo "                  
                  </select>
                </div>
              </div>
            </div>
            <div class=\"tab-pane\" id=\"tab-store\">
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-name\">";
        // line 108
        echo ($context["entry_name"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"config_name\" value=\"";
        // line 110
        echo ($context["config_name"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_name"] ?? null);
        echo "\" id=\"input-name\" class=\"form-control\" />
                  ";
        // line 111
        if (($context["error_name"] ?? null)) {
            // line 112
            echo "                  <div class=\"text-danger\">";
            echo ($context["error_name"] ?? null);
            echo "</div>
                  ";
        }
        // line 113
        echo " </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-owner\">";
        // line 116
        echo ($context["entry_owner"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"config_owner\" value=\"";
        // line 118
        echo ($context["config_owner"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_owner"] ?? null);
        echo "\" id=\"input-owner\" class=\"form-control\" />
                  ";
        // line 119
        if (($context["error_owner"] ?? null)) {
            // line 120
            echo "                  <div class=\"text-danger\">";
            echo ($context["error_owner"] ?? null);
            echo "</div>
                  ";
        }
        // line 121
        echo " </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-address\">";
        // line 124
        echo ($context["entry_address"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <textarea name=\"config_address\" placeholder=\"";
        // line 126
        echo ($context["entry_address"] ?? null);
        echo "\" rows=\"5\" id=\"input-address\" class=\"form-control\">";
        echo ($context["config_address"] ?? null);
        echo "</textarea>
                  ";
        // line 127
        if (($context["error_address"] ?? null)) {
            // line 128
            echo "                  <div class=\"text-danger\">";
            echo ($context["error_address"] ?? null);
            echo "</div>
                  ";
        }
        // line 129
        echo " </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-geocode\"><span data-toggle=\"tooltip\" data-container=\"#tab-general\" title=\"";
        // line 132
        echo ($context["help_geocode"] ?? null);
        echo "\">";
        echo ($context["entry_geocode"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"config_geocode\" value=\"";
        // line 134
        echo ($context["config_geocode"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_geocode"] ?? null);
        echo "\" id=\"input-geocode\" class=\"form-control\" />
                </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-email\">";
        // line 138
        echo ($context["entry_email"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"config_email\" value=\"";
        // line 140
        echo ($context["config_email"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_email"] ?? null);
        echo "\" id=\"input-email\" class=\"form-control\" />
                  ";
        // line 141
        if (($context["error_email"] ?? null)) {
            // line 142
            echo "                  <div class=\"text-danger\">";
            echo ($context["error_email"] ?? null);
            echo "</div>
                  ";
        }
        // line 143
        echo " </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-telephone\">";
        // line 146
        echo ($context["entry_telephone"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"config_telephone\" value=\"";
        // line 148
        echo ($context["config_telephone"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_telephone"] ?? null);
        echo "\" id=\"input-telephone\" class=\"form-control maskPhone\" />
                  ";
        // line 149
        if (($context["error_telephone"] ?? null)) {
            // line 150
            echo "                  <div class=\"text-danger\">";
            echo ($context["error_telephone"] ?? null);
            echo "</div>
                  ";
        }
        // line 151
        echo " </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-fax\">";
        // line 154
        echo ($context["entry_fax"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"config_fax\" value=\"";
        // line 156
        echo ($context["config_fax"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_fax"] ?? null);
        echo "\" id=\"input-fax\" class=\"form-control maskPhone\" />
                </div>
              </div>              
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-image\">";
        // line 160
        echo ($context["entry_image"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\"><a href=\"\" id=\"thumb-image\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 161
        echo ($context["thumb"] ?? null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo ($context["placeholder"] ?? null);
        echo "\" /></a>
                  <input type=\"hidden\" name=\"config_image\" value=\"";
        // line 162
        echo ($context["config_image"] ?? null);
        echo "\" id=\"input-image\" />
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-open\"><span data-toggle=\"tooltip\" data-container=\"#tab-general\" title=\"";
        // line 166
        echo ($context["help_open"] ?? null);
        echo "\">";
        echo ($context["entry_open"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <textarea name=\"config_open\" rows=\"5\" placeholder=\"";
        // line 168
        echo ($context["entry_open"] ?? null);
        echo "\" id=\"input-open\" class=\"form-control\">";
        echo ($context["config_open"] ?? null);
        echo "</textarea>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-comment\"><span data-toggle=\"tooltip\" data-container=\"#tab-general\" title=\"";
        // line 172
        echo ($context["help_comment"] ?? null);
        echo "\">";
        echo ($context["entry_comment"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <textarea name=\"config_comment\" rows=\"5\" placeholder=\"";
        // line 174
        echo ($context["entry_comment"] ?? null);
        echo "\" id=\"input-comment\" class=\"form-control\">";
        echo ($context["config_comment"] ?? null);
        echo "</textarea>
                </div>
              </div>
              ";
        // line 177
        if (($context["locations"] ?? null)) {
            // line 178
            echo "              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" data-container=\"#tab-general\" title=\"";
            // line 179
            echo ($context["help_location"] ?? null);
            echo "\">";
            echo ($context["entry_location"] ?? null);
            echo "</span></label>
                <div class=\"col-sm-10\"> ";
            // line 180
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["locations"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["location"]) {
                // line 181
                echo "                  <div class=\"checkbox\">
                    <label> ";
                // line 182
                if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["location"], "location_id", [], "any", false, false, false, 182), ($context["config_location"] ?? null))) {
                    // line 183
                    echo "                      <input type=\"checkbox\" name=\"config_location[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["location"], "location_id", [], "any", false, false, false, 183);
                    echo "\" checked=\"checked\" />
                      ";
                    // line 184
                    echo twig_get_attribute($this->env, $this->source, $context["location"], "name", [], "any", false, false, false, 184);
                    echo "
                      ";
                } else {
                    // line 186
                    echo "                      <input type=\"checkbox\" name=\"config_location[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["location"], "location_id", [], "any", false, false, false, 186);
                    echo "\" />
                      ";
                    // line 187
                    echo twig_get_attribute($this->env, $this->source, $context["location"], "name", [], "any", false, false, false, 187);
                    echo "
                      ";
                }
                // line 188
                echo " </label>
                  </div>
                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['location'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 190
            echo " </div>
              </div>
              ";
        }
        // line 192
        echo " </div>
            <div class=\"tab-pane\" id=\"tab-local\">
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-country\">";
        // line 195
        echo ($context["entry_country"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_country_id\" id=\"input-country\" class=\"form-control\">
                    
                    ";
        // line 199
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["countries"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 200
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 200) == ($context["config_country_id"] ?? null))) {
                // line 201
                echo "                    
                    <option value=\"";
                // line 202
                echo twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 202);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, false, 202);
                echo "</option>
                    
                    ";
            } else {
                // line 205
                echo "                    
                    <option value=\"";
                // line 206
                echo twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 206);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, false, 206);
                echo "</option>
                    
                    ";
            }
            // line 209
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 210
        echo "                  
                  </select>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-zone\">";
        // line 215
        echo ($context["entry_zone"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_zone_id\" id=\"input-zone\" class=\"form-control\">
                  </select>
                </div>
              </div>
              <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-timezone\">";
        // line 222
        echo ($context["entry_timezone"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_timezone\" id=\"input-timezone\" class=\"form-control\">
                    ";
        // line 225
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["timezones"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["timezone"]) {
            // line 226
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["timezone"], "value", [], "any", false, false, false, 226) == ($context["config_timezone"] ?? null))) {
                // line 227
                echo "                        <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["timezone"], "value", [], "any", false, false, false, 227);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["timezone"], "text", [], "any", false, false, false, 227);
                echo "</option>
                      ";
            } else {
                // line 229
                echo "                        <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["timezone"], "value", [], "any", false, false, false, 229);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["timezone"], "text", [], "any", false, false, false, 229);
                echo "</option>
                      ";
            }
            // line 231
            echo "                   ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['timezone'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 232
        echo "                  </select>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-language\">";
        // line 236
        echo ($context["entry_language"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_language\" id=\"input-language\" class=\"form-control\">
                    
                    ";
        // line 240
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 241
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 241) == ($context["config_language"] ?? null))) {
                // line 242
                echo "                    
                    <option value=\"";
                // line 243
                echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 243);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 243);
                echo "</option>
                    
                    ";
            } else {
                // line 246
                echo "                    
                    <option value=\"";
                // line 247
                echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 247);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 247);
                echo "</option>
                    
                    ";
            }
            // line 250
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 251
        echo "                  
                  </select>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-admin-language\">";
        // line 256
        echo ($context["entry_admin_language"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_admin_language\" id=\"input-admin-language\" class=\"form-control\">
                    
                    ";
        // line 260
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 261
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 261) == ($context["config_admin_language"] ?? null))) {
                // line 262
                echo "                    
                    <option value=\"";
                // line 263
                echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 263);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 263);
                echo "</option>
                    
                    ";
            } else {
                // line 266
                echo "                    
                    <option value=\"";
                // line 267
                echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 267);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 267);
                echo "</option>
                    
                    ";
            }
            // line 270
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 271
        echo "                  
                  </select>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-currency\"><span data-toggle=\"tooltip\" title=\"";
        // line 276
        echo ($context["help_currency"] ?? null);
        echo "\">";
        echo ($context["entry_currency"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <select name=\"config_currency\" id=\"input-currency\" class=\"form-control\">
                    
                    ";
        // line 280
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["currencies"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
            // line 281
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["currency"], "code", [], "any", false, false, false, 281) == ($context["config_currency"] ?? null))) {
                // line 282
                echo "                    
                    <option value=\"";
                // line 283
                echo twig_get_attribute($this->env, $this->source, $context["currency"], "code", [], "any", false, false, false, 283);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["currency"], "title", [], "any", false, false, false, 283);
                echo "</option>
                    
                    ";
            } else {
                // line 286
                echo "                    
                    <option value=\"";
                // line 287
                echo twig_get_attribute($this->env, $this->source, $context["currency"], "code", [], "any", false, false, false, 287);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["currency"], "title", [], "any", false, false, false, 287);
                echo "</option>
                    
                    ";
            }
            // line 290
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 291
        echo "                  
                  </select>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 296
        echo ($context["help_currency_auto"] ?? null);
        echo "\">";
        echo ($context["entry_currency_auto"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <label class=\"radio-inline\"> ";
        // line 298
        if (($context["config_currency_auto"] ?? null)) {
            // line 299
            echo "                    <input type=\"radio\" name=\"config_currency_auto\" value=\"1\" checked=\"checked\" />
                    ";
            // line 300
            echo ($context["text_yes"] ?? null);
            echo "
                    ";
        } else {
            // line 302
            echo "                    <input type=\"radio\" name=\"config_currency_auto\" value=\"1\" />
                    ";
            // line 303
            echo ($context["text_yes"] ?? null);
            echo "
                    ";
        }
        // line 304
        echo " </label>
                  <label class=\"radio-inline\"> ";
        // line 305
        if ( !($context["config_currency_auto"] ?? null)) {
            // line 306
            echo "                    <input type=\"radio\" name=\"config_currency_auto\" value=\"0\" checked=\"checked\" />
                    ";
            // line 307
            echo ($context["text_no"] ?? null);
            echo "
                    ";
        } else {
            // line 309
            echo "                    <input type=\"radio\" name=\"config_currency_auto\" value=\"0\" />
                    ";
            // line 310
            echo ($context["text_no"] ?? null);
            echo "
                    ";
        }
        // line 311
        echo " </label>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-length-class\">";
        // line 315
        echo ($context["entry_length_class"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_length_class_id\" id=\"input-length-class\" class=\"form-control\">
                    
                    ";
        // line 319
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["length_classes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["length_class"]) {
            // line 320
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["length_class"], "length_class_id", [], "any", false, false, false, 320) == ($context["config_length_class_id"] ?? null))) {
                // line 321
                echo "                    
                    <option value=\"";
                // line 322
                echo twig_get_attribute($this->env, $this->source, $context["length_class"], "length_class_id", [], "any", false, false, false, 322);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["length_class"], "title", [], "any", false, false, false, 322);
                echo "</option>
                    
                    ";
            } else {
                // line 325
                echo "                    
                    <option value=\"";
                // line 326
                echo twig_get_attribute($this->env, $this->source, $context["length_class"], "length_class_id", [], "any", false, false, false, 326);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["length_class"], "title", [], "any", false, false, false, 326);
                echo "</option>
                    
                    ";
            }
            // line 329
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['length_class'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 330
        echo "                  
                  </select>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-weight-class\">";
        // line 335
        echo ($context["entry_weight_class"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_weight_class_id\" id=\"input-weight-class\" class=\"form-control\">
                    
                    ";
        // line 339
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["weight_classes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["weight_class"]) {
            // line 340
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["weight_class"], "weight_class_id", [], "any", false, false, false, 340) == ($context["config_weight_class_id"] ?? null))) {
                // line 341
                echo "                    
                    <option value=\"";
                // line 342
                echo twig_get_attribute($this->env, $this->source, $context["weight_class"], "weight_class_id", [], "any", false, false, false, 342);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["weight_class"], "title", [], "any", false, false, false, 342);
                echo "</option>
                    
                    ";
            } else {
                // line 345
                echo "                    
                    <option value=\"";
                // line 346
                echo twig_get_attribute($this->env, $this->source, $context["weight_class"], "weight_class_id", [], "any", false, false, false, 346);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["weight_class"], "title", [], "any", false, false, false, 346);
                echo "</option>
                    
                    ";
            }
            // line 349
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['weight_class'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 350
        echo "                  
                  </select>
                </div>
              </div>
            </div>
            <div class=\"tab-pane\" id=\"tab-option\">
              <fieldset>
                <legend>";
        // line 357
        echo ($context["text_regras"] ?? null);
        echo "</legend>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-order-minimum\"><span data-toggle=\"tooltip\" title=\"";
        // line 359
        echo ($context["help_order_minimum"] ?? null);
        echo "\">";
        echo ($context["entry_order_minimum"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_order_minimum\" value=\"";
        // line 361
        echo ($context["config_order_minimum"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_order_minimum"] ?? null);
        echo "\" id=\"input-order-minimum\" class=\"form-control\" />
                    ";
        // line 362
        if (($context["error_order_minimum"] ?? null)) {
            // line 363
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_order_minimum"] ?? null);
            echo "</div>
                    ";
        }
        // line 364
        echo " </div>
                </div>
              </fieldset>

              <fieldset>
                <legend>";
        // line 369
        echo ($context["text_product"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 371
        echo ($context["help_product_count"] ?? null);
        echo "\">";
        echo ($context["entry_product_count"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 373
        if (($context["config_product_count"] ?? null)) {
            // line 374
            echo "                      <input type=\"radio\" name=\"config_product_count\" value=\"1\" checked=\"checked\" />
                      ";
            // line 375
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 377
            echo "                      <input type=\"radio\" name=\"config_product_count\" value=\"1\" />
                      ";
            // line 378
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 379
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 380
        if ( !($context["config_product_count"] ?? null)) {
            // line 381
            echo "                      <input type=\"radio\" name=\"config_product_count\" value=\"0\" checked=\"checked\" />
                      ";
            // line 382
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 384
            echo "                      <input type=\"radio\" name=\"config_product_count\" value=\"0\" />
                      ";
            // line 385
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 386
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-admin-limit\"><span data-toggle=\"tooltip\" title=\"";
        // line 390
        echo ($context["help_limit_admin"] ?? null);
        echo "\">";
        echo ($context["entry_limit_admin"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_limit_admin\" value=\"";
        // line 392
        echo ($context["config_limit_admin"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_limit_admin"] ?? null);
        echo "\" id=\"input-admin-limit\" class=\"form-control\" />
                    ";
        // line 393
        if (($context["error_limit_admin"] ?? null)) {
            // line 394
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_limit_admin"] ?? null);
            echo "</div>
                    ";
        }
        // line 395
        echo " </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 399
        echo ($context["text_review"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 401
        echo ($context["help_review"] ?? null);
        echo "\">";
        echo ($context["entry_review"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 403
        if (($context["config_review_status"] ?? null)) {
            // line 404
            echo "                      <input type=\"radio\" name=\"config_review_status\" value=\"1\" checked=\"checked\" />
                      ";
            // line 405
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 407
            echo "                      <input type=\"radio\" name=\"config_review_status\" value=\"1\" />
                      ";
            // line 408
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 409
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 410
        if ( !($context["config_review_status"] ?? null)) {
            // line 411
            echo "                      <input type=\"radio\" name=\"config_review_status\" value=\"0\" checked=\"checked\" />
                      ";
            // line 412
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 414
            echo "                      <input type=\"radio\" name=\"config_review_status\" value=\"0\" />
                      ";
            // line 415
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 416
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 420
        echo ($context["help_review_guest"] ?? null);
        echo "\">";
        echo ($context["entry_review_guest"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 422
        if (($context["config_review_guest"] ?? null)) {
            // line 423
            echo "                      <input type=\"radio\" name=\"config_review_guest\" value=\"1\" checked=\"checked\" />
                      ";
            // line 424
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 426
            echo "                      <input type=\"radio\" name=\"config_review_guest\" value=\"1\" />
                      ";
            // line 427
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 428
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 429
        if ( !($context["config_review_guest"] ?? null)) {
            // line 430
            echo "                      <input type=\"radio\" name=\"config_review_guest\" value=\"0\" checked=\"checked\" />
                      ";
            // line 431
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 433
            echo "                      <input type=\"radio\" name=\"config_review_guest\" value=\"0\" />
                      ";
            // line 434
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 435
        echo " </label>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 440
        echo ($context["text_voucher"] ?? null);
        echo "</legend>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-voucher-min\"><span data-toggle=\"tooltip\" title=\"";
        // line 442
        echo ($context["help_voucher_min"] ?? null);
        echo "\">";
        echo ($context["entry_voucher_min"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_voucher_min\" value=\"";
        // line 444
        echo ($context["config_voucher_min"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_voucher_min"] ?? null);
        echo "\" id=\"input-voucher-min\" class=\"form-control\" />
                    ";
        // line 445
        if (($context["error_voucher_min"] ?? null)) {
            // line 446
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_voucher_min"] ?? null);
            echo "</div>
                    ";
        }
        // line 447
        echo " </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-voucher-max\"><span data-toggle=\"tooltip\" title=\"";
        // line 450
        echo ($context["help_voucher_max"] ?? null);
        echo "\">";
        echo ($context["entry_voucher_max"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_voucher_max\" value=\"";
        // line 452
        echo ($context["config_voucher_max"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_voucher_max"] ?? null);
        echo "\" id=\"input-voucher-max\" class=\"form-control\" />
                    ";
        // line 453
        if (($context["error_voucher_max"] ?? null)) {
            // line 454
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_voucher_max"] ?? null);
            echo "</div>
                    ";
        }
        // line 455
        echo " </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 459
        echo ($context["text_tax"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 461
        echo ($context["entry_tax"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 463
        if (($context["config_tax"] ?? null)) {
            // line 464
            echo "                      <input type=\"radio\" name=\"config_tax\" value=\"1\" checked=\"checked\" />
                      ";
            // line 465
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 467
            echo "                      <input type=\"radio\" name=\"config_tax\" value=\"1\" />
                      ";
            // line 468
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 469
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 470
        if ( !($context["config_tax"] ?? null)) {
            // line 471
            echo "                      <input type=\"radio\" name=\"config_tax\" value=\"0\" checked=\"checked\" />
                      ";
            // line 472
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 474
            echo "                      <input type=\"radio\" name=\"config_tax\" value=\"0\" />
                      ";
            // line 475
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 476
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-tax-default\"><span data-toggle=\"tooltip\" title=\"";
        // line 480
        echo ($context["help_tax_default"] ?? null);
        echo "\">";
        echo ($context["entry_tax_default"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_tax_default\" id=\"input-tax-default\" class=\"form-control\">
                      <option value=\"\">";
        // line 483
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 485
        if ((($context["config_tax_default"] ?? null) == "shipping")) {
            // line 486
            echo "                      
                      <option value=\"shipping\" selected=\"selected\">";
            // line 487
            echo ($context["text_shipping"] ?? null);
            echo "</option>
                      
                      ";
        } else {
            // line 490
            echo "                      
                      <option value=\"shipping\">";
            // line 491
            echo ($context["text_shipping"] ?? null);
            echo "</option>
                      
                      ";
        }
        // line 494
        echo "                      ";
        if ((($context["config_tax_default"] ?? null) == "payment")) {
            // line 495
            echo "                      
                      <option value=\"payment\" selected=\"selected\">";
            // line 496
            echo ($context["text_payment"] ?? null);
            echo "</option>
                      
                      ";
        } else {
            // line 499
            echo "                      
                      <option value=\"payment\">";
            // line 500
            echo ($context["text_payment"] ?? null);
            echo "</option>
                      
                      ";
        }
        // line 503
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-tax-customer\"><span data-toggle=\"tooltip\" title=\"";
        // line 508
        echo ($context["help_tax_customer"] ?? null);
        echo "\">";
        echo ($context["entry_tax_customer"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_tax_customer\" id=\"input-tax-customer\" class=\"form-control\">
                      <option value=\"\">";
        // line 511
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 513
        if ((($context["config_tax_customer"] ?? null) == "shipping")) {
            // line 514
            echo "                      
                      <option value=\"shipping\" selected=\"selected\">";
            // line 515
            echo ($context["text_shipping"] ?? null);
            echo "</option>
                      
                      ";
        } else {
            // line 518
            echo "                      
                      <option value=\"shipping\">";
            // line 519
            echo ($context["text_shipping"] ?? null);
            echo "</option>
                      
                      ";
        }
        // line 522
        echo "                      ";
        if ((($context["config_tax_customer"] ?? null) == "payment")) {
            // line 523
            echo "                      
                      <option value=\"payment\" selected=\"selected\">";
            // line 524
            echo ($context["text_payment"] ?? null);
            echo "</option>
                      
                      ";
        } else {
            // line 527
            echo "                      
                      <option value=\"payment\">";
            // line 528
            echo ($context["text_payment"] ?? null);
            echo "</option>
                      
                      ";
        }
        // line 531
        echo "                    
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 537
        echo ($context["text_account"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 539
        echo ($context["help_customer_online"] ?? null);
        echo "\">";
        echo ($context["entry_customer_online"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 541
        if (($context["config_customer_online"] ?? null)) {
            // line 542
            echo "                      <input type=\"radio\" name=\"config_customer_online\" value=\"1\" checked=\"checked\" />
                      ";
            // line 543
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 545
            echo "                      <input type=\"radio\" name=\"config_customer_online\" value=\"1\" />
                      ";
            // line 546
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 547
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 548
        if ( !($context["config_customer_online"] ?? null)) {
            // line 549
            echo "                      <input type=\"radio\" name=\"config_customer_online\" value=\"0\" checked=\"checked\" />
                      ";
            // line 550
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 552
            echo "                      <input type=\"radio\" name=\"config_customer_online\" value=\"0\" />
                      ";
            // line 553
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 554
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 558
        echo ($context["help_customer_activity"] ?? null);
        echo "\">";
        echo ($context["entry_customer_activity"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 560
        if (($context["config_customer_activity"] ?? null)) {
            // line 561
            echo "                      <input type=\"radio\" name=\"config_customer_activity\" value=\"1\" checked=\"checked\" />
                      ";
            // line 562
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 564
            echo "                      <input type=\"radio\" name=\"config_customer_activity\" value=\"1\" />
                      ";
            // line 565
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 566
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 567
        if ( !($context["config_customer_activity"] ?? null)) {
            // line 568
            echo "                      <input type=\"radio\" name=\"config_customer_activity\" value=\"0\" checked=\"checked\" />
                      ";
            // line 569
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 571
            echo "                      <input type=\"radio\" name=\"config_customer_activity\" value=\"0\" />
                      ";
            // line 572
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 573
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 577
        echo ($context["entry_customer_search"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 579
        if (($context["config_customer_search"] ?? null)) {
            // line 580
            echo "                      <input type=\"radio\" name=\"config_customer_search\" value=\"1\" checked=\"checked\" />
                      ";
            // line 581
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 583
            echo "                      <input type=\"radio\" name=\"config_customer_search\" value=\"1\" />
                      ";
            // line 584
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 585
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 586
        if ( !($context["config_customer_search"] ?? null)) {
            // line 587
            echo "                      <input type=\"radio\" name=\"config_customer_search\" value=\"0\" checked=\"checked\" />
                      ";
            // line 588
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 590
            echo "                      <input type=\"radio\" name=\"config_customer_search\" value=\"0\" />
                      ";
            // line 591
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 592
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-customer-group\"><span data-toggle=\"tooltip\" title=\"";
        // line 596
        echo ($context["help_customer_group"] ?? null);
        echo "\">";
        echo ($context["entry_customer_group"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_customer_group_id\" id=\"input-customer-group\" class=\"form-control\">
                      
                      ";
        // line 600
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["customer_groups"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["customer_group"]) {
            // line 601
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 601) == ($context["config_customer_group_id"] ?? null))) {
                // line 602
                echo "                      
                      <option value=\"";
                // line 603
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 603);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "name", [], "any", false, false, false, 603);
                echo "</option>
                      
                      ";
            } else {
                // line 606
                echo "                      
                      <option value=\"";
                // line 607
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 607);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "name", [], "any", false, false, false, 607);
                echo "</option>
                      
                      ";
            }
            // line 610
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['customer_group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 611
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 616
        echo ($context["help_customer_group_display"] ?? null);
        echo "\">";
        echo ($context["entry_customer_group_display"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\"> ";
        // line 617
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["customer_groups"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["customer_group"]) {
            // line 618
            echo "                    <div class=\"checkbox\">
                      <label> ";
            // line 619
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 619), ($context["config_customer_group_display"] ?? null))) {
                // line 620
                echo "                        <input type=\"checkbox\" name=\"config_customer_group_display[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 620);
                echo "\" checked=\"checked\" />
                        ";
                // line 621
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "name", [], "any", false, false, false, 621);
                echo "
                        ";
            } else {
                // line 623
                echo "                        <input type=\"checkbox\" name=\"config_customer_group_display[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 623);
                echo "\" />
                        ";
                // line 624
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "name", [], "any", false, false, false, 624);
                echo "
                        ";
            }
            // line 625
            echo " </label>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['customer_group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 628
        echo "                    ";
        if (($context["error_customer_group_display"] ?? null)) {
            // line 629
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_customer_group_display"] ?? null);
            echo "</div>
                    ";
        }
        // line 630
        echo " </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 633
        echo ($context["help_customer_price"] ?? null);
        echo "\">";
        echo ($context["entry_customer_price"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 635
        if (($context["config_customer_price"] ?? null)) {
            // line 636
            echo "                      <input type=\"radio\" name=\"config_customer_price\" value=\"1\" checked=\"checked\" />
                      ";
            // line 637
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 639
            echo "                      <input type=\"radio\" name=\"config_customer_price\" value=\"1\" />
                      ";
            // line 640
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 641
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 642
        if ( !($context["config_customer_price"] ?? null)) {
            // line 643
            echo "                      <input type=\"radio\" name=\"config_customer_price\" value=\"0\" checked=\"checked\" />
                      ";
            // line 644
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 646
            echo "                      <input type=\"radio\" name=\"config_customer_price\" value=\"0\" />
                      ";
            // line 647
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 648
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-login-attempts\"><span data-toggle=\"tooltip\" title=\"";
        // line 652
        echo ($context["help_login_attempts"] ?? null);
        echo "\">";
        echo ($context["entry_login_attempts"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_login_attempts\" value=\"";
        // line 654
        echo ($context["config_login_attempts"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_login_attempts"] ?? null);
        echo "\" id=\"input-login-attempts\" class=\"form-control\" />
                    ";
        // line 655
        if (($context["error_login_attempts"] ?? null)) {
            // line 656
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_login_attempts"] ?? null);
            echo "</div>
                    ";
        }
        // line 657
        echo " </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-account\"><span data-toggle=\"tooltip\" title=\"";
        // line 660
        echo ($context["help_account"] ?? null);
        echo "\">";
        echo ($context["entry_account"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_account_id\" id=\"input-account\" class=\"form-control\">
                      <option value=\"0\">";
        // line 663
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 665
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["informations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
            // line 666
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 666) == ($context["config_account_id"] ?? null))) {
                // line 667
                echo "                      
                      <option value=\"";
                // line 668
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 668);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 668);
                echo "</option>
                      
                      ";
            } else {
                // line 671
                echo "                      
                      <option value=\"";
                // line 672
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 672);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 672);
                echo "</option>
                      
                      ";
            }
            // line 675
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 676
        echo "                    
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 682
        echo ($context["text_checkout"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-invoice-prefix\"><span data-toggle=\"tooltip\" title=\"";
        // line 684
        echo ($context["help_invoice_prefix"] ?? null);
        echo "\">";
        echo ($context["entry_invoice_prefix"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_invoice_prefix\" value=\"";
        // line 686
        echo ($context["config_invoice_prefix"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_invoice_prefix"] ?? null);
        echo "\" id=\"input-invoice-prefix\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 690
        echo ($context["help_cart_weight"] ?? null);
        echo "\">";
        echo ($context["entry_cart_weight"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 692
        if (($context["config_cart_weight"] ?? null)) {
            // line 693
            echo "                      <input type=\"radio\" name=\"config_cart_weight\" value=\"1\" checked=\"checked\" />
                      ";
            // line 694
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 696
            echo "                      <input type=\"radio\" name=\"config_cart_weight\" value=\"1\" />
                      ";
            // line 697
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 698
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 699
        if ( !($context["config_cart_weight"] ?? null)) {
            // line 700
            echo "                      <input type=\"radio\" name=\"config_cart_weight\" value=\"0\" checked=\"checked\" />
                      ";
            // line 701
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 703
            echo "                      <input type=\"radio\" name=\"config_cart_weight\" value=\"0\" />
                      ";
            // line 704
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 705
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 709
        echo ($context["help_checkout_guest"] ?? null);
        echo "\">";
        echo ($context["entry_checkout_guest"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 711
        if (($context["config_checkout_guest"] ?? null)) {
            // line 712
            echo "                      <input type=\"radio\" name=\"config_checkout_guest\" value=\"1\" checked=\"checked\" />
                      ";
            // line 713
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 715
            echo "                      <input type=\"radio\" name=\"config_checkout_guest\" value=\"1\" />
                      ";
            // line 716
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 717
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 718
        if ( !($context["config_checkout_guest"] ?? null)) {
            // line 719
            echo "                      <input type=\"radio\" name=\"config_checkout_guest\" value=\"0\" checked=\"checked\" />
                      ";
            // line 720
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 722
            echo "                      <input type=\"radio\" name=\"config_checkout_guest\" value=\"0\" />
                      ";
            // line 723
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 724
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-checkout\"><span data-toggle=\"tooltip\" title=\"";
        // line 728
        echo ($context["help_checkout"] ?? null);
        echo "\">";
        echo ($context["entry_checkout"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_checkout_id\" id=\"input-checkout\" class=\"form-control\">
                      <option value=\"0\">";
        // line 731
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 733
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["informations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
            // line 734
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 734) == ($context["config_checkout_id"] ?? null))) {
                // line 735
                echo "                      
                      <option value=\"";
                // line 736
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 736);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 736);
                echo "</option>
                      
                      ";
            } else {
                // line 739
                echo "                      
                      <option value=\"";
                // line 740
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 740);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 740);
                echo "</option>
                      
                      ";
            }
            // line 743
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 744
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-order-status\"><span data-toggle=\"tooltip\" title=\"";
        // line 749
        echo ($context["help_order_status"] ?? null);
        echo "\">";
        echo ($context["entry_order_status"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_order_status_id\" id=\"input-order-status\" class=\"form-control\">
                      
                      ";
        // line 753
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 754
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 754) == ($context["config_order_status_id"] ?? null))) {
                // line 755
                echo "                      
                      <option value=\"";
                // line 756
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 756);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 756);
                echo "</option>
                      
                      ";
            } else {
                // line 759
                echo "                      
                      <option value=\"";
                // line 760
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 760);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 760);
                echo "</option>
                      
                      ";
            }
            // line 763
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 764
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-process-status\"><span data-toggle=\"tooltip\" title=\"";
        // line 769
        echo ($context["help_processing_status"] ?? null);
        echo "\">";
        echo ($context["entry_processing_status"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <div class=\"well well-sm\" style=\"height: 150px; overflow: auto;\"> ";
        // line 771
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 772
            echo "                      <div class=\"checkbox\">
                        <label> ";
            // line 773
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 773), ($context["config_processing_status"] ?? null))) {
                // line 774
                echo "                          <input type=\"checkbox\" name=\"config_processing_status[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 774);
                echo "\" checked=\"checked\" />
                          ";
                // line 775
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 775);
                echo "
                          ";
            } else {
                // line 777
                echo "                          <input type=\"checkbox\" name=\"config_processing_status[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 777);
                echo "\" />
                          ";
                // line 778
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 778);
                echo "
                          ";
            }
            // line 779
            echo " </label>
                      </div>
                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 781
        echo " </div>
                    ";
        // line 782
        if (($context["error_processing_status"] ?? null)) {
            // line 783
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_processing_status"] ?? null);
            echo "</div>
                    ";
        }
        // line 784
        echo " </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-complete-status\"><span data-toggle=\"tooltip\" title=\"";
        // line 787
        echo ($context["help_complete_status"] ?? null);
        echo "\">";
        echo ($context["entry_complete_status"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <div class=\"well well-sm\" style=\"height: 150px; overflow: auto;\"> ";
        // line 789
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 790
            echo "                      <div class=\"checkbox\">
                        <label> ";
            // line 791
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 791), ($context["config_complete_status"] ?? null))) {
                // line 792
                echo "                          <input type=\"checkbox\" name=\"config_complete_status[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 792);
                echo "\" checked=\"checked\" />
                          ";
                // line 793
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 793);
                echo "
                          ";
            } else {
                // line 795
                echo "                          <input type=\"checkbox\" name=\"config_complete_status[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 795);
                echo "\" />
                          ";
                // line 796
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 796);
                echo "
                          ";
            }
            // line 797
            echo " </label>
                      </div>
                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 799
        echo " </div>
                    ";
        // line 800
        if (($context["error_complete_status"] ?? null)) {
            // line 801
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_complete_status"] ?? null);
            echo "</div>
                    ";
        }
        // line 802
        echo " </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-fraud-status\"><span data-toggle=\"tooltip\" title=\"";
        // line 805
        echo ($context["help_fraud_status"] ?? null);
        echo "\">";
        echo ($context["entry_fraud_status"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_fraud_status_id\" id=\"input-fraud-status\" class=\"form-control\">
                      
                      ";
        // line 809
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 810
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 810) == ($context["config_fraud_status_id"] ?? null))) {
                // line 811
                echo "                      
                      <option value=\"";
                // line 812
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 812);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 812);
                echo "</option>
                      
                      ";
            } else {
                // line 815
                echo "                      
                      <option value=\"";
                // line 816
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 816);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 816);
                echo "</option>
                      
                      ";
            }
            // line 819
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 820
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-api\"><span data-toggle=\"tooltip\" title=\"";
        // line 825
        echo ($context["help_api"] ?? null);
        echo "\">";
        echo ($context["entry_api"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_api_id\" id=\"input-api\" class=\"form-control\">
                      <option value=\"0\">";
        // line 828
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 830
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["apis"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["api"]) {
            // line 831
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["api"], "api_id", [], "any", false, false, false, 831) == ($context["config_api_id"] ?? null))) {
                // line 832
                echo "                      
                      <option value=\"";
                // line 833
                echo twig_get_attribute($this->env, $this->source, $context["api"], "api_id", [], "any", false, false, false, 833);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["api"], "username", [], "any", false, false, false, 833);
                echo "</option>
                      
                      ";
            } else {
                // line 836
                echo "                      
                      <option value=\"";
                // line 837
                echo twig_get_attribute($this->env, $this->source, $context["api"], "api_id", [], "any", false, false, false, 837);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["api"], "username", [], "any", false, false, false, 837);
                echo "</option>
                      
                      ";
            }
            // line 840
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['api'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 841
        echo "                    
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 847
        echo ($context["text_stock"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 849
        echo ($context["help_stock_display"] ?? null);
        echo "\">";
        echo ($context["entry_stock_display"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 851
        if (($context["config_stock_display"] ?? null)) {
            // line 852
            echo "                      <input type=\"radio\" name=\"config_stock_display\" value=\"1\" checked=\"checked\" />
                      ";
            // line 853
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 855
            echo "                      <input type=\"radio\" name=\"config_stock_display\" value=\"1\" />
                      ";
            // line 856
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 857
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 858
        if ( !($context["config_stock_display"] ?? null)) {
            // line 859
            echo "                      <input type=\"radio\" name=\"config_stock_display\" value=\"0\" checked=\"checked\" />
                      ";
            // line 860
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 862
            echo "                      <input type=\"radio\" name=\"config_stock_display\" value=\"0\" />
                      ";
            // line 863
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 864
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 868
        echo ($context["help_stock_warning"] ?? null);
        echo "\">";
        echo ($context["entry_stock_warning"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 870
        if (($context["config_stock_warning"] ?? null)) {
            // line 871
            echo "                      <input type=\"radio\" name=\"config_stock_warning\" value=\"1\" checked=\"checked\" />
                      ";
            // line 872
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 874
            echo "                      <input type=\"radio\" name=\"config_stock_warning\" value=\"1\" />
                      ";
            // line 875
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 876
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 877
        if ( !($context["config_stock_warning"] ?? null)) {
            // line 878
            echo "                      <input type=\"radio\" name=\"config_stock_warning\" value=\"0\" checked=\"checked\" />
                      ";
            // line 879
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 881
            echo "                      <input type=\"radio\" name=\"config_stock_warning\" value=\"0\" />
                      ";
            // line 882
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 883
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 887
        echo ($context["help_stock_checkout"] ?? null);
        echo "\">";
        echo ($context["entry_stock_checkout"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 889
        if (($context["config_stock_checkout"] ?? null)) {
            // line 890
            echo "                      <input type=\"radio\" name=\"config_stock_checkout\" value=\"1\" checked=\"checked\" />
                      ";
            // line 891
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 893
            echo "                      <input type=\"radio\" name=\"config_stock_checkout\" value=\"1\" />
                      ";
            // line 894
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 895
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 896
        if ( !($context["config_stock_checkout"] ?? null)) {
            // line 897
            echo "                      <input type=\"radio\" name=\"config_stock_checkout\" value=\"0\" checked=\"checked\" />
                      ";
            // line 898
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 900
            echo "                      <input type=\"radio\" name=\"config_stock_checkout\" value=\"0\" />
                      ";
            // line 901
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 902
        echo " </label>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 907
        echo ($context["text_affiliate"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-affiliate-group\">";
        // line 909
        echo ($context["entry_affiliate_group"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_affiliate_group_id\" id=\"input-affiliate-group\" class=\"form-control\">
                      
                      ";
        // line 913
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["customer_groups"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["customer_group"]) {
            // line 914
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 914) == ($context["config_affiliate_group_id"] ?? null))) {
                // line 915
                echo "                      
                      <option value=\"";
                // line 916
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 916);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "name", [], "any", false, false, false, 916);
                echo "</option>
                      
                      ";
            } else {
                // line 919
                echo "                      
                      <option value=\"";
                // line 920
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 920);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "name", [], "any", false, false, false, 920);
                echo "</option>
                      
                      ";
            }
            // line 923
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['customer_group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 924
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 929
        echo ($context["help_affiliate_approval"] ?? null);
        echo "\">";
        echo ($context["entry_affiliate_approval"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 931
        if (($context["config_affiliate_approval"] ?? null)) {
            // line 932
            echo "                      <input type=\"radio\" name=\"config_affiliate_approval\" value=\"1\" checked=\"checked\" />
                      ";
            // line 933
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 935
            echo "                      <input type=\"radio\" name=\"config_affiliate_approval\" value=\"1\" />
                      ";
            // line 936
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 937
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 938
        if ( !($context["config_affiliate_approval"] ?? null)) {
            // line 939
            echo "                      <input type=\"radio\" name=\"config_affiliate_approval\" value=\"0\" checked=\"checked\" />
                      ";
            // line 940
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 942
            echo "                      <input type=\"radio\" name=\"config_affiliate_approval\" value=\"0\" />
                      ";
            // line 943
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 944
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 948
        echo ($context["help_affiliate_auto"] ?? null);
        echo "\">";
        echo ($context["entry_affiliate_auto"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 950
        if (($context["config_affiliate_auto"] ?? null)) {
            // line 951
            echo "                      <input type=\"radio\" name=\"config_affiliate_auto\" value=\"1\" checked=\"checked\" />
                      ";
            // line 952
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 954
            echo "                      <input type=\"radio\" name=\"config_affiliate_auto\" value=\"1\" />
                      ";
            // line 955
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 956
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 957
        if ( !($context["config_affiliate_auto"] ?? null)) {
            // line 958
            echo "                      <input type=\"radio\" name=\"config_affiliate_auto\" value=\"0\" checked=\"checked\" />
                      ";
            // line 959
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 961
            echo "                      <input type=\"radio\" name=\"config_affiliate_auto\" value=\"0\" />
                      ";
            // line 962
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 963
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-affiliate-commission\"><span data-toggle=\"tooltip\" title=\"";
        // line 967
        echo ($context["help_affiliate_commission"] ?? null);
        echo "\">";
        echo ($context["entry_affiliate_commission"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_affiliate_commission\" value=\"";
        // line 969
        echo ($context["config_affiliate_commission"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_affiliate_commission"] ?? null);
        echo "\" id=\"input-affiliate-commission\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-affiliate\"><span data-toggle=\"tooltip\" title=\"";
        // line 973
        echo ($context["help_affiliate"] ?? null);
        echo "\">";
        echo ($context["entry_affiliate"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_affiliate_id\" id=\"input-affiliate\" class=\"form-control\">
                      <option value=\"0\">";
        // line 976
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 978
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["informations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
            // line 979
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 979) == ($context["config_affiliate_id"] ?? null))) {
                // line 980
                echo "                      
                      <option value=\"";
                // line 981
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 981);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 981);
                echo "</option>
                      
                      ";
            } else {
                // line 984
                echo "                      
                      <option value=\"";
                // line 985
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 985);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 985);
                echo "</option>
                      
                      ";
            }
            // line 988
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 989
        echo "                    
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 995
        echo ($context["text_return"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-return\"><span data-toggle=\"tooltip\" title=\"";
        // line 997
        echo ($context["help_return"] ?? null);
        echo "\">";
        echo ($context["entry_return"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_return_id\" id=\"input-return\" class=\"form-control\">
                      <option value=\"0\">";
        // line 1000
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 1002
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["informations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
            // line 1003
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 1003) == ($context["config_return_id"] ?? null))) {
                // line 1004
                echo "                      
                      <option value=\"";
                // line 1005
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 1005);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 1005);
                echo "</option>
                      
                      ";
            } else {
                // line 1008
                echo "                      
                      <option value=\"";
                // line 1009
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 1009);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 1009);
                echo "</option>
                      
                      ";
            }
            // line 1012
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1013
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-return-status\"><span data-toggle=\"tooltip\" title=\"";
        // line 1018
        echo ($context["help_return_status"] ?? null);
        echo "\">";
        echo ($context["entry_return_status"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_return_status_id\" id=\"input-return-status\" class=\"form-control\">
                      
                      ";
        // line 1022
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["return_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["return_status"]) {
            // line 1023
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["return_status"], "return_status_id", [], "any", false, false, false, 1023) == ($context["config_return_status_id"] ?? null))) {
                // line 1024
                echo "                      
                      <option value=\"";
                // line 1025
                echo twig_get_attribute($this->env, $this->source, $context["return_status"], "return_status_id", [], "any", false, false, false, 1025);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["return_status"], "name", [], "any", false, false, false, 1025);
                echo "</option>
                      
                      ";
            } else {
                // line 1028
                echo "                      
                      <option value=\"";
                // line 1029
                echo twig_get_attribute($this->env, $this->source, $context["return_status"], "return_status_id", [], "any", false, false, false, 1029);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["return_status"], "name", [], "any", false, false, false, 1029);
                echo "</option>
                      
                      ";
            }
            // line 1032
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['return_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1033
        echo "                    
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 1039
        echo ($context["text_captcha"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 1041
        echo ($context["help_captcha"] ?? null);
        echo "\">";
        echo ($context["entry_captcha"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_captcha\" id=\"input-captcha\" class=\"form-control\">
                      <option value=\"\">";
        // line 1044
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 1046
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["captchas"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["captcha"]) {
            // line 1047
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["captcha"], "value", [], "any", false, false, false, 1047) == ($context["config_captcha"] ?? null))) {
                // line 1048
                echo "                      
                      <option value=\"";
                // line 1049
                echo twig_get_attribute($this->env, $this->source, $context["captcha"], "value", [], "any", false, false, false, 1049);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["captcha"], "text", [], "any", false, false, false, 1049);
                echo "</option>
                      
                      ";
            } else {
                // line 1052
                echo "                      
                      <option value=\"";
                // line 1053
                echo twig_get_attribute($this->env, $this->source, $context["captcha"], "value", [], "any", false, false, false, 1053);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["captcha"], "text", [], "any", false, false, false, 1053);
                echo "</option>
                      
                      ";
            }
            // line 1056
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['captcha'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1057
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 1062
        echo ($context["entry_captcha_page"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <div class=\"well well-sm\" style=\"height: 150px; overflow: auto;\"> ";
        // line 1064
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["captcha_pages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["captcha_page"]) {
            // line 1065
            echo "                      <div class=\"checkbox\">
                        <label> ";
            // line 1066
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["captcha_page"], "value", [], "any", false, false, false, 1066), ($context["config_captcha_page"] ?? null))) {
                // line 1067
                echo "                          <input type=\"checkbox\" name=\"config_captcha_page[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["captcha_page"], "value", [], "any", false, false, false, 1067);
                echo "\" checked=\"checked\" />
                          ";
                // line 1068
                echo twig_get_attribute($this->env, $this->source, $context["captcha_page"], "text", [], "any", false, false, false, 1068);
                echo "
                          ";
            } else {
                // line 1070
                echo "                          <input type=\"checkbox\" name=\"config_captcha_page[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["captcha_page"], "value", [], "any", false, false, false, 1070);
                echo "\" />
                          ";
                // line 1071
                echo twig_get_attribute($this->env, $this->source, $context["captcha_page"], "text", [], "any", false, false, false, 1071);
                echo "
                          ";
            }
            // line 1072
            echo " </label>
                      </div>
                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['captcha_page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1074
        echo " </div>
                  </div>
                </div>
              </fieldset>
            </div>
            <div class=\"tab-pane\" id=\"tab-image\">
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-logo\">";
        // line 1081
        echo ($context["entry_logo"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\"><a href=\"\" id=\"thumb-logo\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 1082
        echo ($context["logo"] ?? null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo ($context["placeholder"] ?? null);
        echo "\" /></a>
                  <input type=\"hidden\" name=\"config_logo\" value=\"";
        // line 1083
        echo ($context["config_logo"] ?? null);
        echo "\" id=\"input-logo\" />
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-icon\"><span data-toggle=\"tooltip\" title=\"";
        // line 1087
        echo ($context["help_icon"] ?? null);
        echo "\">";
        echo ($context["entry_icon"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\"><a href=\"\" id=\"thumb-icon\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 1088
        echo ($context["icon"] ?? null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo ($context["placeholder"] ?? null);
        echo "\" /></a>
                  <input type=\"hidden\" name=\"config_icon\" value=\"";
        // line 1089
        echo ($context["config_icon"] ?? null);
        echo "\" id=\"input-icon\" />
                </div>
              </div>
            </div>
            <div class=\"tab-pane\" id=\"tab-mail\">
              <fieldset>
                <legend>";
        // line 1095
        echo ($context["text_general"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-engine\"><span data-toggle=\"tooltip\" title=\"";
        // line 1097
        echo ($context["help_mail_engine"] ?? null);
        echo "\">";
        echo ($context["entry_mail_engine"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_mail_engine\" id=\"input-mail-engine\" class=\"form-control\">
                      
                      ";
        // line 1101
        if ((($context["config_mail_engine"] ?? null) == "mail")) {
            // line 1102
            echo "                      
                      <option value=\"mail\" selected=\"selected\">";
            // line 1103
            echo ($context["text_mail"] ?? null);
            echo "</option>
                      
                      ";
        } else {
            // line 1106
            echo "                      
                      <option value=\"mail\">";
            // line 1107
            echo ($context["text_mail"] ?? null);
            echo "</option>
                      
                      ";
        }
        // line 1110
        echo "                      ";
        if ((($context["config_mail_engine"] ?? null) == "smtp")) {
            // line 1111
            echo "                      
                      <option value=\"smtp\" selected=\"selected\">";
            // line 1112
            echo ($context["text_smtp"] ?? null);
            echo "</option>
                      
                      ";
        } else {
            // line 1115
            echo "                      
                      <option value=\"smtp\">";
            // line 1116
            echo ($context["text_smtp"] ?? null);
            echo "</option>
                      
                      ";
        }
        // line 1119
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-parameter\"><span data-toggle=\"tooltip\" title=\"";
        // line 1124
        echo ($context["help_mail_parameter"] ?? null);
        echo "\">";
        echo ($context["entry_mail_parameter"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_mail_parameter\" value=\"";
        // line 1126
        echo ($context["config_mail_parameter"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_mail_parameter"] ?? null);
        echo "\" id=\"input-mail-parameter\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-smtp-hostname\"><span data-toggle=\"tooltip\" title=\"";
        // line 1130
        echo ($context["help_mail_smtp_hostname"] ?? null);
        echo "\">";
        echo ($context["entry_mail_smtp_hostname"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_mail_smtp_hostname\" value=\"";
        // line 1132
        echo ($context["config_mail_smtp_hostname"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_mail_smtp_hostname"] ?? null);
        echo "\" id=\"input-mail-smtp-hostname\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-smtp-username\">";
        // line 1136
        echo ($context["entry_mail_smtp_username"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_mail_smtp_username\" value=\"";
        // line 1138
        echo ($context["config_mail_smtp_username"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_mail_smtp_username"] ?? null);
        echo "\" id=\"input-mail-smtp-username\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-smtp-password\"><span data-toggle=\"tooltip\" title=\"";
        // line 1142
        echo ($context["help_mail_smtp_password"] ?? null);
        echo "\">";
        echo ($context["entry_mail_smtp_password"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_mail_smtp_password\" value=\"";
        // line 1144
        echo ($context["config_mail_smtp_password"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_mail_smtp_password"] ?? null);
        echo "\" id=\"input-mail-smtp-password\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-smtp-port\">";
        // line 1148
        echo ($context["entry_mail_smtp_port"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_mail_smtp_port\" value=\"";
        // line 1150
        echo ($context["config_mail_smtp_port"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_mail_smtp_port"] ?? null);
        echo "\" id=\"input-mail-smtp-port\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-smtp-timeout\">";
        // line 1154
        echo ($context["entry_mail_smtp_timeout"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_mail_smtp_timeout\" value=\"";
        // line 1156
        echo ($context["config_mail_smtp_timeout"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_mail_smtp_timeout"] ?? null);
        echo "\" id=\"input-mail-smtp-timeout\" class=\"form-control\" />
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 1161
        echo ($context["text_mail_alert"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 1163
        echo ($context["help_mail_alert"] ?? null);
        echo "\">";
        echo ($context["entry_mail_alert"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <div class=\"well well-sm\" style=\"height: 150px; overflow: auto;\"> ";
        // line 1165
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["mail_alerts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["mail_alert"]) {
            // line 1166
            echo "                      <div class=\"checkbox\">
                        <label> ";
            // line 1167
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["mail_alert"], "value", [], "any", false, false, false, 1167), ($context["config_mail_alert"] ?? null))) {
                // line 1168
                echo "                          <input type=\"checkbox\" name=\"config_mail_alert[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["mail_alert"], "value", [], "any", false, false, false, 1168);
                echo "\" checked=\"checked\" />
                          ";
                // line 1169
                echo twig_get_attribute($this->env, $this->source, $context["mail_alert"], "text", [], "any", false, false, false, 1169);
                echo "
                          ";
            } else {
                // line 1171
                echo "                          <input type=\"checkbox\" name=\"config_mail_alert[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["mail_alert"], "value", [], "any", false, false, false, 1171);
                echo "\" />
                          ";
                // line 1172
                echo twig_get_attribute($this->env, $this->source, $context["mail_alert"], "text", [], "any", false, false, false, 1172);
                echo "
                          ";
            }
            // line 1173
            echo " </label>
                      </div>
                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mail_alert'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1175
        echo " </div>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-alert-email\"><span data-toggle=\"tooltip\" title=\"";
        // line 1179
        echo ($context["help_mail_alert_email"] ?? null);
        echo "\">";
        echo ($context["entry_mail_alert_email"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <textarea name=\"config_mail_alert_email\" rows=\"5\" placeholder=\"";
        // line 1181
        echo ($context["entry_mail_alert_email"] ?? null);
        echo "\" id=\"input-alert-email\" class=\"form-control\">";
        echo ($context["config_mail_alert_email"] ?? null);
        echo "</textarea>
                  </div>
                </div>
              </fieldset>
            </div>
            <div class=\"tab-pane\" id=\"tab-server\">
              <fieldset>
                <legend>";
        // line 1188
        echo ($context["text_general"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 1190
        echo ($context["help_maintenance"] ?? null);
        echo "\">";
        echo ($context["entry_maintenance"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 1192
        if (($context["config_maintenance"] ?? null)) {
            // line 1193
            echo "                      <input type=\"radio\" name=\"config_maintenance\" value=\"1\" checked=\"checked\" />
                      ";
            // line 1194
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 1196
            echo "                      <input type=\"radio\" name=\"config_maintenance\" value=\"1\" />
                      ";
            // line 1197
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 1198
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 1199
        if ( !($context["config_maintenance"] ?? null)) {
            // line 1200
            echo "                      <input type=\"radio\" name=\"config_maintenance\" value=\"0\" checked=\"checked\" />
                      ";
            // line 1201
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 1203
            echo "                      <input type=\"radio\" name=\"config_maintenance\" value=\"0\" />
                      ";
            // line 1204
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 1205
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 1209
        echo ($context["help_seo_url"] ?? null);
        echo "\">";
        echo ($context["entry_seo_url"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 1211
        if (($context["config_seo_url"] ?? null)) {
            // line 1212
            echo "                      <input type=\"radio\" name=\"config_seo_url\" value=\"1\" checked=\"checked\" />
                      ";
            // line 1213
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 1215
            echo "                      <input type=\"radio\" name=\"config_seo_url\" value=\"1\" />
                      ";
            // line 1216
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 1217
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 1218
        if ( !($context["config_seo_url"] ?? null)) {
            // line 1219
            echo "                      <input type=\"radio\" name=\"config_seo_url\" value=\"0\" checked=\"checked\" />
                      ";
            // line 1220
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 1222
            echo "                      <input type=\"radio\" name=\"config_seo_url\" value=\"0\" />
                      ";
            // line 1223
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 1224
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-robots\"><span data-toggle=\"tooltip\" title=\"";
        // line 1228
        echo ($context["help_robots"] ?? null);
        echo "\">";
        echo ($context["entry_robots"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <textarea name=\"config_robots\" rows=\"5\" placeholder=\"";
        // line 1230
        echo ($context["entry_robots"] ?? null);
        echo "\" id=\"input-robots\" class=\"form-control\">";
        echo ($context["config_robots"] ?? null);
        echo "</textarea>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-compression\"><span data-toggle=\"tooltip\" title=\"";
        // line 1234
        echo ($context["help_compression"] ?? null);
        echo "\">";
        echo ($context["entry_compression"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_compression\" value=\"";
        // line 1236
        echo ($context["config_compression"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_compression"] ?? null);
        echo "\" id=\"input-compression\" class=\"form-control\" />
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 1241
        echo ($context["text_security"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 1243
        echo ($context["help_secure"] ?? null);
        echo "\">";
        echo ($context["entry_secure"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 1245
        if (($context["config_secure"] ?? null)) {
            // line 1246
            echo "                      <input type=\"radio\" name=\"config_secure\" value=\"1\" checked=\"checked\" />
                      ";
            // line 1247
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 1249
            echo "                      <input type=\"radio\" name=\"config_secure\" value=\"1\" />
                      ";
            // line 1250
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 1251
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 1252
        if ( !($context["config_secure"] ?? null)) {
            // line 1253
            echo "                      <input type=\"radio\" name=\"config_secure\" value=\"0\" checked=\"checked\" />
                      ";
            // line 1254
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 1256
            echo "                      <input type=\"radio\" name=\"config_secure\" value=\"0\" />
                      ";
            // line 1257
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 1258
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 1262
        echo ($context["help_password"] ?? null);
        echo "\">";
        echo ($context["entry_password"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 1264
        if (($context["config_password"] ?? null)) {
            // line 1265
            echo "                      <input type=\"radio\" name=\"config_password\" value=\"1\" checked=\"checked\" />
                      ";
            // line 1266
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 1268
            echo "                      <input type=\"radio\" name=\"config_password\" value=\"1\" />
                      ";
            // line 1269
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 1270
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 1271
        if ( !($context["config_password"] ?? null)) {
            // line 1272
            echo "                      <input type=\"radio\" name=\"config_password\" value=\"0\" checked=\"checked\" />
                      ";
            // line 1273
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 1275
            echo "                      <input type=\"radio\" name=\"config_password\" value=\"0\" />
                      ";
            // line 1276
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 1277
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 1281
        echo ($context["help_shared"] ?? null);
        echo "\">";
        echo ($context["entry_shared"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 1283
        if (($context["config_shared"] ?? null)) {
            // line 1284
            echo "                      <input type=\"radio\" name=\"config_shared\" value=\"1\" checked=\"checked\" />
                      ";
            // line 1285
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 1287
            echo "                      <input type=\"radio\" name=\"config_shared\" value=\"1\" />
                      ";
            // line 1288
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 1289
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 1290
        if ( !($context["config_shared"] ?? null)) {
            // line 1291
            echo "                      <input type=\"radio\" name=\"config_shared\" value=\"0\" checked=\"checked\" />
                      ";
            // line 1292
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 1294
            echo "                      <input type=\"radio\" name=\"config_shared\" value=\"0\" />
                      ";
            // line 1295
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 1296
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-encryption\"><span data-toggle=\"tooltip\" title=\"";
        // line 1300
        echo ($context["help_encryption"] ?? null);
        echo "\">";
        echo ($context["entry_encryption"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <textarea name=\"config_encryption\" rows=\"5\" placeholder=\"";
        // line 1302
        echo ($context["entry_encryption"] ?? null);
        echo "\" id=\"input-encryption\" class=\"form-control\">";
        echo ($context["config_encryption"] ?? null);
        echo "</textarea>
                    ";
        // line 1303
        if (($context["error_encryption"] ?? null)) {
            // line 1304
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_encryption"] ?? null);
            echo "</div>
                    ";
        }
        // line 1305
        echo " </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 1309
        echo ($context["text_upload"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-file-max-size\"><span data-toggle=\"tooltip\" title=\"";
        // line 1311
        echo ($context["help_file_max_size"] ?? null);
        echo "\">";
        echo ($context["entry_file_max_size"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_file_max_size\" value=\"";
        // line 1313
        echo ($context["config_file_max_size"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_file_max_size"] ?? null);
        echo "\" id=\"input-file-max-size\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-file-ext-allowed\"><span data-toggle=\"tooltip\" title=\"";
        // line 1317
        echo ($context["help_file_ext_allowed"] ?? null);
        echo "\">";
        echo ($context["entry_file_ext_allowed"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <textarea name=\"config_file_ext_allowed\" rows=\"5\" placeholder=\"";
        // line 1319
        echo ($context["entry_file_ext_allowed"] ?? null);
        echo "\" id=\"input-file-ext-allowed\" class=\"form-control\">";
        echo ($context["config_file_ext_allowed"] ?? null);
        echo "</textarea>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-file-mime-allowed\"><span data-toggle=\"tooltip\" title=\"";
        // line 1323
        echo ($context["help_file_mime_allowed"] ?? null);
        echo "\">";
        echo ($context["entry_file_mime_allowed"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <textarea name=\"config_file_mime_allowed\" rows=\"5\" placeholder=\"";
        // line 1325
        echo ($context["entry_file_mime_allowed"] ?? null);
        echo "\" id=\"input-file-mime-allowed\" class=\"form-control\">";
        echo ($context["config_file_mime_allowed"] ?? null);
        echo "</textarea>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 1330
        echo ($context["text_error"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 1332
        echo ($context["entry_error_display"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 1334
        if (($context["config_error_display"] ?? null)) {
            // line 1335
            echo "                      <input type=\"radio\" name=\"config_error_display\" value=\"1\" checked=\"checked\" />
                      ";
            // line 1336
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 1338
            echo "                      <input type=\"radio\" name=\"config_error_display\" value=\"1\" />
                      ";
            // line 1339
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 1340
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 1341
        if ( !($context["config_error_display"] ?? null)) {
            // line 1342
            echo "                      <input type=\"radio\" name=\"config_error_display\" value=\"0\" checked=\"checked\" />
                      ";
            // line 1343
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 1345
            echo "                      <input type=\"radio\" name=\"config_error_display\" value=\"0\" />
                      ";
            // line 1346
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 1347
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 1351
        echo ($context["entry_error_log"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 1353
        if (($context["config_error_log"] ?? null)) {
            // line 1354
            echo "                      <input type=\"radio\" name=\"config_error_log\" value=\"1\" checked=\"checked\" />
                      ";
            // line 1355
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 1357
            echo "                      <input type=\"radio\" name=\"config_error_log\" value=\"1\" />
                      ";
            // line 1358
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 1359
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 1360
        if ( !($context["config_error_log"] ?? null)) {
            // line 1361
            echo "                      <input type=\"radio\" name=\"config_error_log\" value=\"0\" checked=\"checked\" />
                      ";
            // line 1362
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 1364
            echo "                      <input type=\"radio\" name=\"config_error_log\" value=\"0\" />
                      ";
            // line 1365
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 1366
        echo "</label>
                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-error-filename\">";
        // line 1370
        echo ($context["entry_error_filename"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_error_filename\" value=\"";
        // line 1372
        echo ($context["config_error_filename"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_error_filename"] ?? null);
        echo "\" id=\"input-error-filename\" class=\"form-control\" />
                    ";
        // line 1373
        if (($context["error_log"] ?? null)) {
            // line 1374
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_log"] ?? null);
            echo "</div>
                    ";
        }
        // line 1375
        echo " </div>
                </div>
              </fieldset>
            </div>
            <!-- custom -->
            <div class=\"tab-pane\" id=\"tab-redesocial\">
              <div class=\"row\">
                <div class=\"col-sm-12 col-md-6\">
                    <fieldset>
                    <legend>Facebook</legend>
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Exibe a Box do Facebook no rodapé da Loja.\">Habilitar</span></label>
                      <div class=\"col-sm-10\">
                        <label class=\"radio-inline\"> ";
        // line 1388
        if (($context["config_facebook"] ?? null)) {
            // line 1389
            echo "                          <input type=\"radio\" name=\"config_facebook\" value=\"1\" checked=\"checked\" />
                          ";
            // line 1390
            echo ($context["text_yes"] ?? null);
            echo "
                          ";
        } else {
            // line 1392
            echo "                          <input type=\"radio\" name=\"config_facebook\" value=\"1\" />
                          ";
            // line 1393
            echo ($context["text_yes"] ?? null);
            echo "
                          ";
        }
        // line 1394
        echo " </label>
                        <label class=\"radio-inline\"> ";
        // line 1395
        if ( !($context["config_facebook"] ?? null)) {
            // line 1396
            echo "                          <input type=\"radio\" name=\"config_facebook\" value=\"0\" checked=\"checked\" />
                          ";
            // line 1397
            echo ($context["text_no"] ?? null);
            echo "
                          ";
        } else {
            // line 1399
            echo "                          <input type=\"radio\" name=\"config_facebook\" value=\"0\" />
                          ";
            // line 1400
            echo ($context["text_no"] ?? null);
            echo "
                          ";
        }
        // line 1401
        echo " </label>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Coloque a URL completa da sua Página do Facebook.\">URL</span></label>
                      <div class=\"col-sm-10\">
                        <input type=\"text\" name=\"config_facebook_url\" value=\"";
        // line 1407
        echo ($context["config_facebook_url"] ?? null);
        echo "\" placeholder=\"https://www.facebook.com/jpwtechdigital/\" id=\"input-config-facebook-url\" class=\"form-control\" />
                        <span class=\"help-block\">É necessário colocar a URL completa de sua página. Por exemplo: <i>https://www.facebook.com/jpwtechdigital/</i></span>
                      </div>
                    </div><!-- form-group -->
                  </fieldset>
                  <fieldset>
                    <legend>Instagram</legend>
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Exibe a Widget do Instagram no rodapé da Loja.\">Habilitar</span></label>
                      <div class=\"col-sm-10\">
                        <label class=\"radio-inline\"> ";
        // line 1417
        if (($context["config_instagram"] ?? null)) {
            // line 1418
            echo "                          <input type=\"radio\" name=\"config_instagram\" value=\"1\" checked=\"checked\" />
                          ";
            // line 1419
            echo ($context["text_yes"] ?? null);
            echo "
                          ";
        } else {
            // line 1421
            echo "                          <input type=\"radio\" name=\"config_instagram\" value=\"1\" />
                          ";
            // line 1422
            echo ($context["text_yes"] ?? null);
            echo "
                          ";
        }
        // line 1423
        echo " </label>
                        <label class=\"radio-inline\"> ";
        // line 1424
        if ( !($context["config_instagram"] ?? null)) {
            // line 1425
            echo "                          <input type=\"radio\" name=\"config_instagram\" value=\"0\" checked=\"checked\" />
                          ";
            // line 1426
            echo ($context["text_no"] ?? null);
            echo "
                          ";
        } else {
            // line 1428
            echo "                          <input type=\"radio\" name=\"config_instagram\" value=\"0\" />
                          ";
            // line 1429
            echo ($context["text_no"] ?? null);
            echo "
                          ";
        }
        // line 1430
        echo " </label>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"É necessário colocar apenas o nickname da sua página. Por exemplo: jpwtechdigital\">Nickname</span></label>
                      <div class=\"col-sm-10\">
                        <input type=\"text\" name=\"config_instagram_url\" value=\"";
        // line 1436
        echo ($context["config_instagram_url"] ?? null);
        echo "\" placeholder=\"jpwtechdigital\" id=\"input-config-instagram-url\" class=\"form-control\" />
                        <span class=\"help-block\">É necessário colocar apenas o nickname da sua página. Por exemplo: <i>jpwtechdigital</i></span>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"É necessário se registrar no site SnapWidget, criando seu widget será gerado um código. Dúvida entre em contato com nosso suporte.\">Código Widget</span></label>
                      <div class=\"col-sm-10\">
                        <input type=\"text\" name=\"config_instagram_widget\" value=\"";
        // line 1443
        echo ($context["config_instagram_widget"] ?? null);
        echo "\" placeholder=\"Código Widget\" id=\"input-config-instagram-widget\" class=\"form-control\" />
                        <span class=\"help-block\">É necessário se registrar no site <a href=\"https://snapwidget.com/\" target=\"_Blank\">SnapWidget</a>, criando seu widget será gerado um código. Dúvida entre em contato com nosso suporte.</span>
                      </div>
                    </div><!-- form-group -->
                  </fieldset>                
                </div><!-- col -->
                <div class=\"col-sm-12 col-md-6 pull-righ\">
                  <fieldset>
                    <legend>Botão Flutuante</legend>
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Exibe a Widget do Instagram no rodapé da Loja.\">Habilitar</span></label>
                      <div class=\"col-sm-10\">
                        <label class=\"radio-inline\"> ";
        // line 1455
        if (($context["config_bfloat"] ?? null)) {
            // line 1456
            echo "                          <input type=\"radio\" name=\"config_bfloat\" value=\"1\" checked=\"checked\" />
                          ";
            // line 1457
            echo ($context["text_yes"] ?? null);
            echo "
                          ";
        } else {
            // line 1459
            echo "                          <input type=\"radio\" name=\"config_bfloat\" value=\"1\" />
                          ";
            // line 1460
            echo ($context["text_yes"] ?? null);
            echo "
                          ";
        }
        // line 1461
        echo " </label>
                        <label class=\"radio-inline\"> ";
        // line 1462
        if ( !($context["config_bfloat"] ?? null)) {
            // line 1463
            echo "                          <input type=\"radio\" name=\"config_bfloat\" value=\"0\" checked=\"checked\" />
                          ";
            // line 1464
            echo ($context["text_no"] ?? null);
            echo "
                          ";
        } else {
            // line 1466
            echo "                          <input type=\"radio\" name=\"config_bfloat\" value=\"0\" />
                          ";
            // line 1467
            echo ($context["text_no"] ?? null);
            echo "
                          ";
        }
        // line 1468
        echo " </label>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\">
                        <span data-toggle=\"tooltip\" title=\"Exemplo: (00) 9999-9999\">WhatsApp</span></label>
                      <div class=\"col-sm-10\">
                        <input type=\"text\" name=\"config_bfloat_whatsapp\" value=\"";
        // line 1475
        echo ($context["config_bfloat_whatsapp"] ?? null);
        echo "\" placeholder=\"(00) 9999-9999\" id=\"input-config-bfloat-whatsapp\" class=\"form-control maskPhone\" />
                        <span class=\"help-block\">Exemplo: <i>(00) 9999-9999</i></span>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\">
                        <span data-toggle=\"tooltip\" title=\"Exemplo: jpwtechdigital\">Facebook</span></label>
                      <div class=\"col-sm-10\">
                        <div class=\"input-group\">
                          <span class=\"input-group-addon\" id=\"basic-addon1\">https://facebook.com/</span>
                          <input type=\"text\" name=\"config_bfloat_facebook\" value=\"";
        // line 1485
        echo ($context["config_bfloat_facebook"] ?? null);
        echo "\" placeholder=\"suapagina\" id=\"input-config-bfloat-facebook\" class=\"form-control\" />
                        </div>                        
                        <span class=\"help-block\">Exemplo: <i>jpwtechdigital</i></span>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\">
                        <span data-toggle=\"tooltip\" title=\"Exemplo: jpwtechdigital\">Messeger</span></label>
                      <div class=\"col-sm-10\">
                        <div class=\"input-group\">
                          <span class=\"input-group-addon\" id=\"basic-addon1\">https://m.me/</span>
                          <input type=\"text\" name=\"config_bfloat_messenger\" value=\"";
        // line 1496
        echo ($context["config_bfloat_messenger"] ?? null);
        echo "\" placeholder=\"suapagina\" id=\"input-config-bfloat-messenger\" class=\"form-control\" />
                        </div>                        
                        <span class=\"help-block\">Exemplo: <i>jpwtechdigital</i></span>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\">
                        <span data-toggle=\"tooltip\" title=\"Exemplo: jpwtechdigital\">Instagram</span></label>
                      <div class=\"col-sm-10\">
                        <div class=\"input-group\">
                          <span class=\"input-group-addon\" id=\"basic-addon1\">https://instagram.com/</span>
                          <input type=\"text\" name=\"config_bfloat_instagram\" value=\"";
        // line 1507
        echo ($context["config_bfloat_instagram"] ?? null);
        echo "\" placeholder=\"suapagina\" id=\"input-config-bfloat-instagram\" class=\"form-control\" />
                        </div>                        
                        <span class=\"help-block\">Exemplo: <i>jpwtechdigital</i></span>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\">
                        <span data-toggle=\"tooltip\" title=\"Insira seu e-mail para contato\">E-mail</span></label>
                      <div class=\"col-sm-10\">
                          <input type=\"text\" name=\"config_bfloat_mail\" value=\"";
        // line 1516
        echo ($context["config_bfloat_mail"] ?? null);
        echo "\" placeholder=\"email@dominio.com.br\" id=\"input-config-bfloat-mail\" class=\"form-control\" />                
                        <span class=\"help-block\">Exemplo: <i>contato@dominio.com.br</i></span>
                      </div>
                    </div><!-- form-group -->
                  </fieldset>  
                </div><!-- col-md-5 -->
              </div><!-- row -->

            </div>
            <!-- custom -->
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type=\"text/javascript\"><!--
\$('select[name=\\'config_theme\\']').on('change', function() {
\t\$.ajax({
\t\turl: 'index.php?route=setting/setting/theme&user_token=";
        // line 1534
        echo ($context["user_token"] ?? null);
        echo "&theme=' + this.value,
\t\tdataType: 'html',
\t\tbeforeSend: function() {
\t\t\t\$('select[name=\\'config_theme\\']').prop('disabled', true);
\t\t},
\t\tcomplete: function() {
\t\t\t\$('select[name=\\'config_theme\\']').prop('disabled', false);
\t\t},
\t\tsuccess: function(html) {
\t\t\t\$('#theme').attr('src', html);
\t\t},
\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t}
\t});
});

\$('select[name=\\'config_theme\\']').trigger('change');
//--></script> 
  <script type=\"text/javascript\"><!--
\$('select[name=\\'config_country_id\\']').on('change', function() {
\t\$.ajax({
\t\turl: 'index.php?route=localisation/country/country&user_token=";
        // line 1556
        echo ($context["user_token"] ?? null);
        echo "&country_id=' + this.value,
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('select[name=\\'config_country_id\\']').prop('disabled', true);
\t\t},
\t\tcomplete: function() {
\t\t\t\$('select[name=\\'config_country_id\\']').prop('disabled', false);
\t\t},
\t\tsuccess: function(json) {
\t\t\thtml = '<option value=\"\">";
        // line 1565
        echo ($context["text_select"] ?? null);
        echo "</option>';

\t\t\tif (json['zone'] && json['zone'] != '') {
\t\t\t\tfor (i = 0; i < json['zone'].length; i++) {
          \t\t\thtml += '<option value=\"' + json['zone'][i]['zone_id'] + '\"';

\t\t\t\t\tif (json['zone'][i]['zone_id'] == '";
        // line 1571
        echo ($context["config_zone_id"] ?? null);
        echo "') {
            \t\t\thtml += ' selected=\"selected\"';
\t\t\t\t\t}

\t\t\t\t\thtml += '>' + json['zone'][i]['name'] + '</option>';
\t\t\t\t}
\t\t\t} else {
\t\t\t\thtml += '<option value=\"0\" selected=\"selected\">";
        // line 1578
        echo ($context["text_none"] ?? null);
        echo "</option>';
\t\t\t}

\t\t\t\$('select[name=\\'config_zone_id\\']').html(html);
\t\t\t
\t\t\t\$('#button-save').prop('disabled', false);
\t\t},
\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t}
\t});
});

\$('select[name=\\'config_country_id\\']').trigger('change');
//--></script></div>

<script>
  
\$('input[name=\"config_meta_title\"]').on('keyup', function() {
  \$('input[name=\"config_name\"]').val(\$(this).val());
});

\$('input[name=\"input-email\"]').on('keyup', function() {
  \$('input[name=\"config_bfloat_mail\"]').val(\$(this).val());
});

</script>
";
        // line 1605
        echo ($context["footer"] ?? null);
        echo " ";
    }

    public function getTemplateName()
    {
        return "setting/setting.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  4025 => 1605,  3995 => 1578,  3985 => 1571,  3976 => 1565,  3964 => 1556,  3939 => 1534,  3918 => 1516,  3906 => 1507,  3892 => 1496,  3878 => 1485,  3865 => 1475,  3856 => 1468,  3851 => 1467,  3848 => 1466,  3843 => 1464,  3840 => 1463,  3838 => 1462,  3835 => 1461,  3830 => 1460,  3827 => 1459,  3822 => 1457,  3819 => 1456,  3817 => 1455,  3802 => 1443,  3792 => 1436,  3784 => 1430,  3779 => 1429,  3776 => 1428,  3771 => 1426,  3768 => 1425,  3766 => 1424,  3763 => 1423,  3758 => 1422,  3755 => 1421,  3750 => 1419,  3747 => 1418,  3745 => 1417,  3732 => 1407,  3724 => 1401,  3719 => 1400,  3716 => 1399,  3711 => 1397,  3708 => 1396,  3706 => 1395,  3703 => 1394,  3698 => 1393,  3695 => 1392,  3690 => 1390,  3687 => 1389,  3685 => 1388,  3670 => 1375,  3664 => 1374,  3662 => 1373,  3656 => 1372,  3651 => 1370,  3645 => 1366,  3640 => 1365,  3637 => 1364,  3632 => 1362,  3629 => 1361,  3627 => 1360,  3624 => 1359,  3619 => 1358,  3616 => 1357,  3611 => 1355,  3608 => 1354,  3606 => 1353,  3601 => 1351,  3595 => 1347,  3590 => 1346,  3587 => 1345,  3582 => 1343,  3579 => 1342,  3577 => 1341,  3574 => 1340,  3569 => 1339,  3566 => 1338,  3561 => 1336,  3558 => 1335,  3556 => 1334,  3551 => 1332,  3546 => 1330,  3536 => 1325,  3529 => 1323,  3520 => 1319,  3513 => 1317,  3504 => 1313,  3497 => 1311,  3492 => 1309,  3486 => 1305,  3480 => 1304,  3478 => 1303,  3472 => 1302,  3465 => 1300,  3459 => 1296,  3454 => 1295,  3451 => 1294,  3446 => 1292,  3443 => 1291,  3441 => 1290,  3438 => 1289,  3433 => 1288,  3430 => 1287,  3425 => 1285,  3422 => 1284,  3420 => 1283,  3413 => 1281,  3407 => 1277,  3402 => 1276,  3399 => 1275,  3394 => 1273,  3391 => 1272,  3389 => 1271,  3386 => 1270,  3381 => 1269,  3378 => 1268,  3373 => 1266,  3370 => 1265,  3368 => 1264,  3361 => 1262,  3355 => 1258,  3350 => 1257,  3347 => 1256,  3342 => 1254,  3339 => 1253,  3337 => 1252,  3334 => 1251,  3329 => 1250,  3326 => 1249,  3321 => 1247,  3318 => 1246,  3316 => 1245,  3309 => 1243,  3304 => 1241,  3294 => 1236,  3287 => 1234,  3278 => 1230,  3271 => 1228,  3265 => 1224,  3260 => 1223,  3257 => 1222,  3252 => 1220,  3249 => 1219,  3247 => 1218,  3244 => 1217,  3239 => 1216,  3236 => 1215,  3231 => 1213,  3228 => 1212,  3226 => 1211,  3219 => 1209,  3213 => 1205,  3208 => 1204,  3205 => 1203,  3200 => 1201,  3197 => 1200,  3195 => 1199,  3192 => 1198,  3187 => 1197,  3184 => 1196,  3179 => 1194,  3176 => 1193,  3174 => 1192,  3167 => 1190,  3162 => 1188,  3150 => 1181,  3143 => 1179,  3137 => 1175,  3129 => 1173,  3124 => 1172,  3119 => 1171,  3114 => 1169,  3109 => 1168,  3107 => 1167,  3104 => 1166,  3100 => 1165,  3093 => 1163,  3088 => 1161,  3078 => 1156,  3073 => 1154,  3064 => 1150,  3059 => 1148,  3050 => 1144,  3043 => 1142,  3034 => 1138,  3029 => 1136,  3020 => 1132,  3013 => 1130,  3004 => 1126,  2997 => 1124,  2990 => 1119,  2984 => 1116,  2981 => 1115,  2975 => 1112,  2972 => 1111,  2969 => 1110,  2963 => 1107,  2960 => 1106,  2954 => 1103,  2951 => 1102,  2949 => 1101,  2940 => 1097,  2935 => 1095,  2926 => 1089,  2920 => 1088,  2914 => 1087,  2907 => 1083,  2901 => 1082,  2897 => 1081,  2888 => 1074,  2880 => 1072,  2875 => 1071,  2870 => 1070,  2865 => 1068,  2860 => 1067,  2858 => 1066,  2855 => 1065,  2851 => 1064,  2846 => 1062,  2839 => 1057,  2833 => 1056,  2825 => 1053,  2822 => 1052,  2814 => 1049,  2811 => 1048,  2808 => 1047,  2804 => 1046,  2799 => 1044,  2791 => 1041,  2786 => 1039,  2778 => 1033,  2772 => 1032,  2764 => 1029,  2761 => 1028,  2753 => 1025,  2750 => 1024,  2747 => 1023,  2743 => 1022,  2734 => 1018,  2727 => 1013,  2721 => 1012,  2713 => 1009,  2710 => 1008,  2702 => 1005,  2699 => 1004,  2696 => 1003,  2692 => 1002,  2687 => 1000,  2679 => 997,  2674 => 995,  2666 => 989,  2660 => 988,  2652 => 985,  2649 => 984,  2641 => 981,  2638 => 980,  2635 => 979,  2631 => 978,  2626 => 976,  2618 => 973,  2609 => 969,  2602 => 967,  2596 => 963,  2591 => 962,  2588 => 961,  2583 => 959,  2580 => 958,  2578 => 957,  2575 => 956,  2570 => 955,  2567 => 954,  2562 => 952,  2559 => 951,  2557 => 950,  2550 => 948,  2544 => 944,  2539 => 943,  2536 => 942,  2531 => 940,  2528 => 939,  2526 => 938,  2523 => 937,  2518 => 936,  2515 => 935,  2510 => 933,  2507 => 932,  2505 => 931,  2498 => 929,  2491 => 924,  2485 => 923,  2477 => 920,  2474 => 919,  2466 => 916,  2463 => 915,  2460 => 914,  2456 => 913,  2449 => 909,  2444 => 907,  2437 => 902,  2432 => 901,  2429 => 900,  2424 => 898,  2421 => 897,  2419 => 896,  2416 => 895,  2411 => 894,  2408 => 893,  2403 => 891,  2400 => 890,  2398 => 889,  2391 => 887,  2385 => 883,  2380 => 882,  2377 => 881,  2372 => 879,  2369 => 878,  2367 => 877,  2364 => 876,  2359 => 875,  2356 => 874,  2351 => 872,  2348 => 871,  2346 => 870,  2339 => 868,  2333 => 864,  2328 => 863,  2325 => 862,  2320 => 860,  2317 => 859,  2315 => 858,  2312 => 857,  2307 => 856,  2304 => 855,  2299 => 853,  2296 => 852,  2294 => 851,  2287 => 849,  2282 => 847,  2274 => 841,  2268 => 840,  2260 => 837,  2257 => 836,  2249 => 833,  2246 => 832,  2243 => 831,  2239 => 830,  2234 => 828,  2226 => 825,  2219 => 820,  2213 => 819,  2205 => 816,  2202 => 815,  2194 => 812,  2191 => 811,  2188 => 810,  2184 => 809,  2175 => 805,  2170 => 802,  2164 => 801,  2162 => 800,  2159 => 799,  2151 => 797,  2146 => 796,  2141 => 795,  2136 => 793,  2131 => 792,  2129 => 791,  2126 => 790,  2122 => 789,  2115 => 787,  2110 => 784,  2104 => 783,  2102 => 782,  2099 => 781,  2091 => 779,  2086 => 778,  2081 => 777,  2076 => 775,  2071 => 774,  2069 => 773,  2066 => 772,  2062 => 771,  2055 => 769,  2048 => 764,  2042 => 763,  2034 => 760,  2031 => 759,  2023 => 756,  2020 => 755,  2017 => 754,  2013 => 753,  2004 => 749,  1997 => 744,  1991 => 743,  1983 => 740,  1980 => 739,  1972 => 736,  1969 => 735,  1966 => 734,  1962 => 733,  1957 => 731,  1949 => 728,  1943 => 724,  1938 => 723,  1935 => 722,  1930 => 720,  1927 => 719,  1925 => 718,  1922 => 717,  1917 => 716,  1914 => 715,  1909 => 713,  1906 => 712,  1904 => 711,  1897 => 709,  1891 => 705,  1886 => 704,  1883 => 703,  1878 => 701,  1875 => 700,  1873 => 699,  1870 => 698,  1865 => 697,  1862 => 696,  1857 => 694,  1854 => 693,  1852 => 692,  1845 => 690,  1836 => 686,  1829 => 684,  1824 => 682,  1816 => 676,  1810 => 675,  1802 => 672,  1799 => 671,  1791 => 668,  1788 => 667,  1785 => 666,  1781 => 665,  1776 => 663,  1768 => 660,  1763 => 657,  1757 => 656,  1755 => 655,  1749 => 654,  1742 => 652,  1736 => 648,  1731 => 647,  1728 => 646,  1723 => 644,  1720 => 643,  1718 => 642,  1715 => 641,  1710 => 640,  1707 => 639,  1702 => 637,  1699 => 636,  1697 => 635,  1690 => 633,  1685 => 630,  1679 => 629,  1676 => 628,  1668 => 625,  1663 => 624,  1658 => 623,  1653 => 621,  1648 => 620,  1646 => 619,  1643 => 618,  1639 => 617,  1633 => 616,  1626 => 611,  1620 => 610,  1612 => 607,  1609 => 606,  1601 => 603,  1598 => 602,  1595 => 601,  1591 => 600,  1582 => 596,  1576 => 592,  1571 => 591,  1568 => 590,  1563 => 588,  1560 => 587,  1558 => 586,  1555 => 585,  1550 => 584,  1547 => 583,  1542 => 581,  1539 => 580,  1537 => 579,  1532 => 577,  1526 => 573,  1521 => 572,  1518 => 571,  1513 => 569,  1510 => 568,  1508 => 567,  1505 => 566,  1500 => 565,  1497 => 564,  1492 => 562,  1489 => 561,  1487 => 560,  1480 => 558,  1474 => 554,  1469 => 553,  1466 => 552,  1461 => 550,  1458 => 549,  1456 => 548,  1453 => 547,  1448 => 546,  1445 => 545,  1440 => 543,  1437 => 542,  1435 => 541,  1428 => 539,  1423 => 537,  1415 => 531,  1409 => 528,  1406 => 527,  1400 => 524,  1397 => 523,  1394 => 522,  1388 => 519,  1385 => 518,  1379 => 515,  1376 => 514,  1374 => 513,  1369 => 511,  1361 => 508,  1354 => 503,  1348 => 500,  1345 => 499,  1339 => 496,  1336 => 495,  1333 => 494,  1327 => 491,  1324 => 490,  1318 => 487,  1315 => 486,  1313 => 485,  1308 => 483,  1300 => 480,  1294 => 476,  1289 => 475,  1286 => 474,  1281 => 472,  1278 => 471,  1276 => 470,  1273 => 469,  1268 => 468,  1265 => 467,  1260 => 465,  1257 => 464,  1255 => 463,  1250 => 461,  1245 => 459,  1239 => 455,  1233 => 454,  1231 => 453,  1225 => 452,  1218 => 450,  1213 => 447,  1207 => 446,  1205 => 445,  1199 => 444,  1192 => 442,  1187 => 440,  1180 => 435,  1175 => 434,  1172 => 433,  1167 => 431,  1164 => 430,  1162 => 429,  1159 => 428,  1154 => 427,  1151 => 426,  1146 => 424,  1143 => 423,  1141 => 422,  1134 => 420,  1128 => 416,  1123 => 415,  1120 => 414,  1115 => 412,  1112 => 411,  1110 => 410,  1107 => 409,  1102 => 408,  1099 => 407,  1094 => 405,  1091 => 404,  1089 => 403,  1082 => 401,  1077 => 399,  1071 => 395,  1065 => 394,  1063 => 393,  1057 => 392,  1050 => 390,  1044 => 386,  1039 => 385,  1036 => 384,  1031 => 382,  1028 => 381,  1026 => 380,  1023 => 379,  1018 => 378,  1015 => 377,  1010 => 375,  1007 => 374,  1005 => 373,  998 => 371,  993 => 369,  986 => 364,  980 => 363,  978 => 362,  972 => 361,  965 => 359,  960 => 357,  951 => 350,  945 => 349,  937 => 346,  934 => 345,  926 => 342,  923 => 341,  920 => 340,  916 => 339,  909 => 335,  902 => 330,  896 => 329,  888 => 326,  885 => 325,  877 => 322,  874 => 321,  871 => 320,  867 => 319,  860 => 315,  854 => 311,  849 => 310,  846 => 309,  841 => 307,  838 => 306,  836 => 305,  833 => 304,  828 => 303,  825 => 302,  820 => 300,  817 => 299,  815 => 298,  808 => 296,  801 => 291,  795 => 290,  787 => 287,  784 => 286,  776 => 283,  773 => 282,  770 => 281,  766 => 280,  757 => 276,  750 => 271,  744 => 270,  736 => 267,  733 => 266,  725 => 263,  722 => 262,  719 => 261,  715 => 260,  708 => 256,  701 => 251,  695 => 250,  687 => 247,  684 => 246,  676 => 243,  673 => 242,  670 => 241,  666 => 240,  659 => 236,  653 => 232,  647 => 231,  639 => 229,  631 => 227,  628 => 226,  624 => 225,  618 => 222,  608 => 215,  601 => 210,  595 => 209,  587 => 206,  584 => 205,  576 => 202,  573 => 201,  570 => 200,  566 => 199,  559 => 195,  554 => 192,  549 => 190,  541 => 188,  536 => 187,  531 => 186,  526 => 184,  521 => 183,  519 => 182,  516 => 181,  512 => 180,  506 => 179,  503 => 178,  501 => 177,  493 => 174,  486 => 172,  477 => 168,  470 => 166,  463 => 162,  457 => 161,  453 => 160,  444 => 156,  439 => 154,  434 => 151,  428 => 150,  426 => 149,  420 => 148,  415 => 146,  410 => 143,  404 => 142,  402 => 141,  396 => 140,  391 => 138,  382 => 134,  375 => 132,  370 => 129,  364 => 128,  362 => 127,  356 => 126,  351 => 124,  346 => 121,  340 => 120,  338 => 119,  332 => 118,  327 => 116,  322 => 113,  316 => 112,  314 => 111,  308 => 110,  303 => 108,  294 => 101,  288 => 100,  280 => 97,  277 => 96,  269 => 93,  266 => 92,  263 => 91,  259 => 90,  252 => 86,  244 => 80,  238 => 79,  230 => 76,  227 => 75,  219 => 72,  216 => 71,  213 => 70,  209 => 69,  202 => 65,  193 => 61,  188 => 59,  179 => 55,  174 => 53,  169 => 50,  163 => 49,  161 => 48,  155 => 47,  150 => 45,  142 => 40,  138 => 39,  134 => 38,  129 => 36,  125 => 35,  121 => 34,  117 => 33,  112 => 31,  106 => 28,  102 => 26,  94 => 22,  91 => 21,  83 => 17,  81 => 16,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "setting/setting.twig", "");
    }
}
