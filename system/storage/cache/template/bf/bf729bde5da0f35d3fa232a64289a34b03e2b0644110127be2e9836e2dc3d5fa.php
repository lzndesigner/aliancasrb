<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* sky_blue/template/common/header.twig */
class __TwigTemplate_f09342e34fc1dcaf6568329fd1225901b5871deaaf0b82448049139cee7b2be8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir=\"";
        // line 3
        echo ($context["direction"] ?? null);
        echo "\" lang=\"";
        echo ($context["lang"] ?? null);
        echo "\" class=\"ie8\"><![endif]-->
<!--[if IE 9 ]><html dir=\"";
        // line 4
        echo ($context["direction"] ?? null);
        echo "\" lang=\"";
        echo ($context["lang"] ?? null);
        echo "\" class=\"ie9\"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir=\"";
        // line 6
        echo ($context["direction"] ?? null);
        echo "\" lang=\"";
        echo ($context["lang"] ?? null);
        echo "\">
<!--<![endif]-->

<head>
  <meta charset=\"UTF-8\" />
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
  <title>";
        // line 13
        echo ($context["title"] ?? null);
        echo "</title>
  <base href=\"";
        // line 14
        echo ($context["base"] ?? null);
        echo "\" />
  <!-- // add url image product to head -->
  <meta name=\"robots\" content=\"follow\" />
  <meta name=\"googlebot\" content=\"index, follow, all\" />
  <meta name=\"language\" content=\"pt-br\" />
  <meta name=\"revisit-after\" content=\"3 days\">
  <meta name=\"rating\" content=\"general\" />
  <meta property=\"og:locale\" content=\"pt_BR\" />
  <meta property=\"og:type\" content=\"website\" />
  ";
        // line 23
        if (($context["fbMetas"] ?? null)) {
            // line 24
            echo "  ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["fbMetas"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["fbMeta"]) {
                // line 25
                echo "  <meta property=\"og:image:url\" content=\"";
                echo ($context["base"] ?? null);
                echo "image/";
                echo twig_get_attribute($this->env, $this->source, $context["fbMeta"], "content", [], "any", false, false, false, 25);
                echo "\" />
  <meta property=\"og:image:type\" content=\"image/jpeg\" />
  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fbMeta'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "  ";
        } else {
            // line 29
            echo "  <meta property=\"og:image:url\" content=\"";
            echo ($context["base"] ?? null);
            echo "_galerias/facebook.jpg\" />
  <meta property=\"og:image:type\" content=\"image/jpeg\" />
  ";
        }
        // line 32
        echo "
  ";
        // line 33
        if (($context["description"] ?? null)) {
            // line 34
            echo "  <meta name=\"description\" content=\"";
            echo ($context["description"] ?? null);
            echo "\" />
  <meta property=\"og:description\" content=\"";
            // line 35
            echo ($context["description"] ?? null);
            echo "\" />
  ";
        }
        // line 37
        echo "  <!-- // end: add url image product to head -->
  ";
        // line 38
        if (($context["keywords"] ?? null)) {
            // line 39
            echo "  <meta name=\"keywords\" content=\"";
            echo ($context["keywords"] ?? null);
            echo "\" />
  ";
        }
        // line 41
        echo "  <script src=\"catalog/view/javascript/jquery/jquery-2.1.1.min.js\" type=\"text/javascript\"></script>
  <script src=\"https://kit.fontawesome.com/04571ab3d2.js\" crossorigin=\"anonymous\"></script>
  <link href=\"catalog/view/javascript/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\" />
  <script src=\"catalog/view/javascript/bootstrap/js/bootstrap.min.js\" type=\"text/javascript\"></script>
  <link href=\"//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700\" rel=\"stylesheet\" type=\"text/css\" />
  <link href=\"//fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap\" rel=\"stylesheet\">
  <link href=\"//fonts.googleapis.com/css2?family=Play:wght@400;700&display=swap\" rel=\"stylesheet\">
  <link href=\"catalog/view/theme/";
        // line 48
        echo ($context["theme"] ?? null);
        echo "/stylesheet/stylesheet.css?1\" rel=\"stylesheet\">
  <link href=\"catalog/view/theme/";
        // line 49
        echo ($context["theme"] ?? null);
        echo "/stylesheet/custom.css?1";
        echo twig_random($this->env);
        echo "\" rel=\"stylesheet\">
  <link href=\"catalog/view/theme/";
        // line 50
        echo ($context["theme"] ?? null);
        echo "/stylesheet/responsive.css?1";
        echo twig_random($this->env);
        echo "\" rel=\"stylesheet\">
  ";
        // line 51
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["styles"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 52
            echo "  <link href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "href", [], "any", false, false, false, 52);
            echo "\" type=\"text/css\" rel=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "rel", [], "any", false, false, false, 52);
            echo "\" media=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "media", [], "any", false, false, false, 52);
            echo "\" />
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "  ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["scripts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 55
            echo "  <script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "  <script src=\"catalog/view/javascript/common.js?1\" type=\"text/javascript\"></script>
  ";
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["links"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 59
            echo "  <link href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["link"], "href", [], "any", false, false, false, 59);
            echo "\" rel=\"";
            echo twig_get_attribute($this->env, $this->source, $context["link"], "rel", [], "any", false, false, false, 59);
            echo "\" />
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "  ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["analytics"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["analytic"]) {
            // line 62
            echo "  ";
            echo $context["analytic"];
            echo "
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['analytic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "
  <script src=\"catalog/view/javascript/jquery/jquery.mask.js\" type=\"text/javascript\"></script>
  <script type=\"text/javascript\">
    \$(document).ready(function () {
      // \$('input[name=\"postcode\"]').mask('00000-000');
      \$(\"input[name='postcode']\").mask('00000-000', { reverse: true, placeholder: \"00000-000\" });
      \$('input[name=\"custom_field[account][1]\"]').mask('000.000.000-00', { reverse: true });
      \$('input[name=\"telephone\"]').mask(\"(99) 99999-9999\");
      \$('input[name=\"telephone\"]').on(\"blur\", function () {
        var last = \$(this).val().substr(\$(this).val().indexOf(\"-\") + 1);

        if (last.length == 3) {
          var move = \$(this).val().substr(\$(this).val().indexOf(\"-\") - 1, 1);
          var lastfour = move + last;
          var first = \$(this).val().substr(0, 9);

          \$(this).val(first + '-' + lastfour);
        }
      });
    });
  </script>

</head>

<body class=\"";
        // line 88
        echo ($context["class"] ?? null);
        echo "\">
  <div class=\"header-desktop\">
    <div class=\"top-header\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-sm-4\">
            <div class=\"linkHeaders\">
              <ul class=\"d-flex\">
                <li id=\"center-help\" class=\"flex-fill\">
                  <a href=\"javascript:;\">
                    <div class=\"content\">
                      <p>Central de <b>Atendimento</b> <i class=\"fa fa-angle-down\"></i></p>
                    </div>
                  </a>
                  <div class=\"sub-menu\">
                    <ul>
                      <li>
                        <div><i class=\"fab fa-whatsapp\"></i> <b>Telefone</b></div>
                        <span><a href=\"https://web.whatsapp.com/send?phone=+55";
        // line 106
        echo ($context["telephoneLink"] ?? null);
        echo "\">";
        echo ($context["telephone"] ?? null);
        // line 107
        echo "</a></span>
                      </li>
                      ";
        // line 109
        if (($context["cellphone"] ?? null)) {
            // line 110
            echo "                      <li>
                        <div><i class=\"fa fa-phone\"></i> <b>Celular</b></div>
                        <span><a href=\"https://web.whatsapp.com/send?phone=+55";
            // line 112
            echo ($context["cellphoneLink"] ?? null);
            echo "\">";
            echo ($context["cellphone"] ?? null);
            // line 113
            echo "</a></span>
                      </li>
                      ";
        }
        // line 116
        echo "                      <li class=\"e-mail\">
                        <div><i class=\"fa fa-comment\"></i> <b>Atendimento E-mail</b></div>
                        <span><a href=\"mailto:";
        // line 118
        echo ($context["email_admin"] ?? null);
        echo "\">";
        echo ($context["email_admin"] ?? null);
        echo "</a></span>
                      </li>
                      ";
        // line 120
        if (($context["hour_open"] ?? null)) {
            // line 121
            echo "                      <li>
                        <div><i class=\"fas fa-clock\"></i> <b>Horário de Atendimento</b></div>
                        <p>";
            // line 123
            echo twig_replace_filter(($context["hour_open"] ?? null), ["/space/" => "<br>"]);
            echo "</p>
                      </li>
                      ";
        }
        // line 126
        echo "                    </ul>
                  </div>
                </li><!-- end central de atendimento -->
            </div>
          </div><!-- col-4 -->
          ";
        // line 131
        if (($context["bfloat_status"] ?? null)) {
            // line 132
            echo "          <div class=\"col-sm-4 pull-right text-right\">
            <div class=\"header-sociais\">
              <ul>
                ";
            // line 135
            if (($context["bfloat_whatsapp"] ?? null)) {
                // line 136
                echo "                <li><a href=\"https://web.whatsapp.com/send?phone=+55";
                echo ($context["bfloat_whatsapp"] ?? null);
                echo "\" target=\"_Blank\"
                    class=\"whatsapp\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"WhatsApp\"><i
                      class=\"fab fa-whatsapp\"></i></a></li>
                ";
            }
            // line 140
            echo "  
                ";
            // line 141
            if (($context["bfloat_facebook"] ?? null)) {
                // line 142
                echo "                <li><a href=\"https://facebook.com/";
                echo ($context["bfloat_facebook"] ?? null);
                echo "\" target=\"_Blank\" class=\"facebook\"
                    data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Facebook\"><i class=\"fab fa-facebook\"></i></a></li>
                ";
            }
            // line 145
            echo "  
                ";
            // line 146
            if (($context["bfloat_messenger"] ?? null)) {
                // line 147
                echo "                <li><a href=\"https://m.me/";
                echo ($context["bfloat_messenger"] ?? null);
                echo "\" target=\"_Blank\" class=\"facebook-messenger\"
                    data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Messenger\"><i
                      class=\"fab fa-facebook-messenger\"></i></a></li>
                ";
            }
            // line 151
            echo "  
                ";
            // line 152
            if (($context["bfloat_instagram"] ?? null)) {
                // line 153
                echo "                <li><a href=\"https://instagram.com/";
                echo ($context["bfloat_instagram"] ?? null);
                echo "\" target=\"_Blank\" class=\"instagram\"
                    data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Instagram\"><i class=\"fab fa-instagram\"></i></a>
                </li>
                ";
            }
            // line 157
            echo "  
                ";
            // line 158
            if (($context["bfloat_mail"] ?? null)) {
                // line 159
                echo "                <li><a href=\"mailto:";
                echo ($context["bfloat_mail"] ?? null);
                echo "\" class=\"comment\" data-toggle=\"tooltip\" data-placement=\"bottom\"
                    title=\"E-mail\"><i class=\"fa fa-comment\"></i></a></li>
                ";
            }
            // line 162
            echo "              </ul>
            </div>
          </div>
          ";
        }
        // line 166
        echo "        </div>
      </div>
    </div>
    <header id=\"headerDesktop\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-sm-3\">
            <div id=\"logo\">";
        // line 173
        if (($context["logo"] ?? null)) {
            echo "<a href=\"";
            echo ($context["home"] ?? null);
            echo "\"><img src=\"";
            echo ($context["logo"] ?? null);
            echo "\" title=\"";
            echo ($context["name"] ?? null);
            echo "\" alt=\"";
            echo ($context["name"] ?? null);
            echo "\"
                  class=\"img-responsive\" /></a>";
        } else {
            // line 175
            echo "              <h1><a href=\"";
            echo ($context["home"] ?? null);
            echo "\">";
            echo ($context["name"] ?? null);
            echo "</a></h1>
              ";
        }
        // line 177
        echo "            </div>
          </div>
          <div class=\"col-sm-9\">
            <div class=\"row secondMenu\">
              <div class=\"col-sm-8\">";
        // line 181
        echo ($context["search"] ?? null);
        echo "</div><!-- col-5 -->
              <div class=\"col-sm-4\">
                <div class=\"linkHeaders\">
                  <ul class=\"d-flex\">
                    <li id=\"center-user\" class=\"flex-fill\">
                      <a href=\"";
        // line 186
        echo ($context["account"] ?? null);
        echo "\">
                        <div class=\"icon\">
                          <i class=\"fa fa-user\"></i>
                        </div>
                        <div class=\"content\">
                          ";
        // line 191
        if ( !($context["logged"] ?? null)) {
            // line 192
            echo "                          <p>Entrar ou <br><b>Cadastrar</b> <i class=\"fa fa-angle-down\"></i></p>
                          ";
        } else {
            // line 194
            echo "                          <p>Bem-vindo, <br><b>";
            echo (((twig_length_filter($this->env, ($context["name_user"] ?? null)) > 10)) ? ((twig_slice($this->env, ($context["name_user"] ?? null), 0, 10) . ".")) : (($context["name_user"] ?? null)));
            // line 195
            echo "</b> <i class=\"fa fa-angle-down\"></i></p>
                          ";
        }
        // line 197
        echo "                        </div>
                      </a>
                      <div class=\"sub-menu\">
                        <div class=\"d-flex\">
                          <div class=\"col-sm-6 flex-fill\">
                            <div class=\"text-center\">
                              <div class=\"icon\"><i class=\"fa fa-user\"></i></div>
                              <div class=\"content\">
                                ";
        // line 205
        if (($context["logged"] ?? null)) {
            // line 206
            echo "                                <h4>Olá, <br> <b>";
            echo ($context["name_user"] ?? null);
            echo "</b></h4>
                                ";
        } else {
            // line 208
            echo "                                <h4>Olá, <br> <b>Bem-vindo!</b></h4>
                                <a href=\"";
            // line 209
            echo ($context["account"] ?? null);
            echo "\" class=\"btn btn-sm btn-primary\">Minha Conta</a>
                                ";
        }
        // line 211
        echo "                              </div>
                            </div>
                          </div><!-- flex-fill -->
                          <div class=\"col-sm-6 flex-fill\">
                            <ul>
                              ";
        // line 216
        if (($context["logged"] ?? null)) {
            // line 217
            echo "                              <li><a href=\"";
            echo ($context["account"] ?? null);
            echo "\">";
            echo ($context["text_account"] ?? null);
            echo "</a></li>
                              <li><a href=\"";
            // line 218
            echo ($context["order"] ?? null);
            echo "\">";
            echo ($context["text_order"] ?? null);
            echo "</a></li>
                              <li><a href=\"";
            // line 219
            echo ($context["transaction"] ?? null);
            echo "\">";
            echo ($context["text_transaction"] ?? null);
            echo "</a></li>
                              <li><a href=\"";
            // line 220
            echo ($context["logout"] ?? null);
            echo "\">";
            echo ($context["text_logout"] ?? null);
            echo "</a></li>
                              ";
        } else {
            // line 222
            echo "                              <li><a href=\"";
            echo ($context["login"] ?? null);
            echo "\"><i class=\"fa fa-user\"></i> ";
            echo ($context["text_login"] ?? null);
            echo "</a></li>
                              <li><a href=\"";
            // line 223
            echo ($context["register"] ?? null);
            echo "\"><i class=\"fa fa-plus\"></i> ";
            echo ($context["text_register"] ?? null);
            echo "</a></li>
                              ";
        }
        // line 225
        echo "                              <li><a href=\"";
        echo ($context["wishlist"] ?? null);
        echo "\" id=\"wishlist-total\" title=\"";
        echo ($context["text_wishlist"] ?? null);
        echo "\"><i
                                    class=\"fa fa-heart\"></i> <span>";
        // line 226
        echo ($context["text_wishlist"] ?? null);
        echo "</span></a></li>
                              <li class=\"divider\"></li>
                              <li><i class=\"fa fa-truck\"></i> <b>Rastrear Pedido</b>
                                <form method=\"post\" action=\"https://www.linkcorreios.com.br/?\" id=\"rastrearobjeto\"
                                  name=\"rastrearobjeto\">
                                  <input type=\"text\" name=\"codrastreio\" id=\"codrastreio\" value=\"\"
                                    placeholder=\"OJ614509055BR\" class=\"form-control\">
                                  <button type=\"button\" class=\"btn btn-primary\"><i class=\"fa fa-search\"></i></button>
                                </form>
                              </li>
                            </ul>

                          </div><!-- flex-fill -->
                        </div>
                      </div>
                    </li><!-- end central do usuário -->
                    ";
        // line 242
        echo ($context["cart"] ?? null);
        echo "
                  </ul>
                </div><!-- linksHeaders -->
              </div><!-- col-7 -->
            </div><!-- row -->
          </div><!-- col -->
        </div><!-- row -->
      </div><!-- container -->
    </header>
    ";
        // line 251
        echo ($context["menu"] ?? null);
        echo "
  </div><!-- header-desktop -->

  <div class=\"header-mobile\">
    <nav id=\"top\" class=\"mobile-top\">
      <div class=\"container\">
        <div class=\"d-flex\">
          <div class=\"\">
            <a href=\"javascript:history.go(-1);\" class=\"btn ml-3\"><i class=\"fa fa-angle-left\"></i></a>
          </div>
          <div class=\"\">
            <a href=\"index.php?route=account/account\" class=\"btn\"><i class=\"fa fa-user\"></i></a>
          </div>
          <div class=\"\">
            ";
        // line 265
        echo ($context["cart"] ?? null);
        echo "
          </div>
          <div class=\"\">
            <a href=\"javascript:;\" class=\"btn btnToggleSearch\"><i class=\"fa fa-search\"></i></a>
          </div>
          <div class=\"ml-auto\">
            <a href=\"javascript:;\" class=\"btn btnToggleMenu mr-3\"><i class=\"fa fa-bars\"></i></a>
          </div>
        </div>
      </div>
    </nav>
    <header id=\"headerMobile\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-sm-12\">
            <div id=\"logo\">";
        // line 280
        if (($context["logo"] ?? null)) {
            echo "<a href=\"";
            echo ($context["home"] ?? null);
            echo "\"><img src=\"";
            echo ($context["logo"] ?? null);
            echo "\" title=\"";
            echo ($context["name"] ?? null);
            echo "\" alt=\"";
            echo ($context["name"] ?? null);
            echo "\"
                  class=\"img-responsive\" /></a>";
        } else {
            // line 282
            echo "              <h1><a href=\"";
            echo ($context["home"] ?? null);
            echo "\">";
            echo ($context["name"] ?? null);
            echo "</a></h1>
              ";
        }
        // line 284
        echo "            </div>
          </div>
        </div>
      </div>
      <div class=\"container\">
        <div class=\"box-search hide\">
          ";
        // line 290
        echo ($context["search"] ?? null);
        echo "
        </div>
      </div>
    </header>

    <div class=\"menu-mobile\">
      <a href=\"javascript:;\" class=\"btnCloseToggleMenu\"><i class=\"fa fa-times\"></i></a>
      ";
        // line 297
        echo ($context["menu"] ?? null);
        echo "
    </div>
  </div><!-- header-mobile -->


  ";
        // line 302
        if (($context["bfloat_status"] ?? null)) {
            // line 303
            echo "  <div class=\"headerbuttonsfloat\">
    <ul>
      <li><a href=\"javascript:;\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Entre em Contato\"><i
            class=\"fa fa-comments\"></i></a>
        <ul>
          ";
            // line 308
            if (($context["bfloat_whatsapp"] ?? null)) {
                // line 309
                echo "          <li><a href=\"https://web.whatsapp.com/send?phone=+55";
                echo ($context["bfloat_whatsapp"] ?? null);
                echo "\" target=\"_Blank\" class=\"whatsapp\"
              data-toggle=\"tooltip\" data-placement=\"right\" title=\"WhatsApp\"><i class=\"fab fa-whatsapp\"></i></a></li>
          ";
            }
            // line 312
            echo "
          ";
            // line 313
            if (($context["bfloat_facebook"] ?? null)) {
                // line 314
                echo "          <li><a href=\"https://facebook.com/";
                echo ($context["bfloat_facebook"] ?? null);
                echo "\" target=\"_Blank\" class=\"facebook\"
              data-toggle=\"tooltip\" data-placement=\"right\" title=\"Facebook\"><i class=\"fab fa-facebook\"></i></a></li>
          ";
            }
            // line 317
            echo "
          ";
            // line 318
            if (($context["bfloat_messenger"] ?? null)) {
                // line 319
                echo "          <li><a href=\"https://m.me/";
                echo ($context["bfloat_messenger"] ?? null);
                echo "\" target=\"_Blank\" class=\"facebook-messenger\"
              data-toggle=\"tooltip\" data-placement=\"right\" title=\"Messenger\"><i
                class=\"fab fa-facebook-messenger\"></i></a></li>
          ";
            }
            // line 323
            echo "
          ";
            // line 324
            if (($context["bfloat_instagram"] ?? null)) {
                // line 325
                echo "          <li><a href=\"https://instagram.com/";
                echo ($context["bfloat_instagram"] ?? null);
                echo "\" target=\"_Blank\" class=\"instagram\"
              data-toggle=\"tooltip\" data-placement=\"right\" title=\"Instagram\"><i class=\"fab fa-instagram\"></i></a></li>
          ";
            }
            // line 328
            echo "
          ";
            // line 329
            if (($context["bfloat_mail"] ?? null)) {
                // line 330
                echo "          <li><a href=\"mailto:";
                echo ($context["bfloat_mail"] ?? null);
                echo "\" class=\"comment\" data-toggle=\"tooltip\" data-placement=\"right\"
              title=\"E-mail\"><i class=\"fa fa-comment\"></i></a></li>
          ";
            }
            // line 333
            echo "        </ul>
      </li>
    </ul>
  </div>
  ";
        }
        // line 338
        echo "
  <main class=\"content_geral\">";
    }

    public function getTemplateName()
    {
        return "sky_blue/template/common/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  732 => 338,  725 => 333,  718 => 330,  716 => 329,  713 => 328,  706 => 325,  704 => 324,  701 => 323,  693 => 319,  691 => 318,  688 => 317,  681 => 314,  679 => 313,  676 => 312,  669 => 309,  667 => 308,  660 => 303,  658 => 302,  650 => 297,  640 => 290,  632 => 284,  624 => 282,  611 => 280,  593 => 265,  576 => 251,  564 => 242,  545 => 226,  538 => 225,  531 => 223,  524 => 222,  517 => 220,  511 => 219,  505 => 218,  498 => 217,  496 => 216,  489 => 211,  484 => 209,  481 => 208,  475 => 206,  473 => 205,  463 => 197,  459 => 195,  456 => 194,  452 => 192,  450 => 191,  442 => 186,  434 => 181,  428 => 177,  420 => 175,  407 => 173,  398 => 166,  392 => 162,  385 => 159,  383 => 158,  380 => 157,  372 => 153,  370 => 152,  367 => 151,  359 => 147,  357 => 146,  354 => 145,  347 => 142,  345 => 141,  342 => 140,  334 => 136,  332 => 135,  327 => 132,  325 => 131,  318 => 126,  312 => 123,  308 => 121,  306 => 120,  299 => 118,  295 => 116,  290 => 113,  286 => 112,  282 => 110,  280 => 109,  276 => 107,  272 => 106,  251 => 88,  225 => 64,  216 => 62,  211 => 61,  200 => 59,  196 => 58,  193 => 57,  184 => 55,  179 => 54,  166 => 52,  162 => 51,  156 => 50,  150 => 49,  146 => 48,  137 => 41,  131 => 39,  129 => 38,  126 => 37,  121 => 35,  116 => 34,  114 => 33,  111 => 32,  104 => 29,  101 => 28,  89 => 25,  84 => 24,  82 => 23,  70 => 14,  66 => 13,  54 => 6,  47 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "sky_blue/template/common/header.twig", "");
    }
}
