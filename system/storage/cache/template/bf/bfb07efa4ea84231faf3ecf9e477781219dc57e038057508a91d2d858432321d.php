<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/shipping/frenet.twig */
class __TwigTemplate_6d33d1cb7fc4198f16ddea23fb55204f9ea4681436d0def4f69e2c013465578c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo " ";
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-frenet\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a>
      </div>
      <h1>";
        // line 9
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 12
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 12);
            echo " \">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 12);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 18
        if (($context["error_warning"] ?? null)) {
            echo " 
    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            // line 19
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times</button>
    </div>
    ";
        }
        // line 23
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 25
        echo ($context["text_edit"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
      \t<form action=\"";
        // line 28
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-frenet\" class=\"form-horizontal\">
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-postcode\">";
        // line 30
        echo ($context["entry_postcode"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"shipping_frenet_postcode\" value=\"";
        // line 32
        echo ($context["shipping_frenet_postcode"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_postcode"] ?? null);
        echo "\" id=\"input-postcode\" class=\"form-control\" />
              ";
        // line 33
        if (($context["error_postcode"] ?? null)) {
            echo " 
              <div class=\"text-danger\">";
            // line 34
            echo ($context["error_postcode"] ?? null);
            echo "</div>
              ";
        }
        // line 36
        echo "            </div>
          </div>
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-msg_prazo\">";
        // line 39
        echo ($context["entry_msg_prazo"] ?? null);
        echo " </label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"shipping_frenet_msg_prazo\" value=\"";
        // line 41
        echo ($context["shipping_frenet_msg_prazo"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["help_msg_prazo"] ?? null);
        echo "\" id=\"input-msg_prazo\" class=\"form-control\" />
            </div>
          </div>
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-contrato-codigo\"><span data-toggle=\"tooltip\" title=\"";
        // line 45
        echo ($context["help_frenet_key"] ?? null);
        echo " \">";
        echo ($context["entry_frenet_key"] ?? null);
        echo " </span></label>
            <div class=\"col-sm-10\">
              <div class=\"row\">
                <div class=\"col-sm-4\">
                  <input type=\"text\" name=\"shipping_frenet_contrato_codigo\" value=\"";
        // line 49
        echo ($context["shipping_frenet_contrato_codigo"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_frenet_key_codigo"] ?? null);
        echo " \" id=\"input-contrato-codigo\" class=\"form-control\" />
                </div>
                <div class=\"col-sm-4\">
                  <input type=\"text\" name=\"shipping_frenet_contrato_senha\" value=\"";
        // line 52
        echo ($context["shipping_frenet_contrato_senha"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_frenet_key_senha"] ?? null);
        echo " \" id=\"input-contrato-senha\" class=\"form-control\" />
                </div>
                <div class=\"col-sm-4\">
                </div>
              </div>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 60
        echo ($context["entry_status"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"shipping_frenet_status\" id=\"input-status\" class=\"form-control\">
                ";
        // line 63
        if (($context["shipping_frenet_status"] ?? null)) {
            echo " 
                <option value=\"1\" selected=\"selected\">";
            // line 64
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\">";
            // line 65
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        } else {
            // line 67
            echo "                <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 68
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        }
        // line 70
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-sort-order\">";
        // line 74
        echo ($context["entry_sort_order"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"shipping_frenet_sort_order\" value=\"";
        // line 76
        echo ($context["shipping_frenet_sort_order"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_sort_order"] ?? null);
        echo "\" id=\"input-sort-order\" class=\"form-control\" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
";
        // line 84
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/shipping/frenet.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  232 => 84,  219 => 76,  214 => 74,  208 => 70,  203 => 68,  198 => 67,  193 => 65,  189 => 64,  185 => 63,  179 => 60,  166 => 52,  158 => 49,  149 => 45,  140 => 41,  135 => 39,  130 => 36,  125 => 34,  121 => 33,  115 => 32,  110 => 30,  105 => 28,  99 => 25,  95 => 23,  88 => 19,  84 => 18,  78 => 14,  67 => 12,  63 => 11,  58 => 9,  51 => 7,  47 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/shipping/frenet.twig", "");
    }
}
