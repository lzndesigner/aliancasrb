<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* crystal_white/template/extension/module/html.twig */
class __TwigTemplate_7211cddafed5716644681b072c9dc9dec409ad4d95a0e318e37198087de87d18 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div>";
        if (($context["heading_title"] ?? null)) {
            // line 2
            echo "\t<div class=\"heading-page\">
\t\t<h1 class=\"title\">";
            // line 3
            echo ($context["heading_title"] ?? null);
            echo "</h1>
\t</div>
\t";
        }
        // line 6
        echo "\t";
        echo ($context["html"] ?? null);
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "crystal_white/template/extension/module/html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  43 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "crystal_white/template/extension/module/html.twig", "");
    }
}
