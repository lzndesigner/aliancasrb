<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* crystal_white/template/extension/module/featured.twig */
class __TwigTemplate_2631b5dbc39ae962ca0e45ce75567f669e0c8160513022ab3c099096cc7661b7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container\">
\t<div class=\"module-general module-featured\">
\t\t<div class=\"heading-page\">
\t\t\t<h3>";
        // line 4
        echo ($context["heading_title"] ?? null);
        echo "</h3>
\t\t</div>
\t\t<div class=\"row\">

\t\t\t";
        // line 8
        if (($context["type"] ?? null)) {
            // line 9
            echo "\t\t\t\t<div class=\"swiper-viewport\">
\t\t\t\t\t<div id=\"banner";
            // line 10
            echo ($context["module"] ?? null);
            echo "\" class=\"swiper-container\">
\t\t\t\t\t\t<div class=\"swiper-wrapper\">
\t\t\t\t\t\t";
        }
        // line 13
        echo "
\t\t\t\t\t\t";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 15
            echo "\t\t\t\t\t\t\t";
            if (($context["type"] ?? null)) {
                // line 16
                echo "\t\t\t\t\t\t\t\t<div class=\"product-layout swiper-slide\">
\t\t\t\t\t\t\t\t";
            } else {
                // line 18
                echo "\t\t\t\t\t\t\t\t\t<div class=\"product-layout col-lg-3 col-md-3 col-sm-4 col-xs-6\">
\t\t\t\t\t\t\t\t\t";
            }
            // line 20
            echo "\t\t\t\t\t\t\t\t\t<div class=\"product-thumb transition\">
\t\t\t\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t";
            // line 22
            if (twig_get_attribute($this->env, $this->source, $context["product"], "valor_desconto", [], "any", false, false, false, 22)) {
                // line 23
                echo "\t\t\t\t\t\t\t\t\t\t\t<span class=\"badge badge-danger badge-desconto\">";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "valor_desconto", [], "any", false, false, false, 23);
                echo "%</span>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 25
            echo "\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 25);
            echo "\"><img src=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 25);
            echo "\" alt=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 25);
            echo "\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 25);
            echo "\" class=\"img-responsive\"/></a>
\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn-wishlist\" data-toggle=\"tooltip\" title=\"";
            // line 26
            echo ($context["button_wishlist"] ?? null);
            echo "\" onclick=\"wishlist.add('";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 26);
            echo "');\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-heart\"></i>
\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t\t\t\t<h4>
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 32
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 32);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 32);
            echo "</a>
\t\t\t\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t\t\t\t";
            // line 34
            if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 34)) {
                // line 35
                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                if ((twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 35) == "R\$ 0,00")) {
                    // line 36
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-current\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<small>Sob-consulta</small>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 46
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 48
                    if ( !twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 48)) {
                        // line 49
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-current\">";
                        // line 50
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 50);
                        echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 53
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-old\">";
                        // line 54
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 54);
                        echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                        // line 57
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 57);
                        echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 60
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 63
                echo "\t\t\t\t\t\t\t\t\t\t\t\t<p>";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "parcelamento", [], "any", false, false, false, 63);
                echo "</p>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 65
            echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
            // line 66
            if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 66)) {
                // line 67
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 68
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 68) < $context["i"])) {
                        // line 69
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 73
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 78
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 79
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 79)) {
                    // line 80
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span>(";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 80);
                    echo ")</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 82
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 83
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 84
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 88
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<span>(0)</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 90
            echo "\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t";
            // line 91
            if ( !twig_get_attribute($this->env, $this->source, $context["product"], "quantity", [], "any", false, false, false, 91)) {
                // line 92
                echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"alert alert-danger alert-stock\">Sem estoque</div>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 94
            echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"button-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 95
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 95);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"\">";
            // line 96
            echo ($context["button_conferir"] ?? null);
            echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t";
            // line 99
            if ( !twig_get_attribute($this->env, $this->source, $context["product"], "shipping", [], "any", false, false, false, 99)) {
                // line 100
                echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"shipping-grid\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"./_galerias/shipping/frete_gratis_gif.gif\" alt=\"Frete Grátis\"
\t\t\t\t\t\t\t\t\t\t\t\t\tclass=\"img-responsive m-3\">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 105
            echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- product-layout -->
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 110
        echo "\t\t\t\t\t\t\t";
        if (($context["type"] ?? null)) {
            // line 111
            echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- swiper-wrappe -->
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- swiper-container -->
\t\t\t\t\t\t<div class=\"swiper-pagination slideshow";
            // line 115
            echo ($context["module"] ?? null);
            echo "\"></div>
\t\t\t\t\t\t<div class=\"swiper-pager\">
\t\t\t\t\t\t\t<div class=\"swiper-button-next featured-btn-next";
            // line 117
            echo ($context["module"] ?? null);
            echo "\"></div>
\t\t\t\t\t\t\t<div class=\"swiper-button-prev featured-btn-prev";
            // line 118
            echo ($context["module"] ?? null);
            echo "\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- swiper-pager -->
\t\t\t\t\t</div>
\t\t\t\t\t<!-- swiper-viewport -->
\t\t\t\t";
        }
        // line 124
        echo "
\t\t\t</div>
\t\t\t<!-- row -->
\t\t</div>
\t\t<!-- module -->
\t</div>
\t<!-- container -->
";
        // line 131
        if (($context["type"] ?? null)) {
            // line 132
            echo "<script type=\"text/javascript\">
<!--
\$('#banner";
            // line 134
            echo ($context["module"] ?? null);
            echo "').swiper({
mode: 'horizontal',
slidesPerView: 4,
pagination: '.slideshow";
            // line 137
            echo ($context["module"] ?? null);
            echo "',
paginationClickable: true,
nextButton: '.random-btn-next";
            // line 139
            echo ($context["module"] ?? null);
            echo "',
prevButton: '.random-btn-prev";
            // line 140
            echo ($context["module"] ?? null);
            echo "',
// spaceBetween: 30,
autoplay: ";
            // line 142
            echo ($context["module"] ?? null);
            echo "00,
autoplayDisableOnInteraction: true,
loop: true,
breakpoints: {
350: {
slidesPerView: 1
},
640: {
slidesPerView: 2
},
768: {
slidesPerView: 3
},
1024: {
slidesPerView: 4
}
}
});
--></script>
";
        }
    }

    public function getTemplateName()
    {
        return "crystal_white/template/extension/module/featured.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  342 => 142,  337 => 140,  333 => 139,  328 => 137,  322 => 134,  318 => 132,  316 => 131,  307 => 124,  298 => 118,  294 => 117,  289 => 115,  283 => 111,  280 => 110,  270 => 105,  263 => 100,  261 => 99,  255 => 96,  251 => 95,  248 => 94,  244 => 92,  242 => 91,  239 => 90,  235 => 88,  226 => 84,  221 => 83,  218 => 82,  212 => 80,  209 => 79,  203 => 78,  196 => 73,  190 => 69,  187 => 68,  182 => 67,  180 => 66,  177 => 65,  171 => 63,  166 => 60,  160 => 57,  154 => 54,  151 => 53,  145 => 50,  142 => 49,  140 => 48,  136 => 46,  124 => 36,  121 => 35,  119 => 34,  112 => 32,  101 => 26,  90 => 25,  84 => 23,  82 => 22,  78 => 20,  74 => 18,  70 => 16,  67 => 15,  63 => 14,  60 => 13,  54 => 10,  51 => 9,  49 => 8,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "crystal_white/template/extension/module/featured.twig", "");
    }
}
