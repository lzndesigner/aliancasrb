<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* setting/setting.twig */
class __TwigTemplate_86f6c4ce6dbb210d769167b69f5dd334b583541c7f30fd984bdcfa4b9a95bb9a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" id=\"button-save\" form=\"form-setting\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\" disabled=\"disabled\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\"> ";
        // line 16
        if (($context["error_warning"] ?? null)) {
            // line 17
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 21
        echo "    ";
        if (($context["success"] ?? null)) {
            // line 22
            echo "    <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 26
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 28
        echo ($context["text_edit"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 31
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-setting\" class=\"form-horizontal\">
          <ul class=\"nav nav-tabs\">
            <li class=\"active\"><a href=\"#tab-general\" data-toggle=\"tab\">";
        // line 33
        echo ($context["tab_general"] ?? null);
        echo "</a></li>
            <li><a href=\"#tab-store\" data-toggle=\"tab\">";
        // line 34
        echo ($context["tab_store"] ?? null);
        echo "</a></li>
            <li><a href=\"#tab-local\" data-toggle=\"tab\">";
        // line 35
        echo ($context["tab_local"] ?? null);
        echo "</a></li>
            <li><a href=\"#tab-image\" data-toggle=\"tab\">";
        // line 36
        echo ($context["tab_image"] ?? null);
        echo "</a></li>
            <li><a href=\"#tab-redesocial\" data-toggle=\"tab\">Rede Social</a></li>
            <li><a href=\"#tab-option\" data-toggle=\"tab\">";
        // line 38
        echo ($context["tab_option"] ?? null);
        echo "</a></li>
            <li><a href=\"#tab-mail\" data-toggle=\"tab\">";
        // line 39
        echo ($context["tab_mail"] ?? null);
        echo "</a></li>
            <li><a href=\"#tab-server\" data-toggle=\"tab\">";
        // line 40
        echo ($context["tab_server"] ?? null);
        echo "</a></li>
          </ul>
          <div class=\"tab-content\">
            <div class=\"tab-pane active\" id=\"tab-general\">
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 45
        echo ($context["help_maintenance"] ?? null);
        echo "\">";
        echo ($context["entry_maintenance"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <label class=\"radio-inline\"> ";
        // line 47
        if (($context["config_maintenance"] ?? null)) {
            // line 48
            echo "                    <input type=\"radio\" name=\"config_maintenance\" value=\"1\" checked=\"checked\" />
                    ";
            // line 49
            echo ($context["text_yes"] ?? null);
            echo "
                    ";
        } else {
            // line 51
            echo "                    <input type=\"radio\" name=\"config_maintenance\" value=\"1\" />
                    ";
            // line 52
            echo ($context["text_yes"] ?? null);
            echo "
                    ";
        }
        // line 53
        echo " </label>
                  <label class=\"radio-inline\"> ";
        // line 54
        if ( !($context["config_maintenance"] ?? null)) {
            // line 55
            echo "                    <input type=\"radio\" name=\"config_maintenance\" value=\"0\" checked=\"checked\" />
                    ";
            // line 56
            echo ($context["text_no"] ?? null);
            echo "
                    ";
        } else {
            // line 58
            echo "                    <input type=\"radio\" name=\"config_maintenance\" value=\"0\" />
                    ";
            // line 59
            echo ($context["text_no"] ?? null);
            echo "
                    ";
        }
        // line 60
        echo " </label>
                </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-meta-title\">";
        // line 64
        echo ($context["entry_meta_title"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"config_meta_title\" value=\"";
        // line 66
        echo ($context["config_meta_title"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_meta_title"] ?? null);
        echo "\" id=\"input-meta-title\" class=\"form-control\" />
                  ";
        // line 67
        if (($context["error_meta_title"] ?? null)) {
            // line 68
            echo "                  <div class=\"text-danger\">";
            echo ($context["error_meta_title"] ?? null);
            echo "</div>
                  ";
        }
        // line 69
        echo " </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-meta-description\">";
        // line 72
        echo ($context["entry_meta_description"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <textarea name=\"config_meta_description\" rows=\"5\" placeholder=\"";
        // line 74
        echo ($context["entry_meta_description"] ?? null);
        echo "\" id=\"input-meta-description\" class=\"form-control\">";
        echo ($context["config_meta_description"] ?? null);
        echo "</textarea>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-meta-keyword\">";
        // line 78
        echo ($context["entry_meta_keyword"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <textarea name=\"config_meta_keyword\" rows=\"5\" placeholder=\"";
        // line 80
        echo ($context["entry_meta_keyword"] ?? null);
        echo "\" id=\"input-meta-keyword\" class=\"form-control\">";
        echo ($context["config_meta_keyword"] ?? null);
        echo "</textarea>
                </div>
              </div>
              <div class=\"form-group hide d-none\">
                <label class=\"col-sm-2 control-label\" for=\"input-theme\">";
        // line 84
        echo ($context["entry_theme"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_theme\" id=\"input-theme\" class=\"form-control\">
                    
                    ";
        // line 88
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["themes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["theme"]) {
            // line 89
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["theme"], "value", [], "any", false, false, false, 89) == ($context["config_theme"] ?? null))) {
                // line 90
                echo "                    
                    <option value=\"";
                // line 91
                echo twig_get_attribute($this->env, $this->source, $context["theme"], "value", [], "any", false, false, false, 91);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["theme"], "text", [], "any", false, false, false, 91);
                echo "</option>
                    
                    ";
            } else {
                // line 94
                echo "                    
                    <option value=\"";
                // line 95
                echo twig_get_attribute($this->env, $this->source, $context["theme"], "value", [], "any", false, false, false, 95);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["theme"], "text", [], "any", false, false, false, 95);
                echo "</option>
                    
                    ";
            }
            // line 98
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['theme'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 99
        echo "                  
                  </select>
                  <br />
                  <img src=\"\" alt=\"\" id=\"theme\" class=\"img-thumbnail\" /></div>
              </div>
              <div class=\"form-group hide d-none\">
                <label class=\"col-sm-2 control-label\" for=\"input-layout\">";
        // line 105
        echo ($context["entry_layout"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_layout_id\" id=\"input-layout\" class=\"form-control\">
                    
                    ";
        // line 109
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["layouts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["layout"]) {
            // line 110
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["layout"], "layout_id", [], "any", false, false, false, 110) == ($context["config_layout_id"] ?? null))) {
                // line 111
                echo "                    
                    <option value=\"";
                // line 112
                echo twig_get_attribute($this->env, $this->source, $context["layout"], "layout_id", [], "any", false, false, false, 112);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["layout"], "name", [], "any", false, false, false, 112);
                echo "</option>
                    
                    ";
            } else {
                // line 115
                echo "                    
                    <option value=\"";
                // line 116
                echo twig_get_attribute($this->env, $this->source, $context["layout"], "layout_id", [], "any", false, false, false, 116);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["layout"], "name", [], "any", false, false, false, 116);
                echo "</option>
                    
                    ";
            }
            // line 119
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['layout'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 120
        echo "                  
                  </select>
                </div>
              </div>
            </div>
            <div class=\"tab-pane\" id=\"tab-store\">
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-name\">";
        // line 127
        echo ($context["entry_name"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"config_name\" value=\"";
        // line 129
        echo ($context["config_name"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_name"] ?? null);
        echo "\" id=\"input-name\" class=\"form-control\" />
                  ";
        // line 130
        if (($context["error_name"] ?? null)) {
            // line 131
            echo "                  <div class=\"text-danger\">";
            echo ($context["error_name"] ?? null);
            echo "</div>
                  ";
        }
        // line 132
        echo " </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-owner\">";
        // line 135
        echo ($context["entry_owner"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"config_owner\" value=\"";
        // line 137
        echo ($context["config_owner"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_owner"] ?? null);
        echo "\" id=\"input-owner\" class=\"form-control\" />
                  ";
        // line 138
        if (($context["error_owner"] ?? null)) {
            // line 139
            echo "                  <div class=\"text-danger\">";
            echo ($context["error_owner"] ?? null);
            echo "</div>
                  ";
        }
        // line 140
        echo " </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-address\">";
        // line 143
        echo ($context["entry_address"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <textarea name=\"config_address\" placeholder=\"";
        // line 145
        echo ($context["entry_address"] ?? null);
        echo "\" rows=\"5\" id=\"input-address\" class=\"form-control\">";
        echo ($context["config_address"] ?? null);
        echo "</textarea>
                  ";
        // line 146
        if (($context["error_address"] ?? null)) {
            // line 147
            echo "                  <div class=\"text-danger\">";
            echo ($context["error_address"] ?? null);
            echo "</div>
                  ";
        }
        // line 148
        echo " </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-geocode\"><span data-toggle=\"tooltip\" data-container=\"#tab-general\" title=\"";
        // line 151
        echo ($context["help_geocode"] ?? null);
        echo "\">";
        echo ($context["entry_geocode"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"config_geocode\" value=\"";
        // line 153
        echo ($context["config_geocode"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_geocode"] ?? null);
        echo "\" id=\"input-geocode\" class=\"form-control\" />
                </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-email\">";
        // line 157
        echo ($context["entry_email"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"config_email\" value=\"";
        // line 159
        echo ($context["config_email"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_email"] ?? null);
        echo "\" id=\"input-email\" class=\"form-control\" />
                  ";
        // line 160
        if (($context["error_email"] ?? null)) {
            // line 161
            echo "                  <div class=\"text-danger\">";
            echo ($context["error_email"] ?? null);
            echo "</div>
                  ";
        }
        // line 162
        echo " </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-telephone\">";
        // line 165
        echo ($context["entry_telephone"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"config_telephone\" value=\"";
        // line 167
        echo ($context["config_telephone"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_telephone"] ?? null);
        echo "\" id=\"input-telephone\" class=\"form-control maskPhone\" />
                  ";
        // line 168
        if (($context["error_telephone"] ?? null)) {
            // line 169
            echo "                  <div class=\"text-danger\">";
            echo ($context["error_telephone"] ?? null);
            echo "</div>
                  ";
        }
        // line 170
        echo " </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-fax\">";
        // line 173
        echo ($context["entry_fax"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"config_fax\" value=\"";
        // line 175
        echo ($context["config_fax"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_fax"] ?? null);
        echo "\" id=\"input-fax\" class=\"form-control maskPhone\" />
                </div>
              </div>              
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-image\">";
        // line 179
        echo ($context["entry_image"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\"><a href=\"\" id=\"thumb-image\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 180
        echo ($context["thumb"] ?? null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo ($context["placeholder"] ?? null);
        echo "\" /></a>
                  <input type=\"hidden\" name=\"config_image\" value=\"";
        // line 181
        echo ($context["config_image"] ?? null);
        echo "\" id=\"input-image\" />
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-open\"><span data-toggle=\"tooltip\" data-container=\"#tab-general\" title=\"";
        // line 185
        echo ($context["help_open"] ?? null);
        echo "\">";
        echo ($context["entry_open"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <textarea name=\"config_open\" rows=\"5\" placeholder=\"";
        // line 187
        echo ($context["entry_open"] ?? null);
        echo "\" id=\"input-open\" class=\"form-control\">";
        echo ($context["config_open"] ?? null);
        echo "</textarea>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-comment\"><span data-toggle=\"tooltip\" data-container=\"#tab-general\" title=\"";
        // line 191
        echo ($context["help_comment"] ?? null);
        echo "\">";
        echo ($context["entry_comment"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <textarea name=\"config_comment\" rows=\"5\" placeholder=\"";
        // line 193
        echo ($context["entry_comment"] ?? null);
        echo "\" id=\"input-comment\" class=\"form-control\">";
        echo ($context["config_comment"] ?? null);
        echo "</textarea>
                </div>
              </div>
              ";
        // line 196
        if (($context["locations"] ?? null)) {
            // line 197
            echo "              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" data-container=\"#tab-general\" title=\"";
            // line 198
            echo ($context["help_location"] ?? null);
            echo "\">";
            echo ($context["entry_location"] ?? null);
            echo "</span></label>
                <div class=\"col-sm-10\"> ";
            // line 199
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["locations"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["location"]) {
                // line 200
                echo "                  <div class=\"checkbox\">
                    <label> ";
                // line 201
                if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["location"], "location_id", [], "any", false, false, false, 201), ($context["config_location"] ?? null))) {
                    // line 202
                    echo "                      <input type=\"checkbox\" name=\"config_location[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["location"], "location_id", [], "any", false, false, false, 202);
                    echo "\" checked=\"checked\" />
                      ";
                    // line 203
                    echo twig_get_attribute($this->env, $this->source, $context["location"], "name", [], "any", false, false, false, 203);
                    echo "
                      ";
                } else {
                    // line 205
                    echo "                      <input type=\"checkbox\" name=\"config_location[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["location"], "location_id", [], "any", false, false, false, 205);
                    echo "\" />
                      ";
                    // line 206
                    echo twig_get_attribute($this->env, $this->source, $context["location"], "name", [], "any", false, false, false, 206);
                    echo "
                      ";
                }
                // line 207
                echo " </label>
                  </div>
                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['location'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 209
            echo " </div>
              </div>
              ";
        }
        // line 211
        echo " </div>
            <div class=\"tab-pane\" id=\"tab-local\">
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-country\">";
        // line 214
        echo ($context["entry_country"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_country_id\" id=\"input-country\" class=\"form-control\">
                    
                    ";
        // line 218
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["countries"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 219
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 219) == ($context["config_country_id"] ?? null))) {
                // line 220
                echo "                    
                    <option value=\"";
                // line 221
                echo twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 221);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, false, 221);
                echo "</option>
                    
                    ";
            } else {
                // line 224
                echo "                    
                    <option value=\"";
                // line 225
                echo twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 225);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, false, 225);
                echo "</option>
                    
                    ";
            }
            // line 228
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 229
        echo "                  
                  </select>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-zone\">";
        // line 234
        echo ($context["entry_zone"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_zone_id\" id=\"input-zone\" class=\"form-control\">
                  </select>
                </div>
              </div>
              <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-timezone\">";
        // line 241
        echo ($context["entry_timezone"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_timezone\" id=\"input-timezone\" class=\"form-control\">
                    ";
        // line 244
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["timezones"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["timezone"]) {
            // line 245
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["timezone"], "value", [], "any", false, false, false, 245) == ($context["config_timezone"] ?? null))) {
                // line 246
                echo "                        <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["timezone"], "value", [], "any", false, false, false, 246);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["timezone"], "text", [], "any", false, false, false, 246);
                echo "</option>
                      ";
            } else {
                // line 248
                echo "                        <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["timezone"], "value", [], "any", false, false, false, 248);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["timezone"], "text", [], "any", false, false, false, 248);
                echo "</option>
                      ";
            }
            // line 250
            echo "                   ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['timezone'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 251
        echo "                  </select>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-language\">";
        // line 255
        echo ($context["entry_language"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_language\" id=\"input-language\" class=\"form-control\">
                    
                    ";
        // line 259
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 260
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 260) == ($context["config_language"] ?? null))) {
                // line 261
                echo "                    
                    <option value=\"";
                // line 262
                echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 262);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 262);
                echo "</option>
                    
                    ";
            } else {
                // line 265
                echo "                    
                    <option value=\"";
                // line 266
                echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 266);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 266);
                echo "</option>
                    
                    ";
            }
            // line 269
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 270
        echo "                  
                  </select>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-admin-language\">";
        // line 275
        echo ($context["entry_admin_language"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_admin_language\" id=\"input-admin-language\" class=\"form-control\">
                    
                    ";
        // line 279
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 280
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 280) == ($context["config_admin_language"] ?? null))) {
                // line 281
                echo "                    
                    <option value=\"";
                // line 282
                echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 282);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 282);
                echo "</option>
                    
                    ";
            } else {
                // line 285
                echo "                    
                    <option value=\"";
                // line 286
                echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 286);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 286);
                echo "</option>
                    
                    ";
            }
            // line 289
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 290
        echo "                  
                  </select>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-currency\"><span data-toggle=\"tooltip\" title=\"";
        // line 295
        echo ($context["help_currency"] ?? null);
        echo "\">";
        echo ($context["entry_currency"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <select name=\"config_currency\" id=\"input-currency\" class=\"form-control\">
                    
                    ";
        // line 299
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["currencies"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
            // line 300
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["currency"], "code", [], "any", false, false, false, 300) == ($context["config_currency"] ?? null))) {
                // line 301
                echo "                    
                    <option value=\"";
                // line 302
                echo twig_get_attribute($this->env, $this->source, $context["currency"], "code", [], "any", false, false, false, 302);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["currency"], "title", [], "any", false, false, false, 302);
                echo "</option>
                    
                    ";
            } else {
                // line 305
                echo "                    
                    <option value=\"";
                // line 306
                echo twig_get_attribute($this->env, $this->source, $context["currency"], "code", [], "any", false, false, false, 306);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["currency"], "title", [], "any", false, false, false, 306);
                echo "</option>
                    
                    ";
            }
            // line 309
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 310
        echo "                  
                  </select>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 315
        echo ($context["help_currency_auto"] ?? null);
        echo "\">";
        echo ($context["entry_currency_auto"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <label class=\"radio-inline\"> ";
        // line 317
        if (($context["config_currency_auto"] ?? null)) {
            // line 318
            echo "                    <input type=\"radio\" name=\"config_currency_auto\" value=\"1\" checked=\"checked\" />
                    ";
            // line 319
            echo ($context["text_yes"] ?? null);
            echo "
                    ";
        } else {
            // line 321
            echo "                    <input type=\"radio\" name=\"config_currency_auto\" value=\"1\" />
                    ";
            // line 322
            echo ($context["text_yes"] ?? null);
            echo "
                    ";
        }
        // line 323
        echo " </label>
                  <label class=\"radio-inline\"> ";
        // line 324
        if ( !($context["config_currency_auto"] ?? null)) {
            // line 325
            echo "                    <input type=\"radio\" name=\"config_currency_auto\" value=\"0\" checked=\"checked\" />
                    ";
            // line 326
            echo ($context["text_no"] ?? null);
            echo "
                    ";
        } else {
            // line 328
            echo "                    <input type=\"radio\" name=\"config_currency_auto\" value=\"0\" />
                    ";
            // line 329
            echo ($context["text_no"] ?? null);
            echo "
                    ";
        }
        // line 330
        echo " </label>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-length-class\">";
        // line 334
        echo ($context["entry_length_class"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_length_class_id\" id=\"input-length-class\" class=\"form-control\">
                    
                    ";
        // line 338
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["length_classes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["length_class"]) {
            // line 339
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["length_class"], "length_class_id", [], "any", false, false, false, 339) == ($context["config_length_class_id"] ?? null))) {
                // line 340
                echo "                    
                    <option value=\"";
                // line 341
                echo twig_get_attribute($this->env, $this->source, $context["length_class"], "length_class_id", [], "any", false, false, false, 341);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["length_class"], "title", [], "any", false, false, false, 341);
                echo "</option>
                    
                    ";
            } else {
                // line 344
                echo "                    
                    <option value=\"";
                // line 345
                echo twig_get_attribute($this->env, $this->source, $context["length_class"], "length_class_id", [], "any", false, false, false, 345);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["length_class"], "title", [], "any", false, false, false, 345);
                echo "</option>
                    
                    ";
            }
            // line 348
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['length_class'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 349
        echo "                  
                  </select>
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-weight-class\">";
        // line 354
        echo ($context["entry_weight_class"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"config_weight_class_id\" id=\"input-weight-class\" class=\"form-control\">
                    
                    ";
        // line 358
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["weight_classes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["weight_class"]) {
            // line 359
            echo "                    ";
            if ((twig_get_attribute($this->env, $this->source, $context["weight_class"], "weight_class_id", [], "any", false, false, false, 359) == ($context["config_weight_class_id"] ?? null))) {
                // line 360
                echo "                    
                    <option value=\"";
                // line 361
                echo twig_get_attribute($this->env, $this->source, $context["weight_class"], "weight_class_id", [], "any", false, false, false, 361);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["weight_class"], "title", [], "any", false, false, false, 361);
                echo "</option>
                    
                    ";
            } else {
                // line 364
                echo "                    
                    <option value=\"";
                // line 365
                echo twig_get_attribute($this->env, $this->source, $context["weight_class"], "weight_class_id", [], "any", false, false, false, 365);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["weight_class"], "title", [], "any", false, false, false, 365);
                echo "</option>
                    
                    ";
            }
            // line 368
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['weight_class'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 369
        echo "                  
                  </select>
                </div>
              </div>
            </div>
            <div class=\"tab-pane\" id=\"tab-option\">
              <fieldset>
                <legend>";
        // line 376
        echo ($context["text_regras"] ?? null);
        echo "</legend>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-order-minimum\"><span data-toggle=\"tooltip\" title=\"";
        // line 378
        echo ($context["help_order_minimum"] ?? null);
        echo "\">";
        echo ($context["entry_order_minimum"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_order_minimum\" value=\"";
        // line 380
        echo ($context["config_order_minimum"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_order_minimum"] ?? null);
        echo "\" id=\"input-order-minimum\" class=\"form-control\" />
                    ";
        // line 381
        if (($context["error_order_minimum"] ?? null)) {
            // line 382
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_order_minimum"] ?? null);
            echo "</div>
                    ";
        }
        // line 383
        echo " </div>
                </div>
              </fieldset>

              <fieldset>
                <legend>";
        // line 388
        echo ($context["text_product"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 390
        echo ($context["help_product_count"] ?? null);
        echo "\">";
        echo ($context["entry_product_count"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 392
        if (($context["config_product_count"] ?? null)) {
            // line 393
            echo "                      <input type=\"radio\" name=\"config_product_count\" value=\"1\" checked=\"checked\" />
                      ";
            // line 394
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 396
            echo "                      <input type=\"radio\" name=\"config_product_count\" value=\"1\" />
                      ";
            // line 397
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 398
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 399
        if ( !($context["config_product_count"] ?? null)) {
            // line 400
            echo "                      <input type=\"radio\" name=\"config_product_count\" value=\"0\" checked=\"checked\" />
                      ";
            // line 401
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 403
            echo "                      <input type=\"radio\" name=\"config_product_count\" value=\"0\" />
                      ";
            // line 404
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 405
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-admin-limit\"><span data-toggle=\"tooltip\" title=\"";
        // line 409
        echo ($context["help_limit_admin"] ?? null);
        echo "\">";
        echo ($context["entry_limit_admin"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_limit_admin\" value=\"";
        // line 411
        echo ($context["config_limit_admin"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_limit_admin"] ?? null);
        echo "\" id=\"input-admin-limit\" class=\"form-control\" />
                    ";
        // line 412
        if (($context["error_limit_admin"] ?? null)) {
            // line 413
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_limit_admin"] ?? null);
            echo "</div>
                    ";
        }
        // line 414
        echo " </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 418
        echo ($context["text_review"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 420
        echo ($context["help_review"] ?? null);
        echo "\">";
        echo ($context["entry_review"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 422
        if (($context["config_review_status"] ?? null)) {
            // line 423
            echo "                      <input type=\"radio\" name=\"config_review_status\" value=\"1\" checked=\"checked\" />
                      ";
            // line 424
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 426
            echo "                      <input type=\"radio\" name=\"config_review_status\" value=\"1\" />
                      ";
            // line 427
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 428
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 429
        if ( !($context["config_review_status"] ?? null)) {
            // line 430
            echo "                      <input type=\"radio\" name=\"config_review_status\" value=\"0\" checked=\"checked\" />
                      ";
            // line 431
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 433
            echo "                      <input type=\"radio\" name=\"config_review_status\" value=\"0\" />
                      ";
            // line 434
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 435
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 439
        echo ($context["help_review_guest"] ?? null);
        echo "\">";
        echo ($context["entry_review_guest"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 441
        if (($context["config_review_guest"] ?? null)) {
            // line 442
            echo "                      <input type=\"radio\" name=\"config_review_guest\" value=\"1\" checked=\"checked\" />
                      ";
            // line 443
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 445
            echo "                      <input type=\"radio\" name=\"config_review_guest\" value=\"1\" />
                      ";
            // line 446
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 447
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 448
        if ( !($context["config_review_guest"] ?? null)) {
            // line 449
            echo "                      <input type=\"radio\" name=\"config_review_guest\" value=\"0\" checked=\"checked\" />
                      ";
            // line 450
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 452
            echo "                      <input type=\"radio\" name=\"config_review_guest\" value=\"0\" />
                      ";
            // line 453
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 454
        echo " </label>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 459
        echo ($context["text_voucher"] ?? null);
        echo "</legend>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-voucher-min\"><span data-toggle=\"tooltip\" title=\"";
        // line 461
        echo ($context["help_voucher_min"] ?? null);
        echo "\">";
        echo ($context["entry_voucher_min"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_voucher_min\" value=\"";
        // line 463
        echo ($context["config_voucher_min"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_voucher_min"] ?? null);
        echo "\" id=\"input-voucher-min\" class=\"form-control\" />
                    ";
        // line 464
        if (($context["error_voucher_min"] ?? null)) {
            // line 465
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_voucher_min"] ?? null);
            echo "</div>
                    ";
        }
        // line 466
        echo " </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-voucher-max\"><span data-toggle=\"tooltip\" title=\"";
        // line 469
        echo ($context["help_voucher_max"] ?? null);
        echo "\">";
        echo ($context["entry_voucher_max"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_voucher_max\" value=\"";
        // line 471
        echo ($context["config_voucher_max"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_voucher_max"] ?? null);
        echo "\" id=\"input-voucher-max\" class=\"form-control\" />
                    ";
        // line 472
        if (($context["error_voucher_max"] ?? null)) {
            // line 473
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_voucher_max"] ?? null);
            echo "</div>
                    ";
        }
        // line 474
        echo " </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 478
        echo ($context["text_tax"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 480
        echo ($context["entry_tax"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 482
        if (($context["config_tax"] ?? null)) {
            // line 483
            echo "                      <input type=\"radio\" name=\"config_tax\" value=\"1\" checked=\"checked\" />
                      ";
            // line 484
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 486
            echo "                      <input type=\"radio\" name=\"config_tax\" value=\"1\" />
                      ";
            // line 487
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 488
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 489
        if ( !($context["config_tax"] ?? null)) {
            // line 490
            echo "                      <input type=\"radio\" name=\"config_tax\" value=\"0\" checked=\"checked\" />
                      ";
            // line 491
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 493
            echo "                      <input type=\"radio\" name=\"config_tax\" value=\"0\" />
                      ";
            // line 494
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 495
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-tax-default\"><span data-toggle=\"tooltip\" title=\"";
        // line 499
        echo ($context["help_tax_default"] ?? null);
        echo "\">";
        echo ($context["entry_tax_default"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_tax_default\" id=\"input-tax-default\" class=\"form-control\">
                      <option value=\"\">";
        // line 502
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 504
        if ((($context["config_tax_default"] ?? null) == "shipping")) {
            // line 505
            echo "                      
                      <option value=\"shipping\" selected=\"selected\">";
            // line 506
            echo ($context["text_shipping"] ?? null);
            echo "</option>
                      
                      ";
        } else {
            // line 509
            echo "                      
                      <option value=\"shipping\">";
            // line 510
            echo ($context["text_shipping"] ?? null);
            echo "</option>
                      
                      ";
        }
        // line 513
        echo "                      ";
        if ((($context["config_tax_default"] ?? null) == "payment")) {
            // line 514
            echo "                      
                      <option value=\"payment\" selected=\"selected\">";
            // line 515
            echo ($context["text_payment"] ?? null);
            echo "</option>
                      
                      ";
        } else {
            // line 518
            echo "                      
                      <option value=\"payment\">";
            // line 519
            echo ($context["text_payment"] ?? null);
            echo "</option>
                      
                      ";
        }
        // line 522
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-tax-customer\"><span data-toggle=\"tooltip\" title=\"";
        // line 527
        echo ($context["help_tax_customer"] ?? null);
        echo "\">";
        echo ($context["entry_tax_customer"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_tax_customer\" id=\"input-tax-customer\" class=\"form-control\">
                      <option value=\"\">";
        // line 530
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 532
        if ((($context["config_tax_customer"] ?? null) == "shipping")) {
            // line 533
            echo "                      
                      <option value=\"shipping\" selected=\"selected\">";
            // line 534
            echo ($context["text_shipping"] ?? null);
            echo "</option>
                      
                      ";
        } else {
            // line 537
            echo "                      
                      <option value=\"shipping\">";
            // line 538
            echo ($context["text_shipping"] ?? null);
            echo "</option>
                      
                      ";
        }
        // line 541
        echo "                      ";
        if ((($context["config_tax_customer"] ?? null) == "payment")) {
            // line 542
            echo "                      
                      <option value=\"payment\" selected=\"selected\">";
            // line 543
            echo ($context["text_payment"] ?? null);
            echo "</option>
                      
                      ";
        } else {
            // line 546
            echo "                      
                      <option value=\"payment\">";
            // line 547
            echo ($context["text_payment"] ?? null);
            echo "</option>
                      
                      ";
        }
        // line 550
        echo "                    
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 556
        echo ($context["text_account"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 558
        echo ($context["help_customer_online"] ?? null);
        echo "\">";
        echo ($context["entry_customer_online"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 560
        if (($context["config_customer_online"] ?? null)) {
            // line 561
            echo "                      <input type=\"radio\" name=\"config_customer_online\" value=\"1\" checked=\"checked\" />
                      ";
            // line 562
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 564
            echo "                      <input type=\"radio\" name=\"config_customer_online\" value=\"1\" />
                      ";
            // line 565
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 566
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 567
        if ( !($context["config_customer_online"] ?? null)) {
            // line 568
            echo "                      <input type=\"radio\" name=\"config_customer_online\" value=\"0\" checked=\"checked\" />
                      ";
            // line 569
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 571
            echo "                      <input type=\"radio\" name=\"config_customer_online\" value=\"0\" />
                      ";
            // line 572
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 573
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 577
        echo ($context["help_customer_activity"] ?? null);
        echo "\">";
        echo ($context["entry_customer_activity"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 579
        if (($context["config_customer_activity"] ?? null)) {
            // line 580
            echo "                      <input type=\"radio\" name=\"config_customer_activity\" value=\"1\" checked=\"checked\" />
                      ";
            // line 581
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 583
            echo "                      <input type=\"radio\" name=\"config_customer_activity\" value=\"1\" />
                      ";
            // line 584
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 585
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 586
        if ( !($context["config_customer_activity"] ?? null)) {
            // line 587
            echo "                      <input type=\"radio\" name=\"config_customer_activity\" value=\"0\" checked=\"checked\" />
                      ";
            // line 588
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 590
            echo "                      <input type=\"radio\" name=\"config_customer_activity\" value=\"0\" />
                      ";
            // line 591
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 592
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 596
        echo ($context["entry_customer_search"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 598
        if (($context["config_customer_search"] ?? null)) {
            // line 599
            echo "                      <input type=\"radio\" name=\"config_customer_search\" value=\"1\" checked=\"checked\" />
                      ";
            // line 600
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 602
            echo "                      <input type=\"radio\" name=\"config_customer_search\" value=\"1\" />
                      ";
            // line 603
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 604
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 605
        if ( !($context["config_customer_search"] ?? null)) {
            // line 606
            echo "                      <input type=\"radio\" name=\"config_customer_search\" value=\"0\" checked=\"checked\" />
                      ";
            // line 607
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 609
            echo "                      <input type=\"radio\" name=\"config_customer_search\" value=\"0\" />
                      ";
            // line 610
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 611
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-customer-group\"><span data-toggle=\"tooltip\" title=\"";
        // line 615
        echo ($context["help_customer_group"] ?? null);
        echo "\">";
        echo ($context["entry_customer_group"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_customer_group_id\" id=\"input-customer-group\" class=\"form-control\">
                      
                      ";
        // line 619
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["customer_groups"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["customer_group"]) {
            // line 620
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 620) == ($context["config_customer_group_id"] ?? null))) {
                // line 621
                echo "                      
                      <option value=\"";
                // line 622
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 622);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "name", [], "any", false, false, false, 622);
                echo "</option>
                      
                      ";
            } else {
                // line 625
                echo "                      
                      <option value=\"";
                // line 626
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 626);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "name", [], "any", false, false, false, 626);
                echo "</option>
                      
                      ";
            }
            // line 629
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['customer_group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 630
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 635
        echo ($context["help_customer_group_display"] ?? null);
        echo "\">";
        echo ($context["entry_customer_group_display"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\"> ";
        // line 636
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["customer_groups"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["customer_group"]) {
            // line 637
            echo "                    <div class=\"checkbox\">
                      <label> ";
            // line 638
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 638), ($context["config_customer_group_display"] ?? null))) {
                // line 639
                echo "                        <input type=\"checkbox\" name=\"config_customer_group_display[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 639);
                echo "\" checked=\"checked\" />
                        ";
                // line 640
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "name", [], "any", false, false, false, 640);
                echo "
                        ";
            } else {
                // line 642
                echo "                        <input type=\"checkbox\" name=\"config_customer_group_display[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 642);
                echo "\" />
                        ";
                // line 643
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "name", [], "any", false, false, false, 643);
                echo "
                        ";
            }
            // line 644
            echo " </label>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['customer_group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 647
        echo "                    ";
        if (($context["error_customer_group_display"] ?? null)) {
            // line 648
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_customer_group_display"] ?? null);
            echo "</div>
                    ";
        }
        // line 649
        echo " </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 652
        echo ($context["help_customer_price"] ?? null);
        echo "\">";
        echo ($context["entry_customer_price"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 654
        if (($context["config_customer_price"] ?? null)) {
            // line 655
            echo "                      <input type=\"radio\" name=\"config_customer_price\" value=\"1\" checked=\"checked\" />
                      ";
            // line 656
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 658
            echo "                      <input type=\"radio\" name=\"config_customer_price\" value=\"1\" />
                      ";
            // line 659
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 660
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 661
        if ( !($context["config_customer_price"] ?? null)) {
            // line 662
            echo "                      <input type=\"radio\" name=\"config_customer_price\" value=\"0\" checked=\"checked\" />
                      ";
            // line 663
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 665
            echo "                      <input type=\"radio\" name=\"config_customer_price\" value=\"0\" />
                      ";
            // line 666
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 667
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-login-attempts\"><span data-toggle=\"tooltip\" title=\"";
        // line 671
        echo ($context["help_login_attempts"] ?? null);
        echo "\">";
        echo ($context["entry_login_attempts"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_login_attempts\" value=\"";
        // line 673
        echo ($context["config_login_attempts"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_login_attempts"] ?? null);
        echo "\" id=\"input-login-attempts\" class=\"form-control\" />
                    ";
        // line 674
        if (($context["error_login_attempts"] ?? null)) {
            // line 675
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_login_attempts"] ?? null);
            echo "</div>
                    ";
        }
        // line 676
        echo " </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-account\"><span data-toggle=\"tooltip\" title=\"";
        // line 679
        echo ($context["help_account"] ?? null);
        echo "\">";
        echo ($context["entry_account"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_account_id\" id=\"input-account\" class=\"form-control\">
                      <option value=\"0\">";
        // line 682
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 684
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["informations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
            // line 685
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 685) == ($context["config_account_id"] ?? null))) {
                // line 686
                echo "                      
                      <option value=\"";
                // line 687
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 687);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 687);
                echo "</option>
                      
                      ";
            } else {
                // line 690
                echo "                      
                      <option value=\"";
                // line 691
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 691);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 691);
                echo "</option>
                      
                      ";
            }
            // line 694
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 695
        echo "                    
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 701
        echo ($context["text_checkout"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-invoice-prefix\"><span data-toggle=\"tooltip\" title=\"";
        // line 703
        echo ($context["help_invoice_prefix"] ?? null);
        echo "\">";
        echo ($context["entry_invoice_prefix"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_invoice_prefix\" value=\"";
        // line 705
        echo ($context["config_invoice_prefix"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_invoice_prefix"] ?? null);
        echo "\" id=\"input-invoice-prefix\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 709
        echo ($context["help_cart_weight"] ?? null);
        echo "\">";
        echo ($context["entry_cart_weight"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 711
        if (($context["config_cart_weight"] ?? null)) {
            // line 712
            echo "                      <input type=\"radio\" name=\"config_cart_weight\" value=\"1\" checked=\"checked\" />
                      ";
            // line 713
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 715
            echo "                      <input type=\"radio\" name=\"config_cart_weight\" value=\"1\" />
                      ";
            // line 716
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 717
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 718
        if ( !($context["config_cart_weight"] ?? null)) {
            // line 719
            echo "                      <input type=\"radio\" name=\"config_cart_weight\" value=\"0\" checked=\"checked\" />
                      ";
            // line 720
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 722
            echo "                      <input type=\"radio\" name=\"config_cart_weight\" value=\"0\" />
                      ";
            // line 723
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 724
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 728
        echo ($context["help_checkout_guest"] ?? null);
        echo "\">";
        echo ($context["entry_checkout_guest"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 730
        if (($context["config_checkout_guest"] ?? null)) {
            // line 731
            echo "                      <input type=\"radio\" name=\"config_checkout_guest\" value=\"1\" checked=\"checked\" />
                      ";
            // line 732
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 734
            echo "                      <input type=\"radio\" name=\"config_checkout_guest\" value=\"1\" />
                      ";
            // line 735
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 736
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 737
        if ( !($context["config_checkout_guest"] ?? null)) {
            // line 738
            echo "                      <input type=\"radio\" name=\"config_checkout_guest\" value=\"0\" checked=\"checked\" />
                      ";
            // line 739
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 741
            echo "                      <input type=\"radio\" name=\"config_checkout_guest\" value=\"0\" />
                      ";
            // line 742
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 743
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-checkout\"><span data-toggle=\"tooltip\" title=\"";
        // line 747
        echo ($context["help_checkout"] ?? null);
        echo "\">";
        echo ($context["entry_checkout"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_checkout_id\" id=\"input-checkout\" class=\"form-control\">
                      <option value=\"0\">";
        // line 750
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 752
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["informations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
            // line 753
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 753) == ($context["config_checkout_id"] ?? null))) {
                // line 754
                echo "                      
                      <option value=\"";
                // line 755
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 755);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 755);
                echo "</option>
                      
                      ";
            } else {
                // line 758
                echo "                      
                      <option value=\"";
                // line 759
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 759);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 759);
                echo "</option>
                      
                      ";
            }
            // line 762
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 763
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-order-status\"><span data-toggle=\"tooltip\" title=\"";
        // line 768
        echo ($context["help_order_status"] ?? null);
        echo "\">";
        echo ($context["entry_order_status"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_order_status_id\" id=\"input-order-status\" class=\"form-control\">
                      
                      ";
        // line 772
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 773
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 773) == ($context["config_order_status_id"] ?? null))) {
                // line 774
                echo "                      
                      <option value=\"";
                // line 775
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 775);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 775);
                echo "</option>
                      
                      ";
            } else {
                // line 778
                echo "                      
                      <option value=\"";
                // line 779
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 779);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 779);
                echo "</option>
                      
                      ";
            }
            // line 782
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 783
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-process-status\"><span data-toggle=\"tooltip\" title=\"";
        // line 788
        echo ($context["help_processing_status"] ?? null);
        echo "\">";
        echo ($context["entry_processing_status"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <div class=\"well well-sm\" style=\"height: 150px; overflow: auto;\"> ";
        // line 790
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 791
            echo "                      <div class=\"checkbox\">
                        <label> ";
            // line 792
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 792), ($context["config_processing_status"] ?? null))) {
                // line 793
                echo "                          <input type=\"checkbox\" name=\"config_processing_status[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 793);
                echo "\" checked=\"checked\" />
                          ";
                // line 794
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 794);
                echo "
                          ";
            } else {
                // line 796
                echo "                          <input type=\"checkbox\" name=\"config_processing_status[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 796);
                echo "\" />
                          ";
                // line 797
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 797);
                echo "
                          ";
            }
            // line 798
            echo " </label>
                      </div>
                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 800
        echo " </div>
                    ";
        // line 801
        if (($context["error_processing_status"] ?? null)) {
            // line 802
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_processing_status"] ?? null);
            echo "</div>
                    ";
        }
        // line 803
        echo " </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-complete-status\"><span data-toggle=\"tooltip\" title=\"";
        // line 806
        echo ($context["help_complete_status"] ?? null);
        echo "\">";
        echo ($context["entry_complete_status"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <div class=\"well well-sm\" style=\"height: 150px; overflow: auto;\"> ";
        // line 808
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 809
            echo "                      <div class=\"checkbox\">
                        <label> ";
            // line 810
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 810), ($context["config_complete_status"] ?? null))) {
                // line 811
                echo "                          <input type=\"checkbox\" name=\"config_complete_status[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 811);
                echo "\" checked=\"checked\" />
                          ";
                // line 812
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 812);
                echo "
                          ";
            } else {
                // line 814
                echo "                          <input type=\"checkbox\" name=\"config_complete_status[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 814);
                echo "\" />
                          ";
                // line 815
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 815);
                echo "
                          ";
            }
            // line 816
            echo " </label>
                      </div>
                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 818
        echo " </div>
                    ";
        // line 819
        if (($context["error_complete_status"] ?? null)) {
            // line 820
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_complete_status"] ?? null);
            echo "</div>
                    ";
        }
        // line 821
        echo " </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-fraud-status\"><span data-toggle=\"tooltip\" title=\"";
        // line 824
        echo ($context["help_fraud_status"] ?? null);
        echo "\">";
        echo ($context["entry_fraud_status"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_fraud_status_id\" id=\"input-fraud-status\" class=\"form-control\">
                      
                      ";
        // line 828
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["order_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 829
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 829) == ($context["config_fraud_status_id"] ?? null))) {
                // line 830
                echo "                      
                      <option value=\"";
                // line 831
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 831);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 831);
                echo "</option>
                      
                      ";
            } else {
                // line 834
                echo "                      
                      <option value=\"";
                // line 835
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "order_status_id", [], "any", false, false, false, 835);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["order_status"], "name", [], "any", false, false, false, 835);
                echo "</option>
                      
                      ";
            }
            // line 838
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 839
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-api\"><span data-toggle=\"tooltip\" title=\"";
        // line 844
        echo ($context["help_api"] ?? null);
        echo "\">";
        echo ($context["entry_api"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_api_id\" id=\"input-api\" class=\"form-control\">
                      <option value=\"0\">";
        // line 847
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 849
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["apis"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["api"]) {
            // line 850
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["api"], "api_id", [], "any", false, false, false, 850) == ($context["config_api_id"] ?? null))) {
                // line 851
                echo "                      
                      <option value=\"";
                // line 852
                echo twig_get_attribute($this->env, $this->source, $context["api"], "api_id", [], "any", false, false, false, 852);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["api"], "username", [], "any", false, false, false, 852);
                echo "</option>
                      
                      ";
            } else {
                // line 855
                echo "                      
                      <option value=\"";
                // line 856
                echo twig_get_attribute($this->env, $this->source, $context["api"], "api_id", [], "any", false, false, false, 856);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["api"], "username", [], "any", false, false, false, 856);
                echo "</option>
                      
                      ";
            }
            // line 859
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['api'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 860
        echo "                    
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 866
        echo ($context["text_stock"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 868
        echo ($context["help_stock_display"] ?? null);
        echo "\">";
        echo ($context["entry_stock_display"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 870
        if (($context["config_stock_display"] ?? null)) {
            // line 871
            echo "                      <input type=\"radio\" name=\"config_stock_display\" value=\"1\" checked=\"checked\" />
                      ";
            // line 872
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 874
            echo "                      <input type=\"radio\" name=\"config_stock_display\" value=\"1\" />
                      ";
            // line 875
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 876
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 877
        if ( !($context["config_stock_display"] ?? null)) {
            // line 878
            echo "                      <input type=\"radio\" name=\"config_stock_display\" value=\"0\" checked=\"checked\" />
                      ";
            // line 879
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 881
            echo "                      <input type=\"radio\" name=\"config_stock_display\" value=\"0\" />
                      ";
            // line 882
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 883
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 887
        echo ($context["help_stock_warning"] ?? null);
        echo "\">";
        echo ($context["entry_stock_warning"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 889
        if (($context["config_stock_warning"] ?? null)) {
            // line 890
            echo "                      <input type=\"radio\" name=\"config_stock_warning\" value=\"1\" checked=\"checked\" />
                      ";
            // line 891
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 893
            echo "                      <input type=\"radio\" name=\"config_stock_warning\" value=\"1\" />
                      ";
            // line 894
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 895
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 896
        if ( !($context["config_stock_warning"] ?? null)) {
            // line 897
            echo "                      <input type=\"radio\" name=\"config_stock_warning\" value=\"0\" checked=\"checked\" />
                      ";
            // line 898
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 900
            echo "                      <input type=\"radio\" name=\"config_stock_warning\" value=\"0\" />
                      ";
            // line 901
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 902
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 906
        echo ($context["help_stock_checkout"] ?? null);
        echo "\">";
        echo ($context["entry_stock_checkout"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 908
        if (($context["config_stock_checkout"] ?? null)) {
            // line 909
            echo "                      <input type=\"radio\" name=\"config_stock_checkout\" value=\"1\" checked=\"checked\" />
                      ";
            // line 910
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 912
            echo "                      <input type=\"radio\" name=\"config_stock_checkout\" value=\"1\" />
                      ";
            // line 913
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 914
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 915
        if ( !($context["config_stock_checkout"] ?? null)) {
            // line 916
            echo "                      <input type=\"radio\" name=\"config_stock_checkout\" value=\"0\" checked=\"checked\" />
                      ";
            // line 917
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 919
            echo "                      <input type=\"radio\" name=\"config_stock_checkout\" value=\"0\" />
                      ";
            // line 920
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 921
        echo " </label>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 926
        echo ($context["text_affiliate"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-affiliate-group\">";
        // line 928
        echo ($context["entry_affiliate_group"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_affiliate_group_id\" id=\"input-affiliate-group\" class=\"form-control\">
                      
                      ";
        // line 932
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["customer_groups"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["customer_group"]) {
            // line 933
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 933) == ($context["config_affiliate_group_id"] ?? null))) {
                // line 934
                echo "                      
                      <option value=\"";
                // line 935
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 935);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "name", [], "any", false, false, false, 935);
                echo "</option>
                      
                      ";
            } else {
                // line 938
                echo "                      
                      <option value=\"";
                // line 939
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "customer_group_id", [], "any", false, false, false, 939);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["customer_group"], "name", [], "any", false, false, false, 939);
                echo "</option>
                      
                      ";
            }
            // line 942
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['customer_group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 943
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 948
        echo ($context["help_affiliate_approval"] ?? null);
        echo "\">";
        echo ($context["entry_affiliate_approval"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 950
        if (($context["config_affiliate_approval"] ?? null)) {
            // line 951
            echo "                      <input type=\"radio\" name=\"config_affiliate_approval\" value=\"1\" checked=\"checked\" />
                      ";
            // line 952
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 954
            echo "                      <input type=\"radio\" name=\"config_affiliate_approval\" value=\"1\" />
                      ";
            // line 955
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 956
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 957
        if ( !($context["config_affiliate_approval"] ?? null)) {
            // line 958
            echo "                      <input type=\"radio\" name=\"config_affiliate_approval\" value=\"0\" checked=\"checked\" />
                      ";
            // line 959
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 961
            echo "                      <input type=\"radio\" name=\"config_affiliate_approval\" value=\"0\" />
                      ";
            // line 962
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 963
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 967
        echo ($context["help_affiliate_auto"] ?? null);
        echo "\">";
        echo ($context["entry_affiliate_auto"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 969
        if (($context["config_affiliate_auto"] ?? null)) {
            // line 970
            echo "                      <input type=\"radio\" name=\"config_affiliate_auto\" value=\"1\" checked=\"checked\" />
                      ";
            // line 971
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 973
            echo "                      <input type=\"radio\" name=\"config_affiliate_auto\" value=\"1\" />
                      ";
            // line 974
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 975
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 976
        if ( !($context["config_affiliate_auto"] ?? null)) {
            // line 977
            echo "                      <input type=\"radio\" name=\"config_affiliate_auto\" value=\"0\" checked=\"checked\" />
                      ";
            // line 978
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 980
            echo "                      <input type=\"radio\" name=\"config_affiliate_auto\" value=\"0\" />
                      ";
            // line 981
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 982
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-affiliate-commission\"><span data-toggle=\"tooltip\" title=\"";
        // line 986
        echo ($context["help_affiliate_commission"] ?? null);
        echo "\">";
        echo ($context["entry_affiliate_commission"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_affiliate_commission\" value=\"";
        // line 988
        echo ($context["config_affiliate_commission"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_affiliate_commission"] ?? null);
        echo "\" id=\"input-affiliate-commission\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-affiliate\"><span data-toggle=\"tooltip\" title=\"";
        // line 992
        echo ($context["help_affiliate"] ?? null);
        echo "\">";
        echo ($context["entry_affiliate"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_affiliate_id\" id=\"input-affiliate\" class=\"form-control\">
                      <option value=\"0\">";
        // line 995
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 997
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["informations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
            // line 998
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 998) == ($context["config_affiliate_id"] ?? null))) {
                // line 999
                echo "                      
                      <option value=\"";
                // line 1000
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 1000);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 1000);
                echo "</option>
                      
                      ";
            } else {
                // line 1003
                echo "                      
                      <option value=\"";
                // line 1004
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 1004);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 1004);
                echo "</option>
                      
                      ";
            }
            // line 1007
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1008
        echo "                    
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 1014
        echo ($context["text_return"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-return\"><span data-toggle=\"tooltip\" title=\"";
        // line 1016
        echo ($context["help_return"] ?? null);
        echo "\">";
        echo ($context["entry_return"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_return_id\" id=\"input-return\" class=\"form-control\">
                      <option value=\"0\">";
        // line 1019
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 1021
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["informations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
            // line 1022
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 1022) == ($context["config_return_id"] ?? null))) {
                // line 1023
                echo "                      
                      <option value=\"";
                // line 1024
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 1024);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 1024);
                echo "</option>
                      
                      ";
            } else {
                // line 1027
                echo "                      
                      <option value=\"";
                // line 1028
                echo twig_get_attribute($this->env, $this->source, $context["information"], "information_id", [], "any", false, false, false, 1028);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 1028);
                echo "</option>
                      
                      ";
            }
            // line 1031
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1032
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-return-status\"><span data-toggle=\"tooltip\" title=\"";
        // line 1037
        echo ($context["help_return_status"] ?? null);
        echo "\">";
        echo ($context["entry_return_status"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_return_status_id\" id=\"input-return-status\" class=\"form-control\">
                      
                      ";
        // line 1041
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["return_statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["return_status"]) {
            // line 1042
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["return_status"], "return_status_id", [], "any", false, false, false, 1042) == ($context["config_return_status_id"] ?? null))) {
                // line 1043
                echo "                      
                      <option value=\"";
                // line 1044
                echo twig_get_attribute($this->env, $this->source, $context["return_status"], "return_status_id", [], "any", false, false, false, 1044);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["return_status"], "name", [], "any", false, false, false, 1044);
                echo "</option>
                      
                      ";
            } else {
                // line 1047
                echo "                      
                      <option value=\"";
                // line 1048
                echo twig_get_attribute($this->env, $this->source, $context["return_status"], "return_status_id", [], "any", false, false, false, 1048);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["return_status"], "name", [], "any", false, false, false, 1048);
                echo "</option>
                      
                      ";
            }
            // line 1051
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['return_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1052
        echo "                    
                    </select>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 1058
        echo ($context["text_captcha"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 1060
        echo ($context["help_captcha"] ?? null);
        echo "\">";
        echo ($context["entry_captcha"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_captcha\" id=\"input-captcha\" class=\"form-control\">
                      <option value=\"\">";
        // line 1063
        echo ($context["text_none"] ?? null);
        echo "</option>
                      
                      ";
        // line 1065
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["captchas"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["captcha"]) {
            // line 1066
            echo "                      ";
            if ((twig_get_attribute($this->env, $this->source, $context["captcha"], "value", [], "any", false, false, false, 1066) == ($context["config_captcha"] ?? null))) {
                // line 1067
                echo "                      
                      <option value=\"";
                // line 1068
                echo twig_get_attribute($this->env, $this->source, $context["captcha"], "value", [], "any", false, false, false, 1068);
                echo "\" selected=\"selected\">";
                echo twig_get_attribute($this->env, $this->source, $context["captcha"], "text", [], "any", false, false, false, 1068);
                echo "</option>
                      
                      ";
            } else {
                // line 1071
                echo "                      
                      <option value=\"";
                // line 1072
                echo twig_get_attribute($this->env, $this->source, $context["captcha"], "value", [], "any", false, false, false, 1072);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["captcha"], "text", [], "any", false, false, false, 1072);
                echo "</option>
                      
                      ";
            }
            // line 1075
            echo "                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['captcha'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1076
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 1081
        echo ($context["entry_captcha_page"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <div class=\"well well-sm\" style=\"height: 150px; overflow: auto;\"> ";
        // line 1083
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["captcha_pages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["captcha_page"]) {
            // line 1084
            echo "                      <div class=\"checkbox\">
                        <label> ";
            // line 1085
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["captcha_page"], "value", [], "any", false, false, false, 1085), ($context["config_captcha_page"] ?? null))) {
                // line 1086
                echo "                          <input type=\"checkbox\" name=\"config_captcha_page[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["captcha_page"], "value", [], "any", false, false, false, 1086);
                echo "\" checked=\"checked\" />
                          ";
                // line 1087
                echo twig_get_attribute($this->env, $this->source, $context["captcha_page"], "text", [], "any", false, false, false, 1087);
                echo "
                          ";
            } else {
                // line 1089
                echo "                          <input type=\"checkbox\" name=\"config_captcha_page[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["captcha_page"], "value", [], "any", false, false, false, 1089);
                echo "\" />
                          ";
                // line 1090
                echo twig_get_attribute($this->env, $this->source, $context["captcha_page"], "text", [], "any", false, false, false, 1090);
                echo "
                          ";
            }
            // line 1091
            echo " </label>
                      </div>
                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['captcha_page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1093
        echo " </div>
                  </div>
                </div>
              </fieldset>
            </div>
            <div class=\"tab-pane\" id=\"tab-image\">
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-logo\">";
        // line 1100
        echo ($context["entry_logo"] ?? null);
        echo "</label>
                <div class=\"col-sm-10\"><a href=\"\" id=\"thumb-logo\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 1101
        echo ($context["logo"] ?? null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo ($context["placeholder"] ?? null);
        echo "\" /></a>
                  <input type=\"hidden\" name=\"config_logo\" value=\"";
        // line 1102
        echo ($context["config_logo"] ?? null);
        echo "\" id=\"input-logo\" />
                </div>
              </div>
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-icon\"><span data-toggle=\"tooltip\" title=\"";
        // line 1106
        echo ($context["help_icon"] ?? null);
        echo "\">";
        echo ($context["entry_icon"] ?? null);
        echo "</span></label>
                <div class=\"col-sm-10\"><a href=\"\" id=\"thumb-icon\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 1107
        echo ($context["icon"] ?? null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo ($context["placeholder"] ?? null);
        echo "\" /></a>
                  <input type=\"hidden\" name=\"config_icon\" value=\"";
        // line 1108
        echo ($context["config_icon"] ?? null);
        echo "\" id=\"input-icon\" />
                </div>
              </div>
            </div>
            <div class=\"tab-pane\" id=\"tab-mail\">
              <fieldset>
                <legend>";
        // line 1114
        echo ($context["text_general"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-engine\"><span data-toggle=\"tooltip\" title=\"";
        // line 1116
        echo ($context["help_mail_engine"] ?? null);
        echo "\">";
        echo ($context["entry_mail_engine"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <select name=\"config_mail_engine\" id=\"input-mail-engine\" class=\"form-control\">
                      
                      ";
        // line 1120
        if ((($context["config_mail_engine"] ?? null) == "mail")) {
            // line 1121
            echo "                      
                      <option value=\"mail\" selected=\"selected\">";
            // line 1122
            echo ($context["text_mail"] ?? null);
            echo "</option>
                      
                      ";
        } else {
            // line 1125
            echo "                      
                      <option value=\"mail\">";
            // line 1126
            echo ($context["text_mail"] ?? null);
            echo "</option>
                      
                      ";
        }
        // line 1129
        echo "                      ";
        if ((($context["config_mail_engine"] ?? null) == "smtp")) {
            // line 1130
            echo "                      
                      <option value=\"smtp\" selected=\"selected\">";
            // line 1131
            echo ($context["text_smtp"] ?? null);
            echo "</option>
                      
                      ";
        } else {
            // line 1134
            echo "                      
                      <option value=\"smtp\">";
            // line 1135
            echo ($context["text_smtp"] ?? null);
            echo "</option>
                      
                      ";
        }
        // line 1138
        echo "                    
                    </select>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-parameter\"><span data-toggle=\"tooltip\" title=\"";
        // line 1143
        echo ($context["help_mail_parameter"] ?? null);
        echo "\">";
        echo ($context["entry_mail_parameter"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_mail_parameter\" value=\"";
        // line 1145
        echo ($context["config_mail_parameter"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_mail_parameter"] ?? null);
        echo "\" id=\"input-mail-parameter\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-smtp-hostname\"><span data-toggle=\"tooltip\" title=\"";
        // line 1149
        echo ($context["help_mail_smtp_hostname"] ?? null);
        echo "\">";
        echo ($context["entry_mail_smtp_hostname"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_mail_smtp_hostname\" value=\"";
        // line 1151
        echo ($context["config_mail_smtp_hostname"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_mail_smtp_hostname"] ?? null);
        echo "\" id=\"input-mail-smtp-hostname\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-smtp-username\">";
        // line 1155
        echo ($context["entry_mail_smtp_username"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_mail_smtp_username\" value=\"";
        // line 1157
        echo ($context["config_mail_smtp_username"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_mail_smtp_username"] ?? null);
        echo "\" id=\"input-mail-smtp-username\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-smtp-password\"><span data-toggle=\"tooltip\" title=\"";
        // line 1161
        echo ($context["help_mail_smtp_password"] ?? null);
        echo "\">";
        echo ($context["entry_mail_smtp_password"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_mail_smtp_password\" value=\"";
        // line 1163
        echo ($context["config_mail_smtp_password"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_mail_smtp_password"] ?? null);
        echo "\" id=\"input-mail-smtp-password\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-smtp-port\">";
        // line 1167
        echo ($context["entry_mail_smtp_port"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_mail_smtp_port\" value=\"";
        // line 1169
        echo ($context["config_mail_smtp_port"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_mail_smtp_port"] ?? null);
        echo "\" id=\"input-mail-smtp-port\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-smtp-timeout\">";
        // line 1173
        echo ($context["entry_mail_smtp_timeout"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_mail_smtp_timeout\" value=\"";
        // line 1175
        echo ($context["config_mail_smtp_timeout"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_mail_smtp_timeout"] ?? null);
        echo "\" id=\"input-mail-smtp-timeout\" class=\"form-control\" />
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 1180
        echo ($context["text_mail_alert"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 1182
        echo ($context["help_mail_alert"] ?? null);
        echo "\">";
        echo ($context["entry_mail_alert"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <div class=\"well well-sm\" style=\"height: 150px; overflow: auto;\"> ";
        // line 1184
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["mail_alerts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["mail_alert"]) {
            // line 1185
            echo "                      <div class=\"checkbox\">
                        <label> ";
            // line 1186
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["mail_alert"], "value", [], "any", false, false, false, 1186), ($context["config_mail_alert"] ?? null))) {
                // line 1187
                echo "                          <input type=\"checkbox\" name=\"config_mail_alert[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["mail_alert"], "value", [], "any", false, false, false, 1187);
                echo "\" checked=\"checked\" />
                          ";
                // line 1188
                echo twig_get_attribute($this->env, $this->source, $context["mail_alert"], "text", [], "any", false, false, false, 1188);
                echo "
                          ";
            } else {
                // line 1190
                echo "                          <input type=\"checkbox\" name=\"config_mail_alert[]\" value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["mail_alert"], "value", [], "any", false, false, false, 1190);
                echo "\" />
                          ";
                // line 1191
                echo twig_get_attribute($this->env, $this->source, $context["mail_alert"], "text", [], "any", false, false, false, 1191);
                echo "
                          ";
            }
            // line 1192
            echo " </label>
                      </div>
                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mail_alert'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1194
        echo " </div>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-mail-alert-email\"><span data-toggle=\"tooltip\" title=\"";
        // line 1198
        echo ($context["help_mail_alert_email"] ?? null);
        echo "\">";
        echo ($context["entry_mail_alert_email"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <textarea name=\"config_mail_alert_email\" rows=\"5\" placeholder=\"";
        // line 1200
        echo ($context["entry_mail_alert_email"] ?? null);
        echo "\" id=\"input-alert-email\" class=\"form-control\">";
        echo ($context["config_mail_alert_email"] ?? null);
        echo "</textarea>
                  </div>
                </div>
              </fieldset>
            </div>
            <div class=\"tab-pane\" id=\"tab-server\">
              <fieldset>
                <legend>";
        // line 1207
        echo ($context["text_general"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 1209
        echo ($context["help_seo_url"] ?? null);
        echo "\">";
        echo ($context["entry_seo_url"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 1211
        if (($context["config_seo_url"] ?? null)) {
            // line 1212
            echo "                      <input type=\"radio\" name=\"config_seo_url\" value=\"1\" checked=\"checked\" />
                      ";
            // line 1213
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 1215
            echo "                      <input type=\"radio\" name=\"config_seo_url\" value=\"1\" />
                      ";
            // line 1216
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 1217
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 1218
        if ( !($context["config_seo_url"] ?? null)) {
            // line 1219
            echo "                      <input type=\"radio\" name=\"config_seo_url\" value=\"0\" checked=\"checked\" />
                      ";
            // line 1220
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 1222
            echo "                      <input type=\"radio\" name=\"config_seo_url\" value=\"0\" />
                      ";
            // line 1223
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 1224
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-robots\"><span data-toggle=\"tooltip\" title=\"";
        // line 1228
        echo ($context["help_robots"] ?? null);
        echo "\">";
        echo ($context["entry_robots"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <textarea name=\"config_robots\" rows=\"5\" placeholder=\"";
        // line 1230
        echo ($context["entry_robots"] ?? null);
        echo "\" id=\"input-robots\" class=\"form-control\">";
        echo ($context["config_robots"] ?? null);
        echo "</textarea>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-compression\"><span data-toggle=\"tooltip\" title=\"";
        // line 1234
        echo ($context["help_compression"] ?? null);
        echo "\">";
        echo ($context["entry_compression"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_compression\" value=\"";
        // line 1236
        echo ($context["config_compression"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_compression"] ?? null);
        echo "\" id=\"input-compression\" class=\"form-control\" />
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 1241
        echo ($context["text_security"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 1243
        echo ($context["help_secure"] ?? null);
        echo "\">";
        echo ($context["entry_secure"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 1245
        if (($context["config_secure"] ?? null)) {
            // line 1246
            echo "                      <input type=\"radio\" name=\"config_secure\" value=\"1\" checked=\"checked\" />
                      ";
            // line 1247
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 1249
            echo "                      <input type=\"radio\" name=\"config_secure\" value=\"1\" />
                      ";
            // line 1250
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 1251
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 1252
        if ( !($context["config_secure"] ?? null)) {
            // line 1253
            echo "                      <input type=\"radio\" name=\"config_secure\" value=\"0\" checked=\"checked\" />
                      ";
            // line 1254
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 1256
            echo "                      <input type=\"radio\" name=\"config_secure\" value=\"0\" />
                      ";
            // line 1257
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 1258
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 1262
        echo ($context["help_password"] ?? null);
        echo "\">";
        echo ($context["entry_password"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 1264
        if (($context["config_password"] ?? null)) {
            // line 1265
            echo "                      <input type=\"radio\" name=\"config_password\" value=\"1\" checked=\"checked\" />
                      ";
            // line 1266
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 1268
            echo "                      <input type=\"radio\" name=\"config_password\" value=\"1\" />
                      ";
            // line 1269
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 1270
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 1271
        if ( !($context["config_password"] ?? null)) {
            // line 1272
            echo "                      <input type=\"radio\" name=\"config_password\" value=\"0\" checked=\"checked\" />
                      ";
            // line 1273
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 1275
            echo "                      <input type=\"radio\" name=\"config_password\" value=\"0\" />
                      ";
            // line 1276
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 1277
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"";
        // line 1281
        echo ($context["help_shared"] ?? null);
        echo "\">";
        echo ($context["entry_shared"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 1283
        if (($context["config_shared"] ?? null)) {
            // line 1284
            echo "                      <input type=\"radio\" name=\"config_shared\" value=\"1\" checked=\"checked\" />
                      ";
            // line 1285
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 1287
            echo "                      <input type=\"radio\" name=\"config_shared\" value=\"1\" />
                      ";
            // line 1288
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 1289
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 1290
        if ( !($context["config_shared"] ?? null)) {
            // line 1291
            echo "                      <input type=\"radio\" name=\"config_shared\" value=\"0\" checked=\"checked\" />
                      ";
            // line 1292
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 1294
            echo "                      <input type=\"radio\" name=\"config_shared\" value=\"0\" />
                      ";
            // line 1295
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 1296
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-encryption\"><span data-toggle=\"tooltip\" title=\"";
        // line 1300
        echo ($context["help_encryption"] ?? null);
        echo "\">";
        echo ($context["entry_encryption"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <textarea name=\"config_encryption\" rows=\"5\" placeholder=\"";
        // line 1302
        echo ($context["entry_encryption"] ?? null);
        echo "\" id=\"input-encryption\" class=\"form-control\">";
        echo ($context["config_encryption"] ?? null);
        echo "</textarea>
                    ";
        // line 1303
        if (($context["error_encryption"] ?? null)) {
            // line 1304
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_encryption"] ?? null);
            echo "</div>
                    ";
        }
        // line 1305
        echo " </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 1309
        echo ($context["text_upload"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-file-max-size\"><span data-toggle=\"tooltip\" title=\"";
        // line 1311
        echo ($context["help_file_max_size"] ?? null);
        echo "\">";
        echo ($context["entry_file_max_size"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_file_max_size\" value=\"";
        // line 1313
        echo ($context["config_file_max_size"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_file_max_size"] ?? null);
        echo "\" id=\"input-file-max-size\" class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-file-ext-allowed\"><span data-toggle=\"tooltip\" title=\"";
        // line 1317
        echo ($context["help_file_ext_allowed"] ?? null);
        echo "\">";
        echo ($context["entry_file_ext_allowed"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <textarea name=\"config_file_ext_allowed\" rows=\"5\" placeholder=\"";
        // line 1319
        echo ($context["entry_file_ext_allowed"] ?? null);
        echo "\" id=\"input-file-ext-allowed\" class=\"form-control\">";
        echo ($context["config_file_ext_allowed"] ?? null);
        echo "</textarea>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-file-mime-allowed\"><span data-toggle=\"tooltip\" title=\"";
        // line 1323
        echo ($context["help_file_mime_allowed"] ?? null);
        echo "\">";
        echo ($context["entry_file_mime_allowed"] ?? null);
        echo "</span></label>
                  <div class=\"col-sm-10\">
                    <textarea name=\"config_file_mime_allowed\" rows=\"5\" placeholder=\"";
        // line 1325
        echo ($context["entry_file_mime_allowed"] ?? null);
        echo "\" id=\"input-file-mime-allowed\" class=\"form-control\">";
        echo ($context["config_file_mime_allowed"] ?? null);
        echo "</textarea>
                  </div>
                </div>
              </fieldset>
              <fieldset>
                <legend>";
        // line 1330
        echo ($context["text_error"] ?? null);
        echo "</legend>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 1332
        echo ($context["entry_error_display"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 1334
        if (($context["config_error_display"] ?? null)) {
            // line 1335
            echo "                      <input type=\"radio\" name=\"config_error_display\" value=\"1\" checked=\"checked\" />
                      ";
            // line 1336
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 1338
            echo "                      <input type=\"radio\" name=\"config_error_display\" value=\"1\" />
                      ";
            // line 1339
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 1340
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 1341
        if ( !($context["config_error_display"] ?? null)) {
            // line 1342
            echo "                      <input type=\"radio\" name=\"config_error_display\" value=\"0\" checked=\"checked\" />
                      ";
            // line 1343
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 1345
            echo "                      <input type=\"radio\" name=\"config_error_display\" value=\"0\" />
                      ";
            // line 1346
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 1347
        echo " </label>
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 1351
        echo ($context["entry_error_log"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <label class=\"radio-inline\"> ";
        // line 1353
        if (($context["config_error_log"] ?? null)) {
            // line 1354
            echo "                      <input type=\"radio\" name=\"config_error_log\" value=\"1\" checked=\"checked\" />
                      ";
            // line 1355
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        } else {
            // line 1357
            echo "                      <input type=\"radio\" name=\"config_error_log\" value=\"1\" />
                      ";
            // line 1358
            echo ($context["text_yes"] ?? null);
            echo "
                      ";
        }
        // line 1359
        echo " </label>
                    <label class=\"radio-inline\"> ";
        // line 1360
        if ( !($context["config_error_log"] ?? null)) {
            // line 1361
            echo "                      <input type=\"radio\" name=\"config_error_log\" value=\"0\" checked=\"checked\" />
                      ";
            // line 1362
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        } else {
            // line 1364
            echo "                      <input type=\"radio\" name=\"config_error_log\" value=\"0\" />
                      ";
            // line 1365
            echo ($context["text_no"] ?? null);
            echo "
                      ";
        }
        // line 1366
        echo "</label>
                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\" for=\"input-error-filename\">";
        // line 1370
        echo ($context["entry_error_filename"] ?? null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"config_error_filename\" value=\"";
        // line 1372
        echo ($context["config_error_filename"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_error_filename"] ?? null);
        echo "\" id=\"input-error-filename\" class=\"form-control\" />
                    ";
        // line 1373
        if (($context["error_log"] ?? null)) {
            // line 1374
            echo "                    <div class=\"text-danger\">";
            echo ($context["error_log"] ?? null);
            echo "</div>
                    ";
        }
        // line 1375
        echo " </div>
                </div>
              </fieldset>
            </div>
            <!-- custom -->
            <div class=\"tab-pane\" id=\"tab-redesocial\">
              <div class=\"row\">
                <div class=\"col-sm-12 col-md-6\">
                    <fieldset>
                    <legend>Facebook</legend>
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Exibe a Box do Facebook no rodapé da Loja.\">Habilitar</span></label>
                      <div class=\"col-sm-10\">
                        <label class=\"radio-inline\"> ";
        // line 1388
        if (($context["config_facebook"] ?? null)) {
            // line 1389
            echo "                          <input type=\"radio\" name=\"config_facebook\" value=\"1\" checked=\"checked\" />
                          ";
            // line 1390
            echo ($context["text_yes"] ?? null);
            echo "
                          ";
        } else {
            // line 1392
            echo "                          <input type=\"radio\" name=\"config_facebook\" value=\"1\" />
                          ";
            // line 1393
            echo ($context["text_yes"] ?? null);
            echo "
                          ";
        }
        // line 1394
        echo " </label>
                        <label class=\"radio-inline\"> ";
        // line 1395
        if ( !($context["config_facebook"] ?? null)) {
            // line 1396
            echo "                          <input type=\"radio\" name=\"config_facebook\" value=\"0\" checked=\"checked\" />
                          ";
            // line 1397
            echo ($context["text_no"] ?? null);
            echo "
                          ";
        } else {
            // line 1399
            echo "                          <input type=\"radio\" name=\"config_facebook\" value=\"0\" />
                          ";
            // line 1400
            echo ($context["text_no"] ?? null);
            echo "
                          ";
        }
        // line 1401
        echo " </label>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Coloque a URL completa da sua Página do Facebook.\">URL</span></label>
                      <div class=\"col-sm-10\">
                        <input type=\"text\" name=\"config_facebook_url\" value=\"";
        // line 1407
        echo ($context["config_facebook_url"] ?? null);
        echo "\" placeholder=\"https://www.facebook.com/innsystem/\" id=\"input-config-facebook-url\" class=\"form-control\" />
                        <span class=\"help-block\">É necessário colocar a URL completa de sua página. Por exemplo: <i>https://www.facebook.com/innsystem/</i></span>
                      </div>
                    </div><!-- form-group -->
                  </fieldset>
                  <fieldset>
                    <legend>Instagram</legend>
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Exibe a Widget do Instagram no rodapé da Loja.\">Habilitar</span></label>
                      <div class=\"col-sm-10\">
                        <label class=\"radio-inline\"> ";
        // line 1417
        if (($context["config_instagram"] ?? null)) {
            // line 1418
            echo "                          <input type=\"radio\" name=\"config_instagram\" value=\"1\" checked=\"checked\" />
                          ";
            // line 1419
            echo ($context["text_yes"] ?? null);
            echo "
                          ";
        } else {
            // line 1421
            echo "                          <input type=\"radio\" name=\"config_instagram\" value=\"1\" />
                          ";
            // line 1422
            echo ($context["text_yes"] ?? null);
            echo "
                          ";
        }
        // line 1423
        echo " </label>
                        <label class=\"radio-inline\"> ";
        // line 1424
        if ( !($context["config_instagram"] ?? null)) {
            // line 1425
            echo "                          <input type=\"radio\" name=\"config_instagram\" value=\"0\" checked=\"checked\" />
                          ";
            // line 1426
            echo ($context["text_no"] ?? null);
            echo "
                          ";
        } else {
            // line 1428
            echo "                          <input type=\"radio\" name=\"config_instagram\" value=\"0\" />
                          ";
            // line 1429
            echo ($context["text_no"] ?? null);
            echo "
                          ";
        }
        // line 1430
        echo " </label>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"É necessário colocar apenas o nickname da sua página. Por exemplo: innsystem\">Nickname</span></label>
                      <div class=\"col-sm-10\">
                        <input type=\"text\" name=\"config_instagram_url\" value=\"";
        // line 1436
        echo ($context["config_instagram_url"] ?? null);
        echo "\" placeholder=\"innsystem\" id=\"input-config-instagram-url\" class=\"form-control\" />
                        <span class=\"help-block\">É necessário colocar apenas o nickname da sua página. Por exemplo: <i>innsystem</i></span>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"É necessário se registrar no site SnapWidget, criando seu widget será gerado um código. Dúvida entre em contato com nosso suporte.\">Código Widget</span></label>
                      <div class=\"col-sm-10\">
                        <input type=\"text\" name=\"config_instagram_widget\" value=\"";
        // line 1443
        echo ($context["config_instagram_widget"] ?? null);
        echo "\" placeholder=\"Código Widget\" id=\"input-config-instagram-widget\" class=\"form-control\" />
                        <span class=\"help-block\">É necessário se registrar no site <a href=\"https://snapwidget.com/\" target=\"_Blank\">SnapWidget</a>, criando seu widget será gerado um código. Dúvida entre em contato com nosso suporte.</span>
                      </div>
                    </div><!-- form-group -->
                  </fieldset>                
                </div><!-- col -->
                <div class=\"col-sm-12 col-md-6 pull-righ\">
                  <fieldset>
                    <legend>Botão Flutuante</legend>
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Exibe a Widget do Instagram no rodapé da Loja.\">Habilitar</span></label>
                      <div class=\"col-sm-10\">
                        <label class=\"radio-inline\"> ";
        // line 1455
        if (($context["config_bfloat"] ?? null)) {
            // line 1456
            echo "                          <input type=\"radio\" name=\"config_bfloat\" value=\"1\" checked=\"checked\" />
                          ";
            // line 1457
            echo ($context["text_yes"] ?? null);
            echo "
                          ";
        } else {
            // line 1459
            echo "                          <input type=\"radio\" name=\"config_bfloat\" value=\"1\" />
                          ";
            // line 1460
            echo ($context["text_yes"] ?? null);
            echo "
                          ";
        }
        // line 1461
        echo " </label>
                        <label class=\"radio-inline\"> ";
        // line 1462
        if ( !($context["config_bfloat"] ?? null)) {
            // line 1463
            echo "                          <input type=\"radio\" name=\"config_bfloat\" value=\"0\" checked=\"checked\" />
                          ";
            // line 1464
            echo ($context["text_no"] ?? null);
            echo "
                          ";
        } else {
            // line 1466
            echo "                          <input type=\"radio\" name=\"config_bfloat\" value=\"0\" />
                          ";
            // line 1467
            echo ($context["text_no"] ?? null);
            echo "
                          ";
        }
        // line 1468
        echo " </label>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\">
                        <span data-toggle=\"tooltip\" title=\"Exemplo: (00) 9999-9999\">WhatsApp</span></label>
                      <div class=\"col-sm-10\">
                        <input type=\"text\" name=\"config_bfloat_whatsapp\" value=\"";
        // line 1475
        echo ($context["config_bfloat_whatsapp"] ?? null);
        echo "\" placeholder=\"(00) 9999-9999\" id=\"input-config-bfloat-whatsapp\" class=\"form-control maskPhone\" />
                        <span class=\"help-block\">Exemplo: <i>(00) 9999-9999</i></span>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\">
                        <span data-toggle=\"tooltip\" title=\"Exemplo: innsystem\">Facebook</span></label>
                      <div class=\"col-sm-10\">
                        <div class=\"input-group\">
                          <span class=\"input-group-addon\" id=\"basic-addon1\">https://facebook.com/</span>
                          <input type=\"text\" name=\"config_bfloat_facebook\" value=\"";
        // line 1485
        echo ($context["config_bfloat_facebook"] ?? null);
        echo "\" placeholder=\"suapagina\" id=\"input-config-bfloat-facebook\" class=\"form-control\" />
                        </div>                        
                        <span class=\"help-block\">Exemplo: <i>innsystem</i></span>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\">
                        <span data-toggle=\"tooltip\" title=\"Exemplo: innsystem\">Messeger</span></label>
                      <div class=\"col-sm-10\">
                        <div class=\"input-group\">
                          <span class=\"input-group-addon\" id=\"basic-addon1\">https://m.me/</span>
                          <input type=\"text\" name=\"config_bfloat_messenger\" value=\"";
        // line 1496
        echo ($context["config_bfloat_messenger"] ?? null);
        echo "\" placeholder=\"suapagina\" id=\"input-config-bfloat-messenger\" class=\"form-control\" />
                        </div>                        
                        <span class=\"help-block\">Exemplo: <i>innsystem</i></span>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\">
                        <span data-toggle=\"tooltip\" title=\"Exemplo: innsystem\">Instagram</span></label>
                      <div class=\"col-sm-10\">
                        <div class=\"input-group\">
                          <span class=\"input-group-addon\" id=\"basic-addon1\">https://instagram.com/</span>
                          <input type=\"text\" name=\"config_bfloat_instagram\" value=\"";
        // line 1507
        echo ($context["config_bfloat_instagram"] ?? null);
        echo "\" placeholder=\"suapagina\" id=\"input-config-bfloat-instagram\" class=\"form-control\" />
                        </div>                        
                        <span class=\"help-block\">Exemplo: <i>innsystem</i></span>
                      </div>
                    </div><!-- form-group -->
                    <div class=\"form-group\">
                      <label class=\"col-sm-2 control-label\">
                        <span data-toggle=\"tooltip\" title=\"Insira seu e-mail para contato\">E-mail</span></label>
                      <div class=\"col-sm-10\">
                          <input type=\"text\" name=\"config_bfloat_mail\" value=\"";
        // line 1516
        echo ($context["config_bfloat_mail"] ?? null);
        echo "\" placeholder=\"email@dominio.com.br\" id=\"input-config-bfloat-mail\" class=\"form-control\" />                
                        <span class=\"help-block\">Exemplo: <i>contato@dominio.com.br</i></span>
                      </div>
                    </div><!-- form-group -->
                  </fieldset>  
                </div><!-- col-md-5 -->
              </div><!-- row -->

            </div>
            <!-- custom -->
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type=\"text/javascript\"><!--
\$('select[name=\\'config_theme\\']').on('change', function() {
\t\$.ajax({
\t\turl: 'index.php?route=setting/setting/theme&user_token=";
        // line 1534
        echo ($context["user_token"] ?? null);
        echo "&theme=' + this.value,
\t\tdataType: 'html',
\t\tbeforeSend: function() {
\t\t\t\$('select[name=\\'config_theme\\']').prop('disabled', true);
\t\t},
\t\tcomplete: function() {
\t\t\t\$('select[name=\\'config_theme\\']').prop('disabled', false);
\t\t},
\t\tsuccess: function(html) {
\t\t\t\$('#theme').attr('src', html);
\t\t},
\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t}
\t});
});

\$('select[name=\\'config_theme\\']').trigger('change');
//--></script> 
  <script type=\"text/javascript\"><!--
\$('select[name=\\'config_country_id\\']').on('change', function() {
\t\$.ajax({
\t\turl: 'index.php?route=localisation/country/country&user_token=";
        // line 1556
        echo ($context["user_token"] ?? null);
        echo "&country_id=' + this.value,
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('select[name=\\'config_country_id\\']').prop('disabled', true);
\t\t},
\t\tcomplete: function() {
\t\t\t\$('select[name=\\'config_country_id\\']').prop('disabled', false);
\t\t},
\t\tsuccess: function(json) {
\t\t\thtml = '<option value=\"\">";
        // line 1565
        echo ($context["text_select"] ?? null);
        echo "</option>';

\t\t\tif (json['zone'] && json['zone'] != '') {
\t\t\t\tfor (i = 0; i < json['zone'].length; i++) {
          \t\t\thtml += '<option value=\"' + json['zone'][i]['zone_id'] + '\"';

\t\t\t\t\tif (json['zone'][i]['zone_id'] == '";
        // line 1571
        echo ($context["config_zone_id"] ?? null);
        echo "') {
            \t\t\thtml += ' selected=\"selected\"';
\t\t\t\t\t}

\t\t\t\t\thtml += '>' + json['zone'][i]['name'] + '</option>';
\t\t\t\t}
\t\t\t} else {
\t\t\t\thtml += '<option value=\"0\" selected=\"selected\">";
        // line 1578
        echo ($context["text_none"] ?? null);
        echo "</option>';
\t\t\t}

\t\t\t\$('select[name=\\'config_zone_id\\']').html(html);
\t\t\t
\t\t\t\$('#button-save').prop('disabled', false);
\t\t},
\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t}
\t});
});

\$('select[name=\\'config_country_id\\']').trigger('change');
//--></script></div>

<script>
  
\$('input[name=\"config_meta_title\"]').on('keyup', function() {
  \$('input[name=\"config_name\"]').val(\$(this).val());
});

\$('input[name=\"input-email\"]').on('keyup', function() {
  \$('input[name=\"config_bfloat_mail\"]').val(\$(this).val());
});

</script>
";
        // line 1605
        echo ($context["footer"] ?? null);
        echo " ";
    }

    public function getTemplateName()
    {
        return "setting/setting.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  4025 => 1605,  3995 => 1578,  3985 => 1571,  3976 => 1565,  3964 => 1556,  3939 => 1534,  3918 => 1516,  3906 => 1507,  3892 => 1496,  3878 => 1485,  3865 => 1475,  3856 => 1468,  3851 => 1467,  3848 => 1466,  3843 => 1464,  3840 => 1463,  3838 => 1462,  3835 => 1461,  3830 => 1460,  3827 => 1459,  3822 => 1457,  3819 => 1456,  3817 => 1455,  3802 => 1443,  3792 => 1436,  3784 => 1430,  3779 => 1429,  3776 => 1428,  3771 => 1426,  3768 => 1425,  3766 => 1424,  3763 => 1423,  3758 => 1422,  3755 => 1421,  3750 => 1419,  3747 => 1418,  3745 => 1417,  3732 => 1407,  3724 => 1401,  3719 => 1400,  3716 => 1399,  3711 => 1397,  3708 => 1396,  3706 => 1395,  3703 => 1394,  3698 => 1393,  3695 => 1392,  3690 => 1390,  3687 => 1389,  3685 => 1388,  3670 => 1375,  3664 => 1374,  3662 => 1373,  3656 => 1372,  3651 => 1370,  3645 => 1366,  3640 => 1365,  3637 => 1364,  3632 => 1362,  3629 => 1361,  3627 => 1360,  3624 => 1359,  3619 => 1358,  3616 => 1357,  3611 => 1355,  3608 => 1354,  3606 => 1353,  3601 => 1351,  3595 => 1347,  3590 => 1346,  3587 => 1345,  3582 => 1343,  3579 => 1342,  3577 => 1341,  3574 => 1340,  3569 => 1339,  3566 => 1338,  3561 => 1336,  3558 => 1335,  3556 => 1334,  3551 => 1332,  3546 => 1330,  3536 => 1325,  3529 => 1323,  3520 => 1319,  3513 => 1317,  3504 => 1313,  3497 => 1311,  3492 => 1309,  3486 => 1305,  3480 => 1304,  3478 => 1303,  3472 => 1302,  3465 => 1300,  3459 => 1296,  3454 => 1295,  3451 => 1294,  3446 => 1292,  3443 => 1291,  3441 => 1290,  3438 => 1289,  3433 => 1288,  3430 => 1287,  3425 => 1285,  3422 => 1284,  3420 => 1283,  3413 => 1281,  3407 => 1277,  3402 => 1276,  3399 => 1275,  3394 => 1273,  3391 => 1272,  3389 => 1271,  3386 => 1270,  3381 => 1269,  3378 => 1268,  3373 => 1266,  3370 => 1265,  3368 => 1264,  3361 => 1262,  3355 => 1258,  3350 => 1257,  3347 => 1256,  3342 => 1254,  3339 => 1253,  3337 => 1252,  3334 => 1251,  3329 => 1250,  3326 => 1249,  3321 => 1247,  3318 => 1246,  3316 => 1245,  3309 => 1243,  3304 => 1241,  3294 => 1236,  3287 => 1234,  3278 => 1230,  3271 => 1228,  3265 => 1224,  3260 => 1223,  3257 => 1222,  3252 => 1220,  3249 => 1219,  3247 => 1218,  3244 => 1217,  3239 => 1216,  3236 => 1215,  3231 => 1213,  3228 => 1212,  3226 => 1211,  3219 => 1209,  3214 => 1207,  3202 => 1200,  3195 => 1198,  3189 => 1194,  3181 => 1192,  3176 => 1191,  3171 => 1190,  3166 => 1188,  3161 => 1187,  3159 => 1186,  3156 => 1185,  3152 => 1184,  3145 => 1182,  3140 => 1180,  3130 => 1175,  3125 => 1173,  3116 => 1169,  3111 => 1167,  3102 => 1163,  3095 => 1161,  3086 => 1157,  3081 => 1155,  3072 => 1151,  3065 => 1149,  3056 => 1145,  3049 => 1143,  3042 => 1138,  3036 => 1135,  3033 => 1134,  3027 => 1131,  3024 => 1130,  3021 => 1129,  3015 => 1126,  3012 => 1125,  3006 => 1122,  3003 => 1121,  3001 => 1120,  2992 => 1116,  2987 => 1114,  2978 => 1108,  2972 => 1107,  2966 => 1106,  2959 => 1102,  2953 => 1101,  2949 => 1100,  2940 => 1093,  2932 => 1091,  2927 => 1090,  2922 => 1089,  2917 => 1087,  2912 => 1086,  2910 => 1085,  2907 => 1084,  2903 => 1083,  2898 => 1081,  2891 => 1076,  2885 => 1075,  2877 => 1072,  2874 => 1071,  2866 => 1068,  2863 => 1067,  2860 => 1066,  2856 => 1065,  2851 => 1063,  2843 => 1060,  2838 => 1058,  2830 => 1052,  2824 => 1051,  2816 => 1048,  2813 => 1047,  2805 => 1044,  2802 => 1043,  2799 => 1042,  2795 => 1041,  2786 => 1037,  2779 => 1032,  2773 => 1031,  2765 => 1028,  2762 => 1027,  2754 => 1024,  2751 => 1023,  2748 => 1022,  2744 => 1021,  2739 => 1019,  2731 => 1016,  2726 => 1014,  2718 => 1008,  2712 => 1007,  2704 => 1004,  2701 => 1003,  2693 => 1000,  2690 => 999,  2687 => 998,  2683 => 997,  2678 => 995,  2670 => 992,  2661 => 988,  2654 => 986,  2648 => 982,  2643 => 981,  2640 => 980,  2635 => 978,  2632 => 977,  2630 => 976,  2627 => 975,  2622 => 974,  2619 => 973,  2614 => 971,  2611 => 970,  2609 => 969,  2602 => 967,  2596 => 963,  2591 => 962,  2588 => 961,  2583 => 959,  2580 => 958,  2578 => 957,  2575 => 956,  2570 => 955,  2567 => 954,  2562 => 952,  2559 => 951,  2557 => 950,  2550 => 948,  2543 => 943,  2537 => 942,  2529 => 939,  2526 => 938,  2518 => 935,  2515 => 934,  2512 => 933,  2508 => 932,  2501 => 928,  2496 => 926,  2489 => 921,  2484 => 920,  2481 => 919,  2476 => 917,  2473 => 916,  2471 => 915,  2468 => 914,  2463 => 913,  2460 => 912,  2455 => 910,  2452 => 909,  2450 => 908,  2443 => 906,  2437 => 902,  2432 => 901,  2429 => 900,  2424 => 898,  2421 => 897,  2419 => 896,  2416 => 895,  2411 => 894,  2408 => 893,  2403 => 891,  2400 => 890,  2398 => 889,  2391 => 887,  2385 => 883,  2380 => 882,  2377 => 881,  2372 => 879,  2369 => 878,  2367 => 877,  2364 => 876,  2359 => 875,  2356 => 874,  2351 => 872,  2348 => 871,  2346 => 870,  2339 => 868,  2334 => 866,  2326 => 860,  2320 => 859,  2312 => 856,  2309 => 855,  2301 => 852,  2298 => 851,  2295 => 850,  2291 => 849,  2286 => 847,  2278 => 844,  2271 => 839,  2265 => 838,  2257 => 835,  2254 => 834,  2246 => 831,  2243 => 830,  2240 => 829,  2236 => 828,  2227 => 824,  2222 => 821,  2216 => 820,  2214 => 819,  2211 => 818,  2203 => 816,  2198 => 815,  2193 => 814,  2188 => 812,  2183 => 811,  2181 => 810,  2178 => 809,  2174 => 808,  2167 => 806,  2162 => 803,  2156 => 802,  2154 => 801,  2151 => 800,  2143 => 798,  2138 => 797,  2133 => 796,  2128 => 794,  2123 => 793,  2121 => 792,  2118 => 791,  2114 => 790,  2107 => 788,  2100 => 783,  2094 => 782,  2086 => 779,  2083 => 778,  2075 => 775,  2072 => 774,  2069 => 773,  2065 => 772,  2056 => 768,  2049 => 763,  2043 => 762,  2035 => 759,  2032 => 758,  2024 => 755,  2021 => 754,  2018 => 753,  2014 => 752,  2009 => 750,  2001 => 747,  1995 => 743,  1990 => 742,  1987 => 741,  1982 => 739,  1979 => 738,  1977 => 737,  1974 => 736,  1969 => 735,  1966 => 734,  1961 => 732,  1958 => 731,  1956 => 730,  1949 => 728,  1943 => 724,  1938 => 723,  1935 => 722,  1930 => 720,  1927 => 719,  1925 => 718,  1922 => 717,  1917 => 716,  1914 => 715,  1909 => 713,  1906 => 712,  1904 => 711,  1897 => 709,  1888 => 705,  1881 => 703,  1876 => 701,  1868 => 695,  1862 => 694,  1854 => 691,  1851 => 690,  1843 => 687,  1840 => 686,  1837 => 685,  1833 => 684,  1828 => 682,  1820 => 679,  1815 => 676,  1809 => 675,  1807 => 674,  1801 => 673,  1794 => 671,  1788 => 667,  1783 => 666,  1780 => 665,  1775 => 663,  1772 => 662,  1770 => 661,  1767 => 660,  1762 => 659,  1759 => 658,  1754 => 656,  1751 => 655,  1749 => 654,  1742 => 652,  1737 => 649,  1731 => 648,  1728 => 647,  1720 => 644,  1715 => 643,  1710 => 642,  1705 => 640,  1700 => 639,  1698 => 638,  1695 => 637,  1691 => 636,  1685 => 635,  1678 => 630,  1672 => 629,  1664 => 626,  1661 => 625,  1653 => 622,  1650 => 621,  1647 => 620,  1643 => 619,  1634 => 615,  1628 => 611,  1623 => 610,  1620 => 609,  1615 => 607,  1612 => 606,  1610 => 605,  1607 => 604,  1602 => 603,  1599 => 602,  1594 => 600,  1591 => 599,  1589 => 598,  1584 => 596,  1578 => 592,  1573 => 591,  1570 => 590,  1565 => 588,  1562 => 587,  1560 => 586,  1557 => 585,  1552 => 584,  1549 => 583,  1544 => 581,  1541 => 580,  1539 => 579,  1532 => 577,  1526 => 573,  1521 => 572,  1518 => 571,  1513 => 569,  1510 => 568,  1508 => 567,  1505 => 566,  1500 => 565,  1497 => 564,  1492 => 562,  1489 => 561,  1487 => 560,  1480 => 558,  1475 => 556,  1467 => 550,  1461 => 547,  1458 => 546,  1452 => 543,  1449 => 542,  1446 => 541,  1440 => 538,  1437 => 537,  1431 => 534,  1428 => 533,  1426 => 532,  1421 => 530,  1413 => 527,  1406 => 522,  1400 => 519,  1397 => 518,  1391 => 515,  1388 => 514,  1385 => 513,  1379 => 510,  1376 => 509,  1370 => 506,  1367 => 505,  1365 => 504,  1360 => 502,  1352 => 499,  1346 => 495,  1341 => 494,  1338 => 493,  1333 => 491,  1330 => 490,  1328 => 489,  1325 => 488,  1320 => 487,  1317 => 486,  1312 => 484,  1309 => 483,  1307 => 482,  1302 => 480,  1297 => 478,  1291 => 474,  1285 => 473,  1283 => 472,  1277 => 471,  1270 => 469,  1265 => 466,  1259 => 465,  1257 => 464,  1251 => 463,  1244 => 461,  1239 => 459,  1232 => 454,  1227 => 453,  1224 => 452,  1219 => 450,  1216 => 449,  1214 => 448,  1211 => 447,  1206 => 446,  1203 => 445,  1198 => 443,  1195 => 442,  1193 => 441,  1186 => 439,  1180 => 435,  1175 => 434,  1172 => 433,  1167 => 431,  1164 => 430,  1162 => 429,  1159 => 428,  1154 => 427,  1151 => 426,  1146 => 424,  1143 => 423,  1141 => 422,  1134 => 420,  1129 => 418,  1123 => 414,  1117 => 413,  1115 => 412,  1109 => 411,  1102 => 409,  1096 => 405,  1091 => 404,  1088 => 403,  1083 => 401,  1080 => 400,  1078 => 399,  1075 => 398,  1070 => 397,  1067 => 396,  1062 => 394,  1059 => 393,  1057 => 392,  1050 => 390,  1045 => 388,  1038 => 383,  1032 => 382,  1030 => 381,  1024 => 380,  1017 => 378,  1012 => 376,  1003 => 369,  997 => 368,  989 => 365,  986 => 364,  978 => 361,  975 => 360,  972 => 359,  968 => 358,  961 => 354,  954 => 349,  948 => 348,  940 => 345,  937 => 344,  929 => 341,  926 => 340,  923 => 339,  919 => 338,  912 => 334,  906 => 330,  901 => 329,  898 => 328,  893 => 326,  890 => 325,  888 => 324,  885 => 323,  880 => 322,  877 => 321,  872 => 319,  869 => 318,  867 => 317,  860 => 315,  853 => 310,  847 => 309,  839 => 306,  836 => 305,  828 => 302,  825 => 301,  822 => 300,  818 => 299,  809 => 295,  802 => 290,  796 => 289,  788 => 286,  785 => 285,  777 => 282,  774 => 281,  771 => 280,  767 => 279,  760 => 275,  753 => 270,  747 => 269,  739 => 266,  736 => 265,  728 => 262,  725 => 261,  722 => 260,  718 => 259,  711 => 255,  705 => 251,  699 => 250,  691 => 248,  683 => 246,  680 => 245,  676 => 244,  670 => 241,  660 => 234,  653 => 229,  647 => 228,  639 => 225,  636 => 224,  628 => 221,  625 => 220,  622 => 219,  618 => 218,  611 => 214,  606 => 211,  601 => 209,  593 => 207,  588 => 206,  583 => 205,  578 => 203,  573 => 202,  571 => 201,  568 => 200,  564 => 199,  558 => 198,  555 => 197,  553 => 196,  545 => 193,  538 => 191,  529 => 187,  522 => 185,  515 => 181,  509 => 180,  505 => 179,  496 => 175,  491 => 173,  486 => 170,  480 => 169,  478 => 168,  472 => 167,  467 => 165,  462 => 162,  456 => 161,  454 => 160,  448 => 159,  443 => 157,  434 => 153,  427 => 151,  422 => 148,  416 => 147,  414 => 146,  408 => 145,  403 => 143,  398 => 140,  392 => 139,  390 => 138,  384 => 137,  379 => 135,  374 => 132,  368 => 131,  366 => 130,  360 => 129,  355 => 127,  346 => 120,  340 => 119,  332 => 116,  329 => 115,  321 => 112,  318 => 111,  315 => 110,  311 => 109,  304 => 105,  296 => 99,  290 => 98,  282 => 95,  279 => 94,  271 => 91,  268 => 90,  265 => 89,  261 => 88,  254 => 84,  245 => 80,  240 => 78,  231 => 74,  226 => 72,  221 => 69,  215 => 68,  213 => 67,  207 => 66,  202 => 64,  196 => 60,  191 => 59,  188 => 58,  183 => 56,  180 => 55,  178 => 54,  175 => 53,  170 => 52,  167 => 51,  162 => 49,  159 => 48,  157 => 47,  150 => 45,  142 => 40,  138 => 39,  134 => 38,  129 => 36,  125 => 35,  121 => 34,  117 => 33,  112 => 31,  106 => 28,  102 => 26,  94 => 22,  91 => 21,  83 => 17,  81 => 16,  76 => 13,  65 => 11,  61 => 10,  56 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "setting/setting.twig", "");
    }
}
