<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* crystal_white/template/product/category.twig */
class __TwigTemplate_fbe4ae9b18f3faecd47afa350df80232c29474db9fd3cc59c7dadc7d9c5d55c9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"product-category\" class=\"container\">
\t<ul class=\"breadcrumb\">
\t\t";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 5
            echo "\t\t<li>
\t\t\t<a href=\"";
            // line 6
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 6);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 6);
            echo "</a>
\t\t</li>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "\t</ul>
\t<div class=\"row\">";
        // line 10
        echo ($context["column_left"] ?? null);
        echo "
\t\t";
        // line 11
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 12
            echo "\t\t";
            $context["class"] = "col-sm-6";
            // line 13
            echo "\t\t";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 14
            echo "\t\t";
            $context["class"] = "col-sm-9";
            // line 15
            echo "\t\t";
        } else {
            // line 16
            echo "\t\t";
            $context["class"] = "col-sm-12";
            // line 17
            echo "\t\t";
        }
        // line 18
        echo "\t\t<div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo "
\t\t\t<div class=\"heading-page\">
\t\t\t\t<h1 class=\"title\">";
        // line 20
        echo ($context["heading_title"] ?? null);
        echo "</h1>
\t\t\t</div>
\t\t\t";
        // line 22
        if ((($context["thumb"] ?? null) || ($context["description"] ?? null))) {
            // line 23
            echo "\t\t\t<div class=\"row\">
\t\t\t\t";
            // line 24
            if (($context["thumb"] ?? null)) {
                // line 25
                echo "\t\t\t\t<div class=\"col-sm-2\"><img src=\"";
                echo ($context["thumb"] ?? null);
                echo "\" alt=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" title=\"";
                echo ($context["heading_title"] ?? null);
                echo "\"
\t\t\t\t\t\tclass=\"img-thumbnail\" /></div>
\t\t\t\t";
            }
            // line 28
            echo "\t\t\t\t";
            if (($context["description"] ?? null)) {
                // line 29
                echo "\t\t\t\t<div class=\"col-sm-10\">";
                echo ($context["description"] ?? null);
                echo "</div>
\t\t\t\t";
            }
            // line 31
            echo "\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t<hr>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
        }
        // line 36
        echo "\t\t\t";
        if (($context["categories"] ?? null)) {
            // line 37
            echo "\t\t\t<div class=\"sub-categories\">
\t\t\t\t<h4>";
            // line 38
            echo ($context["text_refine"] ?? null);
            echo "</h4>

\t\t\t\t<div class=\"list-categories\">
\t\t\t\t\t<ul>
\t\t\t\t\t\t";
            // line 42
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 43
                echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"";
                // line 44
                echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 44);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 44);
                echo "</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
        }
        // line 51
        echo "\t\t\t";
        if (($context["products"] ?? null)) {
            // line 52
            echo "
\t\t\t<div class=\"category-options\">
\t\t\t\t<div class=\"pull-right\">
\t\t\t\t\t<b>Exibir por:</b>
\t\t\t\t\t<br>
\t\t\t\t\t<div class=\"btn-group btn-group-sm\">
\t\t\t\t\t\t<button type=\"button\" id=\"list-view\" class=\"btn btn-default\" data-toggle=\"tooltip\"
\t\t\t\t\t\t\ttitle=\"";
            // line 59
            echo ($context["button_list"] ?? null);
            echo "\">
\t\t\t\t\t\t\t<i class=\"fa fa-th-list\"></i>
\t\t\t\t\t\t\t<span class=\"hidden-xs\">Lista</span>
\t\t\t\t\t\t</button>
\t\t\t\t\t\t<button type=\"button\" id=\"grid-view\" class=\"btn btn-default\" data-toggle=\"tooltip\"
\t\t\t\t\t\t\ttitle=\"";
            // line 64
            echo ($context["button_grid"] ?? null);
            echo "\">
\t\t\t\t\t\t\t<i class=\"fa fa-th\"></i>
\t\t\t\t\t\t\t<span class=\"hidden-xs\">Grade</span>
\t\t\t\t\t\t</button>
\t\t\t\t\t</div>
\t\t\t\t\t<select id=\"input-limit\" class=\"form-control select-quantity\" onchange=\"location = this.value;\"
\t\t\t\t\t\tdata-toggle=\"tooltip\" title=\"Quantidade a exibir\">
\t\t\t\t\t\t";
            // line 71
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["limits"]);
            foreach ($context['_seq'] as $context["_key"] => $context["limits"]) {
                // line 72
                echo "\t\t\t\t\t\t";
                if ((twig_get_attribute($this->env, $this->source, $context["limits"], "value", [], "any", false, false, false, 72) == ($context["limit"] ?? null))) {
                    // line 73
                    echo "\t\t\t\t\t\t<option value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["limits"], "href", [], "any", false, false, false, 73);
                    echo "\" selected=\"selected\">";
                    echo twig_get_attribute($this->env, $this->source, $context["limits"], "text", [], "any", false, false, false, 73);
                    echo "</option>
\t\t\t\t\t\t";
                } else {
                    // line 75
                    echo "\t\t\t\t\t\t<option value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["limits"], "href", [], "any", false, false, false, 75);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["limits"], "text", [], "any", false, false, false, 75);
                    echo "</option>
\t\t\t\t\t\t";
                }
                // line 77
                echo "\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['limits'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 78
            echo "\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t\t<div class=\"pull-right\">
\t\t\t\t\t<b>Ordenar por:</b>
\t\t\t\t\t<br>
\t\t\t\t\t<select id=\"input-sort\" class=\"form-control\" onchange=\"location = this.value;\">
\t\t\t\t\t\t";
            // line 84
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["sorts"]);
            foreach ($context['_seq'] as $context["_key"] => $context["sorts"]) {
                // line 85
                echo "\t\t\t\t\t\t";
                if ((twig_get_attribute($this->env, $this->source, $context["sorts"], "value", [], "any", false, false, false, 85) == sprintf("%s-%s", ($context["sort"] ?? null), ($context["order"] ?? null)))) {
                    // line 86
                    echo "\t\t\t\t\t\t<option value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["sorts"], "href", [], "any", false, false, false, 86);
                    echo "\" selected=\"selected\">";
                    echo twig_get_attribute($this->env, $this->source, $context["sorts"], "text", [], "any", false, false, false, 86);
                    echo "</option>
\t\t\t\t\t\t";
                } else {
                    // line 88
                    echo "\t\t\t\t\t\t<option value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["sorts"], "href", [], "any", false, false, false, 88);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["sorts"], "text", [], "any", false, false, false, 88);
                    echo "</option>
\t\t\t\t\t\t";
                }
                // line 90
                echo "\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sorts'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 91
            echo "\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t\t<div class=\"pull-left\">";
            // line 93
            echo ($context["results"] ?? null);
            echo "</div>
\t\t\t</div>

\t\t\t<div class=\"clearfix\"></div>
\t\t\t<div class=\"row\">
\t\t\t\t";
            // line 98
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 99
                echo "\t\t\t\t<div class=\"product-layout product-list col-xs-12\">
\t\t\t\t\t<div class=\"product-thumb transition\">
\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t";
                // line 102
                if (twig_get_attribute($this->env, $this->source, $context["product"], "valor_desconto", [], "any", false, false, false, 102)) {
                    // line 103
                    echo "\t\t\t\t\t\t\t<span class=\"badge badge-danger badge-desconto\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "valor_desconto", [], "any", false, false, false, 103);
                    echo "%</span>
\t\t\t\t\t\t\t";
                }
                // line 105
                echo "\t\t\t\t\t\t\t<a href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 105);
                echo "\"><img src=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 105);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 105);
                echo "\"
\t\t\t\t\t\t\t\t\ttitle=\"";
                // line 106
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 106);
                echo "\" class=\"img-responsive\" /></a>
\t\t\t\t\t\t\t<button type=\"button\" class=\"btn-wishlist\" data-toggle=\"tooltip\"
\t\t\t\t\t\t\t\ttitle=\"";
                // line 108
                echo ($context["button_wishlist"] ?? null);
                echo "\" onclick=\"wishlist.add('";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 108);
                echo "');\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-heart\"></i>
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t<div class=\"titleprice\">
\t\t\t\t\t\t\t\t<h4>
\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 115
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 115);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 115);
                echo "</a>
\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t\t";
                // line 118
                if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 118)) {
                    // line 119
                    echo "\t\t\t\t\t\t\t\t\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 120
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ((twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 120) < $context["i"])) {
                            echo " <span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 124
                            echo "\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 129
                        echo "\t\t\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 130
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 130)) {
                        // line 131
                        echo "\t\t\t\t\t\t\t\t\t\t<span>(";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 131);
                        echo ")</span>
\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 133
                    echo "\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 134
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 135
                        echo "\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 139
                    echo "\t\t\t\t\t\t\t\t\t\t<span>(0)</span>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 141
                echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                // line 142
                if ( !twig_get_attribute($this->env, $this->source, $context["product"], "shipping", [], "any", false, false, false, 142)) {
                    // line 143
                    echo "\t\t\t\t\t\t\t\t<div class=\"shipping-grid\">
\t\t\t\t\t\t\t\t\t<img src=\"./_galerias/shipping/frete_gratis_gif.gif\" alt=\"Frete Grátis\"
\t\t\t\t\t\t\t\t\t\tclass=\"img-responsive\">
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                }
                // line 148
                echo "\t\t\t\t\t\t\t\t";
                if ( !twig_get_attribute($this->env, $this->source, $context["product"], "quantity", [], "any", false, false, false, 148)) {
                    // line 149
                    echo "\t\t\t\t\t\t\t\t<div class=\"alert alert-danger alert-stock\">Sem estoque</div>
\t\t\t\t\t\t\t\t";
                }
                // line 151
                echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"buttonshipping\">
\t\t\t\t\t\t\t\t";
                // line 153
                if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 153)) {
                    // line 154
                    echo "\t\t\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 154) == "R\$ 0,00")) {
                        // line 155
                        echo "\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-current\">
\t\t\t\t\t\t\t\t\t\t\t\t<small>Sob-consulta</small>
\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                    } else {
                        // line 165
                        echo "\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t";
                        // line 167
                        if ( !twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 167)) {
                            // line 168
                            echo "\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-current\">";
                            // line 169
                            echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 169);
                            echo "</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 172
                            echo "\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-old\">";
                            // line 173
                            echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 173);
                            echo "</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                            // line 176
                            echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 176);
                            echo "</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 179
                        echo "\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                    }
                    // line 182
                    echo "\t\t\t\t\t\t\t\t";
                }
                // line 183
                echo "
\t\t\t\t\t\t\t\t<div class=\"button-group\">
\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 185
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 185);
                echo "\">
\t\t\t\t\t\t\t\t\t\t<span class=\"\">";
                // line 186
                echo ($context["button_conferir"] ?? null);
                echo "</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- product-layout -->
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 195
            echo "\t\t\t</div>
\t\t\t<div class=\"d-flex justify-content-center align-items-center custom-pagination\">
\t\t\t\t<div class=\"col-sm-6 text-left\">";
            // line 197
            echo ($context["pagination"] ?? null);
            echo "</div>
\t\t\t\t<div class=\"col-sm-6 text-right\">";
            // line 198
            echo ($context["results"] ?? null);
            echo "</div>
\t\t\t</div>
\t\t\t";
        }
        // line 201
        echo "\t\t\t";
        if (( !($context["categories"] ?? null) &&  !($context["products"] ?? null))) {
            // line 202
            echo "\t\t\t<p>";
            echo ($context["text_empty"] ?? null);
            echo "</p>
\t\t\t<div class=\"buttons\">
\t\t\t\t<div class=\"pull-right\">
\t\t\t\t\t<a href=\"";
            // line 205
            echo ($context["continue"] ?? null);
            echo "\" class=\"btn btn-primary\">";
            echo ($context["button_continue"] ?? null);
            echo "</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
        }
        // line 209
        echo "\t\t\t";
        echo ($context["content_bottom"] ?? null);
        echo "
\t\t</div>
\t\t";
        // line 211
        echo ($context["column_right"] ?? null);
        echo "
\t</div>
</div>
";
        // line 214
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "crystal_white/template/product/category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  535 => 214,  529 => 211,  523 => 209,  514 => 205,  507 => 202,  504 => 201,  498 => 198,  494 => 197,  490 => 195,  475 => 186,  471 => 185,  467 => 183,  464 => 182,  459 => 179,  453 => 176,  447 => 173,  444 => 172,  438 => 169,  435 => 168,  433 => 167,  429 => 165,  417 => 155,  414 => 154,  412 => 153,  408 => 151,  404 => 149,  401 => 148,  394 => 143,  392 => 142,  389 => 141,  385 => 139,  376 => 135,  371 => 134,  368 => 133,  362 => 131,  359 => 130,  353 => 129,  346 => 124,  338 => 120,  333 => 119,  331 => 118,  323 => 115,  311 => 108,  306 => 106,  297 => 105,  291 => 103,  289 => 102,  284 => 99,  280 => 98,  272 => 93,  268 => 91,  262 => 90,  254 => 88,  246 => 86,  243 => 85,  239 => 84,  231 => 78,  225 => 77,  217 => 75,  209 => 73,  206 => 72,  202 => 71,  192 => 64,  184 => 59,  175 => 52,  172 => 51,  166 => 47,  155 => 44,  152 => 43,  148 => 42,  141 => 38,  138 => 37,  135 => 36,  128 => 31,  122 => 29,  119 => 28,  108 => 25,  106 => 24,  103 => 23,  101 => 22,  96 => 20,  88 => 18,  85 => 17,  82 => 16,  79 => 15,  76 => 14,  73 => 13,  70 => 12,  68 => 11,  64 => 10,  61 => 9,  50 => 6,  47 => 5,  43 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "crystal_white/template/product/category.twig", "");
    }
}
