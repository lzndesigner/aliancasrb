<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* common/filemanager.twig */
class __TwigTemplate_12c654c7cb8a6717fb062846a95fa84a0e410b216c8075810f038f477526f6da extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div id=\"filemanager\" class=\"modal-dialog modal-lg\">
  <div class=\"modal-content\">
    <div class=\"modal-header\">
      <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
      <h4 class=\"modal-title\">";
        // line 5
        echo ($context["heading_title"] ?? null);
        echo "</h4>
    </div>
    <div class=\"modal-body\">
      <div class=\"row\">
        <div class=\"col-sm-5\"><a href=\"";
        // line 9
        echo ($context["parent"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_parent"] ?? null);
        echo "\" id=\"button-parent\" class=\"btn btn-default\"><i class=\"fa fa-level-up\"></i></a> <a href=\"";
        echo ($context["refresh"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_refresh"] ?? null);
        echo "\" id=\"button-refresh\" class=\"btn btn-default\"><i class=\"fa fa-refresh\"></i></a>
          <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 10
        echo ($context["button_upload"] ?? null);
        echo "\" id=\"button-upload\" class=\"btn btn-primary\"><i class=\"fa fa-upload\"></i></button>
          <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 11
        echo ($context["button_folder"] ?? null);
        echo "\" id=\"button-folder\" class=\"btn btn-default\"><i class=\"fa fa-folder\"></i></button>
          <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 12
        echo ($context["button_delete"] ?? null);
        echo "\" id=\"button-delete\" class=\"btn btn-danger\"><i class=\"fa fa-trash-o\"></i></button>
        </div>
        <div class=\"col-sm-7\">
          <div class=\"input-group\">
            <input type=\"text\" name=\"search\" value=\"";
        // line 16
        echo ($context["filter_name"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_search"] ?? null);
        echo "\" class=\"form-control\">
            <span class=\"input-group-btn\">
            <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 18
        echo ($context["button_search"] ?? null);
        echo "\" id=\"button-search\" class=\"btn btn-primary\"><i class=\"fa fa-search\"></i></button>
            </span></div>
        </div>
      </div>
      <hr />
      ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_array_batch(($context["images"] ?? null), 4));
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            // line 24
            echo "      <div class=\"row\" id=\"gallery-box\">
      \t<div class=\"loadingUp\"> 
      \t\t<div class=\"d-flex justify-content-center align-items-center\">
\t      \t\t<div class=\"flex-fill\">
\t      \t\t\t<h4>Carregando <i class=\"fa fa-circle-o-notch fa-spin\"></i></h4>
\t      \t\t</div>
      \t\t</div>
      \t</div>
        ";
            // line 32
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["image"]);
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 33
                echo "        <div class=\"col-sm-3 col-xs-6 text-center\">
          ";
                // line 34
                if ((twig_get_attribute($this->env, $this->source, $context["image"], "type", [], "any", false, false, false, 34) == "directory")) {
                    // line 35
                    echo "          <div class=\"text-center\"><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "href", [], "any", false, false, false, 35);
                    echo "\" class=\"directory\" style=\"vertical-align: middle;\"><i class=\"fa fa-folder fa-5x\"></i></a></div>
          <label>
            <input type=\"checkbox\" name=\"path[]\" value=\"";
                    // line 37
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 37);
                    echo "\" />
            ";
                    // line 38
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "name", [], "any", false, false, false, 38);
                    echo "</label>
          ";
                }
                // line 40
                echo "          ";
                if ((twig_get_attribute($this->env, $this->source, $context["image"], "type", [], "any", false, false, false, 40) == "image")) {
                    // line 41
                    echo "          <a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "href", [], "any", false, false, false, 41);
                    echo "\" class=\"thumbnail\"><img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "thumb", [], "any", false, false, false, 41);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "name", [], "any", false, false, false, 41);
                    echo "\" title=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "name", [], "any", false, false, false, 41);
                    echo "\" /></a>
          <label>
            <input type=\"checkbox\" name=\"path[]\" value=\"";
                    // line 43
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 43);
                    echo "\" />
            ";
                    // line 44
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "name", [], "any", false, false, false, 44);
                    echo "</label>
          ";
                }
                // line 46
                echo "        </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "      </div>
      <br />
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "    </div>
    <div class=\"modal-footer\">";
        // line 52
        echo ($context["pagination"] ?? null);
        echo "</div>
  </div>
</div>
<style>
#gallery-box {
    position: relative;
}
.loadingUp {
\tdisplay: none;
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    z-index: 1;
    background: #ffffffb5;
}
.loadingUp .d-flex {
    height: 100%;
    width: 100%;
}
.loadingUp h4 {
    font-weight: bold;
    font-size: 27px;
}
#gallery-box .d-flex {
    display: -ms-flexbox!important;
    display: flex!important;
}
#gallery-box .justify-content-center {
    -ms-flex-pack: center!important;
    justify-content: center!important;
}
#gallery-box .align-items-center {
    -ms-flex-align: center!important;
    align-items: center!important;
}


</style>
<script type=\"text/javascript\"><!--
";
        // line 92
        if (($context["target"] ?? null)) {
            // line 93
            echo "\$('a.thumbnail').on('click', function(e) {
\te.preventDefault();

\t";
            // line 96
            if (($context["thumb"] ?? null)) {
                // line 97
                echo "\t\$('#";
                echo twig_escape_filter($this->env, ($context["thumb"] ?? null), "js");
                echo "').find('img').attr('src', \$(this).find('img').attr('src'));
\t";
            }
            // line 99
            echo "
\t\$('#";
            // line 100
            echo twig_escape_filter($this->env, ($context["target"] ?? null), "js");
            echo "').val(\$(this).parent().find('input').val());

\t\$('#modal-image').modal('hide');
});
";
        }
        // line 105
        echo "
\$('a.directory').on('click', function(e) {
\te.preventDefault();

\t\$('#modal-image').load(\$(this).attr('href'));
});

\$('.pagination a').on('click', function(e) {
\te.preventDefault();

\t\$('#modal-image').load(\$(this).attr('href'));
});

\$('#button-parent').on('click', function(e) {
\te.preventDefault();

\t\$('#modal-image').load(\$(this).attr('href'));
});

\$('#button-refresh').on('click', function(e) {
\te.preventDefault();

\t\$('#modal-image').load(\$(this).attr('href'));
});

\$('input[name=\\'search\\']').on('keydown', function(e) {
\tif (e.which == 13) {
\t\t\$('#button-search').trigger('click');
\t}
});

\$('#button-search').on('click', function(e) {
\tvar url = 'index.php?route=common/filemanager&user_token=";
        // line 137
        echo ($context["user_token"] ?? null);
        echo "&directory=";
        echo ($context["directory"] ?? null);
        echo "';

\tvar filter_name = \$('input[name=\\'search\\']').val();

\tif (filter_name) {
\t\turl += '&filter_name=' + encodeURIComponent(filter_name);
\t}

\t";
        // line 145
        if (($context["thumb"] ?? null)) {
            // line 146
            echo "\turl += '&thumb=' + '";
            echo twig_escape_filter($this->env, ($context["thumb"] ?? null), "js");
            echo "';
\t";
        }
        // line 148
        echo "
\t";
        // line 149
        if (($context["target"] ?? null)) {
            // line 150
            echo "\turl += '&target=' + '";
            echo twig_escape_filter($this->env, ($context["target"] ?? null), "js");
            echo "';
\t";
        }
        // line 152
        echo "
\t\$('#modal-image').load(url);
});
//--></script>
<script type=\"text/javascript\"><!--
\$('#button-upload').on('click', function() {
\t\$('#form-upload').remove();

\t\$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file[]\" value=\"\" multiple=\"multiple\" /></form>');

\t\$('#form-upload input[name=\\'file[]\\']').trigger('click');

\tif (typeof timer != 'undefined') {
    \tclearInterval(timer);
\t}

\ttimer = setInterval(function() {
\t\tif (\$('#form-upload input[name=\\'file[]\\']').val() != '') {
\t\t\tclearInterval(timer);

\t\t\t\$.ajax({
\t\t\t\turl: 'index.php?route=common/filemanager/upload&user_token=";
        // line 173
        echo ($context["user_token"] ?? null);
        echo "&directory=";
        echo ($context["directory"] ?? null);
        echo "',
\t\t\t\ttype: 'post',
\t\t\t\tdataType: 'json',
\t\t\t\tdata: new FormData(\$('#form-upload')[0]),
\t\t\t\tcache: false,
\t\t\t\tcontentType: false,
\t\t\t\tprocessData: false,
\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\$('#button-upload i').replaceWith('<i class=\"fa fa-circle-o-notch fa-spin\"></i>');
\t\t\t\t\t\$('#button-upload').prop('disabled', true);
\t\t\t\t\t\$('#gallery-box .loadingUp').show();
\t\t\t\t},
\t\t\t\tcomplete: function() {
\t\t\t\t\t\$('#button-upload i').replaceWith('<i class=\"fa fa-upload\"></i>');
\t\t\t\t\t\$('#button-upload').prop('disabled', false);
\t\t\t\t\t\$('#gallery-box .loadingUp').hide();
\t\t\t\t},
\t\t\t\tsuccess: function(json) {
\t\t\t\t\tif (json['error']) {
\t\t\t\t\t\talert(json['error']);
\t\t\t\t\t}

\t\t\t\t\tif (json['success']) {
\t\t\t\t\t\talert(json['success']);

\t\t\t\t\t\t\$('#button-refresh').trigger('click');
\t\t\t\t\t}
\t\t\t\t},
\t\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t}
\t\t\t});
\t\t}
\t}, 500);
});

\$('#button-folder').popover({
\thtml: true,
\tplacement: 'bottom',
\ttrigger: 'click',
\ttitle: '";
        // line 213
        echo ($context["entry_folder"] ?? null);
        echo "',
\tcontent: function() {
\t\thtml  = '<div class=\"input-group\">';
\t\thtml += '  <input type=\"text\" name=\"folder\" value=\"\" placeholder=\"";
        // line 216
        echo ($context["entry_folder"] ?? null);
        echo "\" class=\"form-control\">';
\t\thtml += '  <span class=\"input-group-btn\"><button type=\"button\" title=\"";
        // line 217
        echo ($context["button_folder"] ?? null);
        echo "\" id=\"button-create\" class=\"btn btn-primary\"><i class=\"fa fa-plus-circle\"></i></button></span>';
\t\thtml += '</div>';
\t\thtml += '<div class=\"input-group\">';
\t\thtml += '• Use o nome da pasta de acordo com o nome do departamento, produto ou marca por exemplo.<br>• Não use palavras com acentos. <br> • Troque o espaço por hífen (-).';
\t\thtml += '</div>';

\t\treturn html;
\t}
});

\$('#button-folder').on('shown.bs.popover', function() {
\t\$('#button-create').on('click', function() {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=common/filemanager/folder&user_token=";
        // line 230
        echo ($context["user_token"] ?? null);
        echo "&directory=";
        echo ($context["directory"] ?? null);
        echo "',
\t\t\ttype: 'post',
\t\t\tdataType: 'json',
\t\t\tdata: 'folder=' + encodeURIComponent(\$('input[name=\\'folder\\']').val()),
\t\t\tbeforeSend: function() {
\t\t\t\t\$('#button-create').prop('disabled', true);
\t\t\t},
\t\t\tcomplete: function() {
\t\t\t\t\$('#button-create').prop('disabled', false);
\t\t\t},
\t\t\tsuccess: function(json) {
\t\t\t\tif (json['error']) {
\t\t\t\t\talert(json['error']);
\t\t\t\t}

\t\t\t\tif (json['success']) {
\t\t\t\t\talert(json['success']);

\t\t\t\t\t\$('#button-refresh').trigger('click');
\t\t\t\t}
\t\t\t},
\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t}
\t\t});
\t});
});

\$('#modal-image #button-delete').on('click', function(e) {
\tif (confirm('";
        // line 259
        echo ($context["text_confirm"] ?? null);
        echo "')) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=common/filemanager/delete&user_token=";
        // line 261
        echo ($context["user_token"] ?? null);
        echo "',
\t\t\ttype: 'post',
\t\t\tdataType: 'json',
\t\t\tdata: \$('input[name^=\\'path\\']:checked'),
\t\t\tbeforeSend: function() {
\t\t\t\t\$('#button-delete').prop('disabled', true);
\t\t\t},
\t\t\tcomplete: function() {
\t\t\t\t\$('#button-delete').prop('disabled', false);
\t\t\t},
\t\t\tsuccess: function(json) {
\t\t\t\tif (json['error']) {
\t\t\t\t\talert(json['error']);
\t\t\t\t}

\t\t\t\tif (json['success']) {
\t\t\t\t\talert(json['success']);

\t\t\t\t\t\$('#button-refresh').trigger('click');
\t\t\t\t}
\t\t\t},
\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t}
\t\t});
\t}
});
//--></script>
";
    }

    public function getTemplateName()
    {
        return "common/filemanager.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  438 => 261,  433 => 259,  399 => 230,  383 => 217,  379 => 216,  373 => 213,  328 => 173,  305 => 152,  299 => 150,  297 => 149,  294 => 148,  288 => 146,  286 => 145,  273 => 137,  239 => 105,  231 => 100,  228 => 99,  222 => 97,  220 => 96,  215 => 93,  213 => 92,  170 => 52,  167 => 51,  159 => 48,  152 => 46,  147 => 44,  143 => 43,  131 => 41,  128 => 40,  123 => 38,  119 => 37,  113 => 35,  111 => 34,  108 => 33,  104 => 32,  94 => 24,  90 => 23,  82 => 18,  75 => 16,  68 => 12,  64 => 11,  60 => 10,  50 => 9,  43 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "common/filemanager.twig", "");
    }
}
