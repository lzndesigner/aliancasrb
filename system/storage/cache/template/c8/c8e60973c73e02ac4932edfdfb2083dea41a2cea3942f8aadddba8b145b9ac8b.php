<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* crystal_white/template/product/review.twig */
class __TwigTemplate_dd15775687a4cef215b2707d96bd396293fc53b481433f4a4e9cac331c8e2867 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "  ";
        if (($context["reviews"] ?? null)) {
            // line 2
            echo "  <div class=\"boxReviewsClients\">
    <div class=\"heading-page\">
      <h3 class=\"title\">Avaliações do Produto (";
            // line 4
            echo ($context["review_total"] ?? null);
            echo ") </h3>
    </div>
    <ul>
      ";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["reviews"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
                // line 8
                echo "      <li class=\"col-xs-12 col-sm-4 col-md-3\">
<!--       <div class=\"image \">
        <a href=\"_galerias/reviews/gshock_bernado.jpg\" data-lightbox=\"example-1\">
          <img src=\"_galerias/reviews/gshock_bernado.jpg\" alt=\"\">
        </a>
      </div> -->
      <div class=\"conteudo\">
        <div class=\"avatar\"><img src=\"_galerias/avatares/";
                // line 15
                echo twig_get_attribute($this->env, $this->source, $context["review"], "avatar", [], "any", false, false, false, 15);
                echo ".png\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["review"], "author", [], "any", false, false, false, 15);
                echo "\"></div>

        <div class=\"rating\">
          ";
                // line 18
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 19
                    echo "          ";
                    if ((twig_get_attribute($this->env, $this->source, $context["review"], "rating", [], "any", false, false, false, 19) < $context["i"])) {
                        echo " <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span> ";
                    } else {
                        echo " <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span> ";
                    }
                    // line 20
                    echo "          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 21
                echo "        </div>

        <h2>";
                // line 23
                echo twig_get_attribute($this->env, $this->source, $context["review"], "author", [], "any", false, false, false, 23);
                echo "</h2>
        <p>";
                // line 24
                echo twig_get_attribute($this->env, $this->source, $context["review"], "text", [], "any", false, false, false, 24);
                echo "</p>

        <span class=\"date\">";
                // line 26
                echo twig_get_attribute($this->env, $this->source, $context["review"], "date_added", [], "any", false, false, false, 26);
                echo "</span>
      </div>
    </li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "  </ul>
<div class=\"d-flex justify-content-center align-items-center custom-pagination\">
  <div class=\"col-sm-6 text-left\">";
            // line 32
            echo ($context["pagination"] ?? null);
            echo "</div>
  <div class=\"col-sm-6 text-right\">";
            // line 33
            echo ($context["results"] ?? null);
            echo "</div>
</div>
  ";
        }
        // line 35
        echo " ";
    }

    public function getTemplateName()
    {
        return "crystal_white/template/product/review.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 35,  119 => 33,  115 => 32,  111 => 30,  101 => 26,  96 => 24,  92 => 23,  88 => 21,  82 => 20,  75 => 19,  71 => 18,  63 => 15,  54 => 8,  50 => 7,  44 => 4,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "crystal_white/template/product/review.twig", "");
    }
}
