<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* crystal_white/template/product/product.twig */
class __TwigTemplate_1e9ac46ee059d4bba74383eaecbc8df720efb3dff8c0b81558a357cc6ac5c9cd extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"product-product\" class=\"container\">
\t<div class=\"d-flex  align-items-center\">
\t\t<div class=\"flex-fill\">
\t\t\t<ul class=\"breadcrumb\">
\t\t\t\t";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 7
            echo "\t\t\t\t<li>
\t\t\t\t\t<a href=\"";
            // line 8
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 8);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 8);
            echo "</a>
\t\t\t\t</li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "\t\t\t</ul>
\t\t</div>
\t\t<div class=\"\">
\t\t\t<div class=\"d-flex\">
\t\t\t\t<div class=\"mr-3\">
\t\t\t\t\t<h5 style=\"display:inline-block;\">Compartilhar</h5>
\t\t\t\t</div>
\t\t\t\t<div class=\"\">
\t\t\t\t\t<div class=\"addthis_inline_share_toolbox\"></div>
\t\t\t\t\t<button type=\"button\" data-toggle=\"tooltip\" class=\"btn btn-default btn-wishlist hide\"
\t\t\t\t\t\ttitle=\"";
        // line 21
        echo ($context["button_wishlist"] ?? null);
        echo "\" onclick=\"wishlist.add('";
        echo ($context["product_id"] ?? null);
        echo "');\">
\t\t\t\t\t\t<i class=\"fa fa-heart\"></i>
\t\t\t\t\t</button>
\t\t\t\t</div>
\t\t\t</div>

\t\t</div>
\t</div>
\t<script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5922d7a2effa4eb1\"></script>
\t<hr class=\"mt-0 mb-3\">
\t<!-- AddThis Button END -->
\t<div class=\"row\">";
        // line 32
        echo ($context["column_left"] ?? null);
        echo "
\t\t";
        // line 33
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 34
            echo "\t\t";
            $context["class"] = "col-sm-6";
            // line 35
            echo "\t\t";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 36
            echo "\t\t";
            $context["class"] = "col-sm-9";
            // line 37
            echo "\t\t";
        } else {
            // line 38
            echo "\t\t";
            $context["class"] = "col-sm-12";
            // line 39
            echo "\t\t";
        }
        // line 40
        echo "\t\t<div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo "
\t\t\t<div class=\"row\">
\t\t\t\t";
        // line 42
        if ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 43
            echo "\t\t\t\t";
            $context["class"] = "col-sm-6";
            // line 44
            echo "\t\t\t\t";
        } else {
            // line 45
            echo "\t\t\t\t";
            $context["class"] = "col-sm-7";
            // line 46
            echo "\t\t\t\t";
        }
        // line 47
        echo "\t\t\t\t<div class=\"";
        echo ($context["class"] ?? null);
        echo "\">
\t\t\t\t\t";
        // line 48
        if ((($context["thumb"] ?? null) || ($context["images"] ?? null))) {
            // line 49
            echo "\t\t\t\t\t";
            if (($context["manufacturer"] ?? null)) {
                // line 50
                echo "\t\t\t\t\t<h2 class=\"manufacturer visible-xs\">
\t\t\t\t\t\t<a href=\"";
                // line 51
                echo ($context["manufacturers"] ?? null);
                echo "\">";
                echo ($context["manufacturer"] ?? null);
                echo "</a>
\t\t\t\t\t</h2>
\t\t\t\t\t";
            }
            // line 54
            echo "\t\t\t\t\t<h1 class=\"title visible-xs\">";
            echo ($context["heading_title"] ?? null);
            echo "</h1>
\t\t\t\t\t<div class=\"gallery-products\">
\t\t\t\t\t\t";
            // line 56
            if (($context["thumb"] ?? null)) {
                // line 57
                echo "\t\t\t\t\t\t<img id=\"zoom_01\" src=\"";
                echo ($context["popup"] ?? null);
                echo "\" data-zoom-image=\"";
                echo ($context["popup"] ?? null);
                echo "\" alt=\"";
                echo ($context["heading_title"] ?? null);
                echo "\"
\t\t\t\t\t\t\tclass=\"img-responsive\">
\t\t\t\t\t\t";
            }
            // line 60
            echo "\t\t\t\t\t\t<p><small><i class=\"fa fa-search\"></i> Use o mouse e rode a bolinha para aproximar a
\t\t\t\t\t\t\t\timagem</small></p>
\t\t\t\t\t\t";
            // line 62
            if (($context["images"] ?? null)) {
                // line 63
                echo "\t\t\t\t\t\t<div id=\"gallery_09\">
\t\t\t\t\t\t\t";
                // line 64
                if (($context["thumb"] ?? null)) {
                    // line 65
                    echo "\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"item-img active\" data-image=\"";
                    echo ($context["popup"] ?? null);
                    echo "\"
\t\t\t\t\t\t\t\tdata-zoom-image=\"";
                    // line 66
                    echo ($context["popup"] ?? null);
                    echo "\">
\t\t\t\t\t\t\t\t<img src=\"";
                    // line 67
                    echo ($context["thumb"] ?? null);
                    echo "\" data-zoom-image=\"";
                    echo ($context["popup"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\"
\t\t\t\t\t\t\t\t\tstyle=\"width:90px;\">
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t";
                }
                // line 71
                echo "\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["images"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 72
                    echo "\t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"item-img\" data-image=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "popup", [], "any", false, false, false, 72);
                    echo "\"
\t\t\t\t\t\t\t\tdata-zoom-image=\"";
                    // line 73
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "popup", [], "any", false, false, false, 73);
                    echo "\">
\t\t\t\t\t\t\t\t<img src=\"";
                    // line 74
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "thumb", [], "any", false, false, false, 74);
                    echo "\" data-zoom-image=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "popup", [], "any", false, false, false, 74);
                    echo "\"
\t\t\t\t\t\t\t\t\talt=\"";
                    // line 75
                    echo ($context["heading_title"] ?? null);
                    echo "\" style=\"width:90px;\">
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 78
                echo "\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            }
            // line 80
            echo "\t\t\t\t\t</div>

\t\t\t\t\t<script>
          // var owl = \$('.owl-carousel');
          // owl.owlCarousel({
          //   loop: true,
          //   items: 1,
          //   dots: true,
          //   thumbs: true,
          //   thumbImage: true,
          //   thumbLoop: true,
          //   thumbContainerClass: 'owl-thumbs',
          //   thumbItemClass: 'owl-thumb-item'
          // });

\t\t\t\t\t</script>
\t\t\t\t\t<script src=\"catalog/view/javascript/jquery/jquery.elevatezoom.js\" type=\"text/javascript\"></script>
\t\t\t\t\t<script type=\"text/javascript\">\$(document).ready(function () {
\t\t\t\t\t\t\tvar resoWidth = \$(window).width();

\t\t\t\t\t\t\tif (resoWidth >= 967) {
\t\t\t\t\t\t\t\t\$(\"#zoom_01\").elevateZoom({
\t\t\t\t\t\t\t\t\tgallery: \"gallery_09\",
\t\t\t\t\t\t\t\t\tgalleryActiveClass: \"active\",
\t\t\t\t\t\t\t\t\tscrollZoom: true,
\t\t\t\t\t\t\t\t\timageCrossfade: true,
\t\t\t\t\t\t\t\t\teasing: true,
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\$(\"#zoom_01\").elevateZoom({
\t\t\t\t\t\t\t\t\tgallery: \"gallery_09\",
\t\t\t\t\t\t\t\t\tgalleryActiveClass: \"active\",
\t\t\t\t\t\t\t\t\tconstrainType: \"height\", constrainSize: 274, zoomType: \"lens\", containLensZoom: true,
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}
\t\t\t\t\t\t});</script>
\t\t\t\t\t";
        }
        // line 117
        echo "\t\t\t\t</div>
\t\t\t\t";
        // line 118
        if ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 119
            echo "\t\t\t\t";
            $context["class"] = "col-sm-6";
            // line 120
            echo "\t\t\t\t";
        } else {
            // line 121
            echo "\t\t\t\t";
            $context["class"] = "col-sm-5";
            // line 122
            echo "\t\t\t\t";
        }
        // line 123
        echo "\t\t\t\t<div class=\"";
        echo ($context["class"] ?? null);
        echo "\">
\t\t\t\t\t";
        // line 124
        if (($context["manufacturer"] ?? null)) {
            // line 125
            echo "\t\t\t\t\t<h2 class=\"manufacturer\">
\t\t\t\t\t\t<a href=\"";
            // line 126
            echo ($context["manufacturers"] ?? null);
            echo "\">";
            echo ($context["manufacturer"] ?? null);
            echo "</a>
\t\t\t\t\t</h2>
\t\t\t\t\t";
        }
        // line 129
        echo "\t\t\t\t\t<h1 class=\"title\">";
        echo ($context["heading_title"] ?? null);
        echo "</h1>
\t\t\t\t\t<div class=\"product-info\">
\t\t\t\t\t\t<ul class=\"list-unstyled\">

\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<b>";
        // line 134
        echo ($context["text_model"] ?? null);
        echo "</b>
\t\t\t\t\t\t\t\t";
        // line 135
        echo ($context["model"] ?? null);
        echo "
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
        // line 137
        if (($context["reward"] ?? null)) {
            // line 138
            echo "\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<b>";
            // line 139
            echo ($context["text_reward"] ?? null);
            echo "</b>
\t\t\t\t\t\t\t\t";
            // line 140
            echo ($context["reward"] ?? null);
            echo "
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
        }
        // line 143
        echo "\t\t\t\t\t\t\t<!-- <li><b>";
        echo ($context["text_stock"] ?? null);
        echo "</b> ";
        echo ($context["stock"] ?? null);
        echo "</li> -->
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>


\t\t\t\t\t";
        // line 148
        if (($context["price"] ?? null)) {
            // line 149
            echo "\t\t\t\t\t";
            if ((($context["price"] ?? null) == "R\$ 0,00")) {
                // line 150
                echo "\t\t\t\t\t<div class=\"product-price\">
\t\t\t\t\t\t<span class=\"price-new\">Sob-consulta</span>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- product-price -->
\t\t\t\t\t";
            } else {
                // line 155
                echo "\t\t\t\t\t<div class=\"product-price\">
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t";
                // line 157
                if ( !($context["special"] ?? null)) {
                    // line 158
                    echo "\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<small>por</small>
\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                    // line 160
                    echo ($context["price"] ?? null);
                    echo "</span>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
                } else {
                    // line 163
                    echo "\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<small>de</small>
\t\t\t\t\t\t\t\t<span class=\"price-old\">";
                    // line 165
                    echo ($context["price"] ?? null);
                    echo "</span>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<small>por</small>
\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                    // line 169
                    echo ($context["special"] ?? null);
                    echo "</span>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t";
                }
                // line 172
                echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<!--             <div class=\"parcelamento-product\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li>";
                // line 175
                echo ($context["parcelamento"] ?? null);
                echo "</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>  -->
\t\t\t\t\t\t";
                // line 178
                if (($context["tax"] ?? null)) {
                    // line 179
                    echo "\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t<span>";
                    // line 180
                    echo ($context["text_tax"] ?? null);
                    echo "
\t\t\t\t\t\t\t";
                    // line 181
                    echo ($context["tax"] ?? null);
                    echo "</span>
\t\t\t\t\t\t";
                }
                // line 183
                echo "
\t\t\t\t\t\t";
                // line 184
                if (($context["points"] ?? null)) {
                    // line 185
                    echo "\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t<span>";
                    // line 186
                    echo ($context["text_points"] ?? null);
                    echo "
\t\t\t\t\t\t\t";
                    // line 187
                    echo ($context["points"] ?? null);
                    echo "</span>
\t\t\t\t\t\t";
                }
                // line 189
                echo "
\t\t\t\t\t</div>
\t\t\t\t\t<!-- product-price -->
\t\t\t\t\t";
            }
            // line 193
            echo "\t\t\t\t\t";
        }
        // line 194
        echo "\t\t\t\t\t";
        if (($context["price"] ?? null)) {
            // line 195
            echo "\t\t\t\t\t";
            if (($context["discounts"] ?? null)) {
                // line 196
                echo "\t\t\t\t\t<div class=\"product-discount\">
\t\t\t\t\t\t<h3>Compre e ganhe desconto!</h3>
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t";
                // line 199
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["discounts"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["discount"]) {
                    // line 200
                    echo "\t\t\t\t\t\t\t<li>";
                    echo twig_get_attribute($this->env, $this->source, $context["discount"], "quantity", [], "any", false, false, false, 200);
                    echo ($context["text_discount"] ?? null);
                    echo twig_get_attribute($this->env, $this->source, $context["discount"], "price", [], "any", false, false, false, 200);
                    echo "</li>
\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['discount'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 202
                echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t\t";
            }
            // line 205
            echo "\t\t\t\t\t";
        }
        // line 206
        echo "
\t\t\t\t\t<div id=\"product\" class=\"product-options\">
\t\t\t\t\t\t";
        // line 208
        if (($context["quantity"] ?? null)) {
            // line 209
            echo "\t\t\t\t\t\t";
            if (($context["options"] ?? null)) {
                // line 210
                echo "\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                    // line 211
                    echo "\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 211) == "select")) {
                        // line 212
                        echo "\t\t\t\t\t\t<div class=\"form-group";
                        if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 212)) {
                            echo " required ";
                        }
                        echo " custom\">
\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-option";
                        // line 213
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 213);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 213);
                        // line 214
                        echo "</label>
\t\t\t\t\t\t\t<select name=\"option[";
                        // line 215
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 215);
                        echo "]\"
\t\t\t\t\t\t\t\tid=\"input-option";
                        // line 216
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 216);
                        echo "\" class=\"form-control\">
\t\t\t\t\t\t\t\t<option value=\"\">";
                        // line 217
                        echo ($context["text_select"] ?? null);
                        echo "</option>
\t\t\t\t\t\t\t\t";
                        // line 218
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 218));
                        foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                            // line 219
                            echo "\t\t\t\t\t\t\t\t<option value=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 219);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 219);
                            echo "
\t\t\t\t\t\t\t\t\t";
                            // line 220
                            if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 220)) {
                                // line 221
                                echo "\t\t\t\t\t\t\t\t\t(";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 221);
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 221);
                                echo ")
\t\t\t\t\t\t\t\t\t";
                            }
                            // line 223
                            echo "\t\t\t\t\t\t\t\t</option>
\t\t\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 225
                        echo "\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                    }
                    // line 228
                    echo "\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 228) == "radio")) {
                        // line 229
                        echo "\t\t\t\t\t\t<div class=\"form-group";
                        if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 229)) {
                            echo " required ";
                        }
                        echo " custom\">
\t\t\t\t\t\t\t<div class=\"custom-control custom-radio\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">";
                        // line 231
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 231);
                        echo "</label>
\t\t\t\t\t\t\t\t<div id=\"input-option";
                        // line 232
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 232);
                        echo "\">
\t\t\t\t\t\t\t\t\t";
                        // line 233
                        if (((((twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 233) == "Numeração") || (twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 233) == "Tamanhos")) || (twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 233) == "Tamanho")) || (twig_get_attribute($this->env, $this->source,                         // line 234
$context["option"], "name", [], "any", false, false, false, 234) == "Memória"))) {
                            // line 235
                            echo "\t\t\t\t\t\t\t\t\t<ul class=\"ulRadiosCss\">
\t\t\t\t\t\t\t\t\t\t";
                            // line 236
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 236));
                            foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                                // line 237
                                echo "\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t\t\t\t\t\t\t<label>
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"option[";
                                // line 240
                                echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 240);
                                echo "]\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\tid=\"";
                                // line 241
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 241);
                                echo "\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\tvalue=\"";
                                // line 242
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 242);
                                echo "\" ";
                                if (twig_get_attribute($this->env, $this->source,                                 // line 243
$context["option_value"], "price", [], "any", false, false, false, 243)) {
                                    // line 244
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\tdata-priceoption=\"";
                                    echo twig_replace_filter(twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 244), ["R\$ " => "", "," => "."]);
                                    echo "\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                } else {
                                    // line 245
                                    echo " data-priceoption=\"0\" ";
                                }
                                echo " />
\t\t\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 247
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 247);
                                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 248
                                if (twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 248)) {
                                    echo "<img src=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 248);
                                    echo "\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\talt=\"";
                                    // line 249
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 249);
                                    echo " ";
                                    if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 249)) {
                                        echo " ";
                                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 249);
                                        echo " ";
                                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 249);
                                        echo " ";
                                    }
                                    echo "\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tclass=\"img-thumbnail\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 252
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 252)) {
                                    // line 253
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<small>(";
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 253);
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 253);
                                    // line 254
                                    echo ")</small>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 256
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 261
                            echo "\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 263
                            echo "\t\t\t\t\t\t\t\t\t";
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 263));
                            foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                                // line 264
                                echo "\t\t\t\t\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" id=\"";
                                // line 265
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 265);
                                echo "\"
\t\t\t\t\t\t\t\t\t\t\tname=\"option[";
                                // line 266
                                echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 266);
                                echo "]\"
\t\t\t\t\t\t\t\t\t\t\tvalue=\"";
                                // line 267
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 267);
                                echo "\"
\t\t\t\t\t\t\t\t\t\t\tclass=\"custom-control-input\" ";
                                // line 268
                                if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 268)) {
                                    // line 269
                                    echo "\t\t\t\t\t\t\t\t\t\t\tdata-priceoption=\"";
                                    echo twig_replace_filter(twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 269), ["R\$ " => "", "," => "."]);
                                    echo "\"
\t\t\t\t\t\t\t\t\t\t\t";
                                } else {
                                    // line 270
                                    echo " data-priceoption=\"0\" ";
                                }
                                echo " />
\t\t\t\t\t\t\t\t\t\t<label class=\"custom-control-label\"
\t\t\t\t\t\t\t\t\t\t\tfor=\"";
                                // line 272
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 272);
                                echo "\">
\t\t\t\t\t\t\t\t\t\t\t";
                                // line 273
                                if (twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 273)) {
                                    // line 274
                                    echo "\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 274);
                                    echo "\"
\t\t\t\t\t\t\t\t\t\t\t\talt=\"";
                                    // line 275
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 275);
                                    echo " ";
                                    if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 275)) {
                                        echo " ";
                                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 275);
                                        echo " ";
                                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 275);
                                        echo " ";
                                    }
                                    echo "\"
\t\t\t\t\t\t\t\t\t\t\t\tclass=\"img-thumbnail\" />
\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 278
                                echo "\t\t\t\t\t\t\t\t\t\t\t";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 278);
                                echo "
\t\t\t\t\t\t\t\t\t\t\t";
                                // line 279
                                if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 279)) {
                                    // line 280
                                    echo "\t\t\t\t\t\t\t\t\t\t\t(";
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 280);
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 280);
                                    echo ")
\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 282
                                echo "\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 285
                            echo "
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 287
                        echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                    }
                    // line 291
                    echo "\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 291) == "checkbox")) {
                        // line 292
                        echo "\t\t\t\t\t\t<div class=\"form-group";
                        if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 292)) {
                            echo " required ";
                        }
                        echo " custom\">
\t\t\t\t\t\t\t<div class=\"custom-control custom-checkbox\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">";
                        // line 294
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 294);
                        echo "</label>
\t\t\t\t\t\t\t\t<div id=\"input-option";
                        // line 295
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 295);
                        echo "\">
\t\t\t\t\t\t\t\t\t";
                        // line 296
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 296));
                        foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                            // line 297
                            echo "\t\t\t\t\t\t\t\t\t<div class=\"checkbox\">

\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"";
                            // line 299
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 299);
                            echo "\"
\t\t\t\t\t\t\t\t\t\t\tname=\"option[";
                            // line 300
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 300);
                            echo "][]\"
\t\t\t\t\t\t\t\t\t\t\tvalue=\"";
                            // line 301
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 301);
                            echo "\"
\t\t\t\t\t\t\t\t\t\t\tclass=\"custom-control-input\" ";
                            // line 302
                            if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 302)) {
                                // line 303
                                echo "\t\t\t\t\t\t\t\t\t\t\tdata-priceoption=\"";
                                echo twig_replace_filter(twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 303), ["R\$ " => "", "," => "."]);
                                echo "\"
\t\t\t\t\t\t\t\t\t\t\t";
                            } else {
                                // line 304
                                echo " data-priceoption=\"0\" ";
                            }
                            echo " />
\t\t\t\t\t\t\t\t\t\t<label class=\"custom-control-label\"
\t\t\t\t\t\t\t\t\t\t\tfor=\"";
                            // line 306
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 306);
                            echo "\">
\t\t\t\t\t\t\t\t\t\t\t";
                            // line 307
                            if (twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 307)) {
                                // line 308
                                echo "\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 308);
                                echo "\"
\t\t\t\t\t\t\t\t\t\t\t\talt=\"";
                                // line 309
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 309);
                                echo " ";
                                if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 309)) {
                                    echo " ";
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 309);
                                    echo " ";
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 309);
                                    echo " ";
                                }
                                echo "\"
\t\t\t\t\t\t\t\t\t\t\t\tclass=\"img-thumbnail\" />
\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 312
                            echo "\t\t\t\t\t\t\t\t\t\t\t";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 312);
                            echo "
\t\t\t\t\t\t\t\t\t\t\t";
                            // line 313
                            if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 313)) {
                                // line 314
                                echo "\t\t\t\t\t\t\t\t\t\t\t(";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 314);
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 314);
                                echo ")
\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 316
                            echo "\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 319
                        echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                    }
                    // line 323
                    echo "\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 323) == "text")) {
                        // line 324
                        echo "\t\t\t\t\t\t<div class=\"form-group";
                        if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 324)) {
                            echo " required ";
                        }
                        echo " custom\">
\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-option";
                        // line 325
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 325);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 325);
                        // line 326
                        echo "</label>
\t\t\t\t\t\t\t<input type=\"text\" name=\"option[";
                        // line 327
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 327);
                        echo "]\" value=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 327);
                        echo "\"
\t\t\t\t\t\t\t\tplaceholder=\"";
                        // line 328
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 328);
                        echo "\" id=\"input-option";
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 328);
                        echo "\"
\t\t\t\t\t\t\t\tclass=\"form-control\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                    }
                    // line 332
                    echo "\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 332) == "textarea")) {
                        // line 333
                        echo "\t\t\t\t\t\t<div class=\"form-group";
                        if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 333)) {
                            echo " required ";
                        }
                        echo " custom\">
\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-option";
                        // line 334
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 334);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 334);
                        // line 335
                        echo "</label>
\t\t\t\t\t\t\t<textarea name=\"option[";
                        // line 336
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 336);
                        echo "]\" rows=\"5\"
\t\t\t\t\t\t\t\tplaceholder=\"";
                        // line 337
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 337);
                        echo "\" id=\"input-option";
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 337);
                        echo "\"
\t\t\t\t\t\t\t\tclass=\"form-control\">";
                        // line 338
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 338);
                        echo "</textarea>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                    }
                    // line 341
                    echo "\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 341) == "file")) {
                        // line 342
                        echo "\t\t\t\t\t\t<div class=\"form-group";
                        if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 342)) {
                            echo " required ";
                        }
                        echo " custom\">
\t\t\t\t\t\t\t<label class=\"control-label\">";
                        // line 343
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 343);
                        echo "</label>
\t\t\t\t\t\t\t<button type=\"button\" id=\"button-upload";
                        // line 344
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 344);
                        echo "\"
\t\t\t\t\t\t\t\tdata-loading-text=\"";
                        // line 345
                        echo ($context["text_loading"] ?? null);
                        echo "\" class=\"btn btn-default btn-block\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-upload\"></i>
\t\t\t\t\t\t\t\t";
                        // line 347
                        echo ($context["button_upload"] ?? null);
                        echo "</button>
\t\t\t\t\t\t\t<input type=\"hidden\" name=\"option[";
                        // line 348
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 348);
                        echo "]\" value=\"\"
\t\t\t\t\t\t\t\tid=\"input-option";
                        // line 349
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 349);
                        echo "\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                    }
                    // line 352
                    echo "\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 352) == "date")) {
                        // line 353
                        echo "\t\t\t\t\t\t<div class=\"form-group";
                        if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 353)) {
                            echo " required ";
                        }
                        echo " custom\">
\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-option";
                        // line 354
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 354);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 354);
                        // line 355
                        echo "</label>
\t\t\t\t\t\t\t<div class=\"input-group date\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"option[";
                        // line 357
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 357);
                        echo "]\"
\t\t\t\t\t\t\t\t\tvalue=\"";
                        // line 358
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 358);
                        echo "\" data-date-format=\"YYYY-MM-DD\"
\t\t\t\t\t\t\t\t\tid=\"input-option";
                        // line 359
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 359);
                        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default\" type=\"button\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-calendar\"></i>
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                    }
                    // line 368
                    echo "\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 368) == "datetime")) {
                        // line 369
                        echo "\t\t\t\t\t\t<div class=\"form-group";
                        if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 369)) {
                            echo " required ";
                        }
                        echo " custom\">
\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-option";
                        // line 370
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 370);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 370);
                        // line 371
                        echo "</label>
\t\t\t\t\t\t\t<div class=\"input-group datetime\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"option[";
                        // line 373
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 373);
                        echo "]\"
\t\t\t\t\t\t\t\t\tvalue=\"";
                        // line 374
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 374);
                        echo "\" data-date-format=\"YYYY-MM-DD HH:mm\"
\t\t\t\t\t\t\t\t\tid=\"input-option";
                        // line 375
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 375);
                        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-calendar\"></i>
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                    }
                    // line 384
                    echo "\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 384) == "time")) {
                        // line 385
                        echo "\t\t\t\t\t\t<div class=\"form-group";
                        if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 385)) {
                            echo " required ";
                        }
                        echo " custom\">
\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-option";
                        // line 386
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 386);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 386);
                        // line 387
                        echo "</label>
\t\t\t\t\t\t\t<div class=\"input-group time\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"option[";
                        // line 389
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 389);
                        echo "]\"
\t\t\t\t\t\t\t\t\tvalue=\"";
                        // line 390
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 390);
                        echo "\" data-date-format=\"HH:mm\"
\t\t\t\t\t\t\t\t\tid=\"input-option";
                        // line 391
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 391);
                        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-calendar\"></i>
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                    }
                    // line 400
                    echo "\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 401
                echo "\t\t\t\t\t\t";
            }
            // line 402
            echo "\t\t\t\t\t\t";
            if (($context["recurrings"] ?? null)) {
                // line 403
                echo "\t\t\t\t\t\t<hr>
\t\t\t\t\t\t<h3>";
                // line 404
                echo ($context["text_payment_recurring"] ?? null);
                echo "</h3>
\t\t\t\t\t\t<div class=\"form-group required custom\">
\t\t\t\t\t\t\t<select name=\"recurring_id\" class=\"form-control\">
\t\t\t\t\t\t\t\t<option value=\"\">";
                // line 407
                echo ($context["text_select"] ?? null);
                echo "</option>
\t\t\t\t\t\t\t\t";
                // line 408
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["recurrings"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["recurring"]) {
                    // line 409
                    echo "\t\t\t\t\t\t\t\t<option value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["recurring"], "recurring_id", [], "any", false, false, false, 409);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["recurring"], "name", [], "any", false, false, false, 409);
                    echo "</option>
\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recurring'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 411
                echo "\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t<div class=\"help-block\" id=\"recurring-description\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            }
            // line 415
            echo "

\t\t\t\t\t\t<div class=\"box-buy\">
\t\t\t\t\t\t\t<div class=\"d-flex flex-column flex-sm-column flex-md-column flex-lg-row\">
\t\t\t\t\t\t\t\t<div class=\"\">
\t\t\t\t\t\t\t\t\t<div class=\"box-add-quantity\">
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"quantity\" value=\"";
            // line 421
            echo ($context["minimum"] ?? null);
            echo "\" size=\"2\" min=\"1\"
\t\t\t\t\t\t\t\t\t\t\tid=\"input-quantity\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-primary\" id=\"minus\">-</button>
\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-primary\" id=\"plus\">+</button>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<script>
\t\t\t\t\t\t\t\t\t\t\$(\"#plus\").click(function () {
\t\t\t\t\t\t\t\t\t\t\tvar newQty = + (\$(\"#input-quantity\").val()) + 1;
\t\t\t\t\t\t\t\t\t\t\t\$(\"#input-quantity\").val(newQty);
\t\t\t\t\t\t\t\t\t\t});

\t\t\t\t\t\t\t\t\t\t\$(\"#minus\").click(function () {
\t\t\t\t\t\t\t\t\t\t\tvar newQty = + (\$(\"#input-quantity\").val()) - 1;
\t\t\t\t\t\t\t\t\t\t\tif (newQty < 1)
\t\t\t\t\t\t\t\t\t\t\t\tnewQty = 1;

\t\t\t\t\t\t\t\t\t\t\t\$(\"#input-quantity\").val(newQty);
\t\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- -->
\t\t\t\t\t\t\t\t<div class=\"flex-grow-1\" id=\"box-button\">
\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"product_id\" value=\"";
            // line 446
            echo ($context["product_id"] ?? null);
            echo "\" />
\t\t\t\t\t\t\t\t\t<button type=\"button\" id=\"button-cart\" data-loading-text=\"";
            // line 447
            echo ($context["text_loading"] ?? null);
            echo "\"
\t\t\t\t\t\t\t\t\t\tclass=\"btn btn-primary btn-lg btn-block\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-shopping-cart\"></i>
\t\t\t\t\t\t\t\t\t\t";
            // line 450
            echo ($context["button_add_cart"] ?? null);
            echo "</button>
\t\t\t\t\t\t\t\t\t<hr class=\"\">
\t\t\t\t\t\t\t\t\t<a href=\"javascript:history.back()\" class=\"btn btn-secondary btn-block\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-reply\"></i>
\t\t\t\t\t\t\t\t\t\tVoltar as compras</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- -->
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- d-flex -->
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
        } else {
            // line 461
            echo "\t\t\t\t\t\t<div class=\"alert alert-danger alert-stock\">Sem estoque</div>
\t\t\t\t\t\t";
        }
        // line 463
        echo "

\t\t\t\t\t\t";
        // line 465
        if ( !($context["shipping"] ?? null)) {
            // line 466
            echo "\t\t\t\t\t\t<img src=\"./_galerias/shipping/frete_gratis_gif.gif\" alt=\"Frete Grátis\"
\t\t\t\t\t\t\tclass=\"img-responsive m-3\">
\t\t\t\t\t\t";
        } else {
            // line 469
            echo "\t\t\t\t\t\t<!-- Simulate Shipping -->
\t\t\t\t\t\t<div class=\"box-shipping\">
\t\t\t\t\t\t\t<select name=\"country_id\" id=\"input-country\" class=\"form-control\">
\t\t\t\t\t\t\t\t<option value=\"\">";
            // line 472
            echo ($context["text_select"] ?? null);
            echo "</option>
\t\t\t\t\t\t\t\t";
            // line 473
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["countries"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
                // line 474
                echo "\t\t\t\t\t\t\t\t";
                if ((twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 474) == ($context["country_id"] ?? null))) {
                    // line 475
                    echo "\t\t\t\t\t\t\t\t<option value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 475);
                    echo "\" selected=\"selected\">";
                    echo twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, false, 475);
                    echo "</option>
\t\t\t\t\t\t\t\t";
                } else {
                    // line 477
                    echo "\t\t\t\t\t\t\t\t<option value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["country"], "country_id", [], "any", false, false, false, 477);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, false, 477);
                    echo "</option>
\t\t\t\t\t\t\t\t";
                }
                // line 479
                echo "\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 480
            echo "\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t<select name=\"zone_id\" id=\"input-zone\" class=\"form-control\"></select>
\t\t\t\t\t\t\t<div class=\"form-inline\">
\t\t\t\t\t\t\t\t<label for=\"\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-truck\"></i>
\t\t\t\t\t\t\t\t\t";
            // line 485
            echo ($context["text_title1"] ?? null);
            echo "</label>
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<input type=\"tel\" value=\"";
            // line 487
            echo ($context["postcode"] ?? null);
            echo "\" maxlength=\"9\" name=\"postcode\"
\t\t\t\t\t\t\t\t\t\tid=\"input-postcode\" class=\"form-control\" placeholder=\"Digite o seu CEP\" />
\t\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t\t<button type=\"button\" id=\"simular\" data-loading-text=\"";
            // line 490
            echo ($context["text_loading"] ?? null);
            echo "\"
\t\t\t\t\t\t\t\t\t\t\tclass=\"button btn btn-sm btn-primary\">Simular Frete</button>
\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- /input-group -->

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- form-inline -->
\t\t\t\t\t\t\t<div class=\"simulacao-frete\" style=\"display:none;\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- form-group -->
\t\t\t\t\t<!-- Simulate Shipping -->
\t\t\t\t\t";
        }
        // line 504
        echo "
\t\t\t\t\t";
        // line 505
        if ((($context["minimum"] ?? null) > 1)) {
            // line 506
            echo "\t\t\t\t\t<div class=\"alert alert-info\">
\t\t\t\t\t\t<i class=\"fa fa-info-circle\"></i>
\t\t\t\t\t\t";
            // line 508
            echo ($context["text_minimum"] ?? null);
            echo "
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 511
        echo "\t\t\t\t\t";
        if (($context["review_status"] ?? null)) {
            // line 512
            echo "\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t";
            // line 514
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 5));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 515
                echo "\t\t\t\t\t\t\t";
                if ((($context["rating"] ?? null) < $context["i"])) {
                    echo " <span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-1x\"></i>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t";
                } else {
                    // line 519
                    echo "\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-stack-1x\"></i>
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-1x\"></i>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t";
                }
                // line 524
                echo "\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 525
            echo "\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<a href=\"#review_shows\" class=\"scrollReview\">";
            // line 526
            echo ($context["reviews"] ?? null);
            echo "</a>
\t\t\t\t\t\t\t\t/
\t\t\t\t\t\t\t\t<a href=\"#form-review\"
\t\t\t\t\t\t\t\t\tonclick=\"\$('a[href=\\'#tab-review\\']').trigger('click'); return false;\"
\t\t\t\t\t\t\t\t\tclass=\"scrollReview\">";
            // line 530
            echo ($context["text_write"] ?? null);
            echo "</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 534
        echo "\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-12 col-md-10 col-md-offset-1\">
\t\t\t\t\t<div class=\"box-description\">
\t\t\t\t\t\t<div class=\"heading-page\">
\t\t\t\t\t\t\t<h3>Descrição</h3>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
        // line 545
        echo ($context["description"] ?? null);
        echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-12 col-md-10 col-md-offset-1\">
\t\t\t\t\t<ul class=\"nav nav-tabs\">
\t\t\t\t\t\t";
        // line 555
        if (($context["attribute_groups"] ?? null)) {
            // line 556
            echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"#tab-specification\" data-toggle=\"tab\">";
            // line 557
            echo ($context["tab_attribute"] ?? null);
            echo "</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
        }
        // line 560
        echo "\t\t\t\t\t\t";
        if (($context["review_status"] ?? null)) {
            // line 561
            echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a href=\"#tab-review\" data-toggle=\"tab\">";
            // line 562
            echo ($context["tab_review"] ?? null);
            echo "</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
        }
        // line 565
        echo "\t\t\t\t\t</ul>
\t\t\t\t\t<div class=\"tab-content\">

\t\t\t\t\t\t";
        // line 568
        if (($context["attribute_groups"] ?? null)) {
            // line 569
            echo "\t\t\t\t\t\t<div class=\"tab-pane\" id=\"tab-specification\">
\t\t\t\t\t\t\t<table class=\"table table-bordered\">
\t\t\t\t\t\t\t\t";
            // line 571
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["attribute_groups"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute_group"]) {
                // line 572
                echo "\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td colspan=\"2\">
\t\t\t\t\t\t\t\t\t\t\t<strong>";
                // line 575
                echo twig_get_attribute($this->env, $this->source, $context["attribute_group"], "name", [], "any", false, false, false, 575);
                echo "</strong>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t";
                // line 580
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["attribute_group"], "attribute", [], "any", false, false, false, 580));
                foreach ($context['_seq'] as $context["_key"] => $context["attribute"]) {
                    // line 581
                    echo "\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>";
                    // line 582
                    echo twig_get_attribute($this->env, $this->source, $context["attribute"], "name", [], "any", false, false, false, 582);
                    echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>";
                    // line 583
                    echo twig_get_attribute($this->env, $this->source, $context["attribute"], "text", [], "any", false, false, false, 583);
                    echo "</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 586
                echo "\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_group'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 588
            echo "\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
        }
        // line 591
        echo "\t\t\t\t\t\t";
        if (($context["review_status"] ?? null)) {
            // line 592
            echo "\t\t\t\t\t\t<div class=\"tab-pane\" id=\"tab-review\">
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-12 col-md-6\">
\t\t\t\t\t\t\t\t\t<div class=\"panel panel-default\">
\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t\t<form class=\"form-horizontal\" id=\"form-review\">
\t\t\t\t\t\t\t\t\t\t\t\t";
            // line 598
            if (($context["review_guest"] ?? null)) {
                // line 599
                echo "\t\t\t\t\t\t\t\t\t\t\t\t<h3>";
                echo ($context["text_write"] ?? null);
                echo "</h3>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group required\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-name\">";
                // line 602
                echo ($context["entry_name"] ?? null);
                // line 603
                echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"name\" value=\"";
                // line 604
                echo ($context["customer_name"] ?? null);
                echo "\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\trequired id=\"input-name\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group required\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-review\">";
                // line 610
                echo ($context["entry_review"] ?? null);
                // line 611
                echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"text\" id=\"input-review\" maxlength=\"155\" required
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tclass=\"form-control\"></textarea>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"help-block\">Limite:
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span id=\"limitLengthReview\"></span>/155 || Mínimo: 25
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tcaracteres
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group required\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"control-label\">";
                // line 622
                echo ($context["entry_rating"] ?? null);
                echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t&nbsp;&nbsp;&nbsp; 1&nbsp;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"rating\" value=\"1\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t&nbsp;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"rating\" value=\"2\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t&nbsp;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"rating\" value=\"3\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t&nbsp;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"rating\" value=\"4\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t&nbsp;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"rating\" value=\"5\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t&nbsp;5
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group required form-avatares\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-sm-12 control-label\">Avatar</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 639
                $context["max"] = 19;
                // line 640
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, ($context["max"] ?? null)));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 641
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"input-avatar-";
                    echo $context["i"];
                    echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"avatar\" id=\"input-avatar-";
                    // line 642
                    echo $context["i"];
                    echo "\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tvalue=\"";
                    // line 643
                    echo $context["i"];
                    echo "\" ";
                    if (($context["i"] == 1)) {
                        echo " checked ";
                    }
                    echo " />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"review-avatar\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"_galerias/avatares/";
                    // line 645
                    echo $context["i"];
                    echo ".png\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\talt=\"Avatar ";
                    // line 646
                    echo $context["i"];
                    echo "\" class=\"img-responsive\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 650
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 652
                echo ($context["captcha"] ?? null);
                echo "
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"buttons clearfix\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"pull-left\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" id=\"button-review\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tdata-loading-text=\"";
                // line 656
                echo ($context["text_loading"] ?? null);
                echo "\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tclass=\"btn btn-sm btn-primary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-check\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tEnviar Comentário</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 663
                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                echo ($context["text_login"] ?? null);
                echo "
\t\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 665
            echo "\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"review\"></div>
\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<a href=\"#form-review\" onclick=\"\$('a[href=\\'#tab-review\\']').trigger('click'); return false;\"
\t\t\t\t\t\t\tclass=\"btn btn-sm btn-primary scrollViewReview\">Deixe seu comentário</a>
\t\t\t\t\t\t";
        }
        // line 675
        echo "\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- hidden-xs tabs -->
\t\t\t</div>
\t\t</div>

\t\t<!-- REVIEW -->
\t\t<div class=\"row\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t";
        // line 685
        if (($context["review_status"] ?? null)) {
            // line 686
            echo "\t\t\t\t\t<div id=\"review_shows\"></div>
\t\t\t\t\t";
        }
        // line 688
        echo "\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<!-- REVIEW -->

\t\t";
        // line 693
        if (($context["products"] ?? null)) {
            // line 694
            echo "\t\t<div class=\"module-general module_product_related\">
\t\t\t<div class=\"heading-page\">
\t\t\t\t<h3>";
            // line 696
            echo ($context["text_related"] ?? null);
            echo "</h3>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t";
            // line 699
            $context["i"] = 0;
            // line 700
            echo "\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 701
                echo "\t\t\t\t";
                if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
                    // line 702
                    echo "\t\t\t\t";
                    $context["class"] = "col-xs-8 col-sm-6";
                    // line 703
                    echo "\t\t\t\t";
                } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
                    // line 704
                    echo "\t\t\t\t";
                    $context["class"] = "col-xs-6 col-md-4";
                    // line 705
                    echo "\t\t\t\t";
                } else {
                    // line 706
                    echo "\t\t\t\t";
                    $context["class"] = "col-xs-12 col-sm-3";
                    // line 707
                    echo "\t\t\t\t";
                }
                // line 708
                echo "\t\t\t\t<div class=\"";
                echo ($context["class"] ?? null);
                echo "\">
\t\t\t\t\t<div class=\"product-layout \">
\t\t\t\t\t\t<div class=\"product-thumb transition\">
\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t";
                // line 712
                if (twig_get_attribute($this->env, $this->source, $context["product"], "valor_desconto", [], "any", false, false, false, 712)) {
                    // line 713
                    echo "\t\t\t\t\t\t\t\t<span class=\"badge badge-danger badge-desconto\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "valor_desconto", [], "any", false, false, false, 713);
                    echo "%</span>
\t\t\t\t\t\t\t\t";
                }
                // line 715
                echo "\t\t\t\t\t\t\t\t<a href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 715);
                echo "\"><img src=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 715);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 715);
                echo "\"
\t\t\t\t\t\t\t\t\t\ttitle=\"";
                // line 716
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 716);
                echo "\" class=\"img-responsive\" /></a>
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn-wishlist\" data-toggle=\"tooltip\"
\t\t\t\t\t\t\t\t\ttitle=\"";
                // line 718
                echo ($context["button_wishlist"] ?? null);
                echo "\" onclick=\"wishlist.add('";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 718);
                echo "');\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-heart\"></i>
\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t<h4>
\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 724
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 724);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 724);
                echo "</a>
\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t";
                // line 726
                if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 726)) {
                    // line 727
                    echo "\t\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t\t";
                    // line 728
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 729
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ((twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 729) < $context["i"])) {
                            echo " <span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 733
                            echo "\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 738
                        echo "\t\t\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 739
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 739)) {
                        // line 740
                        echo "\t\t\t\t\t\t\t\t\t\t(";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 740);
                        echo ")
\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 742
                    echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                }
                // line 744
                echo "\t\t\t\t\t\t\t\t";
                if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 744)) {
                    // line 745
                    echo "\t\t\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 745) == "R\$ 0,00")) {
                        // line 746
                        echo "\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-current\">
\t\t\t\t\t\t\t\t\t\t\t\t<small>Sob-consulta</small>
\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                    } else {
                        // line 756
                        echo "\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t";
                        // line 758
                        if ( !twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 758)) {
                            // line 759
                            echo "\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-current\">";
                            // line 760
                            echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 760);
                            echo "</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 763
                            echo "\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-old\">";
                            // line 764
                            echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 764);
                            echo "</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                            // line 767
                            echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 767);
                            echo "</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 770
                        echo "\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                    }
                    // line 773
                    echo "\t\t\t\t\t\t\t\t";
                }
                // line 774
                echo "\t\t\t\t\t\t\t\t";
                if ( !twig_get_attribute($this->env, $this->source, $context["product"], "quantity", [], "any", false, false, false, 774)) {
                    // line 775
                    echo "\t\t\t\t\t\t\t\t<div class=\"alert alert-danger alert-stock\">Sem estoque</div>
\t\t\t\t\t\t\t\t";
                }
                // line 777
                echo "\t\t\t\t\t\t\t\t<div class=\"button-group\">
\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 778
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 778);
                echo "\">
\t\t\t\t\t\t\t\t\t\t<span class=\"\">";
                // line 779
                echo ($context["button_conferir"] ?? null);
                echo "</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t";
                // line 786
                if (((($context["column_left"] ?? null) && ($context["column_right"] ?? null)) && (((($context["i"] ?? null) + 1) % 2) == 0))) {
                    // line 787
                    echo "\t\t\t\t<div class=\"clearfix visible-md visible-sm\"></div>
\t\t\t\t";
                } elseif ((                // line 788
($context["column_left"] ?? null) || (($context["column_right"] ?? null) && (((($context["i"] ?? null) + 1) % 3) == 0)))) {
                    // line 789
                    echo "\t\t\t\t<div class=\"clearfix visible-md\"></div>
\t\t\t\t";
                } elseif ((((                // line 790
($context["i"] ?? null) + 1) % 4) == 0)) {
                    // line 791
                    echo "\t\t\t\t<div class=\"clearfix visible-md\"></div>
\t\t\t\t";
                }
                // line 793
                echo "\t\t\t\t";
                $context["i"] = (($context["i"] ?? null) + 1);
                // line 794
                echo "\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 795
            echo "\t\t\t</div>
\t\t</div>
\t\t";
        }
        // line 798
        echo "\t\t";
        if (($context["tags"] ?? null)) {
            // line 799
            echo "\t\t<p>";
            echo ($context["text_tags"] ?? null);
            echo "
\t\t\t";
            // line 800
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["tags"] ?? null)) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 801
                echo "\t\t\t";
                if (($context["i"] < (twig_length_filter($this->env, ($context["tags"] ?? null)) - 1))) {
                    echo " <a href=\"";
                    echo twig_get_attribute($this->env, $this->source, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["tags"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[$context["i"]] ?? null) : null), "href", [], "any", false, false, false, 801);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["tags"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[$context["i"]] ?? null) : null), "tag", [], "any", false, false, false, 801);
                    echo "</a>,
\t\t\t\t";
                } else {
                    // line 803
                    echo "\t\t\t\t<a href=\"";
                    echo twig_get_attribute($this->env, $this->source, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["tags"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[$context["i"]] ?? null) : null), "href", [], "any", false, false, false, 803);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["tags"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[$context["i"]] ?? null) : null), "tag", [], "any", false, false, false, 803);
                    echo "</a>
\t\t\t\t";
                }
                // line 805
                echo "\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 806
            echo "\t\t</p>
\t\t";
        }
        // line 808
        echo "\t\t";
        echo ($context["content_bottom"] ?? null);
        echo "
\t</div>
\t";
        // line 810
        echo ($context["column_right"] ?? null);
        echo "
</div>
</div>
<script type=\"text/javascript\">
\t\$('select[name=\\'recurring_id\\'], input[name=\"quantity\"]').change(function () {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=product/product/getRecurringDescription',
\t\t\ttype: 'post',
\t\t\tdata: \$('input[name=\\'product_id\\'], input[name=\\'quantity\\'], select[name=\\'recurring_id\\']'),
\t\t\tdataType: 'json',
\t\t\tbeforeSend: function () {
\t\t\t\t\$('#recurring-description').html('');
\t\t\t},
\t\t\tsuccess: function (json) {
\t\t\t\t\$('.alert-dismissible, .text-danger').remove();

\t\t\t\tif (json['success']) {
\t\t\t\t\t\$('#recurring-description').html(json['success']);
\t\t\t\t}
\t\t\t}
\t\t});
\t});
//--></script>
<script type=\"text/javascript\">
\t\$('#button-cart').on('click', function () {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=checkout/cart/add',
\t\t\ttype: 'post',
\t\t\tdata: \$('#product input[type=\\'text\\'], #product input[type=\\'hidden\\'], #product input[type=\\'radio\\']:checked, #product input[type=\\'checkbox\\']:checked, #product select, #product textarea'),
\t\t\tdataType: 'json',
\t\t\tbeforeSend: function () {
\t\t\t\t\$('#button-cart').button('loading');
\t\t\t},
\t\t\tcomplete: function () {
\t\t\t\t\$('#button-cart').button('reset');
\t\t\t},
\t\t\tsuccess: function (json) {
\t\t\t\t\$('.alert-dismissible, .text-danger').remove();
\t\t\t\t\$('.form-group').removeClass('has-error');

\t\t\t\tif (json['error']) {
\t\t\t\t\tif (json['error']['option']) {
\t\t\t\t\t\tfor (i in json['error']['option']) {
\t\t\t\t\t\t\tvar element = \$('#input-option' + i.replace('_', '-'));

\t\t\t\t\t\t\tif (element.parent().hasClass('input-group')) {
\t\t\t\t\t\t\t\telement.parent().after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\telement.after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}

\t\t\t\t\t}
\t\t\t\t\tif (json['error']['recurring']) {
\t\t\t\t\t\t\$('select[name=\\'recurring_id\\']').after('<div class=\"text-danger\">' + json['error']['recurring'] + '</div>');
\t\t\t\t\t}

\t\t\t\t\t// Highlight any found errors
\t\t\t\t\t\$('.text-danger').parent().addClass('has-error');

\t\t\t\t\t// Scroll page to Elemento Error
\t\t\t\t\t\$('html, body').animate({
\t\t\t\t\t\tscrollTop: \$('#product').first().offset().top
\t\t\t\t\t}, 'slow');
\t\t\t\t}

\t\t\t\tif (json['success']) {
\t\t\t\t\t\$('.box-buy').after('<div class=\"alert alert-success alert-dismissible alert-add-off\">' + json['success'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
\t\t\t\t\tsetTimeout(function () {
\t\t\t\t\t\t\$('.alert.alert-success.alert-dismissible').hide('slow');
\t\t\t\t\t}, 2500);

\t\t\t\t\t\$('#cart #cart-total').html('' + json['total2'] + '');
\t\t\t\t\t\$('#cart-total').html('' + json['total2'] + '');
\t\t\t\t\t\$('.header-mobile #top #cart a #qtaCart').html('' + json['total2'] + '');
\t\t\t\t\t// \$('.header-mobile #top #cart').load('index.php?route=common/header/info div li#cart a');

\t\t\t\t\t\$('html, body').animate({
\t\t\t\t\t\tscrollTop: 0
\t\t\t\t\t}, 'slow');

\t\t\t\t\t\$('#headerDesktop #cart .sub-menu').load('index.php?route=common/cart/info ul');
\t\t\t\t\t\$('.footer-mobile #cart-total').load('index.php?route=common/footer/info div a span');
\t\t\t\t\t// ao clicar em comprar depois de 1.5s envia para a página de carrinho
\t\t\t\t\tsetTimeout(function () {
\t\t\t\t\t\t\$(location).attr('href', 'index.php?route=checkout/cart');
\t\t\t\t\t}, 1500);

\t\t\t\t}
\t\t\t},
\t\t\terror: function (xhr, ajaxOptions, thrownError) {
\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t}
\t\t});
\t});
//--></script>
<script type=\"text/javascript\">
\t\$('.date').datetimepicker({ language: '";
        // line 907
        echo ($context["datepicker"] ?? null);
        echo "', pickTime: false });

\t\$('.datetime').datetimepicker({ language: '";
        // line 909
        echo ($context["datepicker"] ?? null);
        echo "', pickDate: true, pickTime: true });

\t\$('.time').datetimepicker({ language: '";
        // line 911
        echo ($context["datepicker"] ?? null);
        echo "', pickDate: false });

\t\$('.scrollReview').click(function () {
\t\t\$('html, body').animate({
\t\t\tscrollTop: \$(\$(this).attr('href')).offset().top
\t\t}, 750);
\t\treturn false;
\t});

\t\$(\"textarea[maxlength]\").bind('input propertychange', function () {
\t\tvar maxLength = \$(this).attr('maxlength');
\t\tif (\$(this).val().length > maxLength) {
\t\t\t\$(this).val(\$(this).val().substring(0, maxLength));
\t\t}
\t});

\t\$(\"#limitLengthReview\").html('0').addClass('font-bold');
\t\$(\"#input-review\").keyup(function () {
\t\tvar tamanho = \$(\"#input-review\").val().length;
\t\tif (tamanho <= 25) {
\t\t\t\$(\"#limitLengthReview\").removeClass('text-success');
\t\t\t\$(\"#limitLengthReview\").addClass('text-danger');
\t\t} else {
\t\t\t\$(\"#limitLengthReview\").removeClass('text-danger');
\t\t\t\$(\"#limitLengthReview\").addClass('text-success');
\t\t}
\t\t\$(\"#limitLengthReview\").html(tamanho);
\t});

\t\$('button[id^=\\'button-upload\\']').on('click', function () {
\t\tvar node = this;

\t\t\$('#form-upload').remove();

\t\t\$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file\" /></form>');

\t\t\$('#form-upload input[name=\\'file\\']').trigger('click');

\t\tif (typeof timer != 'undefined') {
\t\t\tclearInterval(timer);
\t\t}

\t\ttimer = setInterval(function () {
\t\t\tif (\$('#form-upload input[name=\\'file\\']').val() != '') {
\t\t\t\tclearInterval(timer);

\t\t\t\t\$.ajax({
\t\t\t\t\turl: 'index.php?route=tool/upload',
\t\t\t\t\ttype: 'post',
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tdata: new FormData(\$('#form-upload')[0]),
\t\t\t\t\tcache: false,
\t\t\t\t\tcontentType: false,
\t\t\t\t\tprocessData: false,
\t\t\t\t\tbeforeSend: function () {
\t\t\t\t\t\t\$(node).button('loading');
\t\t\t\t\t},
\t\t\t\t\tcomplete: function () {
\t\t\t\t\t\t\$(node).button('reset');
\t\t\t\t\t},
\t\t\t\t\tsuccess: function (json) {
\t\t\t\t\t\t\$('.text-danger').remove();

\t\t\t\t\t\tif (json['error']) {
\t\t\t\t\t\t\t\$(node).parent().find('input').after('<div class=\"text-danger\">' + json['error'] + '</div>');
\t\t\t\t\t\t}

\t\t\t\t\t\tif (json['success']) {
\t\t\t\t\t\t\talert(json['success']);

\t\t\t\t\t\t\t\$(node).parent().find('input').val(json['code']);
\t\t\t\t\t\t}
\t\t\t\t\t},
\t\t\t\t\terror: function (xhr, ajaxOptions, thrownError) {
\t\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t\t}
\t\t\t\t});
\t\t\t}
\t\t}, 500);
\t});
//--></script>
<script type=\"text/javascript\">
\t\$('#review_shows').delegate('.pagination a', 'click', function (e) {
\t\te.preventDefault();

\t\t\$('#review_shows').fadeOut('slow');

\t\t\$('#review_shows').load(this.href);

\t\t\$('#review_shows').fadeIn('slow');
\t});

\t\$('#review_shows').load('index.php?route=product/product/review&product_id=";
        // line 1003
        echo ($context["product_id"] ?? null);
        echo "');

\t\$(\"#form-review input[name=\\'name\\']\").prop('required', true);

\t\$('#button-review').on('click', function () {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=product/product/write&product_id= ";
        // line 1009
        echo ($context["product_id"] ?? null);
        echo "',
\t\t\ttype: 'post',
\t\t\tdataType: 'json',
\t\t\t// data: \$(\"#form-review\").serialize(),

\t\t\tdata: 'name=' + encodeURIComponent(\$('#form-review input[name=\\'name\\']').val()) + '&text=' + encodeURIComponent(\$('#form-review textarea[name=\\'text\\']').val()) + '&rating=' + encodeURIComponent(\$('#form-review input[name=\\'rating\\']:checked').val() ? \$('#form-review input[name=\\'rating\\']:checked').val() : '') + '&avatar=' + encodeURIComponent(\$('#form-review input[name=\\'avatar\\']:checked').val()),

\t\t\tbeforeSend: function () {
\t\t\t\t\$('#button-review').button('loading');
\t\t\t},
\t\t\tcomplete: function () {
\t\t\t\t\$('#button-review').button('reset');
\t\t\t},
\t\t\tsuccess: function (json) {
\t\t\t\t\$('.alert-dismissible').remove();

\t\t\t\tif (json['error']) {
\t\t\t\t\t\$('#review').after('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + '</div>');
\t\t\t\t}

\t\t\t\tif (json['success']) {
\t\t\t\t\t\$('#review').after('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');

\t\t\t\t\t\$('input[name=\\'name\\']').val('');
\t\t\t\t\t\$('textarea[name=\\'text\\']').val('');
\t\t\t\t\t\$('input[name=\\'rating\\']:checked').prop('checked', false);
\t\t\t\t\t\$('input[name=\\'avatar\\']:checked').prop('checked', false);

\t\t\t\t}
\t\t\t}
\t\t});
\t});

\t\$(document).ready(function () {
\t\t\$('.thumbnails').magnificPopup({
\t\t\ttype: 'image',
\t\t\tdelegate: 'a',
\t\t\tgallery: {
\t\t\t\tenabled: true
\t\t\t}
\t\t});
\t});
//--></script>
<script>

\tjQuery(function () {
\t\t\$(document).ready(function () { // Criar a var que armazena o valor do produto original e retira cifrao e pontuacao
\t\t\tvar priceProduct = Number(\$(\".product-price .price-new\").text().replace(\"R\$ \", \"\").replace(\".\", \"\").replace(\",\", \".\").replace(\"por \", \"\"));

\t\t\t// Quando um item foi click aciona a funcao
\t\t\t\$(\".product-options input, #input-quantity, #minus, #plus\").click(function () { // Quando um input for marcado faz a soma do valor do produto
\t\t\t\tvar total = \$('.product-options input:checked').get().reduce(function (tot, el) {
\t\t\t\t\treturn tot + Number(el.dataset.priceoption);
\t\t\t\t}, priceProduct);

\t\t\t\t// Formata o valor da soma e insere em uma var
\t\t\t\tvar totalFormated = total.toLocaleString(\"pt-BR\", {
\t\t\t\t\tstyle: \"currency\",
\t\t\t\t\tcurrency: \"BRL\"
\t\t\t\t});

\t\t\t\t// Aplica no HTML o novo valor
\t\t\t\t\$('.product-price .price-new').text(totalFormated);

\t\t\t\t// Caso a quantidade for alterada faz a multiplicacao
\t\t\t\tvar qta = \$(\"#input-quantity\").val();
\t\t\t\tvar totalFinale = total * qta;

\t\t\t\t// Aplica novamente no HTML o novo valor
\t\t\t\t\$('.product-price .price-new').text(totalFinale.toLocaleString(\"pt-BR\", {
\t\t\t\t\tstyle: \"currency\",
\t\t\t\t\tcurrency: \"BRL\"
\t\t\t\t}));
\t\t\t\t// console.log('Final:'+ total +' quando mutiplicado'+ totalFinale);

\t\t\t});
\t\t});
\t});</script><!-- Simulador de Frete -->
<script type=\"text/javascript\">
\t\$('#simular').on('click', function () {
\t\tcalcularfrete();
\t\t\$.ajax({
\t\t\turl: 'index.php?route=product/product/quoteProduct',
\t\t\ttype: 'post',
\t\t\tdata: \$('#product input[type=\\'text\\'],#product input[type=\\'hidden\\'],#product input[type=\\'radio\\']:checked,#product input[type=\\'checkbox\\']:checked,#product select,#product textarea'),
\t\t\tdataType: 'json',
\t\t\tbeforeSend: function () {
\t\t\t\t\$('#simular').button('loading')
\t\t\t},
\t\t\tcomplete: function () { },
\t\t\tsuccess: function (json) {
\t\t\t\t\$('.alert, .text-danger').remove();
\t\t\t\t\$.ajax({
\t\t\t\t\turl: 'index.php?route=product/product/quote',
\t\t\t\t\ttype: 'post',
\t\t\t\t\tdata: 'country_id=' + \$('#input-country').val() + '&zone_id=' + \$('#input-zone').val() + '&postcode=' + encodeURIComponent(\$('#input-postcode').val()),
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function (json) {
\t\t\t\t\t\tif (json['error']) {
\t\t\t\t\t\t\tif (json['error']['warning']) {
\t\t\t\t\t\t\t\t\$('.simulacao-frete').after('<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
\t\t\t\t\t\t\t\t\$('html, body').animate({
\t\t\t\t\t\t\t\t\tscrollTop: 0
\t\t\t\t\t\t\t\t}, 'slow');
\t\t\t\t\t\t\t\t\$('#simular').button('reset');
\t\t\t\t\t\t\t\t\$('.fa-spin').remove();
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\tif (json['error']['postcode']) { // alert(\"Atenção: Verifique o CEP Digitado!\");
\t\t\t\t\t\t\t\t\$('#simular').button('reset');
\t\t\t\t\t\t\t\t\$('.simulacao-frete').show().html('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> CEP Não existe, tente novamente. <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
\t\t\t\t\t\t\t\tconsole.log('CEP não existe');
\t\t\t\t\t\t\t\t\$('html, body').animate({
\t\t\t\t\t\t\t\t\tscrollTop: 0
\t\t\t\t\t\t\t\t}, 'slow');
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}

\t\t\t\t\t\tif (json['shipping_method']) {
\t\t\t\t\t\t\tvar cep = \$('#input-postcode').val().replace(/[^0-9]/g, '');
\t\t\t\t\t\t\t\$.getJSON(\"https://viacep.com.br/ws/\" + cep + \"/json/?callback=?\", function (dados) {
\t\t\t\t\t\t\t\tif (!(\"erro\" in dados)) {
\t\t\t\t\t\t\t\t\t\$('.simulacao-frete').hide().html();
\t\t\t\t\t\t\t\t\tif (dados.localidade) {
\t\t\t\t\t\t\t\t\t\tvar cidade = dados.localidade;
\t\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\t\tvar cidade = '';
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\tif (dados.bairro) {
\t\t\t\t\t\t\t\t\t\tvar bairro = dados.bairro;
\t\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\t\tvar bairro = '';
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\tvar result = '';
\t\t\t\t\t\t\t\t\tresult += `<div class=\"panel panel-default mt-2\">`;
\t\t\t\t\t\t\t\t\tresult += `<div class=\"panel-body\">`;
\t\t\t\t\t\t\t\t\tresult += `<h4>` + cidade + ` - ` + bairro + `</h4>`;
\t\t\t\t\t\t\t\t\tresult += `<table class=\"table table-striped\">`;
\t\t\t\t\t\t\t\t\tresult += `<thead>`;
\t\t\t\t\t\t\t\t\tresult += `<tr>`;
\t\t\t\t\t\t\t\t\tresult += `<th><span>Método e Prazo</span></th>`;
\t\t\t\t\t\t\t\t\tresult += `<th><span>Custo</span></th>`;
\t\t\t\t\t\t\t\t\tresult += `</tr>`;
\t\t\t\t\t\t\t\t\tresult += `</thead>`;
\t\t\t\t\t\t\t\t\tresult += `<tbody>`;
\t\t\t\t\t\t\t\t\tfor (i in json['shipping_method']) {
\t\t\t\t\t\t\t\t\t\tif (!json['shipping_method'][i]['error']) {
\t\t\t\t\t\t\t\t\t\t\tfor (j in json['shipping_method'][i]['quote']) {
\t\t\t\t\t\t\t\t\t\t\t\tresult += `<tr>`;
\t\t\t\t\t\t\t\t\t\t\t\tresult += `<td class='text-left'><span>\${json['shipping_method'][i]['quote'][j]['title']
\t\t\t\t\t\t\t\t\t\t\t\t\t}</span></td>`;
\t\t\t\t\t\t\t\t\t\t\t\tresult += `<td class='text-left'><span>\${json['shipping_method'][i]['quote'][j]['text']
\t\t\t\t\t\t\t\t\t\t\t\t\t}</span></td>`;
\t\t\t\t\t\t\t\t\t\t\t\tresult += `</tr>`;
\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\t\t\tresult += '<div class=\"alert alert-danger\">' + json['shipping_method'][i]['error'] + '</div>';
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\tresult += '</tbody>';
\t\t\t\t\t\t\t\t\tresult += '</table>';
\t\t\t\t\t\t\t\t\tresult += '</div>';
\t\t\t\t\t\t\t\t\tresult += '</div>';

\t\t\t\t\t\t\t\t\t\$('.fa-spin').remove();

\t\t\t\t\t\t\t\t\t\$('.simulacao-frete').show().html(result);

\t\t\t\t\t\t\t\t\t\$('#simular').button('reset')

\t\t\t\t\t\t\t\t\t\$('input[name=\\'shipping_method\\']').on('change', function () {
\t\t\t\t\t\t\t\t\t\t\$('#button-shipping').prop('disabled', false)
\t\t\t\t\t\t\t\t\t});

\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t});
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t})
\t\t\t},
\t\t\terror: function (xhr, ajaxOptions, thrownError) {
\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t}
\t\t})
\t});</script>
<script type=\"text/javascript\">

\t\$(function () { // Esconde os campos
\t\t\$('#input-zone').hide();
\t\t\$('#input-country').hide();
\t});

\tfunction calcularfrete() {
\t\tvar cep = \$('#input-postcode').val().replace(/[^0-9]/g, '');

\t\t\$.getJSON(\"https://viacep.com.br/ws/\" + cep + \"/json/?callback=?\", function (dados) {


\t\t\tif (!(\"erro\" in dados)) {
\t\t\t\tconsole.log(dados);

\t\t\t\t\$('#input-country').find('option[value=\"30\"]').attr('selected', true);

\t\t\t\t\$.post('index.php?route=product/product/estado_autocompletar&estado=' + unescape(dados.uf), function (zone_id) {
\t\t\t\t\t\$.ajax({
\t\t\t\t\t\turl: 'index.php?route=product/product/country&country_id=30',
\t\t\t\t\t\tdataType: 'json',
\t\t\t\t\t\tbeforeSend: function () {
\t\t\t\t\t\t\t\$('.simulacao-frete').before('<i class=\"fa fa-cog fa-spin\"></i>');
\t\t\t\t\t\t},
\t\t\t\t\t\tcomplete: function () { },
\t\t\t\t\t\tsuccess: function (json) {
\t\t\t\t\t\t\tif (json['postcode_required'] == '1') {
\t\t\t\t\t\t\t\t\$('#input-postcode').parent().parent().addClass('required');

\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\$('#input-postcode').parent().parent().removeClass('required');
\t\t\t\t\t\t\t}

\t\t\t\t\t\t\tvar html = '<option value=\"\"> ";
        // line 1227
        echo ($context["text_select"] ?? null);
        echo "</option>';

\t\t\t\t\t\t\tif (json['zone'] != '') {
\t\t\t\t\t\t\t\tfor (i = 0; i < json['zone'].length; i++) {
\t\t\t\t\t\t\t\t\thtml += '<option value=\"' + json['zone'][i]['zone_id'] + '\"';
\t\t\t\t\t\t\t\t\tif (json['zone'][i]['zone_id'] == zone_id) {
\t\t\t\t\t\t\t\t\t\thtml += ' selected=\"selected\"';
\t\t\t\t\t\t\t\t\t}

\t\t\t\t\t\t\t\t\thtml += '>' + json['zone'][i]['name'] + '</option>';
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\thtml += '<option value=\"0\" selected=\"selected\"> ";
        // line 1239
        echo ($context["text_none"] ?? null);
        echo "</option>';

\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\$('#input-zone').html(html);
\t\t\t\t\t\t}
\t\t\t\t\t});
\t\t\t\t});
\t\t\t} else {
\t\t\t\t\$('#simular').button('reset');
\t\t\t\t\$('.simulacao-frete').show().html('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> CEP Não existe, tente novamente. <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
\t\t\t\tconsole.log('CEP não existe');
\t\t\t\t\$('html, body').animate({
\t\t\t\t\tscrollTop: 0
\t\t\t\t}, 'slow');
\t\t\t}
\t\t});
\t}</script>
<script type=\"text/javascript\">
\t\$('.scrollViewReview').click(function () {
\t\t\$('html, body').animate({
\t\t\tscrollTop: \$(\$(this).attr('href')).offset().top
\t\t}, 750);
\t\treturn false;
\t});</script>
";
        // line 1263
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "crystal_white/template/product/product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2388 => 1263,  2361 => 1239,  2346 => 1227,  2125 => 1009,  2116 => 1003,  2021 => 911,  2016 => 909,  2011 => 907,  1911 => 810,  1905 => 808,  1901 => 806,  1895 => 805,  1887 => 803,  1877 => 801,  1873 => 800,  1868 => 799,  1865 => 798,  1860 => 795,  1854 => 794,  1851 => 793,  1847 => 791,  1845 => 790,  1842 => 789,  1840 => 788,  1837 => 787,  1835 => 786,  1825 => 779,  1821 => 778,  1818 => 777,  1814 => 775,  1811 => 774,  1808 => 773,  1803 => 770,  1797 => 767,  1791 => 764,  1788 => 763,  1782 => 760,  1779 => 759,  1777 => 758,  1773 => 756,  1761 => 746,  1758 => 745,  1755 => 744,  1751 => 742,  1745 => 740,  1742 => 739,  1736 => 738,  1729 => 733,  1721 => 729,  1717 => 728,  1714 => 727,  1712 => 726,  1705 => 724,  1694 => 718,  1689 => 716,  1680 => 715,  1674 => 713,  1672 => 712,  1664 => 708,  1661 => 707,  1658 => 706,  1655 => 705,  1652 => 704,  1649 => 703,  1646 => 702,  1643 => 701,  1638 => 700,  1636 => 699,  1630 => 696,  1626 => 694,  1624 => 693,  1617 => 688,  1613 => 686,  1611 => 685,  1599 => 675,  1587 => 665,  1581 => 663,  1571 => 656,  1564 => 652,  1560 => 650,  1550 => 646,  1546 => 645,  1537 => 643,  1533 => 642,  1528 => 641,  1523 => 640,  1521 => 639,  1501 => 622,  1488 => 611,  1486 => 610,  1477 => 604,  1474 => 603,  1472 => 602,  1465 => 599,  1463 => 598,  1455 => 592,  1452 => 591,  1447 => 588,  1440 => 586,  1431 => 583,  1427 => 582,  1424 => 581,  1420 => 580,  1412 => 575,  1407 => 572,  1403 => 571,  1399 => 569,  1397 => 568,  1392 => 565,  1386 => 562,  1383 => 561,  1380 => 560,  1374 => 557,  1371 => 556,  1369 => 555,  1356 => 545,  1343 => 534,  1336 => 530,  1329 => 526,  1326 => 525,  1320 => 524,  1313 => 519,  1305 => 515,  1301 => 514,  1297 => 512,  1294 => 511,  1288 => 508,  1284 => 506,  1282 => 505,  1279 => 504,  1262 => 490,  1256 => 487,  1251 => 485,  1244 => 480,  1238 => 479,  1230 => 477,  1222 => 475,  1219 => 474,  1215 => 473,  1211 => 472,  1206 => 469,  1201 => 466,  1199 => 465,  1195 => 463,  1191 => 461,  1177 => 450,  1171 => 447,  1167 => 446,  1139 => 421,  1131 => 415,  1125 => 411,  1114 => 409,  1110 => 408,  1106 => 407,  1100 => 404,  1097 => 403,  1094 => 402,  1091 => 401,  1085 => 400,  1073 => 391,  1069 => 390,  1065 => 389,  1061 => 387,  1057 => 386,  1050 => 385,  1047 => 384,  1035 => 375,  1031 => 374,  1027 => 373,  1023 => 371,  1019 => 370,  1012 => 369,  1009 => 368,  997 => 359,  993 => 358,  989 => 357,  985 => 355,  981 => 354,  974 => 353,  971 => 352,  965 => 349,  961 => 348,  957 => 347,  952 => 345,  948 => 344,  944 => 343,  937 => 342,  934 => 341,  928 => 338,  922 => 337,  918 => 336,  915 => 335,  911 => 334,  904 => 333,  901 => 332,  892 => 328,  886 => 327,  883 => 326,  879 => 325,  872 => 324,  869 => 323,  863 => 319,  855 => 316,  848 => 314,  846 => 313,  841 => 312,  827 => 309,  822 => 308,  820 => 307,  816 => 306,  810 => 304,  804 => 303,  802 => 302,  798 => 301,  794 => 300,  790 => 299,  786 => 297,  782 => 296,  778 => 295,  774 => 294,  766 => 292,  763 => 291,  757 => 287,  753 => 285,  745 => 282,  738 => 280,  736 => 279,  731 => 278,  717 => 275,  712 => 274,  710 => 273,  706 => 272,  700 => 270,  694 => 269,  692 => 268,  688 => 267,  684 => 266,  680 => 265,  677 => 264,  672 => 263,  668 => 261,  658 => 256,  654 => 254,  650 => 253,  647 => 252,  633 => 249,  627 => 248,  623 => 247,  617 => 245,  611 => 244,  609 => 243,  606 => 242,  602 => 241,  598 => 240,  593 => 237,  589 => 236,  586 => 235,  584 => 234,  583 => 233,  579 => 232,  575 => 231,  567 => 229,  564 => 228,  559 => 225,  552 => 223,  545 => 221,  543 => 220,  536 => 219,  532 => 218,  528 => 217,  524 => 216,  520 => 215,  517 => 214,  513 => 213,  506 => 212,  503 => 211,  498 => 210,  495 => 209,  493 => 208,  489 => 206,  486 => 205,  481 => 202,  470 => 200,  466 => 199,  461 => 196,  458 => 195,  455 => 194,  452 => 193,  446 => 189,  441 => 187,  437 => 186,  434 => 185,  432 => 184,  429 => 183,  424 => 181,  420 => 180,  417 => 179,  415 => 178,  409 => 175,  404 => 172,  398 => 169,  391 => 165,  387 => 163,  381 => 160,  377 => 158,  375 => 157,  371 => 155,  364 => 150,  361 => 149,  359 => 148,  348 => 143,  342 => 140,  338 => 139,  335 => 138,  333 => 137,  328 => 135,  324 => 134,  315 => 129,  307 => 126,  304 => 125,  302 => 124,  297 => 123,  294 => 122,  291 => 121,  288 => 120,  285 => 119,  283 => 118,  280 => 117,  241 => 80,  237 => 78,  228 => 75,  222 => 74,  218 => 73,  213 => 72,  208 => 71,  197 => 67,  193 => 66,  188 => 65,  186 => 64,  183 => 63,  181 => 62,  177 => 60,  166 => 57,  164 => 56,  158 => 54,  150 => 51,  147 => 50,  144 => 49,  142 => 48,  137 => 47,  134 => 46,  131 => 45,  128 => 44,  125 => 43,  123 => 42,  115 => 40,  112 => 39,  109 => 38,  106 => 37,  103 => 36,  100 => 35,  97 => 34,  95 => 33,  91 => 32,  75 => 21,  63 => 11,  52 => 8,  49 => 7,  45 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "crystal_white/template/product/product.twig", "");
    }
}
