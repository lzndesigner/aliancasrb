<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base/template/common/footer.twig */
class __TwigTemplate_f169a31b5fc9c425ead01e61153831d984a895d8ca60d31c5d971e74dd00e584 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"box-newsletter\">
  <div class=\"container\">
    <div class=\"d-flex flex-column flex-sm-column flex-md-row align-items-center\">
      <div class=\"col-xs-12 col-md-8\">
        <h3><i class=\"fa fa-envelope-open-text\"></i> Cadastre-se e receba nossas novidades! </h3>
      </div><!-- -->

      <div class=\"col-xs-12 col-md-4\">
        <div class=\"input-group mt-4 mt-sm-4 mt-md-0\">
          <input type=\"email\" name=\"newsletter-email\" id=\"newsletter-email\" placeholder=\"E-mail válido\"
            class=\"form-control\">
          <span class=\"input-group-btn\">
            <button type=\"submit\" id=\"btnNewsletter\" onclick=\"return subscribe();\" class=\"btn btn-sm btn-thema\"><i
                class=\"fa fa-check\"></i> Registrar</button>
          </span>
        </div>
      </div><!-- -->
    </div>
    <div id=\"msgRetorno\" class=\"hide\"></div>
  </div>
</div>
</main>

<div class=\"footer-desktop\">
  <footer>
    <div id=\"description\" class=\"container\">
      <div class=\"row\">
        <div class=\"col-sm-3 hidden-xs\">
          <h5>Atendimento</h5>
          <ul class=\"linksAtendimento mb-5\">
            <li>
              <div><i class=\"fab fa-whatsapp\"></i> <b>Telefone</b></div>
              <span><a href=\"https://api.whatsapp.com/send?phone=55";
        // line 33
        echo ($context["telephoneLink"] ?? null);
        echo "\">";
        echo ($context["telephone"] ?? null);
        echo "</a></span>
            </li>
            ";
        // line 35
        if (($context["cellphone"] ?? null)) {
            // line 36
            echo "            <li>
              <div><i class=\"fas fa-phone\"></i> <b>Celular</b></div>
              <span><a href=\"https://api.whatsapp.com/send?phone=55";
            // line 38
            echo ($context["cellphoneLink"] ?? null);
            echo "\">";
            echo ($context["cellphone"] ?? null);
            echo "</a></span>
            </li>
            ";
        }
        // line 41
        echo "            <li class=\"e-mail\">
              <div><i class=\"fa fa-comment\"></i> <b>Atendimento E-mail</b></div>
              <span><a href=\"mailto:";
        // line 43
        echo ($context["email_admin"] ?? null);
        echo "\">";
        echo ($context["email_admin"] ?? null);
        echo "</a></span>
            </li>
            ";
        // line 45
        if (($context["hour_open"] ?? null)) {
            // line 46
            echo "            <li>
              <div><i class=\"fas fa-clock\"></i> <b>Horário de Atendimento</b></div>
              <p>";
            // line 48
            echo twig_replace_filter(($context["hour_open"] ?? null), ["/space/" => "<br>"]);
            echo "</p>
            </li>
            ";
        }
        // line 51
        echo "          </ul>
          <h5>";
        // line 52
        echo ($context["text_information"] ?? null);
        echo "</h5>
          <ul class=\"list-unstyled links-footer\">
            ";
        // line 54
        if (($context["informations"] ?? null)) {
            // line 55
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["informations"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
                // line 56
                echo "            <li><a href=\"";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "href", [], "any", false, false, false, 56);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["information"], "title", [], "any", false, false, false, 56);
                echo "</a></li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 58
            echo "            ";
        }
        // line 59
        echo "            <li><a href=\"";
        echo ($context["contact"] ?? null);
        echo "\">";
        echo ($context["text_contact"] ?? null);
        echo "</a></li>
          </ul>
        </div>
        <div class=\"col-sm-3 hidden-xs\">
          <h5>Categorias</h5>
          ";
        // line 64
        if (($context["categories"] ?? null)) {
            // line 65
            echo "          <ul class=\"list-unstyled links-footer mb-5\">
            ";
            // line 66
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 67
                echo "            ";
                if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 67)) {
                    // line 68
                    echo "            <li class=\"dropdown\">
              <a href=\"";
                    // line 69
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 69);
                    echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 69);
                    echo " <span
                  class=\"openMenu d-inline-block d-sm-inline-block d-md-none\"><i
                    class=\"fa fa-angle-down\"></i></span></a>
              <div class=\"dropdown-menu\">
                <div class=\"dropdown-inner\"> ";
                    // line 73
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 73), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 73)) / twig_round(twig_get_attribute($this->env, $this->source,                     // line 74
$context["category"], "column", [], "any", false, false, false, 74), 1, "ceil"))));
                    foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                        // line 75
                        echo "                  <ul class=\"list-unstyled\">
                    ";
                        // line 76
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($context["children"]);
                        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                            // line 77
                            echo "                    <li><a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 77);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 77);
                            echo "</a></li>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 79
                        echo "                  </ul>
                  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 81
                    echo "                </div>
                <a href=\"";
                    // line 82
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 82);
                    echo "\" class=\"see-all\">";
                    echo ($context["text_all"] ?? null);
                    echo " ";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 82);
                    echo "</a>
              </div>
            </li>
            ";
                } else {
                    // line 86
                    echo "            <li><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 86);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 86);
                    echo "</a></li>
            ";
                }
                // line 88
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 89
            echo "          </ul>
          ";
        }
        // line 91
        echo "
          <h5>";
        // line 92
        echo ($context["text_account"] ?? null);
        echo "</h5>
          <ul class=\"list-unstyled links-footer\">
            <li><a href=\"";
        // line 94
        echo ($context["account"] ?? null);
        echo "\">";
        echo ($context["text_account"] ?? null);
        echo "</a></li>
            <li><a href=\"";
        // line 95
        echo ($context["order"] ?? null);
        echo "\">";
        echo ($context["text_order"] ?? null);
        echo "</a></li>
          </ul>
        </div>
        <div class=\"col-sm-6\">
          <div class=\"row mb-4 pl-0\">
            ";
        // line 100
        if ((($context["facebook_status"] ?? null) && twig_length_filter($this->env, ($context["facebook_url"] ?? null)))) {
            // line 101
            echo "            <div class=\"col-sm-8\">
              <h5>Siga nossa Página</h5>
              <div id=\"fb-root\"></div>
              <script async defer crossorigin=\"anonymous\"
                src=\"https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v7.0&appId=1094654680570525&autoLogAppEvents=1\"
                nonce=\"i5qGv6BN\"></script>
              <div class=\"fb-page\" data-href=\"";
            // line 107
            echo ($context["facebook_url"] ?? null);
            echo "\" data-tabs=\"\" data-width=\"\" data-height=\"\"
                data-small-header=\"false\" data-adapt-container-width=\"true\" data-hide-cover=\"false\"
                data-show-facepile=\"true\">
                <blockquote cite=\"";
            // line 110
            echo ($context["facebook_url"] ?? null);
            echo "\" class=\"fb-xfbml-parse-ignore\"><a href=\"";
            echo ($context["facebook_url"] ?? null);
            echo "\">";
            echo ($context["name"] ?? null);
            // line 111
            echo "</a></blockquote>
              </div>
            </div>
            ";
        }
        // line 115
        echo "            <div class=\"col-sm-4\">
              <h5>Segurança</h5>
              <div class=\"banner_pagamentos\">
                <ul>
                  <li class=\"\"><img src=\"_galerias/security/site_seguro_ssl.png\" class=\"img-responsive\"
                      alt=\"Site Seguro por Certificado SSL\"></li>
                </ul>
              </div>
            </div>
          </div>

          <div class=\"row\">
            <div class=\"col-sm-12\">
              <h5>Formas de Pagamentos</h5>
              <div class=\"banner_pagamentos\">
                <ul>
                  <li class=\"hide\"><img src=\"_galerias/payment/banco_brasil.png\" class=\"img-responsive\"
                      alt=\"Banco do Brasil\"></li>
                  <li class=\"hide\"><img src=\"_galerias/payment/bradesco.png\" class=\"img-responsive\" alt=\"Bradesco\"></li>
                  <li class=\"hide\"><img src=\"_galerias/payment/caixa.png\" class=\"img-responsive\" alt=\"Caixa\"></li>
                  <li class=\"hide\"><img src=\"_galerias/payment/itau.png\" class=\"img-responsive\" alt=\"Itau\"></li>

                  ";
        // line 137
        if ((($context["payment_bank_transfer_status"] ?? null) == 1)) {
            // line 138
            echo "                  <li class=\"\"><img src=\"_galerias/payment/deposito.png\" class=\"img-responsive\" alt=\"Depósito Bancário\">
                  </li>
                  ";
        }
        // line 141
        echo "
                  ";
        // line 142
        if ((($context["payment_pagseguro_lightbox_status"] ?? null) == 1)) {
            // line 143
            echo "                  <li class=\"\"><img src=\"_galerias/payment/pagseguro.png\" class=\"img-responsive\" alt=\"PagSeguro\"></li>
                  ";
        }
        // line 145
        echo "
                  ";
        // line 146
        if ((($context["payment_pp_standard_status"] ?? null) == 1)) {
            // line 147
            echo "                  <li class=\"\"><img src=\"_galerias/payment/paypal.png\" class=\"img-responsive\" alt=\"PayPal\"></li>
                  ";
        }
        // line 149
        echo "
                  <li class=\"hide\"><img src=\"_galerias/payment/boleto.png\" class=\"img-responsive\" alt=\"Boleto Bancário\">
                  </li>
                  <li class=\"hide parcelamento\"><img src=\"_galerias/payment/parcelamento_cartoes.png\"
                      class=\"img-responsive\" alt=\"PayPal\"></li>
                </ul>
              </div>
            </div>

            <div class=\"col-sm-12\">
              <h5>Formas de Envio</h5>
              <div class=\"banner_pagamentos\">
                <ul>
                  <li class=\"\"><img src=\"_galerias/shipping/correios.png\" class=\"img-responsive\" alt=\"Correios\"></li>
                  <li class=\"\"><img src=\"_galerias/shipping/pac.png\" class=\"img-responsive\" alt=\"PAC\"></li>
                  <li class=\"\"><img src=\"_galerias/shipping/sedex.png\" class=\"img-responsive\" alt=\"SEDEX\"></li>
                </ul>
              </div>
            </div>
          </div>
        </div><!-- col-6 -->
      </div>
    </div><!-- container -->
    ";
        // line 172
        if ((((($context["facebook_status"] ?? null) && twig_length_filter($this->env, ($context["facebook_url"] ?? null))) && ($context["instagram_status"] ?? null)) && twig_length_filter($this->env, ($context["instagram_url"] ?? null)))) {
            // line 173
            echo "    <div class=\"bgsocial-midia\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-md-12\">
            <div class=\"col-md-3\">
              <h5>Encontre-nos aqui</h5>
            </div>
            <div class=\"col-md-9\">
              <ul class=\"social-icons text-left\">
                ";
            // line 182
            if ((($context["facebook_status"] ?? null) && twig_length_filter($this->env, ($context["facebook_url"] ?? null)))) {
                // line 183
                echo "                <li class=\"social-icons-facebook\" data-toggle=\"tooltip\" title=\"Facebook\"><a href=\"";
                echo ($context["facebook_url"] ?? null);
                echo "\"
                    target=\"_Blank\"><i class=\"fab fa-facebook\"></i></a></li>
                ";
            }
            // line 186
            echo "                ";
            if ((($context["instagram_status"] ?? null) && twig_length_filter($this->env, ($context["instagram_url"] ?? null)))) {
                // line 187
                echo "                <li class=\"social-icons-instagram\" data-toggle=\"tooltip\" title=\"Instagram\"><a
                    href=\"https://instagram.com/";
                // line 188
                echo ($context["instagram_url"] ?? null);
                echo "\" target=\"_Blank\"><i
                      class=\"fab fa-instagram\"></i></a></li>
                ";
            }
            // line 191
            echo "              </ul>
            </div>
          </div>
        </div>
      </div>
    </div><!-- bgsocial-midia -->
    ";
        }
        // line 198
        echo "    <div id=\"copyright\" class=\"container\">
      <div class=\"row\">
        <div class=\"col-xs-12 col-sm-4 pull-left\">
          <p>";
        // line 201
        echo ($context["endereco"] ?? null);
        echo "</p>
          <p>";
        // line 202
        echo ($context["powered"] ?? null);
        echo "</p>
        </div>
        <div class=\"col-xs-12 col-sm-2 pull-right\">
          <a href=\"https://innsystem.com.br\" target=\"_Blank\" class=\"companyDeveloper\"><b>Tecnologia:</b><br><img
              src=\"_galerias/logoinnsystem.png\" alt=\"InnSystem Inovação em Sistemas\"></a>
        </div>
      </div>
    </div><!-- container -->
  </footer>
</div>
<div class=\"footer-mobile\">
  <footer>
    <div class=\"d-flex\">
      <div class=\"flex-fill\">
        <a href=\"index.php?route=common/home\">
          <i class=\"fa fa-home\"></i>
        </a>
      </div><!-- flex-fill -->
      <div class=\"flex-fill\">
        <a href=\"javascript:;\" class=\"btnToggleMenu\">
          <i class=\"fa fa-bars\"></i>
        </a>
      </div><!-- flex-fill -->
      <div class=\"flex-fill\">
        <a href=\"index.php?route=account/account\">
          <i class=\"fa fa-user\"></i>
        </a>
      </div><!-- flex-fill -->
      <div class=\"flex-fill\">
        <a href=\"index.php?route=checkout/cart\" id=\"cart-footer\">
          <i class=\"fa fa-shopping-cart\"></i>
          <span id=\"cart-total\">";
        // line 233
        echo ($context["text_items"] ?? null);
        echo "</span>
        </a>
      </div><!-- flex-fill -->
    </div>
  </footer>
</div>
";
        // line 239
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["styles"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 240
            echo "<link href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "href", [], "any", false, false, false, 240);
            echo "\" type=\"text/css\" rel=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "rel", [], "any", false, false, false, 240);
            echo "\" media=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "media", [], "any", false, false, false, 240);
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 242
        echo "
";
        // line 243
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["scripts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 244
            echo "<script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 246
        echo "
<script type=\"text/javascript\">
  function subscribe() {
    var formNews = \$('form[name=\"formNewsletter\"]');
    var emailpattern = /^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+\$/;
    var email = \$('#newsletter-email').val();
    var cupom = \"primeiracompra\";
    var cupom_value = \"20% de Desconto\";
    var url_loja = \"";
        // line 254
        echo ($context["url_loja"] ?? null);
        echo "\";
    var mensagem = \"Prontinho! <br> Você irá receber por e-mail nossas promoções e novidades.<br><br><small>* Caso deseja cancelar o <b>newsletter</b> basta responder o e-mail com o assunto <b>SAIR</b></small>\";
    // var mensagem = \"Prontinho! <br> Agora você pode realizar sua primeira comprando usando o cupom para receber \"+cupom_value+\". <br><br> Escolha os produtos em nossa loja, adicione ao carrinho e use o cupom: <h2 style='margin:5px 0;'><b>\"+cupom+\"</b></h2> <br> Você irá receber por e-mail nossas promoções e novidades.<br><br><small>* Caso deseja cancelar o <b>newsletter</b> basta responder o e-mail com o assunto <b>SAIR</b></small>\";
    if (email != \"\") {
      if (!emailpattern.test(email)) {
        \$(\"#msgRetorno\").removeClass('hide').append(\"<div class='alert-dismissible alert alert-danger'>Utilize um e-mail válido! <button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick='return closetAlert();'> <span aria-hidden='true'>&times;</span> </button></div>\");
        \$('#btnNewsletter').html('<i class=\"fa fa-check\"></i> Registrar').attr('disabled', false);
        setTimeout(function () {
          \$(\"#msgRetorno\").addClass('hide').empty();
        }, 8000);

        return false;
      }
      else {
        \$.ajax({
          url: 'index.php?route=common/footer/newsletters',
          type: 'post',
          data: {
            'email': email,
            'mensagem': mensagem
          },
          dataType: 'json',
          beforeSend: function () {
            \$('#btnNewsletter').html('Aguarde').attr('disabled', true);
          },
          success: function (json) {
            if (json.result.status === 'existe') {
              \$(\"#msgRetorno\").removeClass('hide').append(\"<div class='alert-dismissible alert alert-danger'>E-mail já Cadastrado, utilize outro e-mail válido <button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick='return closetAlert();'> <span aria-hidden='true'>&times;</span> </button></div>\");
              \$('#btnNewsletter').html('<i class=\"fa fa-check\"></i> Registrar').attr('disabled', false);
              setTimeout(function () {
                \$(\"#msgRetorno\").addClass('hide').empty();
              }, 8000);
            } else if (json.result.status === 'falha') {
              \$(\"#msgRetorno\").removeClass('hide').append(\"<div class='alert-dismissible alert alert-danger'>Não foi possível cadastrar. <button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick='return closetAlert();'> <span aria-hidden='true'>&times;</span> </button></div>\");
              \$('#btnNewsletter').html('<i class=\"fa fa-check\"></i> Registrar').attr('disabled', false);
              setTimeout(function () {
                \$(\"#msgRetorno\").addClass('hide').empty();
              }, 8000);
            } else if (json.result.status === 'ok') {
              \$(\"#msgRetorno\").removeClass('hide').append(\"<div class='alert-dismissible alert alert-success'>\" + mensagem + \" <button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick='return closetAlert();'> <span aria-hidden='true'>&times;</span> </button></div>\");
              setTimeout(function () {
                \$('#btnNewsletter').html('<i class=\"fa fa-check\"></i> Registrar').attr('disabled', false);
                \$(\"#msgRetorno\").addClass('hide').empty();
              }, 20000);
            }
          },
          afterSend: function () {
            \$('#btnNewsletter').html('<i class=\"fa fa-check\"></i> Registrar').attr('disabled', false);
            setTimeout(function () {
              \$(\"#msgRetorno\").addClass('hide').empty();
            }, 8000);
          }

        });
        return false;
      }
    }
    else {

      \$('#btnNewsletter').html('Aguarde').attr('disabled', true);
      \$(\"#msgRetorno\").removeClass('hide').append(\"<div class='alert-dismissible alert alert-warning'>E-mail é necessário<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick='return closetAlert();'> <span aria-hidden='true'>&times;</span> </button></div>\");
      \$('#btnNewsletter').html('<i class=\"fa fa-check\"></i> Registrar').attr('disabled', false);
      setTimeout(function () {
        \$(\"#msgRetorno\").addClass('hide').empty();
      }, 8000);
      \$(email).focus();
      return false;
    }
  }

  function closetAlert() {
    \$(\"#msgRetorno\").addClass('hide').empty();
    \$('#btnNewsletter').html('<i class=\"fa fa-check\"></i> Registrar').attr('disabled', false);
  }
</script>

<div id=\"box-privacity-ctn\" class=\"box-privacity-ctn\" style=\"display:none\">
  <div class=\"box-privacity d-flex-column d-sm-flex-column d-md-flex align-items-center\" id=\"box-privacity\">
    <div class=\"flex-grow-1 mb-3 mb-sm-3 mb-md-0 mr-md-3\">
      <p><b>Privacidade e os cookies</b>: a gente usa cookies para personalizar anúncios e melhorar a sua experiência no
        site. Ao continuar navegando, você concorda com a nossa <a href=\"/politica-de-privacidade\">Política de
          Privacidade</a>.</p>
    </div>
    <div class=\"box-privacity-newsletter\">
      <a href=\"javascript:buttonClose();\" id=\"button-close\" class=\"btn btn-primary btn-sm\">Continuar e Fechar</a>
    </div>
  </div>
</div>

<script type='text/javascript'>
  \$(function () {

    if (\$.cookie('box-pop-up') == null) {

      \$(\"#box-privacity\").click(function (e) {
        e.stopPropagation();
      });

      \$(\"#button-close\").click(function (e) {
        \$('#box-privacity-ctn').fadeOut();
      });

      \$('#box-privacity-ctn').show();
    }

    var date = new Date();
    // date.setTime(date.getTime() + (30 * 60 * 1000)); //30 min
    date.setTime(date.getTime() + (10)); //10 segundos
    \$.cookie('box-pop-up', 'box-pop', { expires: date });
  });
</script>
\t\t\t
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "base/template/common/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  505 => 254,  495 => 246,  486 => 244,  482 => 243,  479 => 242,  466 => 240,  462 => 239,  453 => 233,  419 => 202,  415 => 201,  410 => 198,  401 => 191,  395 => 188,  392 => 187,  389 => 186,  382 => 183,  380 => 182,  369 => 173,  367 => 172,  342 => 149,  338 => 147,  336 => 146,  333 => 145,  329 => 143,  327 => 142,  324 => 141,  319 => 138,  317 => 137,  293 => 115,  287 => 111,  281 => 110,  275 => 107,  267 => 101,  265 => 100,  255 => 95,  249 => 94,  244 => 92,  241 => 91,  237 => 89,  231 => 88,  223 => 86,  212 => 82,  209 => 81,  202 => 79,  191 => 77,  187 => 76,  184 => 75,  181 => 74,  179 => 73,  170 => 69,  167 => 68,  164 => 67,  160 => 66,  157 => 65,  155 => 64,  144 => 59,  141 => 58,  130 => 56,  125 => 55,  123 => 54,  118 => 52,  115 => 51,  109 => 48,  105 => 46,  103 => 45,  96 => 43,  92 => 41,  84 => 38,  80 => 36,  78 => 35,  71 => 33,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base/template/common/footer.twig", "");
    }
}
