<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/html.twig */
class __TwigTemplate_73236bcf8711b14ea3e6e29fec38c7a52abc81e7018141b10797b942f1dc8491 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_save"] ?? null);
        echo "\"
          class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 8
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i
            class=\"fa fa-reply\"></i></a>
      </div>
      <h1>";
        // line 11
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 14
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 14);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 14);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 20
        if (($context["error_warning"] ?? null)) {
            // line 21
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 25
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 27
        echo ($context["text_edit"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 30
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\"
          class=\"form-horizontal\">
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-name\">";
        // line 33
        echo ($context["entry_name"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"name\" value=\"";
        // line 35
        echo ($context["name"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_name"] ?? null);
        echo "\" id=\"input-name\"
                class=\"form-control\" />
              ";
        // line 37
        if (($context["error_name"] ?? null)) {
            // line 38
            echo "              <div class=\"text-danger\">";
            echo ($context["error_name"] ?? null);
            echo "</div>
              ";
        }
        // line 40
        echo "            </div>
          </div>
          <div class=\"tab-pane\">
            <ul class=\"nav nav-tabs\" id=\"language\">
              ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 45
            echo "              <li><a href=\"#language";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 45);
            echo "\" data-toggle=\"tab\"><img
                    src=\"language/";
            // line 46
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 46);
            echo "/";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "code", [], "any", false, false, false, 46);
            echo ".png\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "name", [], "any", false, false, false, 46);
            echo "\" /> ";
            echo twig_get_attribute($this->env, $this->source,             // line 47
$context["language"], "name", [], "any", false, false, false, 47);
            echo "</a></li>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "            </ul>
            <div class=\"tab-content\">
              ";
        // line 51
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["languages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 52
            echo "              <div class=\"tab-pane\" id=\"language";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 52);
            echo "\">
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-title";
            // line 54
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 54);
            echo "\">";
            echo ($context["entry_title"] ?? null);
            // line 55
            echo "</label>
                  <div class=\"col-sm-10\">
                    <input type=\"text\" name=\"module_description[";
            // line 57
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 57);
            echo "][title]\"
                      placeholder=\"";
            // line 58
            echo ($context["entry_title"] ?? null);
            echo "\" id=\"input-heading";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 58);
            echo "\"
                      value=\"";
            // line 59
            echo (((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["module_description"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 59)] ?? null) : null)) ? (twig_get_attribute($this->env, $this->source, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["module_description"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 59)] ?? null) : null), "title", [], "any", false, false, false, 59)) : (""));
            echo "\"
                      class=\"form-control\" />
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\" for=\"input-description";
            // line 64
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 64);
            echo "\">";
            echo             // line 65
($context["entry_description"] ?? null);
            echo " <br><br> <a href=\"javascript:;\" class=\"btn btn-sm btn-primary\"
                      data-toggle=\"modal\" data-target=\"#exampleModal\">Componentes</a> </label>
                  <div class=\"col-sm-10\">
                    <textarea name=\"module_description[";
            // line 68
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 68);
            echo "][description]\"
                      placeholder=\"";
            // line 69
            echo ($context["entry_description"] ?? null);
            echo "\" id=\"input-description";
            echo twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 69);
            echo "\"
                      data-toggle=\"summernote\"
                      class=\"form-control\">";
            // line 71
            echo (((($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["module_description"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 71)] ?? null) : null)) ? (twig_get_attribute($this->env, $this->source, (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["module_description"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[twig_get_attribute($this->env, $this->source, $context["language"], "language_id", [], "any", false, false, false, 71)] ?? null) : null), "description", [], "any", false, false, false, 71)) : (""));
            echo "</textarea>
                  </div>
                </div>
              </div>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 79
        echo ($context["entry_status"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"status\" id=\"input-status\" class=\"form-control\">
                ";
        // line 82
        if (($context["status"] ?? null)) {
            // line 83
            echo "                <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\">";
            // line 84
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        } else {
            // line 86
            echo "                <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 87
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        }
        // line 89
        echo "              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"
    aria-hidden=\"true\">
    <div class=\"modal-dialog\" role=\"document\">
      <div class=\"modal-content\">
        <div class=\"modal-header\">
          <h5 class=\"modal-title\" id=\"exampleModalLabel\">Componentes para Layout</h5>
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
            <span aria-hidden=\"true\">&times;</span>
          </button>
        </div>
        <div class=\"modal-body\">
          <p><b>Instruções para o uso:</b> Clique para copiar o código desejado e automaticamente irá colar no campo de
            <b>Descrição</b>.
          </p>
          <p><b class=\"text-danger\">Obrigatório:</b> Para melhor utilização entre em contato com o suporte.
          </p>

          <div class=\"row\">
            <div class=\"col-sm-12\">
              <h4>Faixa de Informações <button type=\"button\" class=\"btn btn-sm btn-primary\"
                  onclick=\"CopyToClipboard('faixa_informacoes')\" data-section-target=\"faixa_informacoes\"
                  data-toggle=\"tooltip\" title=\"Copiar código abaixo\"><i class=\"fa fa-copy\"></i></button></h4>

              <img src=\"./../_galerias/auxiliares/help_html_faixa_informacoes.png\" alt=\"\" class=\"img-responsive\">

              <pre id=\"faixa_informacoes\" class=\"pre_help hide\">
              <div class=\"container\">
              <div class=\"row banners-selos clearfix\">
              <div class=\"col-md-4 col-sm-12 col-xs-12\">
              <div class=\"icon-selos\"><i class=\"fa fa-truck\"></i></div>
              <div>
              <h2>Frete grátis</h2>
              <p>acima de R\$ 299,00</p>
              </div><!-- -->
              </div><!-- col-md-3 -->
              <div class=\"col-md-4 col-sm-12 col-xs-12\">
              <div class=\"icon-selos\"><i class=\"fa fa-credit-card\"></i></div>
              <div>
              <h2>Pagamento Fácil</h2>
              <p>Suas Compras em Até 12X.</p>
              </div><!-- -->
              </div><!-- col-md-3 -->
              <div class=\"col-md-4 col-sm-12 col-xs-12\">
              <div class=\"icon-selos\"><i class=\"fa fa-shield\"></i></div>
              <div>
              <h2>Compra Segura</h2>
              <p>Ambiente seguro e certificado</p>
              </div><!-- -->
              </div><!-- col-md-3 -->
              </div>
              </div>
              </pre>
            </div><!-- col-12 -->

            <div class=\"col-sm-12\">
              <hr>
            </div>

            <div class=\"col-sm-12\">
              <h4>Seção com 2 Box de Imagens <button type=\"button\" class=\"btn btn-sm btn-primary\"
                  onclick=\"CopyToClipboard('box_2_imagens')\" data-section-target=\"box_2_imagens\" data-toggle=\"tooltip\"
                  title=\"Copiar código abaixo\"><i class=\"fa fa-copy\"></i></button></h4>

              <img src=\"./../_galerias/auxiliares/help_html_box_2_imagens.png\" alt=\"\" class=\"img-responsive\">

              <pre id=\"box_2_imagens\" class=\"pre_help hide\">
              <div class=\"container html-banners\">
                <div class=\"row\">
                  <div class=\"col-xs-12 col-md-6\">
                    <a href=\"/laptops-e-notebooks\"><img src=\"./image/catalog/banners/box_1.png\"
                        alt=\"Laptops &amp; Notebooks com Promoção\" class=\"img-responsive\"></a>
                  </div>
                  <div class=\"col-xs-12 col-md-6\">
                    <a href=\"/cameras\"><img src=\"./image/catalog/banners/box_2.png\" alt=\"Cameras com Promoção\"
                        class=\"img-responsive\"></a>
                  </div>
                </div>
              </div>
              </pre>
            </div><!-- col-12 -->
          </div>
        </div>
        <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-sm btn-danger\" data-dismiss=\"modal\">Fechar</button>
        </div>
      </div>
    </div>
  </div>


  <link href=\"view/javascript/codemirror/lib/codemirror.css\" rel=\"stylesheet\" />
  <link href=\"view/javascript/codemirror/theme/monokai.css\" rel=\"stylesheet\" />
  <script type=\"text/javascript\" src=\"view/javascript/codemirror/lib/codemirror.js\"></script>
  <script type=\"text/javascript\" src=\"view/javascript/codemirror/lib/xml.js\"></script>
  <script type=\"text/javascript\" src=\"view/javascript/codemirror/lib/formatting.js\"></script>

  <script type=\"text/javascript\" src=\"view/javascript/summernote/summernote.js?";
        // line 194
        echo twig_random($this->env);
        echo "\"></script>
  <link href=\"view/javascript/summernote/summernote.css\" rel=\"stylesheet\" />
  <script type=\"text/javascript\" src=\"view/javascript/summernote/summernote-image-attributes.js\"></script>
  <script type=\"text/javascript\" src=\"view/javascript/summernote/opencart.js\"></script>
  <script type=\"text/javascript\">
    \$('#language a:first').tab('show');
    \$(document).ready(function () {
      \$('.btn-codeview').html('<i class=\"note-icon-code\" style=\"margin-right:5px;\"></i> <b>Código-Fonte</b>');
    });
//--></script>

  <script type=\"text/javascript\">
    function CopyToClipboard(containerid) {
      if (document.selection) {
        var range = document.body.createTextRange();
        range.moveToElementText(document.getElementById(containerid));
        range.select().createTextRange();
        document.execCommand(\"copy\");
      } else if (window.getSelection) {
        var range = document.createRange();
        range.selectNode(document.getElementById(containerid));
        window.getSelection().addRange(range);
        var documentFragment = \$('#' + containerid).html();
        console.log(documentFragment);
        \$('.note-editable.panel-body').html(documentFragment);
        document.execCommand(\"copy\");

        alert(\"Texo copiado. Cole dentro do campo Descrição.\")
      }
    }
//--></script>
</div>
";
        // line 226
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  407 => 226,  372 => 194,  265 => 89,  260 => 87,  255 => 86,  250 => 84,  245 => 83,  243 => 82,  237 => 79,  232 => 76,  221 => 71,  214 => 69,  210 => 68,  204 => 65,  201 => 64,  193 => 59,  187 => 58,  183 => 57,  179 => 55,  175 => 54,  169 => 52,  165 => 51,  161 => 49,  153 => 47,  146 => 46,  141 => 45,  137 => 44,  131 => 40,  125 => 38,  123 => 37,  116 => 35,  111 => 33,  105 => 30,  99 => 27,  95 => 25,  87 => 21,  85 => 20,  79 => 16,  68 => 14,  64 => 13,  59 => 11,  51 => 8,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/module/html.twig", "");
    }
}
