<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* crystal_white/template/extension/module/featured.twig */
class __TwigTemplate_12103064ec557a029e545907bde831a899c1c7ef88e7e86ff2cc065187be3377 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container\">
\t<div class=\"module-general module-featured\">
\t\t<div class=\"heading-page\">
\t\t\t<h3>";
        // line 4
        echo ($context["heading_title"] ?? null);
        echo "</h3>
\t\t</div>
\t\t<div class=\"row\">

\t\t\t";
        // line 8
        if (($context["type"] ?? null)) {
            // line 9
            echo "\t\t\t\t<div class=\"swiper-viewport\">
\t\t\t\t\t<div id=\"banner";
            // line 10
            echo ($context["module"] ?? null);
            echo "\" class=\"swiper-container\">
\t\t\t\t\t\t<div class=\"swiper-wrapper\">
\t\t\t\t\t\t";
        }
        // line 13
        echo "
\t\t\t\t\t\t";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 15
            echo "\t\t\t\t\t\t\t";
            if (($context["type"] ?? null)) {
                // line 16
                echo "\t\t\t\t\t\t\t\t<div class=\"product-layout swiper-slide\">
\t\t\t\t\t\t\t\t";
            } else {
                // line 18
                echo "\t\t\t\t\t\t\t\t\t<div class=\"product-layout col-lg-3 col-md-3 col-sm-4 col-xs-6\">
\t\t\t\t\t\t\t\t\t";
            }
            // line 20
            echo "\t\t\t\t\t\t\t\t\t<div class=\"product-thumb transition\">
\t\t\t\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t";
            // line 22
            if (twig_get_attribute($this->env, $this->source, $context["product"], "valor_desconto", [], "any", false, false, false, 22)) {
                // line 23
                echo "\t\t\t\t\t\t\t\t\t\t\t<span class=\"badge badge-danger badge-desconto\">";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "valor_desconto", [], "any", false, false, false, 23);
                echo "%</span>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 25
            echo "\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 25);
            echo "\"><img src=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 25);
            echo "\" alt=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 25);
            echo "\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 25);
            echo "\" class=\"img-responsive\"/></a>
\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn-wishlist\" data-toggle=\"tooltip\" title=\"";
            // line 26
            echo ($context["button_wishlist"] ?? null);
            echo "\" onclick=\"wishlist.add('";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 26);
            echo "');\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-heart\"></i>
\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t\t\t\t<h4>
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 32
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 32);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 32);
            echo "</a>
\t\t\t\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t\t\t\t";
            // line 34
            if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 34)) {
                // line 35
                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                if ((twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 35) == "R\$ 0,00")) {
                    // line 36
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-current\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<small>Sob-consulta</small>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 46
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<p>";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "parcelamento", [], "any", false, false, false, 46);
                    echo " ou</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 49
                    if ( !twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 49)) {
                        // line 50
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-current\">";
                        // line 51
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 51);
                        echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 54
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-old\">";
                        // line 55
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 55);
                        echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                        // line 58
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 58);
                        echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 61
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 64
                echo "\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 66
            echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
            // line 67
            if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 67)) {
                // line 68
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 69
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 69) < $context["i"])) {
                        // line 70
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 74
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 79
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 80
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 80)) {
                    // line 81
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span>(";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 81);
                    echo ")</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 83
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 84
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 85
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 89
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<span>(0)</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 91
            echo "\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t";
            // line 92
            if ( !twig_get_attribute($this->env, $this->source, $context["product"], "quantity", [], "any", false, false, false, 92)) {
                // line 93
                echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"alert alert-danger alert-stock\">Sem estoque</div>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 95
            echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"button-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 96
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 96);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"\">";
            // line 97
            echo ($context["button_conferir"] ?? null);
            echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t";
            // line 100
            if ( !twig_get_attribute($this->env, $this->source, $context["product"], "shipping", [], "any", false, false, false, 100)) {
                // line 101
                echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"shipping-grid\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"./_galerias/shipping/frete_gratis_gif.gif\" alt=\"Frete Grátis\"
\t\t\t\t\t\t\t\t\t\t\t\t\tclass=\"img-responsive m-3\">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 106
            echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- product-layout -->
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 111
        echo "\t\t\t\t\t\t\t";
        if (($context["type"] ?? null)) {
            // line 112
            echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- swiper-wrappe -->
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- swiper-container -->
\t\t\t\t\t\t<div class=\"swiper-pagination slideshow";
            // line 116
            echo ($context["module"] ?? null);
            echo "\"></div>
\t\t\t\t\t\t<div class=\"swiper-pager\">
\t\t\t\t\t\t\t<div class=\"swiper-button-next featured-btn-next";
            // line 118
            echo ($context["module"] ?? null);
            echo "\"></div>
\t\t\t\t\t\t\t<div class=\"swiper-button-prev featured-btn-prev";
            // line 119
            echo ($context["module"] ?? null);
            echo "\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- swiper-pager -->
\t\t\t\t\t</div>
\t\t\t\t\t<!-- swiper-viewport -->
\t\t\t\t";
        }
        // line 125
        echo "
\t\t\t</div>
\t\t\t<!-- row -->
\t\t</div>
\t\t<!-- module -->
\t</div>
\t<!-- container -->
";
        // line 132
        if (($context["type"] ?? null)) {
            // line 133
            echo "<script type=\"text/javascript\">
<!--
\$('#banner";
            // line 135
            echo ($context["module"] ?? null);
            echo "').swiper({
mode: 'horizontal',
slidesPerView: 4,
pagination: '.slideshow";
            // line 138
            echo ($context["module"] ?? null);
            echo "',
paginationClickable: true,
nextButton: '.random-btn-next";
            // line 140
            echo ($context["module"] ?? null);
            echo "',
prevButton: '.random-btn-prev";
            // line 141
            echo ($context["module"] ?? null);
            echo "',
// spaceBetween: 30,
autoplay: ";
            // line 143
            echo ($context["module"] ?? null);
            echo "00,
autoplayDisableOnInteraction: true,
loop: true,
breakpoints: {
350: {
slidesPerView: 1
},
640: {
slidesPerView: 2
},
768: {
slidesPerView: 3
},
1024: {
slidesPerView: 4
}
}
});
--></script>
";
        }
    }

    public function getTemplateName()
    {
        return "crystal_white/template/extension/module/featured.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  343 => 143,  338 => 141,  334 => 140,  329 => 138,  323 => 135,  319 => 133,  317 => 132,  308 => 125,  299 => 119,  295 => 118,  290 => 116,  284 => 112,  281 => 111,  271 => 106,  264 => 101,  262 => 100,  256 => 97,  252 => 96,  249 => 95,  245 => 93,  243 => 92,  240 => 91,  236 => 89,  227 => 85,  222 => 84,  219 => 83,  213 => 81,  210 => 80,  204 => 79,  197 => 74,  191 => 70,  188 => 69,  183 => 68,  181 => 67,  178 => 66,  174 => 64,  169 => 61,  163 => 58,  157 => 55,  154 => 54,  148 => 51,  145 => 50,  143 => 49,  136 => 46,  124 => 36,  121 => 35,  119 => 34,  112 => 32,  101 => 26,  90 => 25,  84 => 23,  82 => 22,  78 => 20,  74 => 18,  70 => 16,  67 => 15,  63 => 14,  60 => 13,  54 => 10,  51 => 9,  49 => 8,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "crystal_white/template/extension/module/featured.twig", "");
    }
}
