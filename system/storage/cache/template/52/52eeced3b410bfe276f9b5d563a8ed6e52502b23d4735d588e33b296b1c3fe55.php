<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base/template/extension/module/featured.twig */
class __TwigTemplate_74e6b9bf89d1be9dca4c093b01af8b8b1b089f8ed1c52d7faf5bce801698aee1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container\">
\t<div class=\"module-general module-featured\">
\t\t<div class=\"heading-page\">
\t\t\t<h3>";
        // line 4
        echo ($context["heading_title"] ?? null);
        echo "</h3>
\t\t</div>
\t\t<div class=\"row\">

\t\t\t";
        // line 8
        if (($context["type"] ?? null)) {
            // line 9
            echo "\t\t\t\t<div class=\"swiper-viewport\">
\t\t\t\t\t<div id=\"banner";
            // line 10
            echo ($context["module"] ?? null);
            echo "\" class=\"swiper-container\">
\t\t\t\t\t\t<div class=\"swiper-wrapper\">
\t\t\t\t\t\t";
        }
        // line 13
        echo "
\t\t\t\t\t\t";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 15
            echo "\t\t\t\t\t\t\t";
            if (($context["type"] ?? null)) {
                // line 16
                echo "\t\t\t\t\t\t\t\t<div class=\"product-layout swiper-slide\">
\t\t\t\t\t\t\t\t";
            } else {
                // line 18
                echo "\t\t\t\t\t\t\t\t\t<div class=\"product-layout col-lg-3 col-md-3 col-sm-4 col-xs-6\">
\t\t\t\t\t\t\t\t\t";
            }
            // line 20
            echo "\t\t\t\t\t\t\t\t\t<div class=\"product-thumb transition\">
\t\t\t\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t";
            // line 22
            if (twig_get_attribute($this->env, $this->source, $context["product"], "valor_desconto", [], "any", false, false, false, 22)) {
                // line 23
                echo "\t\t\t\t\t\t\t\t\t\t\t<span class=\"badge badge-danger badge-desconto\">";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "valor_desconto", [], "any", false, false, false, 23);
                echo "%</span>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 25
            echo "\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 25);
            echo "\"><img src=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 25);
            echo "\" alt=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 25);
            echo "\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 25);
            echo "\" class=\"img-responsive\"/></a>
\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn-wishlist\" data-toggle=\"tooltip\" title=\"";
            // line 26
            echo ($context["button_wishlist"] ?? null);
            echo "\" onclick=\"wishlist.add('";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 26);
            echo "');\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-heart\"></i>
\t\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t\t\t\t<h4>
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 32
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 32);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 32);
            echo "</a>
\t\t\t\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t\t\t\t";
            // line 34
            if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 34)) {
                // line 35
                echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 36
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 37
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    if ((twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 37) < $context["i"])) {
                        // line 38
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 42
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o fa-stack-2x\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 47
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 48
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 48)) {
                    // line 49
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t(";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 49);
                    echo ")
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 51
                echo "\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 53
            echo "\t\t\t\t\t\t\t\t\t\t\t";
            if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 53)) {
                // line 54
                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                if ((twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 54) == "R\$ 0,00")) {
                    // line 55
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-current\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<small>Sob-consulta</small>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 65
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 67
                    if ( !twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 67)) {
                        // line 68
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-current\">";
                        // line 69
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 69);
                        echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 72
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-old\">";
                        // line 73
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 73);
                        echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                        // line 76
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 76);
                        echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 79
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 82
                echo "\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 83
            echo "\t\t\t\t\t\t\t\t\t\t\t";
            if ( !twig_get_attribute($this->env, $this->source, $context["product"], "quantity", [], "any", false, false, false, 83)) {
                // line 84
                echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"alert alert-danger alert-stock\">Sem estoque</div>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 86
            echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"button-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 87
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 87);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"\">";
            // line 88
            echo ($context["button_conferir"] ?? null);
            echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t";
            // line 91
            if ( !twig_get_attribute($this->env, $this->source, $context["product"], "shipping", [], "any", false, false, false, 91)) {
                // line 92
                echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"shipping-grid\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"./_galerias/shipping/frete_gratis_gif.gif\" alt=\"Frete Grátis\"
\t\t\t\t\t\t\t\t\t\t\t\t\tclass=\"img-responsive m-3\">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 97
            echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<!-- product-layout -->
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 102
        echo "\t\t\t\t\t\t\t";
        if (($context["type"] ?? null)) {
            // line 103
            echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!-- swiper-wrappe -->
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- swiper-container -->
\t\t\t\t\t\t<div class=\"swiper-pagination slideshow";
            // line 107
            echo ($context["module"] ?? null);
            echo "\"></div>
\t\t\t\t\t\t<div class=\"swiper-pager\">
\t\t\t\t\t\t\t<div class=\"swiper-button-next featured-btn-next";
            // line 109
            echo ($context["module"] ?? null);
            echo "\"></div>
\t\t\t\t\t\t\t<div class=\"swiper-button-prev featured-btn-prev";
            // line 110
            echo ($context["module"] ?? null);
            echo "\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<!-- swiper-pager -->
\t\t\t\t\t</div>
\t\t\t\t\t<!-- swiper-viewport -->
\t\t\t\t";
        }
        // line 116
        echo "
\t\t\t</div>
\t\t\t<!-- row -->
\t\t</div>
\t\t<!-- module -->
\t</div>
\t<!-- container -->
";
        // line 123
        if (($context["type"] ?? null)) {
            // line 124
            echo "<script type=\"text/javascript\">
<!--
\$('#banner";
            // line 126
            echo ($context["module"] ?? null);
            echo "').swiper({
mode: 'horizontal',
slidesPerView: 4,
pagination: '.slideshow";
            // line 129
            echo ($context["module"] ?? null);
            echo "',
paginationClickable: true,
nextButton: '.random-btn-next";
            // line 131
            echo ($context["module"] ?? null);
            echo "',
prevButton: '.random-btn-prev";
            // line 132
            echo ($context["module"] ?? null);
            echo "',
// spaceBetween: 30,
autoplay: ";
            // line 134
            echo ($context["module"] ?? null);
            echo "00,
autoplayDisableOnInteraction: true,
loop: true,
breakpoints: {
350: {
slidesPerView: 1
},
640: {
slidesPerView: 2
},
768: {
slidesPerView: 3
},
1024: {
slidesPerView: 4
}
}
});
--></script>
";
        }
    }

    public function getTemplateName()
    {
        return "base/template/extension/module/featured.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  320 => 134,  315 => 132,  311 => 131,  306 => 129,  300 => 126,  296 => 124,  294 => 123,  285 => 116,  276 => 110,  272 => 109,  267 => 107,  261 => 103,  258 => 102,  248 => 97,  241 => 92,  239 => 91,  233 => 88,  229 => 87,  226 => 86,  222 => 84,  219 => 83,  216 => 82,  211 => 79,  205 => 76,  199 => 73,  196 => 72,  190 => 69,  187 => 68,  185 => 67,  181 => 65,  169 => 55,  166 => 54,  163 => 53,  159 => 51,  153 => 49,  150 => 48,  144 => 47,  137 => 42,  131 => 38,  128 => 37,  124 => 36,  121 => 35,  119 => 34,  112 => 32,  101 => 26,  90 => 25,  84 => 23,  82 => 22,  78 => 20,  74 => 18,  70 => 16,  67 => 15,  63 => 14,  60 => 13,  54 => 10,  51 => 9,  49 => 8,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base/template/extension/module/featured.twig", "");
    }
}
