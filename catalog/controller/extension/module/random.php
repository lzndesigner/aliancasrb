<?php
class ControllerExtensionModuleRandom extends Controller
{
	public function index($setting)
	{
		static $module = 50;

		$this->load->language('extension/module/random');

		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/swiper.min.css');
		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/opencart.css');
		$this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.jquery.js');
		$this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.min.js');

		$this->load->model('catalog/product');
		$this->load->model('catalog/random');

		$this->load->model('tool/image');

		$data['heading_title'] = $setting['name'];

		$data['type'] = $setting['type'];

		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		$filter_data = array(
			'sort'  => 'p.date_added',
			'order' => 'DESC',
			'start' => 0,
			'limit' => $setting['limit']
		);


		// $product_info = $this->model_catalog_product->getProduct($product_id);
		$results = $this->model_catalog_random->getRandomProducts($filter_data);
		if ($results) {
			foreach ($results as $result) {

				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if (!is_null($result['special']) && (float)$result['special'] >= 0) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$tax_price = (float)$result['special'];
				} else {
					$special = false;
					$tax_price = (float)$result['price'];
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format($tax_price, $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

				// gera porcentagem de desconto do Valor DE , POR:
				if ($result['special']) {
					$valor_de = $result['price'];
					$valor_por = $result['special'];

					$formula = $valor_por * 100;
					$formula1 = $formula / $valor_de;
					$formula2 = $formula1 - 100;

					$valor_desconto = number_format($formula2, 0, ',', ' ');;
				} else {
					$valor_desconto = false;
				}

				// Valor Parcelado e valor total
				if (!is_null($result['special']) && (float)$result['special'] >= 0) {
					$valor = (float)$result['special'];
				} else {
					$valor = (float)$result['price'];
				}

				$parcelaMinima = '9.60';

				$parc[1] = '1.0000'; // sem juros
				$parc[2] = '1.0000';
				$parc[3] = '1.0604';
				$parc[4] = '1.0759';
				$parc[5] = '1.0915';
				$parc[6] = '1.1072';
				$parc[7] = '1.1231';
				$parc[8] = '1.1392';
				$parc[9] = '1.1555';
				$parc[10] = '1.1717';
				$parc[11] = '1.1883';
				$parc[12] = '1.2048';

				for ($i = 1; $i <= 12; $i++) {
					$conf = ($valor * $parc[$i]) / $i;
					$conf = number_format($conf, 2, ",", ".");

					if ($i == '12') {
						$parcelamento = '<b>' . $i . "<small>x de </small> <span>R$ " . $conf . '</span></b>';
					}
					if ($conf > $parcelaMinima) {
						$parcelamento = '<b>' . $i . "<small>x de </small> <span>R$ " . $conf . '</span></b>';
					} else {
						if ($i == '1') {
							$parcelamento = '<b>' . $i . "<small>x de </small> <span>R$ " . $conf . '</span></b>';
						} else if ($i == '2') {
							$parcelamento = '<b>' . $i . "<small>x de </small> <span>R$ " . $conf . '</span></b>';
						} else {
							$parcelamento = '<b>' . $i . "<small>x de </small> <span>R$ " . $conf . '</span></b>';
						}
					}
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'valor_desconto'     => $valor_desconto,
					'parcelamento'     => $parcelamento,
					'tax'         => $tax,
					'rating'      => $rating,
					'quantity'    => $result['quantity'],
					'shipping'    => $result['shipping'],
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}
		}

		$data['module'] = $module++;


		if ($data['products']) {
			return $this->load->view('extension/module/random', $data);
		}
	}
}
